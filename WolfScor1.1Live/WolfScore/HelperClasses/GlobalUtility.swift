//
//  GlobalUtility.swift
//
//  Created by HiddenBrains on 14/07/16.
//
//

import UIKit
import NVActivityIndicatorView

@objc class GlobalUtility: NSObject {

    var activityIndicator : NVActivityIndicatorView!

	static func showToastMessage(msg: String){
		DispatchQueue.main.async (execute: { () -> Void in
            
            let hud = MBProgressHUD.showAdded(to:APP_DELEGATE.window, animated: true)
			hud?.mode = .text
            hud?.color = UIColor.white
			hud?.detailsLabelText = msg
            hud?.detailsLabelColor = UIColor.black
            hud?.detailsLabelFont = UIFont(name: "Roboto-Bold", size: 18.0)
			hud?.removeFromSuperViewOnHide = true
			hud?.margin = 15.0
			//		hud?.yOffset = Float((UIScreen.main.bounds.height / 2) - 120)
			hud?.hide(true, afterDelay: 2)
		})
	}
  
    
//    func globalActivityIndicator(){
//
//        let frame = CGRect(x: APP_DELEGATE.window-22, y: self.view.center.y-22, width: 45, height: 45)
//        activityIndicatorNew = NVActivityIndicatorView(frame: frame)
//        activityIndicatorNew.center = self.view.center
//        activityIndicatorNew.type = . circleStrokeSpin // add your type
//        activityIndicatorNew.color = UIColor.lightGray // add your color
//        APP_DELEGATE.window?.addSubview(activityIndicatorNew) // or use  webView.addSubview(activityIndicator)
//    }
//
    
    //showActivityIndi function is used to show Activity indicator
    static func showActivityIndi(viewContView: UIView){
        DispatchQueue.main.async(execute: { () -> Void in
            JHUD.show(at: APP_DELEGATE.window, message: nil)
            //            MBProgressHUD.showAdded(to: viewContView, animated: true)
        })
    }
    
    //hideActivityIndi function is used to hide Activity indicator
    static func hideActivityIndi(viewContView: UIView){
        DispatchQueue.main.async(execute: { () -> Void in
            JHUD.hide(for: APP_DELEGATE.window)
            //            MBProgressHUD.hide(for: viewContView, animated: true)
        })
    }
    
	//openUrlFromApp function helps to open any url on browser
	class func openUrlFromApp(url: String)
    {
		let finalUrl = URL(string: url)
		if finalUrl != nil
        {
			if #available(iOS 10.0, *)
            {
				UIApplication.shared.open(finalUrl!, options: [:], completionHandler: nil)
			}
            else
            {
				UIApplication.shared.openURL(finalUrl!)
			}
		}
	}
    
    class func openCallApp(mobileNumber : String?) -> String
    {
        if let url = URL(string: "telprompt:\(mobileNumber!)") {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *)
                {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
                else
                {
                    UIApplication.shared.openURL(url)
                }
                return("");
            }
            else
            {
                return("Device not supported to make a call")
            }
        }
        else
        {}
        return("Invalid phone number")
    }
    
    
    // Returns the most recently presented UIViewController (visible)
    class func getCurrentViewController() -> UIViewController? {
        
        // If the root view is a navigation controller, we can just return the visible ViewController
        if let navigationController = getNavigationController() {
            
            return navigationController.visibleViewController
        }
        
        // Otherwise, we must get the root UIViewController and iterate through presented views
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            
            var currentController: UIViewController! = rootController
            
            // Each ViewController keeps track of the view it has presented, so we
            // can move from the head to the tail, which will always be the current view
            while( currentController.presentedViewController != nil ) {
                
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
    }
    
    // Returns the navigation controller if it exists
    class func getNavigationController() -> UINavigationController? {
        
        if let navigationController = UIApplication.shared.keyWindow?.rootViewController  {
            
            return navigationController as? UINavigationController
        }
        return nil
    }

}
