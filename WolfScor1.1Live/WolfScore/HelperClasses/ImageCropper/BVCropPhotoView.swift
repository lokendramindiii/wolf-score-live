
import UIKit
let kMaskViewBorderWidth: CGFloat = 2.0

class CropImageMaskView:UIView{
    var cropRect: CGRect?
    
    init(frame: CGRect, cropSize: CGSize) {
        super.init(frame: frame)
        self.frame = frame
        self.setCropRect(size: cropSize)
        self.backgroundColor = UIColor.clear
        self.isUserInteractionEnabled = false
    }
    //    override init(frame: CGRect) {
    //        super.init(frame: frame)
    //        self.frame = frame
    //        self.setCropRect(CGSizeMake(320, 298))
    //        self.backgroundColor = UIColor.clearColor()
    //        self.userInteractionEnabled = false
    //    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let context : CGContext = UIGraphicsGetCurrentContext()!
        // context.setFillColor(red:1, green:1, blue:1, alpha: 0.5)
        context.setFillColor(red:0, green:0, blue:0, alpha: 0.5)
        context.fill(rect)
        context.setStrokeColor(UIColor.clear.cgColor)
        context.setLineWidth(2)
        context.stroke(cropRect!, width:kMaskViewBorderWidth)
        context.clear(cropRect!)
    }
    
    //    override func draw(_ rect: CGRect) {
    //        super.draw(rect)
    //        let context : CGContext = UIGraphicsGetCurrentContext()!
    //        context.setFillColor(red:1, green:1, blue:1, alpha: 0.5)
    //        context.fill(rect)
    //
    //        if(cropRect!.intersects( rect ) ){
    //            context.setStrokeColor(UIColor.red.cgColor)
    //            context.setLineWidth(2)
    //            context.addEllipse(in:cropRect!)
    //            context.drawPath(using:.fillStroke)
    //            context.setFillColor( UIColor.clear.cgColor );
    //            context.setBlendMode(CGBlendMode.clear);
    //            context.fillEllipse(in:cropRect!)
    //        }
    //    }
    
    
    //    override func drawRect(rect: CGRect) {
    //        super.drawRect(rect)
    //
    //        let context : CGContextRef = UIGraphicsGetCurrentContext()!
    //        CGContextSetRGBFillColor(context, 1, 1, 1, 0.5)
    //        CGContextFillRect(context, self.bounds)
    //
    //
    ////        CGContextSetStrokeColorWithColor(context, UIColor.redColor().CGColor)
    ////        //CGContextStrokeRectWithWidth(context, cropRect!, kMaskViewBorderWidth)
    ////        CGContextClearRect(context, cropRect!)
    //
    //        CGContextSetStrokeColorWithColor(context, UIColor.redColor().CGColor)
    //        CGContextSetFillColorWithColor(context, UIColor.redColor().CGColor)
    //        CGContextAddEllipseInRect(context, cropRect! )
    //        CGContextDrawPath(context, .FillStroke)
    //
    //
    //    }
    
    
    func setCropRect(size: CGSize){
        let width = self.bounds.size.width/2 - size.width/2.0
        let height = self.bounds.size.height/2 - size.height/2
        
        let offsetX   = ceilf(Float(width))
        let offsetY  = ceilf(Float(height))
        
        let x : CGFloat = CGFloat(offsetX)
        let y : CGFloat = CGFloat(offsetY)
        
        self.cropRect = CGRect(x: x, y: y, width: size.width, height: size.height)
        
    }
}

class BVCropPhotoView: UIView,UIScrollViewDelegate {
    
    var overlayImage: UIImage?
    
    var myView: CropImageMaskView!
    var sourceImage: UIImage?
    var requestFor: Int?   //---1 for gallery image
    
    var scrollView: UIScrollView?
    var overlayView: UIImageView?
    var imageView: UIImageView?
    var cropSize: CGSize?
    var maximumZoomScale: CGFloat?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.scrollView = UIScrollView()
        scrollView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView?.delegate = self
        scrollView?.alwaysBounceVertical = true
        scrollView?.alwaysBounceHorizontal = false
        scrollView?.showsVerticalScrollIndicator = false
        scrollView?.showsHorizontalScrollIndicator = false
        scrollView?.layer.masksToBounds = false
        self.addSubview(scrollView!)
        
        self.imageView = UIImageView()
        imageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.scrollView?.addSubview(imageView!)
        
        //        self.overlayView = UIImageView()
        //        overlayView?.contentMode = UIViewContentMode.Center
        //        overlayView?.autoresizingMask = (UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight)
        //        self.addSubview(overlayView!)
        
        self.maximumZoomScale = 2.5
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.scrollView?.frame = self.bounds;
        self.overlayView?.frame = self.bounds;
        
        if (self.imageView?.image == nil) {
            self.setupZoomScale();
        }
    }
    
    func setupZoomScale(){
        
        self.imageView?.image = self.sourceImage
        self.imageView?.sizeToFit()
        
        self.overlayView?.image = self.overlayImage
        let offsetX = ceil(self.scrollView!.frame.size.width / 2 - self.cropSize!.width / 2)
        let offsetY = ceil(self.scrollView!.frame.size.height / 2 - self.cropSize!.height / 2)
        self.scrollView?.contentInset = UIEdgeInsetsMake(offsetY, offsetX, offsetY, offsetX);
        
        self.scrollView?.contentSize = self.imageView!.frame.size
        
        var zoomScale: CGFloat = 1.0;
        if ( self.imageView!.frame.size.width >= self.imageView!.frame.size.height ) {
            zoomScale = self.cropSize!.height / self.imageView!.frame.size.height;
        }
        else {
            zoomScale = self.cropSize!.width / self.imageView!.frame.size.width;
        }
        
        self.scrollView?.minimumZoomScale = zoomScale
        self.scrollView?.maximumZoomScale = self.maximumZoomScale! * zoomScale
        self.scrollView?.zoomScale = zoomScale
        
        self.scrollView?.contentOffset = CGPoint(x:(self.imageView!.frame.size.width - self.scrollView!.frame.size.width) / 2,y:
            (self.imageView!.frame.size.height - self.scrollView!.frame.size.height) / 2)
        
        //----code to draw crop rect instead of overlay images-----------
        print(self.cropSize)
        self.mask = CropImageMaskView(frame: self.bounds, cropSize:self.cropSize!)
        self.addSubview(mask!)
        self.bringSubview(toFront: mask!)
        
    }
    
    
    func croppedImage() -> UIImage {
        let scale: CGFloat = UIScreen.main.scale
        
        UIGraphicsBeginImageContextWithOptions(self.scrollView!.contentSize, true, scale);
        let  graphicsContext: CGContext = UIGraphicsGetCurrentContext()!;
        
        self.scrollView?.layer.render(in: graphicsContext)
        let sourceImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
        
        var targetFrame: CGRect
        if(requestFor==1){
            targetFrame = CGRect(x: (self.scrollView!.contentInset.left + self.scrollView!.contentOffset.x) * scale, y: (self.scrollView!.contentInset.top + self.scrollView!.contentOffset.y) * scale, width: self.cropSize!.width * scale, height: self.cropSize!.height * scale)
        }
        else{
            if(self.sourceImage?.imageOrientation == UIImageOrientation.up){
                targetFrame = CGRect(x:(self.scrollView!.contentInset.left + self.scrollView!.contentOffset.x+25) * scale,y:
                    (self.scrollView!.contentInset.top + self.scrollView!.contentOffset.y) * scale,width:self.cropSize!.width * scale,height:
                    self.cropSize!.height * scale);
            }
            else{
                if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)
                {
                    targetFrame = CGRect(x:(self.scrollView!.contentInset.left + self.scrollView!.contentOffset.x) * scale,
                                         y:(self.scrollView!.contentInset.top + self.scrollView!.contentOffset.y+17) * scale,
                                         width:self.cropSize!.width * scale,
                                         height:self.cropSize!.height * scale);
                }
                else
                {
                    targetFrame = CGRect(x:(self.scrollView!.contentInset.left + self.scrollView!.contentOffset.x) * scale,y:
                        (self.scrollView!.contentInset.top + self.scrollView!.contentOffset.y+25) * scale,width:
                        self.cropSize!.width * scale,height:
                        self.cropSize!.height * scale);
                }
            }
        }
        
        let contextImage: CGImage?  = sourceImage.cgImage!.cropping(to: targetFrame)
        var finalImage: UIImage?
        if (contextImage != nil) {
            finalImage = UIImage(cgImage: contextImage!, scale: scale, orientation: UIImageOrientation.up)
            //[UIImage imageWithCGImage:contextImage
            
        }
        
        UIGraphicsEndImageContext()
        
        return finalImage!
    }
    
    //--------------ScrollView delegates--------------
    func viewForZooming(in scrollView: UIScrollView) -> UIView?{
        return self.imageView
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return self.scrollView
    }
    
}

