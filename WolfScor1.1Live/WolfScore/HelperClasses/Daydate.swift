//
//  Daydate.swift
//  WolfScore
//
//  Created by Mindiii on 1/30/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

let dayDate = Daydate.shared

class Daydate: NSObject {
    
    static let shared = Daydate()
    
    func getDayWithDate(from strDate : String) -> String{
      
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        df.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let date = df.date(from: strDate)
    
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        dateFormatter.dateFormat = "EEEE, d MMMM YYYY"
        let DateString: String = dateFormatter.string(from: date ?? Date())
        
        return DateString
    }
    
    
    
    func getEEEdMMMMYYYYFormatter(from strDate : String) -> String{
        
        let df = DateFormatter()//2019-04-16 19:00:00
        df.dateFormat = "YYYY-MM-dd HH:mm:ss"
        df.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let date = df.date(from: strDate)
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        dateFormatter.dateFormat = "EEE, MMMM d YYYY"
        let DateString: String = dateFormatter.string(from: date ?? Date())
        
        return DateString
    }
    
    
    func convertStringToDate(strDate:String) -> Date {
        
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        df.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let date = df.date(from: strDate)
        let DateString: String = df.string(from: date!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
       // dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let result = dateFormatter.date(from: DateString)
        
        return result!
    }
    
  
    
    func dayDifference(from interval : TimeInterval) -> String
    {
        let calendar = Calendar.current
        let date = Date(timeIntervalSince1970: interval)
        if calendar.isDateInYesterday(date) { return "Yesterday" }
        else if calendar.isDateInToday(date) { return "Today" }
        else if calendar.isDateInTomorrow(date) { return "Tomorrow" }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 {
                return "\(day)"
            }
            else {
                return "\(day)"
            }
        }
    }
    

    
    func TodayDayDate() -> String {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = NSTimeZone.local
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let result = formatter.string(from: date)

        return result
    }
    
    func YesterdayDayDate() -> String {
        let date = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = NSTimeZone.local
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let result = formatter.string(from: date!)
        return result
    }
    
    func TomorrowDayDate() -> String {
        let date = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = NSTimeZone.local
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let result = formatter.string(from: date!)
        return result
    }
    
    func Dateformate24_hours(strTime:String) -> String {
      
        /*
      //   loks comment --
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        
        let date = dateFormatter.date(from: strTime)
        //dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let Date12 = dateFormatter.string(from: date!)
        //Date12.insert("\n", at: Date12.index(Date12.startIndex, offsetBy: 6)) // prints hel!lo
*/
        // for already timezone local
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date = dateFormatter.date(from: strTime)
        dateFormatter.dateFormat = "HH:mm"
        let Date12 = dateFormatter.string(from: date!)
        return Date12
        //.uppercased()

    }
    func dateformateYYYY_MM_DD(strDate:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        
        let date = dateFormatter.date(from: strDate)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let Date12 = dateFormatter.string(from: date!)
        return Date12
    }
    
    func dateformateMMMM_dd_yyy(strDate:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let date = dateFormatter.date(from: strDate)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let Date12 = dateFormatter.string(from: date!)
        return Date12
    }
    func dateformatedd_MMMM_yyyyy(strDate:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")

        let date = dateFormatter.date(from: strDate)
        dateFormatter.dateFormat = "dd MMMM yyyy"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let Date12 = dateFormatter.string(from: date!)

        return Date12
    }
    
    func dateformatedd_MM_yyyy(strDate:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        
        let date = dateFormatter.date(from: strDate)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let Date12 = dateFormatter.string(from: date!)
        
        return Date12
    }
    
    func dateformateConvertBydd_MM_yyyyToMon_DD_Year(strDate:String) -> String {
        
        if strDate == ""
        {
            let Date12 :String = ""
            return Date12
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        
        let date = dateFormatter.date(from: strDate)
        dateFormatter.dateFormat = "MMM dd, yyyy"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let Date12 = dateFormatter.string(from: date!)
        return Date12
    }
    
    func dateFormatedWithTime(strDate:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //Your date format
//        dateFormatter.timeZone = NSTimeZone.local
//        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
       // dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone according to date format your date string
        guard let date = dateFormatter.date(from: strDate) else {
            fatalError()
        }
        //dateFormatter.dateFormat = "dd/MM/yyyy, hh:mm a" //Your New Date format as per requirement change it own
         dateFormatter.dateFormat = "dd/MM/yyyy, HH:mm"
       //  dateFormatter.timeZone = NSTimeZone.local
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let newDate = dateFormatter.string(from: date) //pass Date here
        return newDate
    }
    
    func dateFormatedWithDayName(strDate:String) -> String {
        let dateFormatter = DateFormatter()
        // "date_time": "2013-02-02 02:30:00",
        dateFormatter.dateFormat = "yyyy-MM-dd" //Your date format
        //        dateFormatter.timeZone = NSTimeZone.local
        //        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone according to date format your date string
        guard let date = dateFormatter.date(from: strDate) else {
            fatalError()
        }
        dateFormatter.dateFormat = "E, dd MMMM yyyy" //Your New Date format as per requirement change it own
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let newDate = dateFormatter.string(from: date) //pass Date here
        return newDate
    }
    
    func CompareDate (strFirstDate:String, currentDate:Date) -> String
    {
 
        var firstDateString :String = ""
        
        let createDate = strFirstDate.split(separator: ".")
        firstDateString = String(createDate[0])
        var strTime :String = ""
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd HH:mm:ss"

        df.timeZone = NSTimeZone.local
        df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        
        //df.timeZone = TimeZone(abbreviation: "UTC")
        let date1: Date? = df.date(from: firstDateString)
        let date2: Date? = currentDate
        //Calculating the time interval
        var secondsBetween1: TimeInterval? = nil
        
        if let aDate1 = date1
        {
            secondsBetween1 = date2?.timeIntervalSince(aDate1)
            let secondsBetween = Int(secondsBetween1!)
            let numberOfDays: Int = secondsBetween / 86400
            let timeResult = Int(secondsBetween) % 86400
            let hour: Int = timeResult / 3600
            let hourResult: Int = timeResult % 3600
            let minute: Int = hourResult / 60
            
            let secs: Int = minute % 3600
            let sec: Int = secs / 60
            
            if numberOfDays > 0 {
                if numberOfDays == 1 {
                    strTime = String(numberOfDays) + " " + NSLocalizedString("day ago", tableName: nil, comment: "")
                }
                    
                else if numberOfDays > 7 && numberOfDays < 30{
                    let week =  numberOfDays / 7
                    if  week == 1{
                        strTime = String(week) + " " + NSLocalizedString("week ago", tableName: nil, comment: "")
                    }else{
                        strTime = String(week) + " " + NSLocalizedString("weeks ago", tableName: nil, comment: "")
                    }
                    
                }else if numberOfDays > 30{
                    let month =  numberOfDays / 30
                    if  month == 1{
                        strTime = String(month) + " " + NSLocalizedString("month ago", tableName: nil, comment: "")
                    }else{
                        strTime = String(month) + " " + NSLocalizedString("months ago", tableName: nil, comment: "")
                    }
                    
                } else if numberOfDays > 365{
                    
                    let year =  numberOfDays / 365
                    if  year == 1{
                        strTime = String(year) + " " + NSLocalizedString("year ago", tableName: nil, comment: "")
                    }else{
                        strTime = String(year) + " " + NSLocalizedString("years ago", tableName: nil, comment: "")
                    }
                    
                }else{
                    strTime = String(numberOfDays) + " " + NSLocalizedString("days ago", tableName: nil, comment: "")
                }
            }
            else if(numberOfDays == 0 && hour > 0)
            {
                if(numberOfDays == 0 && hour == 1)
                {
                    strTime = String(hour) + " " + NSLocalizedString("hr ago", tableName: nil, comment: "")
                }
                else
                {
                    strTime = String(hour) + " " + NSLocalizedString("hrs ago", tableName: nil, comment: "")
                }
            }
            else  if numberOfDays == 0 && hour == 0 && minute > 0 {
                if numberOfDays == 0 && hour == 0 && minute == 1 {
                    
                    strTime = String(minute) + " " +   NSLocalizedString("min ago", tableName: nil, comment: "")
                }
                else
                {
                    
                    strTime = String(minute) + " " +   NSLocalizedString("mins ago", tableName: nil, comment: "")
                }
            }
            else if numberOfDays == 0 && hour == 0 && minute == 0 && sec > 0
            {
                if numberOfDays == 0 && hour == 0 && minute == 0 && sec == 1{
                    strTime = String(sec) + " " +   NSLocalizedString("sec ago", tableName: nil, comment: "")
                }else{
                    
                    strTime = String(sec) + " " +   NSLocalizedString("sec ago", tableName: nil, comment: "")
                }
                
            }else {
                strTime = NSLocalizedString("Just now", tableName: nil, comment: "")
            }
        }
        return strTime
    }

    
    
    func CompareDateNews (strFirstDate:String, currentDate:Date) -> String
    {
        
        var firstDateString :String = ""
        
      //  let createDate = strFirstDate.split(separator: ".")
      //  firstDateString = String(createDate[0])
        var strTime :String = ""
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd HH:mm:ss"
        
        df.timeZone = TimeZone(abbreviation: "GMT+0:00")

      //  df.timeZone = NSTimeZone.local
      //  df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        
        //df.timeZone = TimeZone(abbreviation: "UTC")
        
        
        
        
        let date1: Date? = df.date(from: strFirstDate)
        let date2: Date? = currentDate
        //Calculating the time interval
        var secondsBetween1: TimeInterval? = nil
        
        if let aDate1 = date1
        {
            secondsBetween1 = date2?.timeIntervalSince(aDate1)
            let secondsBetween = Int(secondsBetween1!)
            let numberOfDays: Int = secondsBetween / 86400
            let timeResult = Int(secondsBetween) % 86400
            let hour: Int = timeResult / 3600
            let hourResult: Int = timeResult % 3600
            let minute: Int = hourResult / 60
            
            let secs: Int = minute % 3600
            let sec: Int = secs / 60
            
            if numberOfDays > 0 {
                if numberOfDays == 1 {
                    strTime = String(numberOfDays) + " " + NSLocalizedString("day ago", tableName: nil, comment: "")
                }
                    
                else if numberOfDays > 7 && numberOfDays < 30{
                    let week =  numberOfDays / 7
                    if  week == 1{
                        strTime = String(week) + " " + NSLocalizedString("week ago", tableName: nil, comment: "")
                    }else{
                        strTime = String(week) + " " + NSLocalizedString("weeks ago", tableName: nil, comment: "")
                    }
                    
                }else if numberOfDays > 30{
                    let month =  numberOfDays / 30
                    if  month == 1{
                        strTime = String(month) + " " + NSLocalizedString("month ago", tableName: nil, comment: "")
                    }else{
                        strTime = String(month) + " " + NSLocalizedString("months ago", tableName: nil, comment: "")
                    }
                    
                } else if numberOfDays > 365{
                    
                    let year =  numberOfDays / 365
                    if  year == 1{
                        strTime = String(year) + " " + NSLocalizedString("year ago", tableName: nil, comment: "")
                    }else{
                        strTime = String(year) + " " + NSLocalizedString("years ago", tableName: nil, comment: "")
                    }
                    
                }else{
                    strTime = String(numberOfDays) + " " + NSLocalizedString("days ago", tableName: nil, comment: "")
                }
            }
            else if(numberOfDays == 0 && hour > 0)
            {
                if(numberOfDays == 0 && hour == 1)
                {
                    strTime = String(hour) + " " + NSLocalizedString("hr ago", tableName: nil, comment: "")
                }
                else
                {
                    strTime = String(hour) + " " + NSLocalizedString("hrs ago", tableName: nil, comment: "")
                }
            }
            else  if numberOfDays == 0 && hour == 0 && minute > 0 {
                if numberOfDays == 0 && hour == 0 && minute == 1 {
                    
                    strTime = String(minute) + " " +   NSLocalizedString("min ago", tableName: nil, comment: "")
                }
                else
                {
                    
                    strTime = String(minute) + " " +   NSLocalizedString("mins ago", tableName: nil, comment: "")
                }
            }
            else if numberOfDays == 0 && hour == 0 && minute == 0 && sec > 0
            {
                if numberOfDays == 0 && hour == 0 && minute == 0 && sec == 1{
                    strTime = String(sec) + " " +   NSLocalizedString("sec ago", tableName: nil, comment: "")
                }else{
                    
                    strTime = String(sec) + " " +   NSLocalizedString("sec ago", tableName: nil, comment: "")
                }
                
            }else {
                strTime = NSLocalizedString("Just now", tableName: nil, comment: "")
            }
        }
        return strTime
    }

}
