//
//  WebServiceClass.swift
//  Link
//
//  Created by MINDIII on 10/3/17.
//  Copyright © 2017 MINDIII. All rights reserved.



import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import Toaster

var  strAuthToken : String = ""
var  kUnique_Device_Token : String = ""
var  kstrDeviceId : String = ""

var  kDevice_Tocken : String = ""
var  Api_Key : String = "B1214622-0CC1-4452-8657-B8D7228B8B29"
var  kCurrent_Country : String = ""

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class WebServiceManager: NSObject {
    
    //MARK: - Shared object
    fileprivate var window = UIApplication.shared.keyWindow
    
    //MARK: - Shared object
    private static var sharedNetworkManager: WebServiceManager = {
        let networkManager = WebServiceManager()
        return networkManager
    }()
    
//    private static var sharedNetworkManager: WebServiceManager = {
//        SVProgressHUD.setDefaultStyle(.dark)
//        SVProgressHUD.setDefaultMaskType(.clear)
//        SVProgressHUD.setMinimumDismissTimeInterval(1)
//        SVProgressHUD.setRingThickness(3)
//        SVProgressHUD.setRingRadius(22)
//        let networkManager = WebServiceManager()
//        return networkManager
//    }()
    
    
    // MARK: - Accessors
    class func sharedObject() -> WebServiceManager {
        return sharedNetworkManager
    }
    
    public func requestFor(strURL:String, methodType: HTTPMethod, params : [String:Any]?, success:@escaping(JSON) ->Void, failure:@escaping (Error) ->Void ) {
        
        if !(Connectivity.isConnectedToInternet){
            SVProgressHUD.dismiss()
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            showAlert(message: "", title: NetworkConnection, controller: window!)
            return
        }
        
        if UserDefaults.standard.string(forKey:UserDefaults.Keys.kAuthToken)==nil {
            strAuthToken=""
        }else{
            strAuthToken=UserDefaults.standard.string(forKey:UserDefaults.Keys.kAuthToken)!
        }
        
        let strFinalurl = webUrl.BaseUrl + strURL
        let headers : HTTPHeaders = ["authToken" : strAuthToken]
        
        print("strFinalurl = \(strFinalurl)")
        print("headers = \(headers)")
        print("params = \(String(describing: params))")
        
        Alamofire.request(strFinalurl, method: methodType, parameters: params, headers: headers).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON: \(json)")
                success(json)
            case .failure(let error):
                print(error)
                failure(error)
            }
        }
    }
    
    public func uploadMultipartData(strURL:String, params : [String : AnyObject]?, imageData:Data , fileName:String, mimeType:String, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void){
        
        if !(Connectivity.isConnectedToInternet){
            SVProgressHUD.dismiss()
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            showAlert(message: "", title: NetworkConnection, controller: window!)
            return
        }
        if UserDefaults.standard.string(forKey:UserDefaults.Keys.kAuthToken)==nil {
            strAuthToken=""
        }else{
            strAuthToken=UserDefaults.standard.string(forKey:UserDefaults.Keys.kAuthToken)!
        }
        
        let url = webUrl.BaseUrl+strURL
        
        let headers = ["Auth-Token" : strAuthToken,
                       "Api-Key":Api_Key]

        print("authToken ------- \(strAuthToken)")
        print("Api-Key ------- \(Api_Key)")


        Alamofire.upload(multipartFormData:{ multipartFormData in
            
            if (imageData.count>0){
                multipartFormData.append(imageData,
                                         withName:fileName,
                                         fileName:"file.jpg",
                                         mimeType:mimeType)
            }
            for (key, value) in params! {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }},
                         // usingThreshold:UInt64.init(),
            to:url,
            method:.post,
            headers:headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { responseObject in
                        print(responseObject)
                        if responseObject.result.isSuccess {
                            do {
                                let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                success(dictionary as! Dictionary<String, Any>)
                                
                                let InvalidToken   =   dictionary["message"] as? String ?? ""
                                if InvalidToken == "Invalid token"
                                {
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    _ = appDelegate.Go_ToWelcomeScreen()
                                    
                                    Toast(text: "Login failed because of LogonDenied").show()
                                    self.resetDefaults()
                                }

                                print("dictionary = \(dictionary)")
                            }catch{
                                let error : Error = responseObject.result.error!
                                failure(error)
                            }
                        }
                        if responseObject.result.isFailure {
                            let error : Error = responseObject.result.error!
                            failure(error)
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    failure(encodingError)
                }
        })
    }
    
    public func requestPost(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        if !(Connectivity.isConnectedToInternet){
            SVProgressHUD.dismiss()
            let app = UIApplication.shared.delegate as! AppDelegate
        }
        
        if UserDefaults.standard.string(forKey:UserDefaults.Keys.kAuthToken)==nil {
            strAuthToken=""
        }else{
            strAuthToken=UserDefaults.standard.string(forKey:UserDefaults.Keys.kAuthToken)!
            print(strAuthToken)
        }
        
        let url = webUrl.BaseUrl + strURL
        let headers = ["Auth-Token" : strAuthToken,
                       "Api-Key":Api_Key]
        print("authToken ------- \(strAuthToken)")
        
        Alamofire.request(url, method: .post, parameters: params, headers: headers).responseJSON { responseObject in
            
            if responseObject.result.isSuccess {
                
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    
                    let InvalidToken   =   dictionary["message"] as? String ?? ""
                    if InvalidToken == "Invalid token"
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        _ = appDelegate.Go_ToWelcomeScreen()
                        
                        Toast(text: "Login failed because of LogonDenied").show()
                        self.resetDefaults()
                    }

                }catch{
                    
                }
            }
            if responseObject.result.isFailure {
                
                let str = String(decoding: responseObject.data!, as: UTF8.self)
                print(str)
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    /// For gest_user API
    
    public func request_Geust_User_Post(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        if !(Connectivity.isConnectedToInternet){
            SVProgressHUD.dismiss()
            let app = UIApplication.shared.delegate as! AppDelegate
        }
        let url = webUrl.BaseUrl + strURL
        let headers = ["Api-Key" : Api_Key ]
        Alamofire.request(url, method: .post, parameters: params, headers: headers).responseJSON { responseObject in
            
            if responseObject.result.isSuccess {
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    
                    success(dictionary as! Dictionary<String, Any>)
                }catch{
                    
                }
            }
            if responseObject.result.isFailure {
                
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    
    public func requestGet(strURL:String, params : [String : AnyObject]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        if !(Connectivity.isConnectedToInternet){
            SVProgressHUD.dismiss()
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            showAlert(message: "", title: NetworkConnection, controller: window!)
            return
        }
        if UserDefaults.standard.string(forKey:UserDefaults.Keys.kAuthToken)==nil {
            strAuthToken=""
        }else{
        strAuthToken=UserDefaults.standard.string(forKey:UserDefaults.Keys.kAuthToken)!
        }
        let url = webUrl.BaseUrl + strURL
        let headers = ["Auth-Token" : strAuthToken,
                       "Api-Key":Api_Key]
        print("authToken ------- \(strAuthToken)")
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
           print("requestGet---   \(responseObject)")

            if responseObject.result.isSuccess {
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    
                    success(dictionary as! Dictionary<String, Any>)
                    
                    
                    let InvalidToken   =   dictionary["message"] as? String ?? ""
                    if InvalidToken == "Invalid token"
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        _ = appDelegate.Go_ToWelcomeScreen()
                        Toast(text: "Login failed because of LogonDenied").show()
                        self.resetDefaults()
                    }

                }catch{
                }
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    

    
    
    func resetDefaults() {
        
        let defaults = UserDefaults.standard
        
        let UserEmail = defaults.string(forKey: UserDefaults.Keys.kEmail)
        let UserPassword = defaults.string(forKey: UserDefaults.Keys.kRMPassword)
        let rememberMe = defaults.string(forKey: UserDefaults.Keys.kIsRemember)
        let arrCountry = UserDefaults.standard.array(forKey: UserDefaults.Keys.kCountryArr) as? [[String:Any]] ?? []

        
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        
        defaults.setValue(rememberMe , forKey: UserDefaults.Keys.kIsRemember)
        defaults.setValue(UserEmail , forKey: UserDefaults.Keys.kEmail)
        defaults.setValue(UserPassword , forKey: UserDefaults.Keys.kRMPassword)
        defaults.setValue(arrCountry , forKey: UserDefaults.Keys.kCountryArr)

    }
    func showAlert(message: String = "", title: String , controller: UIWindow) {
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let subView = alertController.view.subviews.first!
            let alertContentView = subView.subviews.first!
            alertContentView.backgroundColor = UIColor.gray
            alertContentView.layer.cornerRadius = 20
            
            let OKAction = UIAlertAction(title: NSLocalizedString("OK", tableName: nil, comment: ""), style: .default, handler: nil)
            alertController.addAction(OKAction)
            
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        })
    }
    func showNetworkAlert(){
        let alert = UIAlertController(title: "No network", message: "Please check your internet connection.", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        alert.show()
    }
    
    func showAlertWithTitle(title:String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        alert.show()
    }
    
}

