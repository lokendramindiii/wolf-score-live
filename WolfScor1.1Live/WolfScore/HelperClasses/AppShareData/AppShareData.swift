//
//  AppSharedData.swift
//  Uconnekt
//
//  Created by Mindiii on 10/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SystemConfiguration
import CoreLocation
//import Firebase
let objAppShareData : AppShareData = AppShareData.sharedObject()

class AppShareData: NSObject,CLLocationManagerDelegate {

    var arrLeagueForFinalSorting = [ModelLeagueList]()
    var uniqDeviceTocken = ""
    weak var timer: Timer?

     var isIntertitalBannerHide = false
      var isTwoMinutesComplete = false

    var localTimeZoneName: String
    {
        return TimeZone.current.identifier
    }
    
    var kServerKey = "key=AAAA_sUc3YE:APA91bFnx2sflGpyrQ_Gg5LL482r8aPOOnK3q3ZDo7ic90qHsAvtZX_tLV9vlohsj976rh7p0CId0z9i1DjKRq-MoulBGGYyA3fTV-g-dgUxY-KvKseMBIBthl8vLNYv_cgf5LJFt8iC"
    
    
    //  client
       var BannerId = "ca-app-pub-3253051663291962/6743538683"
       var InterstitialId = "ca-app-pub-3253051663291962/3055575114"

  
    // Mindiii
    
    //     var BannerId = "ca-app-pub-3940256099942544/2934735716"
    //     var InterstitialId = "ca-app-pub-3940256099942544/4411468910"
    
    // Arvind sir -
    //     var BannerId = "ca-app-pub-3940256099942544/2934735716"
    //     var InterstitialId = "ca-app-pub-3940256099942544/4411468910"
    

    
    //Matches
    var strselctedMatchTab = "my"
    var strselctedOngoingTab = ""
    var strselctedByTimeTab = ""
    var strFilterLeagueId = ""
    
    
    
    var SelectedCalanderDate = Date()
    
    // moveTo XLPageIndex
    var isLonchXLPageIndex = false
    var isAnotherDateMatchVC = false
    var lastSelectedDate = Date()
    var IndexOfController = "1"
    var days = 0
  
    // MatchDetails
    var str_match_Id = ""
    var str_season_Id = ""
    var str_league_id = ""
    var str_Local_Team_Id = ""
    var str_Visiter_Team_Id = ""
    var str_Local_Team_Name = ""
    var str_Visiter_Team_Name = ""

    
    //TeamDetails
    var str_player_Id = ""
    var str_team_Id = ""
    var str_team_Name = ""
    var str_team_Logo = ""
    var str_teamSeason_Id = ""
    
    
    var locationManager: CLLocationManager?
    private static var sharedManager: AppShareData = {
        let manager = AppShareData()
        return manager
    }()
    
    // MARK: - Accessors
    class func sharedObject() -> AppShareData {
        return sharedManager
    }
    
    
    // Code for dropdown table animation
    func hideDropDownView(_ objConstraint: NSLayoutConstraint,ofView: UIView, withHeight:CGFloat){
        UIView.animate(withDuration: 0.15, animations: {() -> Void in
            objConstraint.constant = withHeight
            ofView.superview?.layoutIfNeeded()
        })
    }
    
    func hideDropDownView_WithDuration(_ objConstraint: NSLayoutConstraint,ofView: UIView, withHeight:CGFloat, andDuration: Double){
        UIView.animate(withDuration: andDuration, animations: {() -> Void in
            objConstraint.constant = withHeight
            ofView.superview?.layoutIfNeeded()
        })
    }
    func showDropDownView(_ objConstraint: NSLayoutConstraint,ofView: UIView, withHeight:CGFloat){
        UIView.animate(withDuration: 0.15, animations: {() -> Void in
            objConstraint.constant = withHeight
            ofView.superview?.layoutIfNeeded()
        })
    }
    func hideDropDownViewTyping(objBottomConstraint: NSLayoutConstraint ,ofView: UIView, withButtom:CGFloat){
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            objBottomConstraint.constant = withButtom
            ofView.superview?.layoutIfNeeded()
        })
    }
    func showDropDownViewTyping(objBottomConstraint: NSLayoutConstraint ,ofView: UIView, withButtom:CGFloat){
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            objBottomConstraint.constant = withButtom
            ofView.superview?.layoutIfNeeded()
        })
    }
    
    public class func loadingFooter() -> UIView {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        let v = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.width, height: 40))
        v.addSubview(spinner)
        spinner.center = v.center
        spinner.startAnimating()
        
        return v
    }
    //openUrlFromApp function helps to open any url on browser
    
    
     func openUrlFromApp(url: String)
    {
        let finalUrl = URL(string: url)
        if finalUrl != nil
        {
            if #available(iOS 10.0, *)
            {
                UIApplication.shared.open(finalUrl!, options: [:]
                    , completionHandler: nil)
            }
            else
            {
                UIApplication.shared.openURL(finalUrl!)
            }
        }
    }
      
}
struct ScreenSize {
    static let width = UIScreen.main.bounds.size.width
    static let height = UIScreen.main.bounds.size.height
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}
struct DeviceType {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH <= 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    
}
