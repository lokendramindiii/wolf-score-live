//
//  Constant.swift
//
//  Copyright © 2017 CompanyName. All rights reserved.
//

import Foundation
import UIKit

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate

let IS_IPHONE = UI_USER_INTERFACE_IDIOM() == .phone
let IS_IPHONE_4 = IS_IPHONE && UIScreen.main.bounds.size.height == 480.0
let IS_IPHONE_5 = IS_IPHONE && UIScreen.main.bounds.size.height == 568.0
let IS_IPHONE_6 = IS_IPHONE && UIScreen.main.bounds.size.height == 667.0
let IS_IPHONE_6PLUS = IS_IPHONE && UIScreen.main.nativeScale == 3.0
let IS_IPHONE_6_PLUS = IS_IPHONE && UIScreen.main.bounds.size.height == 736.0
let IS_IPHONE_X = IS_IPHONE && UIScreen.main.bounds.size.height == 812.0

let IS_RETINA = UIScreen.main.scale == 2.0



let kDeviceType = "ios"

let MAIN_FRAME : CGRect = UIScreen.main.bounds

let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let SCREEN_WIDTH_RATIO = UIScreen.main.bounds.size.width/320
let SCREEN_HEIGHT_RATIO = UIScreen.main.bounds.size.height/568

let KNotifiationFavouriteLeague = "FavouriteLeague"
let KNotifiationFavouriteFailure = "FavouriteFailure"
let KNotifiationSelectLeague = "SelectLeague"

    


