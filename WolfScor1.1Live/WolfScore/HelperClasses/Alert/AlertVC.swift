//
//  AlertVC.swift
//  Neonsay
//
//  Created by Mindiii on 3/7/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

var objAlert:AlertVC = AlertVC()

class AlertVC: UIViewController {

   
    // MARK:- ViewLife Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
  
    func showAlert(message: String, title: String = "", controller: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let subview = alertController.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.layer.cornerRadius = 10
        alertContentView.alpha = 1
        alertContentView.layer.borderWidth = 1
        alertContentView.layer.borderColor = UIColor(named: "AppThimColor")?.cgColor
        
            alertController.view.tintColor = UIColor(named: "AppThimColor")
        let OKAction = UIAlertAction(title: NSLocalizedString("OK", tableName: nil, comment: ""), style: .default, handler: nil)
        alertController.addAction(OKAction)
        controller.present(alertController, animated: true, completion: nil)
        view.endEditing(true)
    }
    func showAlertVc(message: String = "", title: String , controller: UIWindow) {
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let subView = alertController.view.subviews.first!
            let alertContentView = subView.subviews.first!
            alertContentView.backgroundColor = UIColor.gray
            alertContentView.layer.cornerRadius = 20
            
            let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(OKAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        })
    }
    func showSessionFailAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please Login Again", preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "LOGOUT", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            //objAppDelegate.logOut()
        })
        alert.addAction(yesButton)
        present(alert, animated: true) {() -> Void in }
    }
}
