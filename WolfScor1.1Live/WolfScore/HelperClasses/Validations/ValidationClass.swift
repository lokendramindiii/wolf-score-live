import UIKit
let objValidation : ValidationClass = ValidationClass()
let objAppDelegate : AppDelegate  = AppDelegate()

class ValidationClass: UIViewController {
    
  // MARK:- ViewLife Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValidEmail(strEmail:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: strEmail)
        return result
    }
    
    func isNameString(nameStr:String)-> Bool{
        let nameRegEx = "^([A-Za-z](\\.)?+(\\s)?[A-Za-z|\\'|\\.]*){1,7}$"
        let nameTest = NSPredicate (format:"SELF MATCHES %@",nameRegEx)
        let result = nameTest.evaluate(with: nameStr)
        return result
    }
    
    
    
    func isvalidPhone(value: String) -> Bool {
        
        if value.count >= 7 {
            return true
        }else{
            return false
        }
        
    }
    
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^([0-9]{3}(-)?[0-9]{3}(-)?[0-9]{2,4})$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    
    func isPwdLenth(password: String) -> Bool {
        if password.count < 6 {
            return true
        }else{
            return false
        }
    }
    
    func isDesLenth(password: String) -> Bool {
        if password.count < 10 {
            return true
        }else{
            return false
        }
    }
    
    func isDesMaxLenth(password: String) -> Bool {
        if password.count > 30 {
            return true
        }else{
            return false
        }
    }
    
    func isQuestionLength(Question: String) -> Bool {
        if Question.count < 20 {
            return true
        }else{
            return false
        }
    }
    
    func isConfPwdLenth(confirmPassword : String) -> Bool {
        if confirmPassword.count >= 6{
            return true
        }else{
            return false
        }
    }
    func isPwd(value: String) -> Bool {
        if value.count > 6{
            return true
        }
        else{
            return false
        }
    }
    
    
    func isName(strFullname: String) -> Bool {
        if strFullname.count <= 1
        {
          return false
        }
        else
        {
            let nameRegEx = "^([A-Za-z](\\.)?+(\\s)?[A-Za-z|\\'|\\.]*){1,7}$"
            let nameTest = NSPredicate (format:"SELF MATCHES %@",nameRegEx)
            let result = nameTest.evaluate(with: strFullname)
            return result
            
        }
    }
    
    
    func AnimationShakeTextField(textField:UITextField){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        
        animation.fromValue = NSValue(cgPoint: CGPoint(x: textField.center.x - 5, y: textField.center.y))
        
        animation.toValue = NSValue(cgPoint: CGPoint(x: textField.center.x + 5, y: textField.center.y))
        textField.layer.add(animation, forKey: "position")
    }
    
    func AnimationShakeLabel(label:UILabel){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        
        animation.fromValue = NSValue(cgPoint: CGPoint(x: label.center.x - 5, y: label.center.y))
        
        animation.toValue = NSValue(cgPoint: CGPoint(x: label.center.x + 5, y: label.center.y))
        
        label.layer.add(animation, forKey: "position")
        
    }
}
