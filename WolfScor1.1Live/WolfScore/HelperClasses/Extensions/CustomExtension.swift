//
//  CustomExtension.swift
//  Mualab
//
//  Created by MINDIII on 11/1/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation
var strAddress = ""
var latitude : String! = ""
var longitude : String! = ""
let objCustom:CustomExtension = CustomExtension()
class CustomExtension: NSObject {
    var locationManager = CLLocationManager()
    var strAddress = ""
    var latitude : String! = ""
    var longitude : String! = ""
}

extension UIImageView
{
    
    func setImageFream() {
        layer.cornerRadius = layer.frame.size.height / 2
        layer.masksToBounds = true
        layer.borderWidth = 4
        layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
    }
    
}

extension UIView
{
   
    func setHeaderViewShadow() {
//        layer.shadowColor = UIColor.darkGray.cgColor
//        layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
//        layer.shadowOpacity = 0.7
//        layer.shadowRadius = 2.0
        //layer.cornerRadius = 8
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        layer.shadowRadius = 10
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
        
    }
    func setviewFream() {
        layer.cornerRadius = 8
        layer.masksToBounds = true
        layer.borderWidth = 0.7
        layer.borderColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        layer.shadowOpacity = 0.7
    }
    
    func setViewCircleFream() {
        layer.cornerRadius = layer.frame.size.height / 2
        layer.masksToBounds = true
        layer.borderWidth = 0.4
        layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func setViewShadow() {
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        layer.shadowOpacity = 0.7
    }
    func setViewShadowOrrange() {
        layer.shadowColor = UIColor.colorConstant.appBlueColor.cgColor
        layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        layer.shadowOpacity = 0.7
    }
    func setShadowOnTable(_ tblObject: UITableView) {
        tblObject.layer.shadowOpacity = 1.0
        tblObject.layer.shadowRadius = 2.0
        tblObject.layer.shadowColor = UIColor.gray.cgColor
        tblObject.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }
    func setShadowOnView(_ viewObject: UIView) {
        viewObject.layer.shadowOpacity = 1.0
        viewObject.layer.shadowRadius = 2.0
        viewObject.layer.shadowColor = UIColor.gray.cgColor
        viewObject.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }
    func setShadowOnButton(_ btnObject: UIButton, color:CGColor) {
        btnObject.layer.shadowOpacity = 1.0
        btnObject.layer.shadowRadius = 2.0
        btnObject.layer.shadowColor = color
        btnObject.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }
    
    func setShadowAndColor(_ viewObject: UIView, color:CGColor) {
        viewObject.layer.shadowOpacity = 1.0
        viewObject.layer.shadowRadius = 2.0
        viewObject.layer.shadowColor = color
        viewObject.layer.shadowOffset = CGSize(width: 0.50, height: 0.50)
    }
    
    // Code for dropdown table animation
    func hideDropDownView(_ objConstraint: NSLayoutConstraint,ofView: UIView, withHeight:CGFloat){
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            objConstraint.constant = withHeight
            ofView.superview?.layoutIfNeeded()
        })
    }
    func showDropDownView(_ objConstraint: NSLayoutConstraint,ofView: UIView, withHeight:CGFloat){
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            objConstraint.constant = withHeight
            ofView.superview?.layoutIfNeeded()
        })
    }
}
extension UIViewController {
    
    func show() {
        let window = UIApplication.shared.delegate?.window
        let visibleVC = window??.visibleViewController
        visibleVC?.present(self, animated: true, completion: nil)
    }
  
}
public extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(vc: self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(vc: nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(vc: tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: pvc)
            } else {
                return vc
            }
        }
    }
}
extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
                placeholderLabel.textColor =  UIColor(red: 154.0 / 255.0, green: 154.0 / 255.0, blue: 154.0 / 255.0, alpha: 1.0)
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.textColor =  UIColor(red: 154.0 / 255.0, green: 154.0 / 255.0, blue: 154.0 / 255.0, alpha: 1.0)
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor =  UIColor(red: 154.0 / 255.0, green: 154.0 / 255.0, blue: 154.0 / 255.0, alpha: 1.0)
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
    
}
//MARK:- Image rotation extension code
extension UIImage {
    
    func fixedOrientation() -> UIImage {
        // No-op if the orientation is already correct
        if (imageOrientation == UIImageOrientation.up) {
            return self
        }
        
        var transform:CGAffineTransform = CGAffineTransform.identity
        
        if (imageOrientation == UIImageOrientation.down
            || imageOrientation == UIImageOrientation.downMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
        }
        
        if (imageOrientation == UIImageOrientation.left
            || imageOrientation == UIImageOrientation.leftMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
        }
        
        if (imageOrientation == UIImageOrientation.right
            || imageOrientation == UIImageOrientation.rightMirrored) {
            
            transform = transform.translatedBy(x: 0, y: size.height);
            transform = transform.rotated(by: CGFloat(-M_PI_2));
        }
        
        if (imageOrientation == UIImageOrientation.upMirrored
            || imageOrientation == UIImageOrientation.downMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }
        
        if (imageOrientation == UIImageOrientation.leftMirrored
            || imageOrientation == UIImageOrientation.rightMirrored) {
            
            transform = transform.translatedBy(x: size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
        }// Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx:CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height),
                                      bitsPerComponent: cgImage!.bitsPerComponent, bytesPerRow: 0,
                                      space: cgImage!.colorSpace!,
                                      bitmapInfo: cgImage!.bitmapInfo.rawValue)!
        
        ctx.concatenate(transform)
        
        
        if (imageOrientation == UIImageOrientation.left
            || imageOrientation == UIImageOrientation.leftMirrored
            || imageOrientation == UIImageOrientation.right
            || imageOrientation == UIImageOrientation.rightMirrored
            ) {
            
            
            ctx.draw(cgImage!, in: CGRect(x:0,y:0,width:size.height,height:size.width))
            
        } else {
            ctx.draw(cgImage!, in: CGRect(x:0,y:0,width:size.width,height:size.height))
        }
        
        
        // And now we just create a new UIImage from the drawing context
        let cgimg:CGImage = ctx.makeImage()!
        let imgEnd:UIImage = UIImage(cgImage: cgimg)
        
        return imgEnd
}
}
extension CustomExtension : CLLocationManagerDelegate {
    func getLocation() -> Void {
        if (CLLocationManager.locationServicesEnabled()){
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                //  self.locationManager.stopUpdatingLocation()
            }
        }else{
            print("set up location access  in info.plist")
        }
    }
    
    //MARK: - Delegates
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("location error is = \(error.localizedDescription)")
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = (manager.location?.coordinate)!
        
        print("Current Locations = \(locValue.latitude)")
        print("Current Locations = \(locValue.longitude)")
        
        let VarLat  = Double(locValue.latitude).rounded(toPlaces:5)
        let VarLong  = Double(locValue.longitude).rounded(toPlaces:5)
        
        latitude = String(VarLat)
        longitude = String(VarLong)
        
        let lcc = CLLocation.init(latitude: VarLat, longitude: VarLong)
        
        strAddress = getAddressFromLocation(location: lcc)
    }
    
    func getAddressFromLocation(location:CLLocation) ->String{
        var addressString = String()
        
        CLGeocoder().reverseGeocodeLocation(location,completionHandler: {(placemarks, error) -> Void in
            print(location)
            self.locationManager.stopUpdatingLocation()
            if error != nil {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm = placemarks?.last
                print(pm?.addressDictionary ?? "")
                
                if let formattedAddress = pm?.addressDictionary?["FormattedAddressLines"] as? [String] {
                    print(formattedAddress.joined(separator: ", "))
                    addressString = formattedAddress.joined(separator: ", ")
                    self.strAddress = addressString
                    
                }
                
                if let locationName = pm?.addressDictionary?["Name"] as? String {
                    print(locationName)
                }
                if let street = pm?.addressDictionary?["Thoroughfare"] as? String {
                    print(street)
                }
                if let city = pm?.addressDictionary?["City"] as? String {
                    print(city)
                }
                if let zip = pm?.addressDictionary?["ZIP"] as? String {
                    print(zip)
                }
                if let country = pm?.addressDictionary?["Country"] as? String {
                    print(country)
                }
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
        return addressString
    }
}
extension FloatingPoint {
    
    typealias Exponent = Int
    // is this okay? how can I write where `Exponent: Int` ?
    
    public func rounded(toPlaces places: Int) -> Self {
        guard places >= 0 else { return self }
        let divisor = Self(Int(pow(10.0, Double(places))))
        //let divisor = Self(sign: .plus, exponent: places, significand: Self(Int(pow(5, Double(places)))))
        return (self * divisor).rounded() / divisor
    }
}
//extension UIViewController{
//    @objc func dismissKeyboard() {
//        view.endEditing(true)
//    }
//    func hideKeyboardWhenTappedAround() {
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
//    }
//}
extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
}
}
extension CustomExtension{
   
    func relativePast(for date : Date, toDate:Date) -> String {
        
        let units = Set<Calendar.Component>([.year, .month, .day])
        let components = Calendar.current.dateComponents(units, from: date, to: toDate)
        var y = 0, m = 0, d = 0
        
        if components.year! > 0 {
            y = components.year!
        }
        if components.month! > 0 {
            m = components.month!
        }
        if (components.day! > 0) {
            d = components.day!
        }
        if y==0 {
            if (m==0 || m==1) && d>1{
              return "\(m) Month, \(d) Days"
            }else if (d==0 || d==1)  && m>1{
              return "\(m) Months, \(d) Day"
            }else{
              return "\(m) Months, \(d) Days"
            }
        }else{
            if (y==0 || y==1) && m>1{
                return "\(y) Year, \(m) Months"
            }else if (m==0 || m==1)  && y>1{
                return "\(y) Years, \(m) Month"
            }else{
                return "\(y) Years, \(m) Months"
            }
        }
    }
    
    func relativePastOnlyDays(for date : Date) -> String {
        let units = Set<Calendar.Component>([.day])
        let components = Calendar.current.dateComponents(units, from: Date(), to: date)
        
        if (components.day! > 0) {
            //return (components.day! > 1 ? "\(components.day!) days ago" : "Yesterday")
            return (components.day! > 1 ? "\(components.day!)" : "0")
        }else{
            return "0"
        }
    }
}
extension CustomExtension{
    func calculateAge(dob : String) -> (year :Int, month : Int, day : Int){
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = df.date(from: dob)
        guard let val = date else{
            return (0, 0, 0)
        }
        var years = 0
        var months = 0
        var days = 0
        
        let cal = NSCalendar.current
        years = cal.component(.year, from: Date()) -  cal.component(.year, from: val)
        
        let currMonth = cal.component(.month, from: Date())
        let birthMonth = cal.component(.month, from: val)
        
        //get difference between current month and birthMonth
        months = currMonth - birthMonth
        //if month difference is in negative then reduce years by one and calculate the number of months.
        if months < 0
        {
            years = years - 1
            months = 12 - birthMonth + currMonth
            if cal.component(.day, from: Date() as Date) < cal.component(.day, from: val){
                months = months - 1
            }
        } else if months == 0 && cal.component(.day, from: Date() as Date) < cal.component(.day, from: val)
        {
            years = years - 1
            months = 11
        }
        
        //Calculate the days
        if cal.component(.day, from: Date()) > cal.component(.day, from: val){
            days = cal.component(.day, from: Date()) - cal.component(.day, from: val)
        }
        else if cal.component(.day, from: Date()) < cal.component(.day, from: val)
        {
            let today = cal.component(.day, from: Date())
            //let date = cal.dateByAddingUnit(.Month, value: -1, toDate: NSDate(), options: [])
            
            days = (cal.component(.day, from: date!) - cal.component(.day, from: val)) + today
        } else
        {
            days = 0
            if months == 12
            {
                years = years + 1
                months = 0
            }
        }
        
        return (years, months, days)
    }
}
// New
extension UILabel{
    func makeOutLine(){
        let strokeTextAttributes = [
            NSAttributedStringKey.strokeColor : UIColor.gray,
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.strokeWidth : -1.0,
            NSAttributedStringKey.font : self.font
            ] as [NSAttributedStringKey : Any]
        self.attributedText = NSMutableAttributedString(string: self.text ?? "", attributes: strokeTextAttributes)
    }
    
    func makeSpacing(lineSpace: CGFloat, font:UIFont){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        let att = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1), NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : lineSpace]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att
        )
        
        DispatchQueue.main.async {
            self.attributedText = attStr
        }
    }
    func makeSpacingNew(lineSpace: CGFloat){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        style.alignment = .center
        let att = [NSAttributedStringKey.font : self.font, NSAttributedStringKey.foregroundColor : self.textColor, NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : lineSpace]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att
        )
        
        DispatchQueue.main.async {
            self.attributedText = attStr
        }
    }
    
    // loks add code makeSpacingWithLeftAlignment
    func makeSpacingWithLeftAlignment(lineSpace: CGFloat, font:UIFont, color: UIColor){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        style.alignment = .left
        let att = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : color, NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : 0.5]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att)
        self.attributedText = attStr
        
        //        DispatchQueue.main.async {
        //            self.attributedText = attStr
        //
        //        }
    }
    
    
    func makeCharacterSpacing(spacing: Float, Linespacing: CGFloat){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = Linespacing
        style.alignment = .center
        let att = [NSAttributedStringKey.font : self.font, NSAttributedStringKey.foregroundColor : self.textColor, NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : spacing]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att)
        self.attributedText = attStr
        
    }
    
    
    func makeSpacingWithCenterAlignment(lineSpace: CGFloat, font:UIFont, color: UIColor){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        style.alignment = .center
        let att = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : color, NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : 0.5]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att
        )
        
        DispatchQueue.main.async {
            self.attributedText = attStr
        }
        
    }
    
    func makeSpacingWithLeftAlignmentPopup(lineSpace: CGFloat, font:UIFont, color: UIColor){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        style.alignment = .left
        let att = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : color, NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : 0.5]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att
        )
        
        DispatchQueue.main.async {
            self.attributedText = attStr
        }
        
    }
    
    func makeCorner(radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
    }
}
extension Date {
    static var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: Date().noon)!
    }
    static var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: Date().noon)!
    }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var dayAfter1: Date {
        return Calendar.current.date(byAdding: .day, value: 2, to: Date().noon)!
    }
    var dayAfter2: Date {
        return Calendar.current.date(byAdding: .day, value: 3, to: Date().noon)!
    }
    var dayAfter3: Date {
        return Calendar.current.date(byAdding: .day, value: 4, to: Date().noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}
//func setImageAppCircle(border_Width:Float, border_color:UIColor) {
//    layer.cornerRadius = layer.frame.size.height / 2
//    layer.masksToBounds = true
//    layer.borderWidth = 3
//    layer.borderColor = colorPink.cgColor
//}




