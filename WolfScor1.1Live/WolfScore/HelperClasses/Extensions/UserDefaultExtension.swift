//
//  UserDefaultExtension.swift
//  Habito
//
//  Created by MINDIII on 11/9/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import Foundation
import UIKit


extension UserDefaults{
    
    enum Keys {
        
        //user Info
        
        static let kCountryArr = "country_arr"
        static let kStripe_Customer_Id = "kStripe_Customer_Id"
        static let kAuthToken = "authToken"
        static let kUnique_Device_Token = "kUnique_Device_Token"
        
        static let kUserId = "userId"
        static let kUserName = "userName"
        static let kUserProfileurl = "profileUrl"
        static let kGuestLogin = "guestLogin"
        static let kGuestUserId = "GuestuserId"

        
        static let kFullName = "fullName"
        static let kSocialId = "socialId"
        static let kRMUserName = "RMUserName"
        static let kRMPassword = "RMPassword"
        static let kEmail = "email"
        static let kBusinessName = "businessNmae"
        static let kProfileImage = "profileImage"
        static let kAddress = "address"
        static let kUserType = "UserType"
        static let kDOB = "DOB"
        static let kPassword = "password"
        static let kGender = "gender"
        static let kabout = "aboutUs"

        static let kIsRemember = "remember"
        static let kStatus = "remember"
        static let kSign = "Alwayssign"
        
        static let kSign_Job = "Alwayssign_job"
        
        static let StorePassword = "Password"
        static let isNotify = "isNotify"
        static let KCheckUserType = "Business"
        static let kBUserTye = "BUserType"
        //
        static let KCurrentAddress = "address"
        static let KCurrentLat = "latitude"
        static let KCurrentLong = "longitude"
        static let isProfile = "isProfile"
        static let myCurrentLat = "myCurrentLat"
        static let myCurrentLong = "myCurrentLong"
        //
        static let kjobTitleName = "jobTitleName"
        static let kspecializationName = "specializationName"
        static let krating = "rating"
        static let kFirDeviceToken = "deviceToken"
        //Elements for Filter
        static let fJobId = "fJobId"
        static let fJobTitle = "fJobTitle"
        static let fSpeId = "fSpeId"
        static let fSpeciality = "fSpeciality"
        static let fValueId = "fValueId"
        static let fValue = "fValue"
        static let fStrengthId = "fStrengthId"
        static let fStrenght = "fStrenght"
        static let fState = "fState"
        static let fCountry = "fCountry"
        static let fCity = "fCity"
        static let fAvailability = "fAvailability"
        static let fLocation = "fLocation"
        static let isFromSearch = "isFromSearch"
        //
        static let fRating = "fRating"
        static let fCompany = "fCompany"
        //
        static let fEmpType = "fEmpType"
        static let fExpSalary = "fExpSalary"
        static let isVerified = "isVerified"
        //
        static let fMinsSalary = "fMinsSalary"
        static let fMaxSalary = "fMaxSalary"
        static let fMaxExp = "fMaxExp"
        static let fMinExp = "fMinExp"
        //
        static let KAlwasysEmail = "AlwasysEmail"
        static let kAlwaysPassword = "AlwaysPassword"
        static let KAlwasysEmail_Job = "AlwasysEmail_Job"
        static let kAlwaysPassword_Job = "AlwaysPassword_Job"
        static let kPhone = "kPhone"
        
        
        //// New Added
        
    }
    
}
