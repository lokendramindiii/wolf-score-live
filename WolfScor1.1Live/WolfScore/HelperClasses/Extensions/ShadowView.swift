//
//  ShadowView.swift
//  Uconnekt
//
//  Created by Mindiii on 28/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ShadowView: UIView {
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.56
        self.layer.shadowOffset = CGSize(width:0,height:0)
        // self.layer.shadowRadius = 0.0
        self.layer.shadowPath = UIBezierPath(rect: CGRect.init(x:-2, y: 2, width: self.frame.size.width+2, height: self.frame.size.height-2)).cgPath
    }
}
class ShadowNormel: UIView {
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.25
        self.layer.shadowOffset = CGSize(width:0,height:0)
        // self.layer.shadowRadius = 0.0
        self.layer.shadowPath = UIBezierPath(rect: CGRect.init(x:-2, y: 2, width: self.frame.size.width+2, height: self.frame.size.height-2)).cgPath
    }
    
}
class SetShadowAppView: UIView {
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        layer.shadowOpacity = 0.7
    }
}
class SetDarkShadowAppView: UIView {
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        layer.shadowColor = UIColor.colorConstant.appDeepBlack.cgColor
        layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        layer.shadowOpacity = 0.7
    }
}
