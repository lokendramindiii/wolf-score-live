//
//  MessageConstant.swift
//  Mualab
//
//  Created by MINDIII on 10/16/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import Foundation
import UIKit


extension UIColor{
    enum colorConstant {
        
        static let appGrayColor = UIColor(red: 51.0 / 255.0, green: 51.0 / 255.0, blue: 51.0 / 255.0, alpha: 1.0)
        
        static let appLightGrayColor = UIColor(red: 130.0 / 255.0, green: 130.0 / 255.0, blue: 130.0 / 255.0, alpha: 1.0)
        // Checkout screen
        
        
        static let sliderLocalColorGrren = UIColor(red: 0.0 / 255.0, green: 133.0 / 255.0, blue: 70.0 / 255.0, alpha: 1.0)
        static let sliderVisitorColorSkyBlue = UIColor(red: 1.0 / 255.0, green: 169.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
        
        
        //Wolf Score codes
        static let appLightBlack = UIColor(red: 35.0 / 255.0, green: 40.0 / 255.0, blue: 44.0 / 255.0, alpha: 1.0)
        static let appDarkBlack = UIColor(red: 31.0 / 255.0, green: 32.0 / 255.0, blue: 36.0 / 255.0, alpha: 1.0)
        static let appDeepBlack = UIColor(red: 14.0 / 255.0, green: 15.0 / 255.0, blue: 18.0 / 255.0, alpha: 1.0)
        static let appBlueColor = UIColor(red: 1.0 / 255.0, green: 169.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
       
        static let appLightBlueColor = UIColor(red: 131.0 / 255.0, green: 150.0 / 255.0, blue: 165.0 / 255.0, alpha: 1.0)
        static let tabBarBackgroundColor = UIColor(red: 27.0 / 255.0, green: 28.0 / 255.0, blue: 32.0 / 255.0, alpha: 1.0)
        
        static let appSectionHeader = UIColor(red: 34.0 / 255.0, green: 45.0 / 255.0, blue: 50.0 / 255.0, alpha: 1.0)
        

        
        //suresh
         static let appGreenColor = UIColor(red: 0, green: 0.537254902, blue: 0.2431372549, alpha: 1)
        
        static let appTimeLineColor = UIColor(red: 0.4431372549, green: 0.4901960784, blue: 0.537254902, alpha: 1)
         static let appRedColor = UIColor(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
   
      static let appTblBgColor = UIColor(red: 0.137254902, green: 0.1764705882, blue: 0.2, alpha: 1)
        

        
    }
 //let col = #colorLiteral(red: 0.137254902, green: 0.1764705882, blue: 0.2, alpha: 1)
}

struct StaticData {
    static func getSalaryData()->[Int]{
        var arrSalary = [Int]()
        for index in 0...10{
            arrSalary.append(index*20000)
        }
        return arrSalary
}
    static func getExprienceData()->[Float]{
        var arrExperience = [Float]()
        for index in 0...20{
            //let val = 0.5
            arrExperience.append(Float(Float(index)*0.5))
        }
        return arrExperience
    }
}
