//
//  WebserviceConstant.swift
//
//
//  Created by MINDIII on 10/25/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

let objWebServiceManager:WebServiceManager = WebServiceManager()

class webUrl{
    
// API Base URLS
    
// Base url  dev url
 // static let BaseUrl : String = "http://dev.wolfscore.info/api_v1"
   
    // live url
    static let BaseUrl : String = "http://wolfscore.info/api_v1"

    static let guest_signup: String = "/service/guest_signup"
    static let get_Country_Name: String = "/users/get_country_list"
    static let get_Local_Teams: String = "/teams/get_local_teams"
    static let get_country_league_team: String = "/teams/get_country_league_team"
    
    // Popular list
    static let get_Popular_Teams: String = "/teams/get_popular_teams"
    static let get_popular_players: String = "/players/get_popular_players"
    static let add_favourites: String = "/users/add_favourites"
    static let get_favorite_list: String = "/users/get_my_favorite_list"
    
    // GetNotification ,Update Notification
    static let get_notification_setting: String = "/users/get_notification_setting"
    static let update_notification_setting: String = "/users/update_notification_setting"
    
    // Get Fixtures Today tomarrow yesterday
    static let get_fixtures: String = "/matches/get_fixtures"
    
    // Search matches by teamid and date
    static let search_matches: String = "/matches/search_matches"
    static let single_favorite_unfavorite: String = "/users/single_favorite_unfavorite"
    
    
    // league Api
    static let get_league_list: String = "/leagues/get_league_list"
    static let add_removeFilterleague: String = "/leagues/add_remove_filtered_league"
    static let get_leagueFilterSequenceList: String = "/leagues/get_league_filter_sequence_list"
    static let set_filterLeagues: String = "/leagues/set_filter_leagues"
    
    // match detail of fixture
    static let get_match_details: String = "/matches/get_match_details"
    static let get_head_to_head: String = "/matches/get_head_to_head"
    static let get_team_lineup: String = "/matches/get_team_lineup"
    static let get_standings_detail: String = "/matches/get_standings_detail"

    //Suresh
    static let get_match_commentary: String = "/matches/get_match_commentary"
    static let get_match_highlight: String = "/matches/get_match_highlight"
    static let get_team_statistics: String = "/matches/get_team_statistics"
    static let get_live_score: String = "/matches/live_score"
    static let get_country_league_list: String = "/leagues/get_country_league_list"
    
    static let get_team_detail: String = "/teams/get_team_detail"
    static let get_team_Table: String = "/matches/get_standings_detail"
    
    static let get_league_Table: String = "/leagues/get_standing_and_statistics_by_league_id"
    
    static let get_league_matches: String = "/leagues/get_league_matches"
 
    static let get_highlights: String = "/matches/get_highlights"
    static let get_player_detail: String = "/players/get_player_detail"
    
    // New added
    
    // LogIn Module
    static let login : String = "/service/login"
    static let signup : String = "/service/signup"
    static let check_is_user_social_registered : String = "/service/check_is_user_social_registered"
    static let logout : String = "/users/logout"
    static let forgot_password : String = "/service/forgot_password"
    static let change_password : String = "/users/change_password"
    static let update_profile : String = "/users/update_profile"
    
    
    static let add_popular_league_favorite_league : String = "/users/add_popular_league_favorite_league"

    // News Section
    static let news_feed : String = "/news/news_feed"
    static let news_Detail : String = "/news/get_news_detail"
    
    // In app purchase verifiy receipt url
    static let verifyReceiptUrl         = "https://sandbox.itunes.apple.com/verifyReceipt"
    
    // static let verifyReceiptUrl         = "https://buy.itunes.apple.com/verifyReceipt"
    
    static let Get_subscription_plan_list         = "/users/get_subscription_plan_list"

    static let SubscriptionUserTransactionDetail         = "/subscription/user_transaction_detail"
    
    // Contact us
    static let GetContent         = "/service/get_content"
    
    
    static let GetPopular_League_and_count         = "/leagues/get_popular_league_and_count"

}

class WsParams  {
    
    static let itunesSharedSecret : String = "7454fabef4f640b19111365ca7ef6ed0"
    
    static let product_id : String = "product_id"
    static let purchase_date : String = "purchase_date"
    static let expires_date : String = "expires_date"
    static let amount : String = "amount"
    static let transaction_id : String = "transaction_id"
    static let origional_transaction_id : String = "origional_transaction_id"
    static let quantity : String = "quantity"
    static let receipt_key : String = "receipt_key"

}

