//
//  MessageConstant.swift
//  Mualab
//
//  Created by MINDIII on 10/16/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import Foundation



//pragma mark - Field validation


 let UnderDev: String = NSLocalizedString("Under development", tableName: nil, comment: "")

let LenghtPassword: String = NSLocalizedString("Password must contain 6 characters", tableName: nil, comment: "")

 let LenghtConfirmPassword: String = NSLocalizedString("Confirm password must be at least 6 characters long", tableName: nil, comment: "")

let EmptyUserName: String = NSLocalizedString("Please enter username", tableName: nil, comment: "")

let EmptyPassword: String = NSLocalizedString("Please enter your password", tableName: nil, comment: "")

let EmptyEmail: String = NSLocalizedString("Please enter your email address", tableName: nil, comment: "")

let EmptyContact: String = NSLocalizedString("Please enter your contact number", tableName: nil, comment: "")

let EmptyConfirmpassword: String = NSLocalizedString("Please enter your confirm password", tableName: nil, comment: "")

let MatchConfirmpassword: String = NSLocalizedString("Password and confirm password do not match", tableName: nil, comment: "")

let NameAlredy: String = NSLocalizedString("That name is already being used", tableName: nil, comment: "")

let BlankSapce: String = NSLocalizedString("username should not contain blank space", tableName: nil, comment: "")

let NetError: String = NSLocalizedString("Something went wrong, please try after sometime", tableName: nil, comment: "")

let NetworkConnection: String = NSLocalizedString("No Network Connection", tableName: nil, comment: "")

let NetworkSession: String = NSLocalizedString("Your current session has expired, please login again", tableName: nil, comment: "")

let InvalidEmail: String = "Please enter valid email address"

let InvaldBusiness: String = "Numerical charaters is not allowed"

//pragma mark - Alert validation
 let kAlertMessage: String = NSLocalizedString("Message", tableName: nil, comment: "")
let kAlert: String = NSLocalizedString("Alert", tableName: nil, comment: "")

// Uconnet
let EmptyUserType:String = "Please select user type"
let EmptyBusinessName: String = NSLocalizedString("Please enter business name", tableName: nil, comment: "")
let EmptyFullName: String = NSLocalizedString("Please enter full name", tableName: nil, comment: "")

let LenghtbusinessName: String = NSLocalizedString("Business name must contain 3 characters ", tableName: nil, comment: "")

let LenghtFullName: String = NSLocalizedString("Full name must contain 3 characters", tableName: nil, comment: "")

let LenghtContact: String = NSLocalizedString("Contact number should be minimum 7 characters and maximum 16 characters long.", tableName: nil, comment: "")
