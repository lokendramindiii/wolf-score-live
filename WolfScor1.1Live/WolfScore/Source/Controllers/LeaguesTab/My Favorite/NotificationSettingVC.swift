//
//  NotificationSettingVC.swift
//  WolfScore
//
//  Created by Mindiii on 25/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import XLPagerTabStrip
import SVProgressHUD
import Toaster
class NotificationSettingVC: UIViewController , IndicatorInfoProvider {
    
    @IBOutlet weak var imageAll: UIImageView!
    @IBOutlet weak var imageGoal: UIImageView!
    @IBOutlet weak var imageRedCards: UIImageView!
    @IBOutlet weak var imageStarted: UIImageView!
    @IBOutlet weak var imageMatchFinishCards: UIImageView!
    
    var arrAllData = [Model_Notification]()
    var obj_Model = Model_Notification.init(dict: [:])
    var isAllOn:Bool = false
    var isGoalOn:Bool = false
    var isRedCardsOn:Bool = false
    var isMatchFinishOn:Bool = false
    var isStartedOn:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.setDefaultMaskType(.clear)
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "  NOTIFICATIONS  ")
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.get_Notification_Status()
    }
    
        override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnNotificationOnAction(_ sender : UIButton){
        self.view.endEditing(true)
        
        if sender.tag == 1{
            if(self.isAllOn == true){
                self.isAllOn = false
                
                self.imageAll.image = UIImage(named: "toggle_white")
                allNotification_Off()
            }else{
                self.isAllOn = true
                self.imageAll.image = UIImage(named: "toggle_active")
                allNotification_On()
            }
        }else if (sender.tag == 2){
            if(self.isGoalOn == true){
                self.isGoalOn = false
                self.imageGoal.image = UIImage(named: "toggle_white")
            }else{
                self.isGoalOn = true
                self.imageGoal.image = UIImage(named: "toggle_active")
            }
        }else if (sender.tag == 3){
            if(self.isRedCardsOn == true){
                self.isRedCardsOn = false
                self.imageRedCards.image = UIImage(named: "toggle_white")
            }else{
                self.isRedCardsOn = true
                self.imageRedCards.image = UIImage(named: "toggle_active")
            }
        }else if (sender.tag == 4){
            if(self.isStartedOn == true){
                self.isStartedOn = false
                self.imageStarted.image = UIImage(named: "toggle_white")
            }else{
                self.isStartedOn = true
                self.imageStarted.image = UIImage(named: "toggle_active")
            }
        }
        else if (sender.tag == 5){
            if(self.isMatchFinishOn == true){
                self.isMatchFinishOn = false
                self.imageMatchFinishCards.image = UIImage(named: "toggle_white")
            }else
            {
                self.isMatchFinishOn = true
                self.imageMatchFinishCards.image = UIImage(named: "toggle_active")
            }
        }
        
        if  self.isGoalOn == true && self.isRedCardsOn == true && self.isStartedOn == true &&  self.isMatchFinishOn == true
        {
            self.isAllOn = true
            self.imageAll.image = UIImage(named: "toggle_active")
            allNotification_On()
        }
        else {
            self.isAllOn = false
            self.imageAll.image = UIImage(named: "toggle_white")
        }
        
    }
    
    @IBAction func btnNextAction(_ sender : UIButton){

        for i in 0..<self.arrAllData.count {
            let objModel: Model_Notification = self.arrAllData[i]
            self.create_json(obj_Model: objModel)
        }
    }
}
// MARK: - Webservice

extension NotificationSettingVC{

    func get_Notification_Status() {
        SVProgressHUD.show()
        
        objWebServiceManager.requestGet(strURL: webUrl.get_notification_setting, params: nil, success: { (response) in
            SVProgressHUD.dismiss()
            print(response)
            let status = response["status"] as? String ?? ""
            let message = response["message"] as? String ?? ""

            if status == "success"{
                if let notification_manage = response["data"] as? [String:Any] {
                    let obj = Model_Notification.init(dict: notification_manage)
                    self.arrAllData.append(obj)
                    self.update_UI(obj_Model: obj)
                }
            }
            else if status == "fail"{
                Toast(text: message).show()
            }
        }) { (error) in
            SVProgressHUD.dismiss()
        }
    }
    
    
    
    func update_UI(obj_Model:Model_Notification) {
        
        if obj_Model.goal == "1" {
            self.isGoalOn = true
            self.imageGoal.image = UIImage(named: "toggle_active")
        }else{
            self.isGoalOn = false
            self.imageGoal.image = UIImage(named: "toggle_white")
        }
        if obj_Model.red_card == "1" {
            self.isRedCardsOn = true
            self.imageRedCards.image = UIImage(named: "toggle_active")
        }else{
            self.isRedCardsOn = false
            self.imageRedCards.image = UIImage(named: "toggle_white")
        }
        if obj_Model.match_finish == "1" {
            self.isMatchFinishOn = true
            self.imageMatchFinishCards.image = UIImage(named: "toggle_active")
        }else{
            self.isMatchFinishOn = false
            self.imageMatchFinishCards.image = UIImage(named: "toggle_white")
        }
        if obj_Model.match_start == "1" {
            self.isStartedOn = true
            self.imageStarted.image = UIImage(named: "toggle_active")
        }else{
            self.isStartedOn = false
            self.imageStarted.image = UIImage(named: "toggle_white")
        }
       
        if obj_Model.goal == "1" && obj_Model.red_card == "1" && obj_Model.match_start == "1" && obj_Model.match_finish == "1" {
            self.isAllOn = true
            self.imageAll.image = UIImage(named: "toggle_active")
        }else{
            self.isAllOn = false
            self.imageAll.image = UIImage(named: "toggle_white")
        }
    }
    
    func create_json(obj_Model:Model_Notification) {
        
        var serviceJosnDict = [String: Any]()
        var serviceJosn = [String: Any]()
        serviceJosn["goal"] = (isGoalOn == true) ? "1" : "0"
        serviceJosn["red_card"] =  (isRedCardsOn == true) ? "1" : "0"
        serviceJosn["match_finish"] = (isMatchFinishOn == true) ? "1" : "0"
        
        serviceJosn["match_start"] = (isStartedOn == true) ? "1" : "0"
        serviceJosnDict = serviceJosn
        var objectString = ""
        
        if let objectData = try? JSONSerialization.data(withJSONObject: serviceJosnDict, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            objectString = String(data: objectData, encoding: .utf8)!
        }
        self.Post_Notification_Status(strJson: objectString)
    }
    
    func Post_Notification_Status(strJson:String) {
    
        SVProgressHUD.show()
        let param = ["notification_key":strJson] as [String: AnyObject]
        print(param)
        objWebServiceManager.requestPost(strURL: webUrl.update_notification_setting, params: param, success: { (response) in
            
            let message = response["message"] as? String ?? ""
            SVProgressHUD.dismiss()
            Toast(text: message).show()
            objAppDelegate.goToTabBar()
        }, failure: { (error) in
            SVProgressHUD.dismiss()
        })
    }
}
extension NotificationSettingVC{
    func allNotification_On() {
        self.isGoalOn = true
        self.isRedCardsOn = true
        self.isMatchFinishOn = true
        self.isStartedOn = true
        self.imageGoal.image = UIImage(named: "toggle_active")
        self.imageRedCards.image = UIImage(named: "toggle_active")
        self.imageMatchFinishCards.image = UIImage(named: "toggle_active")
        self.imageStarted.image = UIImage(named: "toggle_active")
    }
    func allNotification_Off() {
        self.isGoalOn = false
        self.isRedCardsOn = false
        self.isMatchFinishOn = false
        self.isStartedOn = false
        self.imageGoal.image = UIImage(named: "toggle_white")
        self.imageRedCards.image = UIImage(named: "toggle_white")
        self.imageMatchFinishCards.image = UIImage(named: "toggle_white")
        self.imageStarted.image = UIImage(named: "toggle_white")
    }
}
