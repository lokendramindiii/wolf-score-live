//
//  MyFollowingsVC.swift
//  WolfScore
//
//  Created by Mindiii on 23/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVProgressHUD
import AlamofireImage
class MyFollowingsVC: UIViewController,IndicatorInfoProvider{
    
    @IBOutlet weak var collViewTeam: UICollectionView!
    @IBOutlet weak var collViewPlayer: UICollectionView!
    @IBOutlet weak var collViewLeagues: UICollectionView!
    @IBOutlet weak var lblFavTeamCount: UILabel!
    @IBOutlet weak var lblFavPlayerCount: UILabel!
    @IBOutlet weak var lblFavLeaguesCount: UILabel!
    @IBOutlet weak var lblLine1: UILabel!
    @IBOutlet weak var lblLine2: UILabel!
    @IBOutlet weak var lblLine3: UILabel!
    
    var indexPath:Int? = nil
    var arrAllData:[ModelTeam] = []
    var arrAllData_Player:[ModelTopPlayer] = []
    var arrAllData_Team:[ModelTeam] = []
    var arrAllData_Leageus:[ModelLeagueList] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if APP_DELEGATE.isInterstialPresent == true
        {
            APP_DELEGATE.isInterstialPresent = false
            return
        }
        //self.call_Webservice_Demo()
        self.lblLine1.isHidden = true
        self.lblLine2.isHidden = true
        self.lblLine3.isHidden = true
        self.call_Webservice_Get_List()
        
        self.view.layoutIfNeeded()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "  FOLLOWING  ")
    }
    
     func call_Webservice_Demo() {
     let url = Bundle.main.url(forResource: "DemoTeam", withExtension: "plist")!
     let soundsData = try! Data(contentsOf: url)
     
     if let myPlist = try! PropertyListSerialization.propertyList(from: soundsData, options: [], format: nil) as? [String:Any] {
     print(myPlist)
     if let arrData = myPlist["data"] as? [[String: Any]]{
     self.arrAllData.removeAll()
     for dict in arrData{
     let obj = ModelTeam.init(dict: dict)
       // obj.str_add_Item
     self.arrAllData.append(obj)
     }
     self.collViewTeam.reloadData()
        self.lblFavPlayerCount.text = "Players (" + "\(self.arrAllData.count)" + ")"
        self.lblFavTeamCount.text = "Teams (" + "\(self.arrAllData.count)" + ")"
     }
     }
    }

    
    func call_Webservice_Post_Favorite(request_type:String,request_id:String, index:Int,type:String){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":type] as [String:Any]
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
           // let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if type == "team"{
                    
                   self.arrAllData_Team.remove(at: index)
                   self.collViewTeam.reloadData()
                   self.lblFavTeamCount.text = "Teams (" + "\(self.arrAllData_Team.count-1)" + ")"
                    
                }else if type == "player"{
                    
                    self.arrAllData_Player.remove(at: index)
                    self.collViewPlayer.reloadData()
                    self.lblFavPlayerCount.text = "Players (" + "\( self.arrAllData_Player.count-1)" + ")"
                }else if type == "league"{
                        
                    self.arrAllData_Leageus.remove(at: index)
                    self.collViewLeagues.reloadData()
                    self.lblFavLeaguesCount.text = "Leagues (" + "\(self.arrAllData_Leageus.count-1)" + ")"
                }
                
            }else{
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
    }
    
    func call_Webservice_Get_List() {
        SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.get_favorite_list, params: nil, success: { (response) in
            print(response)
            SVProgressHUD.dismiss()
            _ = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                self.lblLine1.isHidden = false
                self.lblLine2.isHidden = false
                self.lblLine3.isHidden = false
                if let data = response["data"] as? [String:Any] {
                    ////// Player List parsing
                    if let player_list = data["player_list"] as? [String:Any] {
                        if let arrData_player_list = player_list["player_list"] as? [[String: Any]]{
                            self.arrAllData_Player.removeAll()
                            let obj = ModelTopPlayer.init(dict: [:])
                            self.arrAllData_Player.append(obj)
                            for dict in arrData_player_list{
                                let obj = ModelTopPlayer.init(dict: dict)
                                //self.arrAllData.append(obj)
                                self.arrAllData_Player.append(obj)
                            }
                           self.collViewPlayer.reloadData()
                        }
                        if let total_Player = player_list["total_records"] as? String{
                            self.lblFavPlayerCount.text = "Players (" + "\(total_Player)" + ")"
                        }
                    }
                    ////// Team List parsing
                    if let team_list = data["team_list"] as? [String:Any] {
                        if let arrData_team_list = team_list["team_list"] as? [[String: Any]]{
                            self.arrAllData_Team.removeAll()
                            let obj = ModelTeam.init(dict: [:])
                            self.arrAllData_Team.append(obj)
                            for dict in arrData_team_list{
                                let obj = ModelTeam.init(dict: dict)
                                //self.arrAllData.append(obj)
                                self.arrAllData_Team.append(obj)
                            }
                            self.collViewTeam.reloadData()
                            if let total_Team = team_list["total_records"] as? String{
                                self.lblFavTeamCount.text = "Teams (" + "\(total_Team)" + ")"
                            }
                        }
                    }
                    
                    ////// Leagues List parsing
                    if let Leagues_list = data["league_list"] as? [String:Any] {
                        if let arrData_Leagues_list = Leagues_list["league_list"] as? [[String: Any]]{
                            self.arrAllData_Leageus.removeAll()
                            let obj = ModelLeagueList.init(fromDictionary: [:])
                            self.arrAllData_Leageus.append(obj)
                            for dict in arrData_Leagues_list{
                                let obj = ModelLeagueList.init(fromDictionary: dict)
                                //self.arrAllData.append(obj)
                                self.arrAllData_Leageus.append(obj)
                            }
                            self.collViewLeagues.reloadData()
                        }
                        if let total_Leagues = Leagues_list["total_records"] as? String{
                            self.lblFavLeaguesCount.text = "Leagues (" + "\(total_Leagues)" + ")"
                        }
                    }
                    
                    
               self.lblFavPlayerCount.setContentHuggingPriority(UILayoutPriority.defaultLow, for:.horizontal)
                    self.lblLine1.setContentHuggingPriority(UILayoutPriority.defaultLow, for:.horizontal)
            }
        }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
        
        
    }
}
// MARK: - CollectionView Delegates & Datasource

extension MyFollowingsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collViewPlayer  {
            return arrAllData_Player.count
        }else if collectionView == self.collViewLeagues  {
            return arrAllData_Leageus.count
        }else{
            return arrAllData_Team.count
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collViewPlayer {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyTeamCollectionViewCell", for: indexPath) as! MyTeamCollectionViewCell
            let objModel = self.arrAllData_Player[indexPath.row]
            if indexPath.row == 0{
                cell.viewCell.isHidden = true
                cell.viewAdd.isHidden = false
            }else{
                cell.viewCell.isHidden = false
                cell.viewAdd.isHidden = true
                //cell.lblName.text = objModel.name
             cell.imgFlag.layer.cornerRadius = cell.imgFlag.frame.size.height/2
                cell.imgFlag.layer.masksToBounds = true
                cell.lblName.text = objModel.common_name
                let strUrl = objModel.player_image
                if let url = URL(string: strUrl){
                    cell.imgFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgFlag.image = UIImage(named: "icon_placeholderTeam")
                }
                
                cell.btnAdd.tag = indexPath.row
                cell.btnAdd.addTarget(self, action: #selector(btnAdd_PlayerAction(_:)), for: .touchUpInside)
                
            }
            return cell
        }else if collectionView == self.collViewLeagues {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyTeamCollectionViewCell", for: indexPath) as! MyTeamCollectionViewCell
            let objModel = self.arrAllData_Leageus[indexPath.row]
            if indexPath.row == 0{
                cell.viewCell.isHidden = true
                cell.viewAdd.isHidden = false
            }else{
                cell.viewCell.isHidden = false
                cell.viewAdd.isHidden = true
                //cell.lblName.text = objModel.name
                cell.imgFlag.layer.cornerRadius = cell.imgFlag.frame.size.height/2
                cell.imgFlag.layer.masksToBounds = true
                cell.lblName.text = objModel.leagueName
                let strUrl = objModel.leagueFlage ?? ""
                if let url = URL(string: strUrl){
                    cell.imgFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgFlag.image =  UIImage(named: "icon_placeholderTeam")
                }
                cell.btnAdd.tag = indexPath.row
                cell.btnAdd.addTarget(self, action: #selector(btnAdd_LeaguesAction(_:)), for: .touchUpInside)
                
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyTeamCollectionViewCell", for: indexPath) as! MyTeamCollectionViewCell
            let objModel = self.arrAllData_Team[indexPath.row]
            if indexPath.row == 0{
                cell.viewCell.isHidden = true
                cell.viewAdd.isHidden = false
            }else{
                cell.viewCell.isHidden = false
                cell.viewAdd.isHidden = true
                cell.lblName.text = objModel.name
                
                let strUrl = objModel.logo_path
                if let url = URL(string: strUrl){
                    cell.imgFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgFlag.image =  UIImage(named: "icon_placeholderTeam")
                }
                
                cell.btnAdd.tag = indexPath.row
                cell.btnAdd.addTarget(self, action: #selector(btnAddItemAction(_:)), for: .touchUpInside)
            }
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = CGFloat((self.collViewTeam.frame.size.width) / 4.0)
        let cellHeight = CGFloat(cellWidth+5)
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{
            return
        }
        if collectionView == self.collViewTeam {
            let objModel = self.arrAllData_Team[indexPath.row]
            self.call_Webservice_Post_Favorite(request_type: "0", request_id: objModel.team_id, index: indexPath.row, type: "team")
            
        }else if collectionView == self.collViewPlayer{
            let objModel = self.arrAllData_Player[indexPath.row]
            self.call_Webservice_Post_Favorite(request_type: "0", request_id: objModel.player_id, index: indexPath.row, type: "player")
        }else if collectionView == self.collViewLeagues{
            let objModel = self.arrAllData_Leageus[indexPath.row]
            self.call_Webservice_Post_Favorite(request_type: "0", request_id: objModel.leagueId, index: indexPath.row, type: "league")
        }
        
    }
    
    
    @IBAction func btnAddItemAction(_ sender : UIButton){
//        if sender.tag == 0{
            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "TopPlayersListVC") as! TopPlayersListVC
            viewController.isFromMyFav = true
            self.present(viewController, animated: true, completion: nil)
//        }else{
//            print("click")
//        }
        
    }
    @IBAction func btnAdd_PlayerAction(_ sender : UIButton){
        
//        if sender.tag == 0{
            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "PopularTeamListVC") as! PopularTeamListVC
            viewController.isFromMyFav = true
            self.present(viewController, animated: true, completion: nil)
//        }else{
//            print("click")
//        }
    }
    
    @IBAction func btnAdd_LeaguesAction(_ sender : UIButton){
        
        //if sender.tag == 0{
            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "TopLeaguesListVC") as! TopLeaguesListVC
            viewController.isFromMyFav = true
            self.present(viewController, animated: true, completion: nil)
        //}else{
          print("click")
       // }
       
    }
    
    
}
