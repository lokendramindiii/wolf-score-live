//
//  MyNotifiactionsVC.swift
//  WolfScore
//
//  Created by Mindiii on 23/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import XLPagerTabStrip
import SVProgressHUD
import Toaster

class MyNotifiactionsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.slideMenuController()?.leftPanGesture?.isEnabled = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.slideMenuController()?.leftPanGesture?.isEnabled = true
    }
    
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let storyboard = UIStoryboard(name: "UserTabbar", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "UserTabbarVC") as! UserTabbarVC
    self.slideMenuController()?.changeMainViewController(mainViewController, close: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
}
