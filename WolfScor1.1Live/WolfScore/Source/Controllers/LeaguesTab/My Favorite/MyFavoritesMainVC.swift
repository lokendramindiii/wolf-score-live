

//
//  MyFavoritesMainVC.swift
//  WolfScore
//
//  Created by Mindiii on 23/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MyFavoritesMainVC: ButtonBarPagerTabStripViewController,UIGestureRecognizerDelegate {

    
    var isfromMyProfile = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.colorConstant.appDarkBlack
        settings.style.buttonBarItemBackgroundColor = UIColor.colorConstant.appDarkBlack
        settings.style.buttonBarItemFont = UIFont(name: "roboto-Medium" , size: 14.0)!
        settings.style.selectedBarHeight = 3.0
        settings.style.selectedBarBackgroundColor = UIColor.colorConstant.appBlueColor
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.colorConstant.appLightBlueColor
            newCell?.label.textColor = .white
            oldCell?.label.font = UIFont(name: "roboto-Medium" , size: 14.0)!
            newCell?.label.font = UIFont(name: "roboto-Bold" , size: 14.0)!
        }
        
        self.view.layoutIfNeeded()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        self.slideMenuController()?.leftPanGesture?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.slideMenuController()?.leftPanGesture?.isEnabled = true
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if  isfromMyProfile == true
        {
        self.navigationController?.popViewController(animated: true)
            return
        }


        let storyboard = UIStoryboard(name: "UserTabbar", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "UserTabbarVC") as! UserTabbarVC

        self.slideMenuController()?.changeMainViewController(mainViewController, close: true)
    }

 
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let child_1 = UIStoryboard(name: "UserTabbar", bundle: nil).instantiateViewController(withIdentifier: "MyFollowingsVC")
        let child_2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationSettingVC")
        return [child_1, child_2]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
