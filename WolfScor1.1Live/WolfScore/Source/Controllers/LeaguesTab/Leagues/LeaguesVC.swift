//
//  LeaguesVC.swift
//  WolfScore
//
//  Created by Mindiii on 28/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster
import AlamofireImage
import GoogleMobileAds
import SkeletonView
struct leagueVc {
    static var arrSelectedLeague = [String]()
}

class LeaguesVC: UIViewController  {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var viewRestOfThe: UIView!
    @IBOutlet weak var viewPopular: UIView!
    @IBOutlet weak var viewBannerBottom: UIView!
    @IBOutlet weak var lblPopuler: UILabel!
    @IBOutlet weak var lblRestOfthewold: UILabel!

    @IBOutlet weak var tblPopular: UITableView!
    @IBOutlet weak var tblPopulerHightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tblCountryLeague: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var tblCountryHightConstraint: NSLayoutConstraint!
    
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!
    
    var arrPopularlocal = [String]()
    
    var arrCountryLeague = [ModelLeagueListGroupByCountry]()
    var arrPopularLeague = [ModelLeagueListGroupByCountry]()
    
    var arrFilterCountryLeague = [ModelLeagueListGroupByCountry]()
    var arrFilterPopularLeague = [ModelLeagueListGroupByCountry]()
    
    var isSearching = false
    var currentHight = 0.0
    
    var isLoadData = false
    var placeholderRow = 10
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.show_skelton()
        arrPopularlocal = ["England","Spain","Germany","Italy","France","United State Of America","World","Europe","South America","Asia","North America","Oceania"]
        
        if arrPopularlocal.contains(kCurrent_Country){
            let index = arrPopularlocal.index(of: kCurrent_Country)
            if index != nil {
                arrPopularlocal.remove(at: index!)
            }
            arrPopularlocal.insert(kCurrent_Country, at: 0)
        }else{
            arrPopularlocal.insert(kCurrent_Country, at: 0)
        }
        
        
        self.tblCountryLeague.rowHeight = UITableViewAutomaticDimension
        self.txtSearch.delegate = self
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Search leagues", tableName: nil, comment: ""), attributes: [NSAttributedStringKey.foregroundColor:UIColor.white
            ])
        
        self.modifyClearButtonWithImage(image: UIImage(named: "icon_clearTxtfild")!, txtfiled: txtSearch)
        
        self.call_Webservice_Get_LeaugeList()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForDetailCell), name: NSNotification.Name(rawValue: KNotifiationFavouriteLeague), object: nil)
        
        activity.isHidden = true
        activity.stopAnimating()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.LeguageDetails), name: NSNotification.Name(rawValue: KNotifiationSelectLeague), object: nil)
        
        self.bannerAdSetup()
        
    }

    
    func show_skelton()
    {
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblPopuler,lblRestOfthewold].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
    }
    
    func hide_skelton()
    {
        [lblPopuler,lblRestOfthewold].forEach { $0?.hideSkeleton()
        }
    }
    
    func modifyClearButtonWithImage(image : UIImage ,txtfiled : UITextField) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(sender:)), for: .touchUpInside)
        txtfiled.rightView = clearButton
        txtfiled.rightViewMode = .whileEditing
    }
    
    @objc func clear(sender : AnyObject) {
        
        self.arrFilterCountryLeague = self.arrCountryLeague
        self.arrFilterPopularLeague = self.arrPopularLeague
        
        self.tblPopular.reloadData()
        self.tblCountryLeague.reloadData()
        activity.isHidden = true
        activity.stopAnimating()
        
        self.txtSearch.text = ""
        self.txtSearch.sendActions(for: .editingChanged)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        selected_my_Index = 1
        
        if APP_DELEGATE.isInterstialPresent == true
        {
            return
        }
        objAppShareData.isLonchXLPageIndex = true
        self.tblPopular.reloadData()
        self.tblCountryLeague.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func flag(country: String) -> String
    {
        let base: UInt32 = 127397
        return String(String.UnicodeScalarView(
            country.unicodeScalars.compactMap({ UnicodeScalar(base + $0.value) })
        ))
    }
    
    func locale(for fullCountryName : String) -> String {
        let locales : String = ""
        for localeCode in NSLocale.isoCountryCodes {
            let identifier = NSLocale(localeIdentifier: localeCode)
            let countryName = identifier.displayName(forKey: NSLocale.Key.countryCode, value: localeCode)
            if fullCountryName.lowercased() == countryName?.lowercased() {
                return localeCode
            }
        }
        return locales
    }
}


// MARK: - UITextField Delegates
extension LeaguesVC:UITextFieldDelegate,UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        // self.view.endEditing(true)
        self.isSearching = false
        self.tblCountryHightConstraint.constant = self.tblCountryLeague.contentSize.height
    }
    
  
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        self.txtSearch.resignFirstResponder()
        self.isSearching = false
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        // NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.loadDataWithPageCount), object: nil)
        self.arrFilterCountryLeague = self.arrCountryLeague
        self.arrFilterPopularLeague = self.arrPopularLeague
        
        self.tblPopular.reloadData()
        self.tblCountryLeague.reloadData()
        activity.isHidden = true
        activity.stopAnimating()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if isLoadData == false
        {
            return  false
        }
        let text = textField.text! as NSString
        if (text.length == 1)  && (string == "") {
            // NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.loadDataWithPageCount), object: nil)
            
            //   self.arrFilterFevoriteLeagues = self.arrFevoriteLeagues
            
            self.arrFilterPopularLeague = self.arrPopularLeague
            self.tblPopular.reloadData()
            
            
            self.arrFilterCountryLeague = self.arrCountryLeague
            self.tblCountryLeague.reloadData()
            activity.isHidden = true
            activity.stopAnimating()
            return true
        }
        
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if !(substring == "") {
            activity.isHidden = false
            activity.startAnimating()
        }
        searchAutocompleteEntries(withSubstring: substring)
        return true
    }
    
    func searchAutocompleteEntries(withSubstring substring: String) {
        if !(substring == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.loadDataWithPageCount), object: nil)
            self.perform(#selector(self.loadDataWithPageCount), with: nil, afterDelay: 0.4)
            //self.loadDataWithPageCount(page: 0, strSearchText: substring)
        }
    }
    
    @objc func loadDataWithPageCount() {
        //        activity.isHidden = false
        //        activity.startAnimating()
        self.isSearching = true
        
        //   self.arrFilterFevoriteLeagues = self.arrFevoriteLeagues.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
        
        self.arrFilterCountryLeague.removeAll()
        self.arrFilterPopularLeague.removeAll()
        
        self.arrFilterCountryLeague = self.arrCountryLeague.filter { $0.countryName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
        
        self.arrFilterPopularLeague = self.arrPopularLeague.filter { $0.countryName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
        
        //------- array populer filter start ------------
        
        var  arrTemp1 = [ModelLeagueListGroupByCountry]()
        for objCountry in self.arrPopularLeague {
            
            if self.arrFilterPopularLeague.count == 0 {
                
                let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                objmodel.countryName = objCountry.countryName
                objmodel.countryId = objCountry.countryId
                objmodel.countryFlage = objCountry.countryFlage
                objmodel.isExpandation = true
                if objmodel.arrLeagueList.count > 0{
                    //self.arrFilterCountryLeague.append(objmodel)
                    arrTemp1.append(objmodel)
                }
                
            }else{
                
                for objFilterCountry in self.arrFilterPopularLeague{
                    if objCountry.countryId != objFilterCountry.countryId {
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                        objmodel.countryName = objCountry.countryName
                        objmodel.countryId = objCountry.countryId
                        objmodel.countryFlage = objCountry.countryFlage
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            //self.arrFilterCountryLeague.append(objmodel)
                            arrTemp1.append(objmodel)
                        }
                        break
                    }else{
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.countryName = objFilterCountry.countryName
                        objmodel.countryId = objFilterCountry.countryId
                        objmodel.countryFlage = objFilterCountry.countryFlage
                        objmodel.arrLeagueList = objFilterCountry.arrLeagueList
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            arrTemp1.append(objmodel)
                        }
                        break
                    }
                }
            }
        }
        
        self.arrFilterPopularLeague.removeAll()
        self.arrFilterPopularLeague = arrTemp1
        
        
        //-----------------------------search filter country  start---------------
        var  arrTemp = [ModelLeagueListGroupByCountry]()
        for objCountry in self.arrCountryLeague {
            
            if self.arrFilterCountryLeague.count == 0 {
                
                let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                objmodel.countryName = objCountry.countryName
                objmodel.countryId = objCountry.countryId
                objmodel.countryFlage = objCountry.countryFlage
                objmodel.isExpandation = true
                if objmodel.arrLeagueList.count > 0{
                    //self.arrFilterCountryLeague.append(objmodel)
                    arrTemp.append(objmodel)
                }
                
            }else{
                
                for objFilterCountry in self.arrFilterCountryLeague{
                    if objCountry.countryId != objFilterCountry.countryId {
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                        objmodel.countryName = objCountry.countryName
                        objmodel.countryId = objCountry.countryId
                        objmodel.countryFlage = objCountry.countryFlage
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            //self.arrFilterCountryLeague.append(objmodel)
                            arrTemp.append(objmodel)
                        }
                        break
                    }else{
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.countryName = objFilterCountry.countryName
                        objmodel.countryId = objFilterCountry.countryId
                        objmodel.countryFlage = objFilterCountry.countryFlage
                        objmodel.arrLeagueList = objFilterCountry.arrLeagueList
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            arrTemp.append(objmodel)
                        }
                        break
                    }
                }
                
            }
        }
        self.arrFilterCountryLeague.removeAll()
        self.arrFilterCountryLeague = arrTemp
        
        if self.txtSearch.text! == ""{
            //  self.arrFilterFevoriteLeagues = self.arrFevoriteLeagues
            self.arrFilterCountryLeague = self.arrCountryLeague
            self.arrFilterPopularLeague = self.arrPopularLeague
            
        }
        if  self.arrFilterCountryLeague.count == 0{
            self.isSearching = false
        }
        if  self.arrFilterCountryLeague.count == 0 {
            self.lblNoRecord.isHidden = false
        }else{
            self.lblNoRecord.isHidden = true
        }
        
        self.tblPopular.reloadData()
        self.tblCountryLeague.reloadData()
        activity.isHidden = true
        activity.stopAnimating()
    }
}


// MARK: - extension IBAction
extension LeaguesVC{
    
    @IBAction func btnMenuAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.slideMenuController()?.toggleLeft()
    }
    
    @objc func LeguageDetails(notification: Notification){
        if let objLeague = notification.object as? ModelLeagueList{
            let viewController = UIStoryboard(name: "LeaguesTab",bundle: nil).instantiateViewController(withIdentifier: "LeaguesDetailsVC") as! LeaguesDetailsVC
            viewController.strleagueName = objLeague.leagueName
            
            if objLeague.isFavorite == "1"
            {
                viewController.isleagueFav = true
            }
            
            objAppShareData.str_league_id = objLeague.leagueId
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    
    @objc func reloadDataForDetailCell(notification: Notification){
        self.tblPopular.reloadData()
        self.tblCountryLeague.reloadData()
        
    }
    
    @objc func btnPopulerDropdownPressed(sender: UIButton){
        
        if self.arrFilterPopularLeague.count == 0
        {
            return
        }
        let objModel = self.arrFilterPopularLeague[sender.tag]
        if objModel.isExpandation == false{
            objModel.isExpandation = true
        }else{
            objModel.isExpandation = false
        }
        self.tblPopular.reloadData()
    }
    
    @objc func btnCountryDropdownPressed(sender: UIButton){
        //let indexPath = IndexPath(row: sender.tag, section: 0)
        //let cell = self.tblCountryLeague.cellForRow(at: indexPath) as! LeagueTableViewCell
        
        if self.arrFilterCountryLeague.count == 0
        {
            return
        }
        
        let objModel = self.arrFilterCountryLeague[sender.tag]
        if objModel.isExpandation == false{
            objModel.isExpandation = true
        }else{
            objModel.isExpandation = false
        }
        
        self.tblCountryLeague.reloadData()
    }
}


// MARK: - TableView Delegates & Datasource
extension LeaguesVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isLoadData == false
        {
            return placeholderRow
        }
        
        if tableView == tblPopular
        {
            if arrFilterPopularLeague.count > 0{
                self.viewPopular.isHidden = false
            }else{
                self.viewPopular.isHidden = true
            }
            return self.arrFilterPopularLeague.count
        }
        else{
            if arrFilterCountryLeague.count > 0{
                self.viewRestOfThe.isHidden = false
            }else{
                self.viewRestOfThe.isHidden = true
            }
            return self.arrFilterCountryLeague.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if isLoadData == false
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCell", for: indexPath) as? LeagueTableViewCell{
                cell.show_skelton()
                
                DispatchQueue.main.async(execute: {() -> Void in
                    self.tblPopulerHightConstraint.constant = self.tblPopular.contentSize.height
                })
                return cell
            }
        }
        
        if tableView == tblPopular
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCell", for: indexPath) as? LeagueTableViewCell{
                
                cell.hide_skelton()
                
                let objModel = self.arrFilterPopularLeague[indexPath.row]
                cell.lblLeagueName.text = objModel.countryName
                
                if let url = URL(string: objModel.countryFlage){
                    cell.imgPopulerLeague.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgPopulerLeague.image = UIImage(named: "icon_placeholderTeam")
                }
                
                if objModel.isExpandation == true{
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi)
                    cell.arrPopulerLeagegs = objModel.arrLeagueList
                    cell.tblPopulerLeagues.reloadData()
                }else
                {
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi*2)
                    cell.arrPopulerLeagegs.removeAll()
                    cell.tblPopulerLeagues.reloadData()
                }
                
                cell.btnAddFavorite.tag = indexPath.row
                cell.btnAddFavorite.addTarget(self, action: #selector(btnPopulerDropdownPressed(sender:)), for: .touchUpInside)
                
                DispatchQueue.main.async(execute: {() -> Void in
                    self.tblPopulerHightConstraint.constant = self.tblPopular.contentSize.height
                })
                return cell
            }
            else{
                return UITableViewCell()
            }
        }
        else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCell", for: indexPath) as? LeagueTableViewCell{
                
                cell.hide_skelton()
                
                let objModel = self.arrFilterCountryLeague[indexPath.row]
                cell.lblLeagueName.text = objModel.countryName
                
                //    if objModel.countryFlage != ""{
                if let url = URL(string: objModel.countryFlage){
                    cell.imgLeague.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgLeague.image = UIImage(named: "icon_placeholderTeam")
                }
                // }
                if objModel.isExpandation == true{
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi)
                    cell.arrCountryLeagegs = objModel.arrLeagueList
                    cell.tblCountryLeagues.reloadData()
                }else{
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi*2)
                    cell.arrCountryLeagegs.removeAll()
                    cell.tblCountryLeagues.reloadData()
                }
                
                cell.btnAddFavorite.tag = indexPath.row
                cell.btnAddFavorite.addTarget(self, action: #selector(btnCountryDropdownPressed(sender:)), for: .touchUpInside)
                
                DispatchQueue.main.async(execute: {() -> Void in
                    self.tblCountryHightConstraint.constant = self.tblCountryLeague.contentSize.height
                })
                return cell
            }
            else{
                return UITableViewCell()
            }
        }
    }
}

// MARK: - Webservice
extension LeaguesVC
{
    
    func call_Webservice_Post_Favorite(request_type:String,request_id:String, index:Int,tableName:String){
        
        self.view.isUserInteractionEnabled = false
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"league"] as [String:Any]
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                self.view.isUserInteractionEnabled = true
            }else{
                self.view.isUserInteractionEnabled = true
                if message == "Cannot select more than 25 leagues"{
                    
                    objAlert.showAlert(message: message, title: "Alert", controller: self)
                }
            }
            
        }) { (error) in
            print(error)
            self.view.isUserInteractionEnabled = true
            SVProgressHUD.dismiss()
        }
    }
    
    func call_Webservice_Get_LeaugeList() {
        //SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.get_country_league_list, params: nil, success: { (response) in
            self.hide_skelton()
            self.isLoadData = true
            SVProgressHUD.dismiss()
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    //country_list
                    if let country_list = data["country_list"] as? [[String: Any]]{
                        
                        // Popular
                        var arr_poplulerleague = [ModelLeagueListGroupByCountry]()
                        
                        for dict in country_list{
                            let obj = ModelLeagueListGroupByCountry.init(fromDictionary: dict)
                            if  self.arrPopularlocal.contains(where: { $0 == obj.countryName }) {
                                if obj.arrLeagueList.count > 0{
                                    arr_poplulerleague.append(obj)
                                }
                            }
                        }
                        
                        for i in 0..<self.arrPopularlocal.count
                        {
                            let filteredArray = arr_poplulerleague.filter(){ $0.countryName.contains(self.arrPopularlocal[i]) }
                            for objNEW in filteredArray{
                                self.arrPopularLeague.append(objNEW)
                                self.arrFilterPopularLeague.append(objNEW)
                            }
                        }
                        
                        self.tblPopular.reloadData()
                        leagueVc.arrSelectedLeague.removeAll()
                        
                        // Rest of the wold
                        for dict in country_list{
                            let obj = ModelLeagueListGroupByCountry.init(fromDictionary: dict)
                            if obj.arrLeagueList.count > 0{
                                
                                for i in 0..<obj.arrLeagueList.count
                                {
                                    if obj.arrLeagueList[i].isFavorite == "1"
                                    {
                                        leagueVc.arrSelectedLeague.append(obj.arrLeagueList[i].leagueId)
                                    }
                                }
                                self.arrCountryLeague.append(obj)
                                self.arrFilterCountryLeague.append(obj)
                            }
                        }
                        self.tblCountryLeague.reloadData()
                    }
                    if   self.arrCountryLeague.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }
            }else{
                if  self.arrCountryLeague.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
                objAlert.showAlert(message: message, title: "Alert", controller: self)
            }
        }) { (error) in
            self.isLoadData = true
            self.hide_skelton()
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}

extension String {
    func emojiToImage() -> UIImage? {
        let size = CGSize(width: 30, height: 35)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.white.set()
        let rect = CGRect(origin: CGPoint(), size: size)
        UIRectFill(rect)
        (self as NSString).draw(in: rect, withAttributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 30)])
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

extension LeaguesVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        //        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        //        addBannerViewToView(bannerView)
        //        bannerViewDfp.adUnitID = objAppShareData.BannerId
        //        bannerViewDfp.rootViewController = self
        //        bannerViewDfp.load(DFPRequest())
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
    }
}


extension UITextField {
    func modifyClearButtonWithImage(image : UIImage) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(sender:) ), for: .touchUpInside)
        self.rightView = clearButton
        self.rightViewMode = .whileEditing
    }
    
    @objc func clear(sender : AnyObject) {
        self.text = ""
        sendActions(for: .editingChanged)
    }
}
