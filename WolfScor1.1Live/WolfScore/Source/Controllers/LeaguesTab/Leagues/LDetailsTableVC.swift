//
//  TableVC.swift
//  WolfScore
//
//  Created by MAC-RG on 01/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVProgressHUD
import AlamofireImage
import GoogleMobileAds

protocol LDetailsTableVC_Delegate:class {
    func leagues_IsfavouriteStatus(_ isfavrouite:Int)
}


class LDetailsTableVC: UIViewController,IndicatorInfoProvider {
    var arrayAllgroup = [ModelMatchGroup]()
    
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!
    var delegate:LDetailsTableVC_Delegate?
    var isLoadData = false
    var placeholderRow = 10
    
    @IBOutlet weak var viewBannerBottom: UIView!
    
    @IBOutlet weak var tblLeagues: UITableView!
    @IBOutlet weak var lblNodata: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblLeagues.backgroundColor = UIColor.colorConstant.appDarkBlack
        self.tblLeagues.reloadData()
        if arrayAllgroup.isEmpty {
            self.call_Webservice_Get_Stating_league_List(str_league_id: objAppShareData.str_league_id)
        }
        self.bannerAdSetup()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "   TABLE   ")
    }
    
    func call_Webservice_Get_Stating_league_List(str_league_id:String){
        //SVProgressHUD.show()
        
        let paramDict = ["league_id":str_league_id,"type":"standing"] as [String:AnyObject]
        
        print(paramDict)
        
        objWebServiceManager.requestGet(strURL: webUrl.get_league_Table, params: paramDict, success: { (response) in
            self.isLoadData = true
            print("--Statnding----------\(response)")
            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                
                if let isfavrouite = response["is_favorite"] as? Int
                {
                    self.delegate?.leagues_IsfavouriteStatus(isfavrouite)
                }
                
                if let data = response["data"] as? [String:Any] {
                    
                    let object =  ModelStanding.init(fromDictionary: data)
                    self.arrayAllgroup = object.arrGroup
                    self.tblLeagues.reloadData()
                    if self.arrayAllgroup.count == 0
                    {
                        self.lblNodata.isHidden = false
                    }else{
                        self.lblNodata.isHidden = true
                    }
                }
            }
            else{
                if self.arrayAllgroup.count == 0
                {
                    self.lblNodata.isHidden = false
                }else{
                    self.lblNodata.isHidden = true
                }
                //GlobalUtility.showToastMessage(msg: message)
            }
            
        }) { (error) in
            self.isLoadData = true
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    
}

extension LDetailsTableVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isLoadData == false{
            return 1
        }
        return self.arrayAllgroup.count
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isLoadData == false{
            return UIView()
        }
        let headerView = UIView()
        headerView.backgroundColor = UIColor.colorConstant.appDarkBlack
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50)
        
        let StandingViewHeader = StandingHeader.instanceFromNib() as! StandingHeader
        StandingViewHeader.frame = CGRect(x: 0, y: 0, width:tableView.bounds.size.width , height: 50)
        StandingViewHeader.bgColor.backgroundColor = UIColor.colorConstant.appDarkBlack
        StandingViewHeader.lbl_groupName.text = "   " +  self.arrayAllgroup[section].name
        let objLeague = self.arrayAllgroup[section]
        if objLeague.arrStandings.count == 0
        {
          self.lblNodata.isHidden = false
           return UIView()
        }
        headerView.addSubview(StandingViewHeader)
        return headerView
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoadData == false
        {
            return placeholderRow
        }
        let objLeague = self.arrayAllgroup[section]
        return objLeague.arrStandings.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoadData == false
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "statndingViewCell", for: indexPath) as? statndingViewCell{
                cell.show_skelton()
                
                return cell
            }
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "statndingViewCell", for: indexPath) as? statndingViewCell{
            cell.hide_skelton()
            let section = indexPath.section
            let index = indexPath.row
            let ObjStanding = self.arrayAllgroup[indexPath.section]
            let objMatch = ObjStanding.arrStandings[indexPath.row]
            
            if let url = URL(string: objMatch.logoPath){
                cell.imgLogoPath.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgLogoPath.image =  UIImage(named: "icon_placeholderTeam")
            }
            
            if section == 0{
                switch (index){
                case 0:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 1:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 2:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 3:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 4:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appBlueColor
                case 5:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appBlueColor
                default:
                    if ObjStanding.arrStandings.count > 6 && self.arrayAllgroup.count<=1
                    {
                        if index == ObjStanding.arrStandings.count - 1 || index == ObjStanding.arrStandings.count - 2 || index == ObjStanding.arrStandings.count - 3
                        {
                            cell.viewline.backgroundColor = UIColor.red
                        }
                        else{
                            cell.viewline.backgroundColor = UIColor.clear
                        }
                    }
                    else
                    {
                        cell.viewline.backgroundColor = UIColor.clear
                    }
                }
            }else{
                cell.viewline.backgroundColor = UIColor.clear
            }
            
            
            if  section == self.arrayAllgroup.count-1 && section != 0{
                if index == ObjStanding.arrStandings.count - 1 || index == ObjStanding.arrStandings.count - 2 || index == ObjStanding.arrStandings.count - 3
                {
                    cell.viewline.backgroundColor = UIColor.red
                }
                else{
                    cell.viewline.backgroundColor = UIColor.clear
                }
            }
            
            //            if objAppShareData.str_Local_Team_Id == objMatch.teamId{
            //                cell.backgroundColor = UIColor.colorConstant.appDarkBlack
            //            }else if objAppShareData.str_Visiter_Team_Id == objMatch.teamId{
            //                cell.backgroundColor = UIColor.colorConstant.appDarkBlack
            //            }else{
            //                cell.backgroundColor = UIColor.clear
            //            }
            
            cell.lblTeamName.text =  objMatch.teamName
            
            cell.lblPositon.text =  String(index + 1) + "  "
            //cell.lblPositon.text =  objMatch.position + "  "
            
            cell.lblGamePlayed.text =  objMatch.gamesPlayed
            
            cell.lblWon.text =  objMatch.won
            
            cell.lbldraw.text =  objMatch.draw
            
            cell.lbllost.text =  objMatch.lost
            
            cell.lblGoalScore.text =  objMatch.goalsScored
            
            cell.lblGoalAgainst.text =  objMatch.goalsAgainst
            
            cell.lblGoalDifferecnc.text =  objMatch.goalDifference
            
            cell.lblPoints.text =  objMatch.points
            
            return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.arrayAllgroup.count == 0{
            return
        }
        let ObjStanding = self.arrayAllgroup[indexPath.section]
        let objTable = ObjStanding.arrStandings[indexPath.row]
        objAppShareData.str_team_Id = objTable.teamId
        
        let viewController = UIStoryboard(name: "TableDetails",bundle: nil).instantiateViewController(withIdentifier: "TableDetailsVC") as! TableDetailsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
extension LDetailsTableVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
        
    }
}
