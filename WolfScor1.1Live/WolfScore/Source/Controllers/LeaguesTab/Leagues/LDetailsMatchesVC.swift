//
//  MatchesVC.swift
//  WolfScore
//
//  Created by MAC-RG on 01/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVProgressHUD
import AlamofireImage
import GoogleMobileAds

class LDetailsMatchesVC: UIViewController,IndicatorInfoProvider {
    
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var tblLeagues: UITableView!
    @IBOutlet weak var viewBannerBottom: UIView!
    
    var arrModelLeague = [LeaugeModel]()
    var arrFinalModelLeague = [LeaugeModel]()
    var currentPageIndex = 1
    var totalPages = 0
    var isLoadData = false
    var placeholderRow = 10
    var arrSortLeagueByFilter = [String]()
    var pullToRefreshCtrl:UIRefreshControl!
    var Ispulltorefresh = false
    var isSortFilter = false
    
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!
    
    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tblLeagues.tableFooterView = UIView()
            } else {
                self.tblLeagues.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblLeagues.backgroundColor = UIColor.colorConstant.appDarkBlack
        SVProgressHUD.setDefaultMaskType(.clear)
        isSortFilter = false
        setPullToRefresh()
        self.currentPageIndex = 1
        self.apiCallGetFixtures()
        
        self.bannerAdSetup()
        self.tblLeagues.register(UINib(nibName: "MatchesSkeltonCell", bundle: Bundle.main), forCellReuseIdentifier: "MatchesSkeltonCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Ispulltorefresh = false
        for i in 0..<12
        {
            let cell = self.tblLeagues.dequeueReusableCell(withIdentifier: "MatchesSkeltonCell")as! MatchesSkeltonCell
            cell.hide_skelton()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "   MATCHES   ")
    }
    
}

//MARK : custom extension
extension LDetailsMatchesVC {
    
    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tblLeagues.refreshControl = pullToRefreshCtrl
        }else{
            tblLeagues.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        sender.endRefreshing()
        if Ispulltorefresh == false
        {
            currentPageIndex = 1
            self.isSortFilter = false
            shouldLoadMore = false
            self.apiCallGetFixtures()
            
            sender.endRefreshing()
        }
    }
    
    func apiCallGetFixtures()
    {
        let dictPram = [
            "page":currentPageIndex,
            "league_id":objAppShareData.str_league_id,
            "time_zone":objAppShareData.localTimeZoneName
            ] as [String: AnyObject]
        self.call_Webservice_Get_fixtures(dict_param: dictPram)
    }
}



// MARK: - TableView Delegates & Datasource
extension LDetailsMatchesVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isLoadData == false{
            return 1
        }
        return self.arrFinalModelLeague.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isLoadData == false{
            return UIView()
        }
        let headerView = UIView()
        
        //headerView.conten.backgroundColor = UIColor.colorConstant.appSectionHeader
        headerView.backgroundColor = UIColor.colorConstant.appDarkBlack
        // create Image in Header
        
        //            let myCustomView = UIImageView(frame: CGRect(x: 14, y: 12, width:
        //                22, height: 22))
        //            let myImage: UIImage = UIImage(named: "circle_transfers_icon")!
        //            myCustomView.image = myImage
        //            headerView.addSubview(myCustomView)
        
        // create Icon in Header
        let myCustomView2 = UIImageView(frame: CGRect(x: tableView.bounds.size.width - 30, y: 16, width:
            14, height: 14))
        let myImage2: UIImage = UIImage(named: "icon_back_Table")!
        myCustomView2.image = myImage2
        headerView.addSubview(myCustomView2)
        
        // Create Lable in Header
        let headerLabel = UILabel(frame: CGRect(x: 14, y: 2, width:
            tableView.frame.size.width - 80, height: 40))
        
        headerLabel.font = UIFont(name: "Roboto-Bold", size: 14)
        headerLabel.textColor = UIColor.white
        headerLabel.numberOfLines = 2
        let date = dayDate.getDayWithDate(from: self.arrFinalModelLeague[section].date)
        headerLabel.text = date
        headerView.addSubview(headerLabel)
        
        // Create Line in Header
        let headerLine = UILabel(frame: CGRect(x: 0, y: 40, width:
            tableView.bounds.size.width, height: 0.4))
        
        headerLine.backgroundColor = UIColor.darkGray
        headerLine.alpha = 0.7
        headerView.addSubview(headerLine)
        
        // Create Line Top in Header
        let headerTopLine = UILabel(frame: CGRect(x: 0, y: 0, width:
            tableView.bounds.size.width, height: 4))
        
        if section == 0{
            headerTopLine.backgroundColor = UIColor.colorConstant.appDeepBlack
            headerTopLine.alpha = 0.7
        }else{
            headerTopLine.backgroundColor = UIColor.colorConstant.appDeepBlack
            headerTopLine.alpha = 0.7
        }
        
        headerView.addSubview(headerTopLine)
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoadData == false
        {
            return placeholderRow
        }
        let objLeague = self.arrFinalModelLeague[section]
        return objLeague.arrMatches.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadData == false
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesSkeltonCell", for: indexPath) as! MatchesSkeltonCell
            cell.show_skelton()
            return cell
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesTableViewCell", for: indexPath) as? MatchesTableViewCell{
            
            cell.viewStatus.layer.cornerRadius = 4.0
            cell.viewStatus.layer.masksToBounds = true
            let objLeague = self.arrFinalModelLeague[indexPath.section]
            
            let objMatch = objLeague.arrMatches[indexPath.row]
            cell.lblLocalTeamName.text = objMatch.strNameloacalTeam
            cell.lblVisitorTeamName.text = objMatch.strNameVisitorTeam
            if objMatch.strStatus == "LIVE"{
                cell.viewAllStatus.isHidden = true
                cell.lblScore.isHidden = false
                cell.viewStatus.isHidden = false
                cell.lblStartTime.isHidden = true
                cell.viewStatus.backgroundColor = UIColor.colorConstant.appGreenColor
                cell.lblStatus.textColor = UIColor.white
                cell.lblStatus.text = objMatch.strStatus
                //cell.lblStatus.font = UIFont(name: "Roboto-Bold", size: 12.0)
                
                cell.lblScore.text = objMatch.strLocalTeamScore + " - " + objMatch.strVisitorTeamScore
                
            }else{
                if objMatch.strStatus == "FT"{
                    
                    cell.viewAllStatus.isHidden = true
                    cell.lblScore.isHidden = false
                    cell.lblStartTime.isHidden = true
                    cell.viewStatus.isHidden = true
                    //cell.lblScore.text = objMatch.strStatus
                    cell.lblScore.text = objMatch.strLocalTeamScore + " - " + objMatch.strVisitorTeamScore
                    if objMatch.strStatus.count >= 3{
                        let status = String(objMatch.strStatus.prefix(3))
                        cell.lblAllStatus.text = status.capitalizingFirstLetter()
                    }else{
                        cell.lblAllStatus.text =  objMatch.strStatus
                    }
                    
                }else{
                    cell.lblStartTime.isHidden = false
                    cell.lblScore.isHidden = true
                    cell.viewStatus.isHidden = true
                    cell.viewAllStatus.isHidden = true
                    cell.lblStartTime.text = dayDate.Dateformate24_hours(strTime: objMatch.strTime)
                }
            }
            
            if let url = URL(string: objMatch.strLogopathLocalTeam ){
                cell.imgLocal.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgLocal.image = UIImage(named: "icon_placeholderTeam")
            }
            
            if let url = URL(string: objMatch.strLogopathVisitorTeam ){
                cell.imgVisitor.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgVisitor.image = UIImage(named: "icon_placeholderTeam")
                
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == (arrFinalModelLeague.count - 1) && shouldLoadMore == true {
            self.shouldLoadMore = false
            self.apiCallGetFixtures()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.arrFinalModelLeague.count==0{
            return
        }
        let objLeague = self.arrFinalModelLeague[indexPath.section]
        let objMatch = objLeague.arrMatches[indexPath.row]
        let viewController = UIStoryboard(name: "MatchesTab",bundle: nil).instantiateViewController(withIdentifier: "MatchDetailMainVC") as! MatchDetailMainVC
        print(objMatch.strLocalTeamId)
        objAppShareData.str_Local_Team_Id = objMatch.strLocalTeamId
        objAppShareData.str_Visiter_Team_Id = objMatch.strVisitorTeamId
        objAppShareData.str_match_Id = objMatch.fixtureId
        objAppShareData.str_season_Id = objMatch.seasonId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
// Api call
extension LDetailsMatchesVC
{
    
    func call_Webservice_Get_fixtures(dict_param:[String:AnyObject]) {
        self.Ispulltorefresh = true
        
        self.lblNoRecord.isHidden = true
        
        //SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.get_league_matches, params: dict_param, success: { (response) in
            SVProgressHUD.dismiss()
            self.isLoadData = true
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                       if let data1 = data["data"] as? [String:Any] {
                        
                        
                        if let data2 = data1["fixtures"] as? [String:Any] {
                            
                            if let arrData = data2["data"] as? [[String: Any]]{
                                
                                if self.currentPageIndex == 1
                                {
                                    self.arrModelLeague.removeAll()
                                    self.arrFinalModelLeague.removeAll()
                                    self.tblLeagues.reloadData()
                                }
                                
                                for dict in arrData{
                                    let obj = LeaugeModel.init(fromDictionary: dict, isPastMatches: false)
                                    self.arrModelLeague.append(obj)
                                }
                                
                                for obj in self.arrModelLeague
                                {
                                    let filteredArray = self.arrModelLeague.filter(){ $0.date.contains(obj.date) }
                                    for objNEW in filteredArray{
                                        if objNEW.arrMatches.count > 0 {
                                            if !obj.arrMatches.contains(objNEW.arrMatches[0]){
                                                obj.arrMatches.append(objNEW.arrMatches[0])
                                            }
                                        }
                                    }
                                    let arrNewCheck = self.arrFinalModelLeague.filter(){ $0.date.contains(obj.date) }
                                    if arrNewCheck.count == 0{
                                        if obj.arrMatches.count > 0{
                                            self.arrFinalModelLeague.append(obj)
                                        }
                                    }
                                }
                                
                                // for revers latest match top on set
                                //  self.arrFinalModelLeague.reverse()
                                
                                // Filter apply
                                if self.isSortFilter == true
                                {
                                    if self.currentPageIndex  < self.totalPages
                                    {
                                        self.currentPageIndex += 1
                                        self.shouldLoadMore = true
                                    }
                                    
                                    self.tblLeagues.reloadData()
                                    self.tblLeagues.setContentOffset(CGPoint.zero, animated:true)
                                }
                                else{
                                    if self.currentPageIndex  < self.totalPages
                                    {
                                        self.currentPageIndex += 1
                                        self.shouldLoadMore = true
                                    }
                                    
                                    self.tblLeagues.reloadData()
                                    
                                    
                                    let CurrentDate = Date()
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "yyyy-MM-dd"
                                    let result = formatter.string(from: CurrentDate)
                                    
                                    var intSection = -1
                                    var isDateAssending  = false
                                    for i  in 0..<self.arrFinalModelLeague.count
                                    {
                                        if self.arrFinalModelLeague[i].date == result
                                        {
                                            intSection = i
                                            isDateAssending = true
                                        }
                                        else
                                        {
                                            var JsonDate = Date()
                                            JsonDate = formatter.date(from: self.arrFinalModelLeague[i].date)!
                                            
                                            var CurrentDate = Date()
                                            CurrentDate = formatter.date(from: result)!
                                            if JsonDate > CurrentDate
                                            {
                                                if isDateAssending == false
                                                {
                                                    intSection = i
                                                    isDateAssending = true
                                                }
                                            }
                                        }
                                    }
                                    
                                    if intSection !=  -1
                                    {
                                    let indexPath = NSIndexPath(row: 0, section: intSection)
                                    self.tblLeagues.scrollToRow(at: indexPath as IndexPath, at: .top, animated: false)
                                    }
                                }
                            }

                        }
                    }
                    
                    if let metadata = data ["meta"] as? [String:Any]
                    {
                        let data1 = metadata["pagination"] as? [String:Any]
                        let TotalPages = data1?["total_pages"] as? Int  ?? 0
                        self.totalPages = TotalPages
                    }
                   
                }
                if self.arrFinalModelLeague.count == 0{
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }
            else{
                
                if self.arrFinalModelLeague.count == 0{
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }
            self.Ispulltorefresh = false
            
        }) { (error) in
            self.isLoadData = true
            print(error)
            self.Ispulltorefresh = false
            SVProgressHUD.dismiss()
        }
    }
    
}

extension LDetailsMatchesVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}
