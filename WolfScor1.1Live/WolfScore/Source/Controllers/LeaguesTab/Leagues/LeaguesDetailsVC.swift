//
//  LeaguesDetailsVC.swift
//  WolfScore
//
//  Created by MAC-RG on 01/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVProgressHUD


class LeaguesDetailsVC: ButtonBarPagerTabStripViewController,UIGestureRecognizerDelegate ,LDetailsTableVC_Delegate {
    
    var TableVC : LDetailsTableVC!
    var MatchesVC : LDetailsMatchesVC!
    
    @IBOutlet weak var leagueName: UILabel!
    @IBOutlet weak var imageLeagueFav: UIImageView!
    
    var strleagueName = ""
    var isleagueFav = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isleagueFav == false
        {
            imageLeagueFav.image = UIImage(named: "white_star")
        }
        else{
            imageLeagueFav.image = UIImage(named: "active_star")
        }
        
        
        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.colorConstant.appDarkBlack
        settings.style.buttonBarItemBackgroundColor = UIColor.colorConstant.appDarkBlack
        settings.style.buttonBarItemFont = UIFont(name: "roboto-Medium" , size: 14.0)!
        settings.style.selectedBarHeight = 1.0
        settings.style.selectedBarBackgroundColor = UIColor.colorConstant.appBlueColor
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.colorConstant.appLightBlueColor
            newCell?.label.textColor = .white
            oldCell?.label.font = UIFont(name: "roboto-Medium" , size: 14.0)!
            newCell?.label.font = UIFont(name: "roboto-Bold" , size: 14.0)!
        }
        
        self.view.layoutIfNeeded()
        //selectedbarcolor()
        self.leagueName.text = strleagueName
    }
    
    
    func leagues_IsfavouriteStatus(_ isfavrouite: Int) {
        
        if isfavrouite == 1
        {
            isleagueFav = true
            imageLeagueFav.image = UIImage(named: "active_star")
        }
        else
        {
            isleagueFav = false
            imageLeagueFav.image = UIImage(named: "white_star")
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        if APP_DELEGATE.isInterstialPresent == true
        {
            return
        }
    }
    
    //  MARK:- Delegate of PagerTabStripViewController
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        self.TableVC = UIStoryboard(name: "LeaguesTab", bundle: nil).instantiateViewController(withIdentifier: "LDetailsTableVC") as? LDetailsTableVC
        self.TableVC.delegate = self
        
        self.MatchesVC = UIStoryboard(name: "LeaguesTab", bundle: nil).instantiateViewController(withIdentifier: "LDetailsMatchesVC") as? LDetailsMatchesVC
        
        return [self.TableVC,self.MatchesVC]
    }
    
    // MARK:- IBAction
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btn_favrouite(_ sender:UIButton)
    {
        
        if leagueVc.arrSelectedLeague.contains(objAppShareData.str_league_id)
        {
            let Indexobj = leagueVc.arrSelectedLeague.index(of: objAppShareData.str_league_id)
            leagueVc.arrSelectedLeague.remove(at: Indexobj!)
        }
        else
        {
            if leagueVc.arrSelectedLeague.count < 25
            {
                leagueVc.arrSelectedLeague.append( objAppShareData.str_league_id)
            }
        }
        
        if isleagueFav == false{
            isleagueFav = true
            imageLeagueFav.image = UIImage(named: "active_star")
            call_Webservice_Post_PopulerFavorite(request_type: "1", request_id:objAppShareData.str_league_id)
        }
        else
        {
            isleagueFav = false
            imageLeagueFav.image = UIImage(named: "white_star")
            call_Webservice_Post_PopulerFavorite(request_type: "0", request_id:objAppShareData.str_league_id)
        }
        
    }
    
    func call_Webservice_Post_PopulerFavorite(request_type:String,request_id:String){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"league"] as [String:Any]
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"
            {
                
            }
            else{
                let message = response["message"] as? String ?? ""
                if message == "Cannot select more than 25 leagues"{
                    self.isleagueFav = false
                    self.imageLeagueFav.image = UIImage(named: "white_star")
                    objAlert.showAlert(message: message, title: "Alert", controller: self)
                }
                
            }
            SVProgressHUD.dismiss()
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    // MARK:- Custom Methods
    
    func selectedbarcolor()
    {
        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.colorConstant.appDarkBlack
        settings.style.buttonBarItemBackgroundColor = UIColor.colorConstant.appDarkBlack
        settings.style.buttonBarItemFont = UIFont(name: "roboto-Medium" , size: 14.0)!
        settings.style.selectedBarHeight = 3.0
        settings.style.selectedBarBackgroundColor = UIColor.colorConstant.appBlueColor
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.colorConstant.appLightBlueColor
            newCell?.label.textColor = UIColor.colorConstant.appLightBlueColor
            oldCell?.label.font = UIFont(name: "roboto-Medium" , size: 14.0)!
            newCell?.label.font = UIFont(name: "roboto-Medium" , size: 14.0)!
        }
    }
    
}
