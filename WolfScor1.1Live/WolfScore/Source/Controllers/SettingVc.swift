//
//  SettingVc.swift
//  WolfScore
//
//  Created by Mindiii on 5/13/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD

class SettingVc: UIViewController {
    
    
    var TermsCondition:String = ""
    var PrivacyPolicy:String = ""
    var ContactUs:String = ""
    var Mail:String = ""
    
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrheading = ["GENERAL"]
    //"Notification",
    //  var arrInfo =    [[ "Filter and sort tournaments", "Select the interface language", "Contact us","Terms & Conditions","Help","Review Our App","Logout"]]
    
    var arrInfo =    [["Contact us","Terms & Conditions","Privacy Policy","Review Our App","Logout"]]
    
    var arrImage =    [[UIImage(named: "icon_contactus"),UIImage(named: "icon_condition"),UIImage(named: "icon_help"),UIImage(named: "icon_review"),UIImage(named: "icon_logout")]]
    
    
    //UIImage(named: "icon_notification"),
    // var arrImage =    [[ UIImage(named: "icon_filter"), UIImage(named: "icon_language"), UIImage(named: "icon_contactus"),UIImage(named: "icon_condition"),UIImage(named: "icon_help"),UIImage(named: "icon_review"),UIImage(named: "icon_logout")]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.call_Webservice_GetContent()
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.colorConstant.appDarkBlack
        
        let userId = UserDefaults.standard.string(forKey: UserDefaults.Keys.kUserId)
        if userId == nil{
            
            //"Notification",
            //             arrInfo =    [[ "Favorite", "Filter and sort tournaments", "Select the interface language", "Contact us","Terms & Conditions","Help","Review Our App","Login"]]
            //            //UIImage(named: "icon_notification"),
            //             arrImage =    [[UIImage(named: "icon_review"), UIImage(named: "icon_filter"), UIImage(named: "icon_language"), UIImage(named: "icon_contactus"),UIImage(named: "icon_condition"),UIImage(named: "icon_help"),UIImage(named: "icon_review"),UIImage(named: "icon_logout")]]
            
            arrInfo =    [[ "Favorite", "Contact us","Terms & Conditions","Privacy Policy","Review Our App","Login"]]
            //UIImage(named: "icon_notification"),
            arrImage =    [[UIImage(named: "icon_review"), UIImage(named: "icon_contactus"),UIImage(named: "icon_condition"),UIImage(named: "icon_help"),UIImage(named: "icon_review"),UIImage(named: "icon_logout")]]
            
        }
        else
        {
            //            //"Notification",
            //             arrInfo =    [[ "Filter and sort tournaments", "Select the interface language", "Contact us","Terms & Conditions","Help","Review Our App","Logout"]]
            //
            //            //[UIImage(named: "icon_notification"),
            //             arrImage =    [[ UIImage(named: "icon_filter"), UIImage(named: "icon_language"), UIImage(named: "icon_contactus"),UIImage(named: "icon_condition"),UIImage(named: "icon_help"),UIImage(named: "icon_review"),UIImage(named: "icon_logout")]]
            
            //"Notification",
            arrInfo =    [[ "Contact us","Terms & Conditions","Privacy Policy","Review Our App","Logout"]]
            
            //[UIImage(named: "icon_notification"),
            arrImage =    [[ UIImage(named: "icon_contactus"),UIImage(named: "icon_condition"),UIImage(named: "icon_help"),UIImage(named: "icon_review"),UIImage(named: "icon_logout")]]
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.slideMenuController()?.leftPanGesture?.isEnabled = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.slideMenuController()?.leftPanGesture?.isEnabled = true
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let storyboard = UIStoryboard(name: "UserTabbar", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "UserTabbarVC") as! UserTabbarVC
        
        self.slideMenuController()?.changeMainViewController(mainViewController, close: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
}

extension SettingVc : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return self.arrheading.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return  55
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.arrheading[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.arrInfo[section].count
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.backgroundView?.backgroundColor = UIColor.colorConstant.appDarkBlack
        header.textLabel?.textColor = UIColor.colorConstant.appLightBlueColor
        header.textLabel?.font = UIFont(name: "Roboto-Bold", size: 18)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SettingViewCell", for: indexPath) as? SettingViewCell{
            cell.lblTitle.text = self.arrInfo[indexPath.section][indexPath.row]
            cell.img.image = self.arrImage[indexPath.section][indexPath.row]
            
            if cell.lblTitle.text == "Notification" || cell.lblTitle.text == "Contact us" || cell.lblTitle.text == "Terms & Conditions" || cell.lblTitle.text == "Help"
            {
                cell.imgRightArrow.isHidden = false
            }
            else{
                cell.imgRightArrow.isHidden = true
                
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.arrInfo[indexPath.section][indexPath.row] == "Favorite" {
            
            let viewController = UIStoryboard(name: "UserTabbar",bundle: nil).instantiateViewController(withIdentifier: "MyFavoritesMainVC") as! MyFavoritesMainVC
            self.navigationController?.pushViewController(viewController, animated: true)
        }
            
        else if self.arrInfo[indexPath.section][indexPath.row] == "Contact us" {
            
            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            viewController.str_MailId = self.Mail
            viewController.str_ContactDescription = self.ContactUs
        self.navigationController?.pushViewController(viewController, animated: true)

        }
            
        else if self.arrInfo[indexPath.section][indexPath.row] == "Terms & Conditions" {
            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
            viewController.strPageType = "Terms & Condition"
            viewController.strUrl = self.TermsCondition
 self.navigationController?.pushViewController(viewController, animated: true)
            
        }
        else if self.arrInfo[indexPath.section][indexPath.row] == "Privacy Policy" {
            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
            viewController.strPageType = "Privacy Policy"
            viewController.strUrl = self.PrivacyPolicy
 self.navigationController?.pushViewController(viewController, animated: true)
            
        }
        else if self.arrInfo[indexPath.section][indexPath.row] == "Logout"
        {
            
            let alert = UIAlertController(title: "WolfScore", message: "Are you sure you want to log out?", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Logout", style: UIAlertAction.Style.destructive, handler: { action in
                
                self.LogoutWebservicesCall()
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        else if  self.arrInfo[indexPath.section][indexPath.row] == "Login"
        {
            UserDefaults.standard.setValue(nil, forKey: UserDefaults.Keys.kGuestLogin)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let aVariable = appDelegate.Go_ToWelcomeScreen()
        }
        
    }
    
    
    
    
    
    func resetDefaults() {
        
        let defaults = UserDefaults.standard
        
        let UserEmail = defaults.string(forKey: UserDefaults.Keys.kEmail)
        let UserPassword = defaults.string(forKey: UserDefaults.Keys.kRMPassword)
        let rememberMe = defaults.string(forKey: UserDefaults.Keys.kIsRemember)
        let arrCountry = UserDefaults.standard.array(forKey: UserDefaults.Keys.kCountryArr) as? [[String:Any]] ?? []

        
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        
        defaults.setValue(rememberMe , forKey: UserDefaults.Keys.kIsRemember)
        defaults.setValue(UserEmail , forKey: UserDefaults.Keys.kEmail)
        defaults.setValue(UserPassword , forKey: UserDefaults.Keys.kRMPassword)
        defaults.setValue(arrCountry , forKey: UserDefaults.Keys.kCountryArr)

    }
}
//MARK :- call_Webservice
extension SettingVc{
    func LogoutWebservicesCall()
    {
        
        SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.logout, params: nil, success: { (response) in
            SVProgressHUD.dismiss()
            _ = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                print("scucess")
                self.slideMenuController()?.closeLeft()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let aVariable = appDelegate.Go_ToWelcomeScreen()
                self.resetDefaults()
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    func call_Webservice_GetContent() {
        SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.GetContent, params: nil, success: { (response) in
            print(response)
            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    self.TermsCondition = data["term_condition"] as? String ?? ""
                    self.PrivacyPolicy = data["privacy_policy"] as? String ?? ""
                    self.ContactUs = data["contact_us"] as? String ?? ""
                    self.Mail = data["mail"] as? String ?? ""
                }
                
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}

