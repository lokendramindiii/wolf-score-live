//
//  TeamDetailHeaderVC.swift
//  WolfScore
//
//  Created by Macbook-Lokendra on 26/07/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import SJSegmentedScrollView
import SkeletonView

class TeamDetailHeaderVC: UIViewController {

    var dict:[String:Any] = [:]
    
    
    var isTeamFavorite = false

    @IBOutlet var headerView: UIView!
    @IBOutlet var viewLeague: UIView!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblLeagueName: UILabel!
    @IBOutlet weak var imgContryFlag: UIImageView!
    @IBOutlet weak var imgLeagueFlag: UIImageView!
    @IBOutlet weak var imgTeamLogo: UIImageView!
    @IBOutlet weak var imgTeamFav: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let data = self.dict["data"] as? [String:Any] {
            
            
            let object =  TableDetailsModelClass.init(dict: data)
            objAppShareData.str_team_Name = object.team_name
            objAppShareData.str_team_Logo = object.team_logo_path
            self.lblTeamName.text = object.team_name
            self.lblCountryName.text = object.country_name
            
            
            if let url = URL(string: object.country_logo_path){
                self.imgContryFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "circle_goal_icon"))
            }
            else
            {
                self.imgContryFlag.image = UIImage(named: "circle_goal_icon")
            }
            
            if let url = URL(string: object.team_logo_path){
                self.imgTeamLogo.af_setImage(withURL: url, placeholderImage: UIImage(named: "circle_goal_icon"))
            }
        }
        
    }
    
    
    func UpdatefromOverviewVC_Favrouite_IsfavouriteStatus(_ isfavrouite: Int) {
        if isfavrouite == 1
        {
            isTeamFavorite = true
            imgTeamFav.image = UIImage(named: "active_star")
        }
        else
        {
            isTeamFavorite = false
            imgTeamFav.image = UIImage(named: "white_star")
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    // MARK:- IBAction
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btn_favrouite(_ sender:UIButton)
    {
        
        SVProgressHUD.show()
        
        if isTeamFavorite == false
        {
            
            isTeamFavorite = true
            imgTeamFav.image = UIImage(named: "active_star")
            call_Webservice_Post_PopulerFavorite(request_type: "1", request_id:objAppShareData.str_team_Id)
        }
        else
        {
            isTeamFavorite = false
            imgTeamFav.image = UIImage(named: "white_star")
            call_Webservice_Post_PopulerFavorite(request_type: "0", request_id:objAppShareData.str_team_Id)
        }
    }
    
    func call_Webservice_Post_PopulerFavorite(request_type:String,request_id:String){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"team"] as [String:Any]
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"{
            }
            else
            {
                let message = response["message"] as? String ?? ""
                self.isTeamFavorite = false
                self.imgTeamFav.image = UIImage(named: "white_star")
                if message == "Cannot select more than 100 teams"{
                    objAlert.showAlert(message: message, title: "Alert", controller: self)
                }
            }
            SVProgressHUD.dismiss()
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}
