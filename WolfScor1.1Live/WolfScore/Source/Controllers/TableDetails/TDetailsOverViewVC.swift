//
//  TDetailsOverViewVC.swift
//  WolfScore
//
//  Created by mac on 12/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import CommonCrypto
import SJSegmentedScrollView
import GoogleMobileAds
import SkeletonView

protocol TDetailsOverViewVC_Delegate:class {
    func Team_IsfavouriteStatus(_ isfavrouite:Int)
    func moveViewController(_ MoveIndex:Int)
}

class TDetailsOverViewVC: UIViewController {
    
    @IBOutlet weak var sidelineCollectionView: UICollectionView!
    @IBOutlet weak var tblTeam: UITableView!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblFounded: UILabel!
    @IBOutlet weak var viewNextMatch: UIView!
    @IBOutlet weak var lblLeagueName: UILabel!
    @IBOutlet weak var lblCoache: UILabel!
    @IBOutlet weak var lblStadium: UILabel!
    @IBOutlet weak var viewSidelineForHide: UIView!
    @IBOutlet weak var lblLocalTeamName: UILabel!
    @IBOutlet weak var lblVisitorTeamName: UILabel!
    @IBOutlet weak var imgLocal: UIImageView!
    @IBOutlet weak var imgVisitor: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblLeagueNameWithMatch: UILabel!
    @IBOutlet weak var viewLeagueNameForHide: UIView!
    @IBOutlet weak var viewBannerBottom: UIView!
    @IBOutlet weak var viewBgTable: UIView!
    @IBOutlet weak var scrollview: UIScrollView!

    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var lbl7: UILabel!
    @IBOutlet weak var lbl8: UILabel!
    
    var arrayAllgroup = [ModelMatchGroup]()
    var arraySideline = [TransferModelClass]()
    var arrModelLeague = [TableModel]()
    var delegate:TDetailsOverViewVC_Delegate?

    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!
     var isLoadData = false
     var placeholderRow = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.show_skelton()
        self.viewLeagueNameForHide.isHidden = true
        self.call_Webservice_Get_Team_Details(str_team_id: objAppShareData.str_team_Id)
        self.bannerAdSetup()
    }
    
  
    
    func show_skelton(){
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        [lbl1,lbl2,lbl3, lbl4 ,lbl5 ,lbl6,lbl7,lbl8].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
    }
    
    func hide_skelton(){
        [lbl1,lbl2,lbl3, lbl4 ,lbl5 ,lbl6,lbl7,lbl8].forEach { $0?.hideSkeleton()
        }
    }
}

extension TDetailsOverViewVC{
    
    func call_Webservice_Get_Team_Details(str_team_id:String){
        //SVProgressHUD.show()
        
        let paramDict = ["team_id":str_team_id,"type":"overview", "time_zone":objAppShareData.localTimeZoneName] as [String:AnyObject]
        
        objWebServiceManager.requestGet(strURL: webUrl.get_team_detail, params: paramDict, success: { (response) in
            self.isLoadData = true
            self.hide_skelton()
            SVProgressHUD.dismiss()
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                
                if let isfavrouite = response["is_favorite"] as? Int
                {
                    self.delegate?.Team_IsfavouriteStatus(isfavrouite)
                }
                
                if let data = response["data"] as? [String:Any] {
                    let object =  TableDetailsModelClass.init(dict: data)
                    
                    if object.team_name == ""{
                        self.lblTeamName.text = "NA"
                    }else{
                        self.lblTeamName.text = object.team_name
                    }
                    
                    if object.team_founded == ""{
                        self.lblFounded.text = "NA"
                    }else{
                        self.lblFounded.text = object.team_founded
                    }
                    
                    if object.choache_name == ""{
                        self.lblCoache.text = "NA"
                    }else{
                        self.lblCoache.text = object.choache_name
                    }
                    
                    if object.stadium_name == ""{
                        self.lblStadium.text = "NA"
                    }else{
                        self.lblStadium.text = object.stadium_name
                    }
                    
                    if let dataNew = data["data"] as? [String:Any] {
                        if let dataN = dataNew["standings"] as? [String:Any] {
                            let obj =  ModelStanding.init(fromDictionary: dataN)
                            
                            for object in obj.arrGroup
                            {
                                if object.arrStandings.count > 0
                                {
                                    self.arrayAllgroup = obj.arrGroup
                                }
                            }
                            if self.arrayAllgroup.count > 0
                            {
                                self.tblTeam.reloadData()
                                self.viewBgTable.isHidden = false
                            }
                            else
                            {
                                self.viewBgTable.isHidden = true
                            }
                        }
                    }
                    
                    if let dataold = data["data"] as? [String:Any] {
                        if let transfers = dataold["league"] as? [String:Any] {
                            if let data = transfers["data"] as? [String:Any]{
                                let strLeagueName = data["name"] as? String ?? ""
                                self.lblLeagueName.text = strLeagueName
                                self.lblLeagueNameWithMatch.text = strLeagueName
                                self.viewLeagueNameForHide.isHidden = false
                            }
                        }
                    }
                    if let dataold = data["data"] as? [String:Any] {
                        if let upcoming = dataold["upcoming"] as? [String: Any]{
                            let obj = TableModel.init(fromDictionary: upcoming, strType: "UPCOMING MATCHES")
                            self.arrModelLeague.append(obj)
                            if self.arrModelLeague.count>0{
                                self.setMatchData()
                            }
                        }
                    }
                    
                    if let dataold = data["data"] as? [String:Any] {
                        if let transfers = dataold["sidelined"] as? [String:Any] {
                            if let data = transfers["data"] as? [[String:Any]]{
                                for  obj in data{
                                    let object =  TransferModelClass.init(dict: obj)
                                    self.arraySideline.append(object)
                                    self.sidelineCollectionView.reloadData()
                                }
                            }
                        }
                        if self.arraySideline.count>0{
                            self.viewSidelineForHide.isHidden = false
                        }else{
                            self.viewSidelineForHide.isHidden = true
                        }
                    }
                }
            }
            else{
                
            }
            
        }) { (error) in
            print(error)
            self.hide_skelton()
            self.isLoadData = true
            SVProgressHUD.dismiss()
        }
    }
    
    @IBAction func btnMatchCellAction(_ sender : UIButton){
        
        let objLeague = self.arrModelLeague[0]
        if objLeague.arrMatches.count>0{
            let objMatch = objLeague.arrMatches[0]
            
            let viewController = UIStoryboard(name: "MatchesTab",bundle: nil).instantiateViewController(withIdentifier: "MatchDetailMainVC") as! MatchDetailMainVC
            objAppShareData.str_Local_Team_Id = objMatch.strLocalTeamId
            objAppShareData.str_Visiter_Team_Id = objMatch.strVisitorTeamId
            objAppShareData.str_match_Id = objMatch.fixtureId
            objAppShareData.str_season_Id = objMatch.seasonId
        self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func setMatchData(){
        let objLeague = self.arrModelLeague[0]
        if objLeague.arrMatches.count>0{
            let objMatch = objLeague.arrMatches[0]
            self.viewNextMatch.isHidden = false
            self.lblLocalTeamName.text = objMatch.strNameloacalTeam
            self.lblVisitorTeamName.text = objMatch.strNameVisitorTeam
            
            if let url = URL(string: objMatch.strLogopathLocalTeam){
                
                self.imgLocal.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                self.imgLocal.image = UIImage(named: "icon_placeholderTeam")
                
            }
            
            if let url = URL(string: objMatch.strLogopathVisitorTeam){
                self.imgVisitor.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                self.imgVisitor.image = UIImage(named: "icon_placeholderTeam")
                
            }
            self.lblStartTime.text = dayDate.Dateformate24_hours(strTime: objMatch.strTime)
            
            let dateTime = dayDate.getEEEdMMMMYYYYFormatter(from: objMatch.strDateTime)
            let date = Date()
            let calendar = Calendar.current
            let year = String(calendar.component(.year, from: date))
            if dateTime.contains(year){
                var arr = dateTime.split(separator: " ")
                if arr.count > 3{
                    arr.remove(at: 3)
                }
                let strDate = arr.joined(separator: " ")
                self.lblDate.text = strDate
            }else{
                self.lblDate.text = dateTime
            }
        }else{
            self.viewNextMatch.isHidden = true
        }
    }
}

//MARK:- Collection view Delegate Methods
extension TDetailsOverViewVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isLoadData == false{
            return 2
        }
        return self.arraySideline.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if isLoadData == false
        {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OverViewCollectionCell", for:
                indexPath) as? OverViewCollectionCell{
                cell.show_skelton()
                return cell
            }
        }
        
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "OverViewCollectionCell", for:
            indexPath) as? OverViewCollectionCell)!
        cell.hide_skelton()
        let objPlayer = self.arraySideline[indexPath.row]
        cell.lblDescription.text = objPlayer.sideline_description
        cell.lblName.text = objPlayer.player_name
        cell.lblHeight.text = objPlayer.player_height
        cell.lblWeight.text = objPlayer.player_weight
        cell.lblCountry.text = objPlayer.player_nationality
        if let url = URL(string: objPlayer.player_logo_path){
            cell.imgPlayer.af_setImage(withURL: url, placeholderImage: UIImage(named: "circle_goal_icon"))
            cell.imgPlayer.layer.cornerRadius = cell.imgPlayer.frame.size.width/2
            cell.imgPlayer.clipsToBounds = true
        }
        return cell
    }
    
}

extension TDetailsOverViewVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isLoadData == false{
            return 1
        }
        if self.arrayAllgroup.count>0{
            return 1
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isLoadData == false{
            return UIView()
        }
        let headerView = UIView()
        headerView.backgroundColor = UIColor.colorConstant.appDarkBlack
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 40)
        
        let StandingViewHeader = StandingHeader.instanceFromNib() as! StandingHeader
        StandingViewHeader.frame = CGRect(x: 0, y: 0, width:tableView.bounds.size.width , height: 40)
        StandingViewHeader.bgColor.backgroundColor = UIColor.colorConstant.appDarkBlack
        StandingViewHeader.lbl_groupName.text = "   " +  self.arrayAllgroup[section].name
        headerView.addSubview(StandingViewHeader)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoadData == false{
            return placeholderRow
        }
        if self.arrayAllgroup.count>0{
            let objLeague = self.arrayAllgroup[section]
            if objLeague.arrStandings.count < 3{
                return objLeague.arrStandings.count
            }else{
                return 3
            }
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadData == false
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "statndingViewCell", for: indexPath) as? statndingViewCell{
                cell.show_skelton()
                return cell
            }
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "statndingViewCell", for: indexPath) as? statndingViewCell{
            cell.hide_skelton()
            let section = indexPath.section
            let index = indexPath.row
            
            let ObjStanding = self.arrayAllgroup[indexPath.section]
            let objMatch = ObjStanding.arrStandings[indexPath.row]
            
            if let url = URL(string: objMatch.logoPath){
                cell.imgLogoPath.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgLogoPath.image = UIImage(named: "icon_placeholderTeam")
                
            }
            
            if section == 0{
                switch (index){
                case 0:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 1:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 2:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 3:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 4:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appBlueColor
                case 5:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appBlueColor
                default:
                    
                    if ObjStanding.arrStandings.count > 6 && self.arrayAllgroup.count<=1
                    {
                        if index == ObjStanding.arrStandings.count - 1 || index == ObjStanding.arrStandings.count - 2 || index == ObjStanding.arrStandings.count - 3
                        {
                            cell.viewline.backgroundColor = UIColor.red
                        }
                        else{
                            cell.viewline.backgroundColor = UIColor.clear
                        }
                    }
                    else
                    {
                        cell.viewline.backgroundColor = UIColor.clear
                    }
                }
            }else{
                cell.viewline.backgroundColor = UIColor.clear
            }
            
            cell.viewline.backgroundColor = UIColor.clear
            //cell.contentView.backgroundColor = UIColor.colorConstant.appLightBlack
            
            cell.lblTeamName.text =  objMatch.teamName
            
            cell.lblPositon.text =  String(index + 1) + "  "
            //cell.lblPositon.text =  objMatch.position + "  "
            
            cell.lblGamePlayed.text =  objMatch.gamesPlayed
            
            cell.lblWon.text =  objMatch.won
            
            cell.lbldraw.text =  objMatch.draw
            
            cell.lbllost.text =  objMatch.lost
            
            cell.lblGoalScore.text =  objMatch.goalsScored
            
            cell.lblGoalAgainst.text =  objMatch.goalsAgainst
            
            cell.lblGoalDifferecnc.text =  objMatch.goalDifference
            
            cell.lblPoints.text =  objMatch.points
            
            return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        delegate?.moveViewController(TableDetailsStuctVc.IndexMove)
    }
}

extension TDetailsOverViewVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}


extension TDetailsOverViewVC: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return self.scrollview
    }
}
