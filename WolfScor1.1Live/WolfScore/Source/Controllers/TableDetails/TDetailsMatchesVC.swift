//
//  TDetailsMatchesVC.swift
//  WolfScore
//
//  Created by mac on 11/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import CommonCrypto
import SJSegmentedScrollView
import GoogleMobileAds

class TDetailsMatchesVC: UIViewController {
    
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var tblMetches: UITableView!
    @IBOutlet weak var viewBannerBottom: UIView!
    
    var arrModelLeague = [TableModel]()
    var arrFinalModelLeague = [TableModel]()
    var currentPageIndex = 1
    var totalPages = 0
    
    var arrSortLeagueByFilter = [String]()
    var pullToRefreshCtrl:UIRefreshControl!
    var Ispulltorefresh = false
    var isSortFilter = false
    
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!
    var isLoadData = false
    var placeholderRow = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblMetches.backgroundColor = UIColor.colorConstant.appDarkBlack
      //  SVProgressHUD.setDefaultMaskType(.clear)
        isSortFilter = false
        setPullToRefresh()
        self.currentPageIndex = 1
        self.bannerAdSetup()
        self.tblMetches.register(UINib(nibName: "MatchesSkeltonCell", bundle: Bundle.main), forCellReuseIdentifier: "MatchesSkeltonCell")
    }
    
    
    func callApiSelfTTDetailsMatchesVC()
    {
        self.apiCallGetFixtures()
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        Ispulltorefresh = false
        for i in 0..<12
        {
            let cell = self.tblMetches.dequeueReusableCell(withIdentifier: "MatchesSkeltonCell")as! MatchesSkeltonCell
            cell.hide_skelton()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tblMetches.tableFooterView = UIView()
            } else {
                self.tblMetches.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }
}

//MARK : custom extension
extension TDetailsMatchesVC {
    
    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.tblMetches.refreshControl = pullToRefreshCtrl
        }else{
            self.tblMetches.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        sender.endRefreshing()
        if Ispulltorefresh == false
        {
            currentPageIndex = 1
            self.isSortFilter = false
            shouldLoadMore = false
            self.apiCallGetFixtures()
            
            sender.endRefreshing()
        }
    }
    
    func apiCallGetFixtures()
    {
        let dictPram = [
            "type":"fixture",
            "team_id":objAppShareData.str_team_Id,"time_zone":objAppShareData.localTimeZoneName
            ] as [String: AnyObject]
        
        self.call_Webservice_Get_fixtures(dict_param: dictPram)
    }
}



// MARK: - TableView Delegates & Datasource
extension TDetailsMatchesVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isLoadData == false{
            return 1
        }
        return self.arrFinalModelLeague.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isLoadData == false{
            return UIView()
        }
        let headerView = UIView()
        
        headerView.backgroundColor = UIColor.colorConstant.appDarkBlack
        
        let headerLabel = UILabel(frame: CGRect(x: 5, y: 2, width:
            tableView.frame.size.width - 80, height: 40))
        
        headerLabel.font = UIFont(name: "Roboto-Bold", size: 15)
        headerLabel.textColor = #colorLiteral(red: 0.5137254902, green: 0.5882352941, blue: 0.6470588235, alpha: 1)
        headerLabel.numberOfLines = 2
        let date = self.arrFinalModelLeague[section].matchType
        headerLabel.text = date
        headerView.addSubview(headerLabel)
        
        // Create Line in Header
        let headerLine = UILabel(frame: CGRect(x: 0, y: 40, width:
            tableView.bounds.size.width, height: 0.4))
        
        headerLine.backgroundColor = UIColor.darkGray
        headerLine.alpha = 0.7
        headerView.addSubview(headerLine)
        
        // Create Line Top in Header
        let headerTopLine = UILabel(frame: CGRect(x: 0, y: 0, width:
            tableView.bounds.size.width, height: 4))
        
        if section == 0{
            headerTopLine.backgroundColor = UIColor.colorConstant.appDeepBlack
            headerTopLine.alpha = 0.7
        }else{
            headerTopLine.backgroundColor = UIColor.colorConstant.appDeepBlack
            headerTopLine.alpha = 0.7
        }
        
        headerView.addSubview(headerTopLine)
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoadData == false
        {
            return placeholderRow
        }
        let objLeague = self.arrFinalModelLeague[section]
        
        return objLeague.arrMatches.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadData == false
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesSkeltonCell", for: indexPath) as! MatchesSkeltonCell
            cell.show_skelton()
            return cell
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesTableViewCell", for: indexPath) as? MatchesTableViewCell{
            let objLeague = self.arrFinalModelLeague[indexPath.section]
            
            let objMatch = objLeague.arrMatches[indexPath.row]
            cell.lblLocalTeamName.text = objMatch.strNameloacalTeam
            cell.lblVisitorTeamName.text = objMatch.strNameVisitorTeam
            
            if let url = URL(string: objMatch.strLogopathLocalTeam){
                cell.imgLocal.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgLocal.image = UIImage(named: "icon_placeholderTeam")
            }
            
            if let url = URL(string: objMatch.strLogopathVisitorTeam){
                cell.imgVisitor.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgVisitor.image = UIImage(named: "icon_placeholderTeam")
            }
            
            
            let dateTime = dayDate.getEEEdMMMMYYYYFormatter(from: objMatch.strDateTime)
            
            cell.lblDate.text = dateTime
            
            if objMatch.strStatus == "LIVE"{
                cell.lblScore.isHidden = false
                cell.lblStartTime.isHidden = true
                cell.lblStatus.textColor = UIColor.white
                cell.lblStatus.text = objMatch.strStatus
                cell.lblStatus.font = UIFont(name: "Roboto-Bold", size: 12.0)
                
                cell.lblScore.text = String(objMatch.strLocalTeamScore) + " - " + String(objMatch.strVisitorTeamScore)
                cell.lblScore.backgroundColor = UIColor.colorConstant.appGreenColor
            }else{
                if objMatch.strStatus == "FT"{
                    cell.lblScore.isHidden = false
                    cell.lblStartTime.isHidden = true
                    cell.lblScore.text = String(objMatch.strLocalTeamScore) + " - " + String(objMatch.strVisitorTeamScore)
                    if objMatch.strLocalTeamScore > objMatch.strVisitorTeamScore && objAppShareData.str_team_Name == objMatch.strNameloacalTeam {
                        cell.lblScore.backgroundColor = UIColor.colorConstant.appGreenColor
                    }else if objMatch.strLocalTeamScore < objMatch.strVisitorTeamScore && objAppShareData.str_team_Name == objMatch.strNameloacalTeam{
                        cell.lblScore.backgroundColor = UIColor.colorConstant.appRedColor
                    }else if objMatch.strLocalTeamScore > objMatch.strVisitorTeamScore && objAppShareData.str_team_Name == objMatch.strNameVisitorTeam {
                        cell.lblScore.backgroundColor = UIColor.colorConstant.appRedColor
                    }else if objMatch.strLocalTeamScore < objMatch.strVisitorTeamScore && objAppShareData.str_team_Name == objMatch.strNameVisitorTeam {
                        cell.lblScore.backgroundColor = UIColor.colorConstant.appGreenColor
                    }else {
                        cell.lblScore.backgroundColor = UIColor.colorConstant.appLightGrayColor
                    }
                }else{
                    cell.lblStartTime.isHidden = false
                    cell.lblScore.isHidden = true
                    cell.lblStartTime.text = dayDate.Dateformate24_hours(strTime: objMatch.strTime)
                    
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.arrFinalModelLeague.count == 0{
            return
        }
        let objLeague = self.arrFinalModelLeague[indexPath.section]
        let objMatch = objLeague.arrMatches[indexPath.row]
        let viewController = UIStoryboard(name: "MatchesTab",bundle: nil).instantiateViewController(withIdentifier: "MatchDetailMainVC") as! MatchDetailMainVC
        print(objMatch.strLocalTeamId)
        objAppShareData.str_Local_Team_Id = objMatch.strLocalTeamId
        objAppShareData.str_Visiter_Team_Id = objMatch.strVisitorTeamId
        objAppShareData.str_match_Id = objMatch.fixtureId
        objAppShareData.str_season_Id = objMatch.seasonId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// Api call
extension TDetailsMatchesVC{
    
    func call_Webservice_Get_fixtures(dict_param:[String:AnyObject]) {
        self.Ispulltorefresh = true
        
        //SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.get_team_detail, params: dict_param, success: { (response) in
            self.isLoadData = true
            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                self.arrModelLeague.removeAll()
                self.arrFinalModelLeague.removeAll()
                self.tblMetches.reloadData()
                if let data = response["data"] as? [String:Any] {
                    
                    if let data = data["data"] as? [String: Any]{
                        
                        if let upcoming = data["upcoming"] as? [String: Any]{
                            
                            let obj = TableModel.init(fromDictionary: upcoming, strType: "UPCOMING MATCHES")
                            self.arrModelLeague.append(obj)
                        }
                        
                        if let latest = data["latest"] as? [String: Any]{
                            let obj = TableModel.init(fromDictionary: latest, strType: "PREVIOUS MATCHES")
                            //  obj.arrMatches =  obj.arrMatches.sorted(by: { $0.strDateTime < $1.strDateTime })
                            self.arrModelLeague.append(obj)
                        }
                        
                        
                        
                        for obj in self.arrModelLeague{
                            let filteredArray = self.arrModelLeague.filter(){ $0.matchType.contains(obj.matchType) }
                            for objNEW in filteredArray{
                                if objNEW.arrMatches.count > 0 {
                                    if !obj.arrMatches.contains(objNEW.arrMatches[0]){
                                        obj.arrMatches.append(objNEW.arrMatches[0])
                                    }
                                }
                            }
                            let arrNewCheck = self.arrFinalModelLeague.filter(){ $0.matchType.contains(obj.matchType) }
                            if arrNewCheck.count == 0{
                                if obj.arrMatches.count > 0{
                                    self.arrFinalModelLeague.append(obj)
                                }
                            }
                        }
                        self.tblMetches.reloadData()
                        if self.arrFinalModelLeague.count == 0{
                            self.lblNoRecord.isHidden = false
                        }else{
                            self.lblNoRecord.isHidden = true
                        }
                    }
                }
            }
            else{
                
                if self.arrFinalModelLeague.count == 0{
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }
            self.Ispulltorefresh = false
            
        }) { (error) in
            print(error)
            self.isLoadData = true
            self.Ispulltorefresh = false
            SVProgressHUD.dismiss()
        }
    }
    
}

extension TDetailsMatchesVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}
extension TDetailsMatchesVC: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return tblMetches
    }
}
