//
//  TableDetailsVC.swift
//  WolfScore
//
//  Created by mac on 11/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import SJSegmentedScrollView
import SkeletonView

struct TableDetailsStuctVc {
    static var IndexMove = 0
    
    static var isTableDidLoad = 0
    static var isMatchesDidLoad = 0
    static var isSquadDidLoad = 0
    static var isTranserDidLoad = 0

}


class TableDetailsVC: SJSegmentedViewController , TDetailsOverViewVC_Delegate{
    
    var isloadPagerTeam = false
    var selectedSegment             : SJSegmentTab?
    var segmentController           : SJSegmentedViewController = SJSegmentedViewController()

    
    // skelton outlet

    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var imgContryFlag: UIImageView!
    @IBOutlet weak var imgTeamLogo: UIImageView!
    @IBOutlet weak var lblfist:UILabel!
    @IBOutlet weak var lblsecond:UILabel!
    @IBOutlet weak var lblthird:UILabel!
    @IBOutlet weak var viewHeaderSkelton:UIView!
    
    // skelton outlet


    var dict:[String:Any] = [:]
    var dictApiResoinse:[String:Any] = [:]

    var TableVC : TDetailsTableVC!
    var MatchesVC : TDetailsMatchesVC!
    var OverViewVC : TDetailsOverViewVC!
    var SquadVC : TDetailsSquadVC!
    var TransfersVC : TDetailsTransfersVC!
    var TeamDetailHeaderVC : TeamDetailHeaderVC!

    var IntMoveVc = -1
    
    var SkeltonBgview:UIView!

    override func viewDidLoad() {
       
        
        if isloadPagerTeam == false
        {
            
            show_skelton()
            SkeltonBgview = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT))
            
            viewHeaderSkelton.frame = CGRect (x: 0, y: 44, width: SCREEN_WIDTH, height: 250)
            SkeltonBgview.addSubview(viewHeaderSkelton)
            SkeltonBgview.backgroundColor = UIColor.colorConstant.appDarkBlack
            self.view.addSubview(SkeltonBgview)
            
            self.call_Webservice_Get_Team_Details(str_team_id: objAppShareData.str_team_Id)
        }
        
        if isloadPagerTeam == true
        {
            
            TableDetailsStuctVc.isTableDidLoad = 0
            TableDetailsStuctVc.isMatchesDidLoad = 0
            TableDetailsStuctVc.isSquadDidLoad = 0
            TableDetailsStuctVc.isTranserDidLoad = 0

            var arrController = [UIViewController]()

            if let response = self.dict["tab_status"] as? [String: Any]{
                let standings = response["standings"] as? Int ?? 0
                let overview = response["overview"] as? Int ?? 0
                let fixture = response["fixture"] as? Int ?? 0
                let transfer = response["transfer"] as? Int ?? 0
                let squad = response["squad"] as? Int ?? 0


                if overview == 1{

                    self.OverViewVC = UIStoryboard(name: "TableDetails", bundle: nil).instantiateViewController(withIdentifier: "TDetailsOverViewVC") as? TDetailsOverViewVC
                    self.OverViewVC.delegate = self
                    self.OverViewVC.title = "  OVERVIEW  "

                    arrController.append(self.OverViewVC)
                }
                if fixture == 1{
                    self.MatchesVC = UIStoryboard(name: "TableDetails", bundle: nil).instantiateViewController(withIdentifier: "TDetailsMatchesVC") as? TDetailsMatchesVC
                    self.MatchesVC.title = "  MATCHES  "

                    arrController.append(self.MatchesVC)
                }
                if standings == 1{
                    self.TableVC = UIStoryboard(name: "TableDetails", bundle: nil).instantiateViewController(withIdentifier: "TDetailsTableVC") as? TDetailsTableVC
                    self.TableVC.title = "  TABLE  "

                    arrController.append(self.TableVC)
                }
                if transfer == 1{
                    self.TransfersVC = UIStoryboard(name: "TableDetails", bundle: nil).instantiateViewController(withIdentifier: "TDetailsTransfersVC") as? TDetailsTransfersVC
                    self.TransfersVC.title = "  TRANSFERS  "
                    arrController.append(self.TransfersVC)
                }
                if squad == 1{

                    self.SquadVC = UIStoryboard(name: "TableDetails", bundle: nil).instantiateViewController(withIdentifier: "TDetailsSquadVC") as? TDetailsSquadVC
                    self.SquadVC.title = "  SQUAD  "
                    arrController.append(self.SquadVC)

                }

                for i  in 0..<arrController.count
                {
                    if  arrController[i] == self.TableVC
                    {
                       IntMoveVc = i
                    }
                }
                
                self.TeamDetailHeaderVC =     UIStoryboard(name: "TableDetails", bundle: nil).instantiateViewController(withIdentifier: "TeamDetailHeaderVC") as? TeamDetailHeaderVC
                
                

                self.TeamDetailHeaderVC!.dict =  self.dictApiResoinse

                self.headerViewController = self.TeamDetailHeaderVC
                self.segmentControllers = arrController

                self.segmentTitleColor = UIColor.colorConstant.appLightBlueColor

                self.selectedSegmentViewColor  = UIColor.colorConstant.appBlueColor

                self.segmentSelectedTitleColor = UIColor.colorConstant.appLightBlueColor
                self.segmentedScrollViewColor = UIColor.colorConstant.appDarkBlack

                self.segmentBackgroundColor = UIColor.colorConstant.appDarkBlack
                self.headerViewHeight = 250
                self.selectedSegmentViewHeight = 3.0

                
                if UIScreen.main.bounds.size.height >= 812.0
                {
                self.headerViewOffsetHeight = 80.0
                }
                else
                {
                  self.headerViewOffsetHeight = 35.0
                }

                self.segmentTitleFont = UIFont(name: "roboto-Medium" , size: 14.0)!

                self.segmentShadow = SJShadow.dark()
                self.showsHorizontalScrollIndicator = false
                self.showsVerticalScrollIndicator = false
                self.segmentBounces = true
                self.delegate = self
                
            }
            
            hide_skelton()
            viewHeaderSkelton.removeFromSuperview()
            SkeltonBgview.removeFromSuperview()

        }
        super.viewDidLoad()
        
    }
    
    
    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblTeamName,lblCountryName, lblfist ,lblsecond ,lblthird].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
        [imgContryFlag,imgTeamLogo ].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
        
    }
    
    
    func hide_skelton()
    {
        [lblTeamName,lblCountryName, lblfist ,lblsecond ,lblthird].forEach { $0?.hideSkeleton()
        }
        [imgContryFlag,imgTeamLogo].forEach { $0?.hideSkeleton()
        }
    }
    
    
// Delegate method  TdetailOverviewVC
    func Team_IsfavouriteStatus(_ isfavrouite: Int) {
        if isfavrouite == 1
        {
            self.TeamDetailHeaderVC.UpdatefromOverviewVC_Favrouite_IsfavouriteStatus(isfavrouite)
        }
        else
        {
            self.TeamDetailHeaderVC.UpdatefromOverviewVC_Favrouite_IsfavouriteStatus(isfavrouite)
        }
    }
    
    func moveViewController(_ MoveIndex: Int) {
        
        if IntMoveVc == -1
        {
        }
        else
        {
            var segment: SJSegmentTab?
            if segments.count > 0 {
                segment = segments[2]
            }
            self.setSelectedSegmentAt(IntMoveVc, animated: true)
        }
    }

    // MARK:- IBAction
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}


extension TableDetailsVC{
    
    func call_Webservice_Get_Team_Details(str_team_id:String){
        //SVProgressHUD.show()
        
        let paramDict = ["team_id":str_team_id,"type":"overview"] as [String:AnyObject]
        
        objWebServiceManager.requestGet(strURL: webUrl.get_team_detail, params: paramDict, success: { (response) in
            self.hide_skelton()
            self.viewHeaderSkelton.isHidden = true
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    self.dictApiResoinse = response
                    self.dict = data
                    self.isloadPagerTeam = true
                    self.viewDidLoad()
                    
                   let object =  TableDetailsModelClass.init(dict: data)
                    objAppShareData.str_team_Name = object.team_name
                    objAppShareData.str_team_Logo = object.team_logo_path
                }
                else
                {
                self.hide_skelton()
                self.viewHeaderSkelton.removeFromSuperview()
                self.SkeltonBgview.removeFromSuperview()
                }
            }
            
        }) { (error) in
            print(error)
            self.SkeltonBgview.removeFromSuperview()
            self.hide_skelton()
            self.viewHeaderSkelton.isHidden = true
            SVProgressHUD.dismiss()
        }
    }
}


extension TableDetailsVC: SJSegmentedViewControllerDelegate {
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        
        if controller == self.TableVC
        {
            if  TableDetailsStuctVc.isTableDidLoad == 0
            {
                print("self.TableVC")
                self.TableVC.callApiSelfTDetailsTableVC()
                TableDetailsStuctVc.isTableDidLoad = 1
            }
        }
        if controller == self.MatchesVC
        {
            if  TableDetailsStuctVc.isMatchesDidLoad == 0
            {
                print("self.MatchesVC")
                self.MatchesVC.callApiSelfTTDetailsMatchesVC()
                TableDetailsStuctVc.isMatchesDidLoad = 1
            }
        }
        if controller == self.SquadVC
        {
            if  TableDetailsStuctVc.isSquadDidLoad == 0
            {
                print("self.SquadVC")
                self.SquadVC.callApiSelfTDetailsSquadVC()
                TableDetailsStuctVc.isSquadDidLoad = 1
            }
        }
        if controller == self.TransfersVC
        {
            if  TableDetailsStuctVc.isTranserDidLoad == 0
            {
                print("self.TransfersVC")
                self.TransfersVC.callApiSelfTDetailsTransfersVC()
                TableDetailsStuctVc.isTranserDidLoad = 1
            }
        }
        
        if selectedSegment != nil {
            selectedSegment?.titleColor(UIColor.colorConstant.appLightBlueColor)
        }
        
        if segments.count > 0 {
            
            selectedSegment = segments[index]
            selectedSegment?.titleColor(UIColor.colorConstant.appLightBlueColor)
        }
    }
}


