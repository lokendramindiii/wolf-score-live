//
//  TableDetailsVC.swift
//  WolfScore
//
//  Created by mac on 11/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD

class PlayerDetailVC: UIViewController {

    
    @IBOutlet var headerView: UIView!
    @IBOutlet var viewLeague: UIView!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblLeagueName: UILabel!
    @IBOutlet weak var imgContryFlag: UIImageView!
    @IBOutlet weak var imgLeagueFlag: UIImageView!
    @IBOutlet weak var imgTeamLogo: UIImageView!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblNewCountryName: UILabel!
    @IBOutlet weak var imgNewContryFlag: UIImageView!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblJerseyNo: UILabel!
    @IBOutlet weak var tableView: UITableView!


    var dict:[String:Any] = [:]
    
    var arrStatsPlayer = [statsModel]()

    
    var TableVC : TDetailsTableVC!
    var MatchesVC : TDetailsMatchesVC!
    var OverViewVC : TDetailsOverViewVC!
    var SquadVC : TDetailsSquadVC!
    var TransfersVC : TDetailsTransfersVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.call_Webservice_Get_Player_Details(str_team_id: objAppShareData.str_player_Id)
        //selectedbarcolor()
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    // MARK:- IBAction
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
}


extension PlayerDetailVC{
    
    func call_Webservice_Get_Player_Details(str_team_id:String){
        SVProgressHUD.show()
        
        let paramDict = ["player_id":objAppShareData.str_player_Id] as [String:AnyObject]
       
        objWebServiceManager.requestGet(strURL: webUrl.get_player_detail, params: paramDict, success: { (response) in
            print("--Statnding----------\(response)")
            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                self.dict = data["data"] as? [String:Any] ?? [:]
                    
                   let object =  PlayerModelClass.init(dict: data)
                    //objAppShareData.str_team_Name = object.player_name
                    //objAppShareData.str_team_Logo = object.team_logo_path
                    self.lblTeamName.text = object.player_name
                    self.lblCountryName.text = object.team_name
                    
                    self.lblHeight.text = object.height
                    self.lblWeight.text = object.weight
                    self.lblNewCountryName.text = object.country_name
                    self.lblDOB.text = dayDate.dateformateConvertBydd_MM_yyyyToMon_DD_Year(strDate: object.dob)
                    
                    let currentDate = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy"
                    dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
                    let date = dateFormatter.date(from: object.dob)
                    let df = DateFormatter()
                    df.dateFormat = "YYYY-MM-dd HH:mm:ss"
                    df.timeZone = NSTimeZone.local
                    df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
                    let strDate = df.string(from: date ?? Date())
                    let strMonths = dayDate.CompareDate(strFirstDate: strDate, currentDate: currentDate)
                    let arr = strMonths.components(separatedBy: " ")
                    var strYear = ""
                    if arr.count > 0{
                        if strMonths == "Just now"
                        {}
                        else{
                        strYear = String(Int(Int(arr[0])!/12))
                        }
                    }
                    self.lblAge.text = strYear + " years"
                    self.lblPosition.text = object.player_position
                    self.lblJerseyNo.text = object.jerseyNo
                    if self.lblJerseyNo.text?.count == 0{
                       self.lblJerseyNo.text = "NA"
                    }
                    if let url = URL(string: object.team_logo){
                        self.imgContryFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "circle_goal_icon"))
                    }
                    else
                    {
                        self.imgContryFlag.image = UIImage(named: "circle_goal_icon")
                    }
                    
                    if let url = URL(string: object.country_logo){
                        self.imgNewContryFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "circle_goal_icon"))
                    }
                    
                    else
                    {
                        self.imgNewContryFlag.image = UIImage(named: "circle_goal_icon")
                    }
                    
                    if let url = URL(string: object.player_image){
                        self.imgTeamLogo.af_setImage(withURL: url, placeholderImage: UIImage(named: "circle_goal_icon"))
                        self.imgTeamLogo.layer.cornerRadius = self.imgTeamLogo.frame.size.width/2
                        self.imgTeamLogo.clipsToBounds = true
                    }
                    
                    self.arrStatsPlayer.removeAll()
                    if object.arrStats.count > 0
                    {
                      self.arrStatsPlayer = object.arrStats
                      self.tableView.reloadData()
                    }
                }
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}
// MARK: - TableView Delegates & Datasource
extension PlayerDetailVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrStatsPlayer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PlayeInfoViewCell", for: indexPath) as? PlayeInfoViewCell{
            
            let obj = arrStatsPlayer[indexPath.row]
            cell.lblGoal.text = obj.goals
            cell.lblyellowCard.text = obj.yellowcards
            cell.lblredCard.text = obj.redcards
            cell.lblAssist.text = obj.assists

            return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
