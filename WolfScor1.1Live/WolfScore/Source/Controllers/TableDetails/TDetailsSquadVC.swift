//
//  TDetailsSquadVC.swift
//  WolfScore
//
//  Created by mac on 16/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import CommonCrypto
import SJSegmentedScrollView
import GoogleMobileAds

class TDetailsSquadVC: UIViewController {
    
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var tblSquad: UITableView!
    @IBOutlet weak var viewBannerBottom: UIView!
    
    var arrModelPlayer = [SquadModelClass]()
    var arrFinalModelPlayer = [SquadModelClass]()
    
    var arrSortLeagueByFilter = [String]()
    var pullToRefreshCtrl:UIRefreshControl!
    var Ispulltorefresh = false
    
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!
    var isLoadData = false
    var placeholderRow = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPullToRefresh()
        self.bannerAdSetup()
    }
    
    func callApiSelfTDetailsSquadVC()
    {
        self.apiCallGetFixtures()
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        Ispulltorefresh = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
  
    
}


//MARK : custom extension
extension TDetailsSquadVC {
    
    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.tblSquad.refreshControl = pullToRefreshCtrl
        }else{
            self.tblSquad.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        sender.endRefreshing()
        if Ispulltorefresh == false
        {
            self.apiCallGetFixtures()
            sender.endRefreshing()
        }
    }
    
    func apiCallGetFixtures()
    {
        let dictPram = [
            "type":"squad",
            "team_id":objAppShareData.str_team_Id
            ] as [String: AnyObject]
        self.call_Webservice_Get_fixtures(dict_param: dictPram)
    }
}



// MARK: - TableView Delegates & Datasource
extension TDetailsSquadVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isLoadData == false
        {
            return 1
        }
        return self.arrFinalModelPlayer.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isLoadData == false
        {
            return UIView()
        }
        let headerView = UIView()
        
        //headerView.conten.backgroundColor = UIColor.colorConstant.appSectionHeader
        headerView.backgroundColor = UIColor.colorConstant.appDarkBlack
        // create Image in Header
        
        //            let myCustomView = UIImageView(frame: CGRect(x: 14, y: 12, width:
        //                22, height: 22))
        //            let myImage: UIImage = UIImage(named: "circle_transfers_icon")!
        //            myCustomView.image = myImage
        //            headerView.addSubview(myCustomView)
        
        // create Icon in Header
        //        let myCustomView2 = UIImageView(frame: CGRect(x: tableView.bounds.size.width - 30, y: 16, width:
        //            14, height: 14))
        //        let myImage2: UIImage = UIImage(named: "icon_back_Table")!
        //        myCustomView2.image = myImage2
        //        headerView.addSubview(myCustomView2)
        
        // Create Lable in Header
        let headerLabel = UILabel(frame: CGRect(x: 14, y: 2, width:
            tableView.frame.size.width/2, height: 40))
        
        headerLabel.font = UIFont(name: "Roboto-Bold", size: 15)
        headerLabel.textColor = #colorLiteral(red: 0.5137254902, green: 0.5882352941, blue: 0.6470588235, alpha: 1)
        headerLabel.numberOfLines = 2
        let position = self.arrFinalModelPlayer[section].position
        headerLabel.text = position.uppercased()
        headerView.addSubview(headerLabel)
        
        // Create Lable in Header
        let headerLabel1 = UILabel(frame: CGRect(x: tableView.frame.size.width/2, y: 2, width:
            tableView.frame.size.width/2-14, height: 40))
        
        headerLabel1.font = UIFont(name: "Roboto-Bold", size: 15)
        headerLabel1.textColor = #colorLiteral(red: 0.5137254902, green: 0.5882352941, blue: 0.6470588235, alpha: 1)
        headerLabel1.numberOfLines = 2
        headerLabel1.textAlignment = .right
        headerLabel1.text = "GOALS SCORED"
        headerView.addSubview(headerLabel1)
        
        // Create Line in Header
        let headerLine = UILabel(frame: CGRect(x: 0, y: 40, width:
            tableView.bounds.size.width, height: 0.4))
        
        headerLine.backgroundColor = UIColor.black
        headerLine.alpha = 1
        headerView.addSubview(headerLine)
        
        // Create Line Top in Header
        let headerTopLine = UILabel(frame: CGRect(x: 0, y: 0, width:
            tableView.bounds.size.width, height: 4))
        
        if section == 0{
            headerTopLine.backgroundColor = UIColor.colorConstant.appDeepBlack
            headerTopLine.alpha = 0.7
        }else{
            headerTopLine.backgroundColor = UIColor.colorConstant.appDeepBlack
            headerTopLine.alpha = 0.7
        }
        
        //headerView.addSubview(headerTopLine)
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoadData == false
        {
            return placeholderRow
        }
        let objPlayer = self.arrFinalModelPlayer[section]
        return objPlayer.arrPlayers.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoadData == false
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SquadTableCell", for: indexPath) as? SquadTableCell{
                cell.show_skelton()
                return cell
            }
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SquadTableCell", for: indexPath) as? SquadTableCell{
            cell.hide_skelton()
            let objSectionPlayer = self.arrFinalModelPlayer[indexPath.section]
            let objPlayer = objSectionPlayer.arrPlayers[indexPath.row]
            cell.lblCountryName.text = objPlayer.country_name
            cell.lblPlayerName.text = objPlayer.player_name
            cell.lblGoals.text = objPlayer.player_goals
            
            if let url = URL(string: objPlayer.country_logo){
                cell.imgViewCountry.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgViewCountry.image = UIImage(named: "icon_placeholderTeam")
            }
            
            
            if let url = URL(string: objPlayer.player_image){
                cell.imgViewPlayer.af_setImage(withURL: url, placeholderImage: UIImage(named: "circle_goal_icon"))
            }
            else
            {
                cell.imgViewPlayer.image = UIImage(named: "circle_goal_icon")
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        if indexPath.section == (arrFinalModelLeague.count - 1) && shouldLoadMore == true {
    //            self.shouldLoadMore = false
    //            self.apiCallGetFixtures()
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.arrFinalModelPlayer.count == 0{
            return
        }
        let objSectionPlayer = self.arrFinalModelPlayer[indexPath.section]
        let objPlayer = objSectionPlayer.arrPlayers[indexPath.row]
        
        objAppShareData.str_player_Id = objPlayer.player_id
        let viewController = UIStoryboard(name: "TableDetails",bundle: nil).instantiateViewController(withIdentifier: "PlayerDetailVC") as! PlayerDetailVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// Api call
extension TDetailsSquadVC{
    
    func call_Webservice_Get_fixtures(dict_param:[String:AnyObject]) {
        self.Ispulltorefresh = true
        
        objWebServiceManager.requestGet(strURL: webUrl.get_team_detail, params: dict_param, success: { (response) in
            SVProgressHUD.dismiss()
            self.isLoadData = true
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                self.arrModelPlayer.removeAll()
                self.arrFinalModelPlayer.removeAll()
                self.tblSquad.reloadData()
                
                if let data = response["data"] as? [String:Any] {
                    if let data = data["data"] as? [String: Any]{
                        
                        if let squad = data["squad"] as? [String: Any]{
                            if let arrData = squad["data"] as? [[String:Any]]  {
                                for dict in arrData{
                                    let objPlayer = PlayerModelClass.init(dict: dict)
                                    
                                    if objPlayer.player_position != ""
                                    {
                                    let objPlayersec = SquadModelClass.init(fromDictionary: squad, strPosition: objPlayer.player_position)
                                    
                                    self.arrModelPlayer.append(objPlayersec)
                                    }
                                }
                            }
                        }
                        
                        for obj in self.arrModelPlayer{
                            let filteredArray = self.arrModelPlayer.filter(){ $0.position.contains(obj.position) }
                            for objNEW in filteredArray{
                                if objNEW.arrPlayers.count > 0 {
                                    if !obj.arrPlayers.contains(objNEW.arrPlayers[0]){
                                        obj.arrPlayers.append(objNEW.arrPlayers[0])
                                    }
                                }
                            }
                            
                            let arrNewCheck = self.arrFinalModelPlayer.filter(){ $0.position.contains(obj.position) }
                            if arrNewCheck.count == 0{
                                obj.arrPlayers = obj.arrPlayers.unique{$0.player_id}
                                if obj.arrPlayers.count > 0{
                                self.arrFinalModelPlayer.append(obj)
                                }
                            }
                        }
                        self.tblSquad.reloadData()
                        if self.arrFinalModelPlayer.count == 0{
                            self.lblNoRecord.isHidden = false
                        }else{
                            self.lblNoRecord.isHidden = true
                        }
                    }
                }
            }
            else{
                
                if self.arrFinalModelPlayer.count == 0{
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }
            self.Ispulltorefresh = false
            
        }) { (error) in
            print(error)
            self.isLoadData = true
            self.Ispulltorefresh = false
            SVProgressHUD.dismiss()
        }
    }
}


extension TDetailsSquadVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}
extension TDetailsSquadVC: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return tblSquad
    }
}
