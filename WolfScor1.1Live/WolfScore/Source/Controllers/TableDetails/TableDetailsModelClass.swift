//
//  TableDetailsModelClass.swift
//  WolfScore
//
//  Created by mac on 12/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit


class TableDetailsModelClass: NSObject {
    
    var current_season_id = ""
    var country_id = ""
    var country_logo_path = ""
    var country_name = ""
    var team_name = ""
    var team_founded = ""
    var team_logo_path = ""
    var league_name = ""
    var league_logo_path = ""
    
    var stadium_name = ""
    var choache_name = ""
    
    init(dict: [String : Any]) {
        if let dataDict = dict["data"] as? [String:Any]  {
            
            if let countryid = dataDict["country_id"] as? Int {
               country_id = String(countryid)
                
                let arrCountry = UserDefaults.standard.array(forKey: UserDefaults.Keys.kCountryArr) as? [[String:Any]] ?? []
                for dict in arrCountry {
                    let id = dict["country_id"] as? String ?? ""
                    if id == String(country_id) {
                        let countryName = dict["country_name"] as? String ?? ""
                        country_name = countryName
                        country_logo_path = dict["country_flag"] as? String ?? ""
                        break
                    }
                }
            }
            
            
            if let currentseasonid = dataDict["current_season_id"] as? Int {
                current_season_id = String(currentseasonid)
                objAppShareData.str_teamSeason_Id = current_season_id
            }
            
            if let name = dataDict["name"] as? String {
                team_name = name
            }
            
            if let logopath = dataDict["logo_path"] as? String {
                team_logo_path = logopath
            }
            
            if let founded = dataDict["founded"] as? Int {
                team_founded = String(founded)
            }
            
            if let leagueDict = dataDict["league"] as? [String:Any]  {
                if let leagueData = leagueDict["data"] as? [String:Any]  {
                    
                    if let name = leagueData["name"] as? String {
                        league_name = name
                    }
                    
                    if let logopath = leagueData["logo_path"] as? String {
                        league_logo_path = logopath
                    }
                }
            }
            
            if let venueDict = dataDict["venue"] as? [String:Any]  {
                if let venueData = venueDict["data"] as? [String:Any]  {
                    
                    if let name = venueData["name"] as? String {
                        stadium_name = name
                    }
                   
                }
            }
            
            if let coachDict = dataDict["coach"] as? [String:Any]  {
                if let coachData = coachDict["data"] as? [String:Any]  {
                    
                    if let name = coachData["fullname"] as? String {
                        choache_name = name
                    }
                    
                }
            }
            
        }
    }
    
}


class TransferModelClass: NSObject {
    
    var player_id = ""
    var player_logo_path = ""
    var player_name = ""
    var player_height = ""
    var player_weight = ""
    var player_nationality = ""
    var sideline_description = ""
    var team_name = ""
    var team_logo_path = ""
    var days = ""
    var in_out_status = ""
    var position = ""
    
    init(dict: [String : Any]) {
        
        if let datetime = dict["date"] as? String {
            let date = dayDate.convertStringToDate(strDate: datetime)
            let diffrence = dayDate.dayDifference(from: date.timeIntervalSince1970)
            if diffrence == "Yesterday"{
                days = "-1"
            }else if diffrence == "Today"{
                days = "0"
            }else if diffrence == "Tomorrow"{
                days = "Tomorrow"
            }else{
                days = diffrence
            }
        }
        
        if let type = dict["type"] as? String {
            in_out_status = type
        }
        if let description = dict["description"] as? String {
            sideline_description = description
        }
        
        if let playerDict = dict["player"] as? [String:Any]  {
            if let data = playerDict["data"] as? [String:Any]  {
                
                if let playerid = data["player_id"] as? String {
                    player_id = playerid
                }else if let playerid = data["player_id"] as? Int {
                    player_id = String(playerid)
                }
                
                var fName = ""
                if let firstname = data["firstname"] as? String {
                    fName = firstname
                }
                var LName = ""
                if let lastname = data["lastname"] as? String {
                    LName = lastname
                }
                
                player_name = fName + " " + LName
                
                if let logopath = data["image_path"] as? String {
                    player_logo_path = logopath
                }
                if let weight = data["weight"] as? String {
                    player_weight = weight
                }
                if let height = data["height"] as? String {
                    player_height = height
                }
                if let nationality = data["nationality"] as? String {
                    player_nationality = nationality
                }
                if let positionDict = data["position"] as? [String:Any]  {
                    if let data = positionDict["data"] as? [String:Any]  {
                        if let name = data["name"] as? String {
                            position = name.first?.description ?? ""
                        }
                    }
                }
            }
        }
        
        
        if let teamDict = dict["team"] as? [String:Any]  {
            if let data = teamDict["data"] as? [String:Any]  {
                
                if let name = data["name"] as? String {
                    team_name = name
                }
                
                if let logopath = data["logo_path"] as? String {
                    team_logo_path = logopath
                }
            }
        }
        
    }
}

class TableModel: NSObject {
    var matchType = ""
    var arrMatches = [MatchesModelClass]()
    init(fromDictionary dictionary: [String:Any], strType:String){
        matchType = strType
        if let arrData = dictionary["data"] as? [[String:Any]]  {
            for dict in arrData{
                let objMatch = MatchesModelClass.init(dict: dict)
                objMatch.matchType = strType
                if strType == "PREVIOUS MATCHES"{
                    if objMatch.strStatus == "FT"{
                      arrMatches.append(objMatch)
                    }
                }
                else{
                    let date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                    let result = formatter.string(from: date)
                    let date1 = formatter.date(from: result)
                    let date2 = formatter.date(from: objMatch.strDate )

                    if date1?.compare(date2!) == .orderedAscending
                    {
                        arrMatches.append(objMatch)
                   }
                }
            }
        }
    }
}

class MatchesModelClass: NSObject {
    
    var matchType = ""
    
    var strStatus  = ""
    var strDate  = ""
    var strDateTime  = ""
    var strTime  = ""
    var fixtureId = ""
    var seasonId = ""
    
    var strLocalTeamScore  = 0
    var strLocalTeamId = ""
    var strVisitorTeamScore = 0
    var strVisitorTeamId = ""
    
    var strLogopathLocalTeam  = ""
    var strLogopathVisitorTeam  = ""
    
    var strNameloacalTeam  = ""
    var strNameVisitorTeam = ""
    
    init(dict:[String:Any]){
        
        if let season_id = dict["season_id"] as? Int  {
             seasonId = String(season_id)
        }
        
        if let id = dict["id"] as? Int  {
            fixtureId = String(id)
        }
        
        if let timeDict = dict["time"] as? [String:Any]  {
            strStatus = timeDict["status"] as? String ?? ""
            if let startingDict = timeDict["starting_at"] as? [String:Any]  {
                strDate = startingDict["date"] as? String ?? ""
                strTime = startingDict["time"] as? String ?? ""
                strDateTime = startingDict["date_time"] as? String ?? ""
            }
        }
        
        if let scoresDict = dict["scores"] as? [String:Any]  {
            strLocalTeamScore = scoresDict["localteam_score"] as? Int ?? 0
            strVisitorTeamScore = scoresDict["visitorteam_score"] as? Int ?? 0
        }
        
        if let localTeam = dict["localTeam"] as? [String:Any]  {
            if let data = localTeam["data"] as? [String:Any]  {
                strNameloacalTeam = data["name"] as? String ?? ""
                strLogopathLocalTeam = data["logo_path"] as? String ?? ""
            }
        }
        
        if let visitorTeam = dict["visitorTeam"] as? [String:Any]  {
             if let data = visitorTeam["data"] as? [String:Any]  {
                strNameVisitorTeam = data["name"] as? String ?? ""
                strLogopathVisitorTeam = data["logo_path"] as? String ?? ""
            }
           
        }
    }
}



class SquadModelClass: NSObject {
    var position = ""
    var arrPlayers = [PlayerModelClass]()
    init(fromDictionary dictionary: [String:Any], strPosition:String){

        if let arrData = dictionary["data"] as? [[String:Any]]  {
            position = strPosition
            for dict in arrData{
                let objPlayer = PlayerModelClass.init(dict: dict)
                if strPosition == objPlayer.player_position{
                   arrPlayers.append(objPlayer)
                }
            }
        }
    }
}

extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        
        return arrayOrdered
    }
}

extension Array where Element: Equatable {
    func removingDuplicates() -> Array {
        return reduce(into: []) { result, element in
            if !result.contains(element) {
                result.append(element)
            }
        }
    }
}

class PlayerModelClass: NSObject {
    
    var player_id = ""
    var player_name = ""
    var player_image = ""
    var player_goals = ""
    var player_position = ""
    var country_logo = ""
    var country_name = ""
    var team_name = ""
    var team_logo = ""

    var height = ""
    var weight = ""
    var dob = ""
    var age = ""
    var jerseyNo = ""
    
    
    var arrStats = [statsModel]()
    
    init(dict:[String:Any]){
        
        if let p_id = dict["player_id"] as? Int  {
            player_id = String(p_id)
        }
        
        
        if let data = dict["data"] as? [String:Any]  {
            
            if let stats = data["stats"] as? [String:Any]  {
                
                if let arrdata = stats["data"] as? [[String:Any]]
                {
                    var int_fiveleague = 1
                    for obj in arrdata
                    {
                        if int_fiveleague <= 5
                        {
                            let obj = statsModel.init(localdictionary: obj)
                            self.arrStats.append(obj)
                            int_fiveleague = int_fiveleague + 1
                        }
                    }
                }
            }
        }
        
        if let goals = dict["goals"] as? Int  {
            player_goals = String(goals)
        }
        
        if let player = dict["player"] as? [String:Any]  {
            if let data = player["data"] as? [String:Any]  {
                player_name = data["fullname"] as? String ?? ""
                player_image = data["image_path"] as? String ?? ""
                
                let arrCountry = UserDefaults.standard.array(forKey: UserDefaults.Keys.kCountryArr) as? [[String:Any]] ?? []
                let country_id = data["country_id"] as? Int  ?? 0
                for dictCountry in arrCountry {
                    let id = dictCountry["country_id"] as? String ?? ""
                    if id == String(country_id) {
                        let countryName = dictCountry["country_name"] as? String ?? ""
                        country_name = countryName
                        country_logo = dictCountry["country_flag"] as? String ?? ""
                        break
                    }
                }
                
                if let position = data["position"] as? [String:Any]  {
                    if let positiondata = position["data"] as? [String:Any]  {
                        player_position = positiondata["name"] as? String ?? ""
                    }
                }
                
            }
        }
        
        if let data = dict["data"] as? [String:Any]  {
            //if let data = player["data"] as? [String:Any]  {
                //player_name = data["fullname"] as? String ?? ""
                let strFirst = data["firstname"] as? String ?? ""
                let strLast = data["lastname"] as? String ?? ""
                player_name = strFirst + " " + strLast
                player_image = data["image_path"] as? String ?? ""
                height = data["height"] as? String ?? ""
                weight = data["weight"] as? String ?? ""
                dob = data["birthdate"] as? String ?? ""
            
            let arrCountry = UserDefaults.standard.array(forKey: UserDefaults.Keys.kCountryArr) as? [[String:Any]] ?? []
                let country_id = data["country_id"] as? Int  ?? 0
                for dictCountry in arrCountry {
                    let id = dictCountry["country_id"] as? String ?? ""
                    if id == String(country_id) {
                        let countryName = dictCountry["country_name"] as? String ?? ""
                        country_name = countryName
                        country_logo = dictCountry["country_flag"] as? String ?? ""
                        break
                    }
                }
                
                if let position = data["position"] as? [String:Any]  {
                    if let positiondata = position["data"] as? [String:Any]  {
                        player_position = positiondata["name"] as? String ?? ""
                    }
                }
            
            if let team = data["team"] as? [String:Any]  {
                if let teamdata = team["data"] as? [String:Any]  {
                    team_name = teamdata["name"] as? String ?? ""
                    team_logo = teamdata["logo_path"] as? String ?? ""
                }
            }
                
        }
    }
}
// local team Model
class statsModel: NSObject {
    
    var yellowcards : String = ""
    var redcards : String = ""
    var goals : String = ""
    var assists : String = ""
    
    init(localdictionary: [String:Any]){
        
        if let yellowCard = localdictionary["yellowcards"] as? Int  {
            yellowcards =  "\(yellowCard)"
        }
        if let redcard = localdictionary["redcards"] as? Int {
            redcards = "\(redcard)"
        }
        if let goal = localdictionary["goals"] as? Int  {
            goals =  "\(goal)"
        }
        if let assist = localdictionary["assists"] as? Int  {
            assists =  "\(assist)"
        }
    }
}

