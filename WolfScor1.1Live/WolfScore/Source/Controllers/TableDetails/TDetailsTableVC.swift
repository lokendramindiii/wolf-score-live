//
//  TDetailsTableVC.swift
//  WolfScore
//
//  Created by mac on 11/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import CommonCrypto
import SJSegmentedScrollView
import GoogleMobileAds

class TDetailsTableVC: UIViewController {
    
    @IBOutlet weak var tblTeam: UITableView!
    @IBOutlet weak var lblNoRecodFound: UILabel!
    @IBOutlet weak var viewBannerBottom: UIView!
    
    var arrayAllgroup = [ModelMatchGroup]()
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!
    var isLoadData = false
    var placeholderRow = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblTeam.backgroundColor = UIColor.colorConstant.appDarkBlack
        
        self.bannerAdSetup()
    }
    
  
    func callApiSelfTDetailsTableVC()
    {
        self.call_Webservice_Get_Stating_Team_List(str_teamSeason_Id: objAppShareData.str_teamSeason_Id)
    }
    
    
    func call_Webservice_Get_Stating_Team_List(str_teamSeason_Id:String){
        //SVProgressHUD.show()
        
        let paramDict = ["season_id":str_teamSeason_Id] as [String:AnyObject]
        
        print(paramDict)
        
        objWebServiceManager.requestGet(strURL: webUrl.get_team_Table, params: paramDict, success: { (response) in
            self.isLoadData = true
            SVProgressHUD.dismiss()
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    let object =  ModelStanding.init(fromDictionary: data)
                    
                
                    for obj in object.arrGroup
                      {
                        
                        if obj.arrStandings.count > 0
                        {
                            self.arrayAllgroup = object.arrGroup
                        }
                    }
                    
                    if self.arrayAllgroup.count == 0
                    {
                        self.tblTeam.isHidden = true
                        self.lblNoRecodFound.isHidden = false
                    }else{
                        self.lblNoRecodFound.isHidden = true
                        self.tblTeam.reloadData()
                    }
                }
            }
            else{
                if self.arrayAllgroup.count == 0
                {
                    self.lblNoRecodFound.isHidden = false
                }else{
                    self.lblNoRecodFound.isHidden = true
                }
                //GlobalUtility.showToastMessage(msg: message)
            }
            
        }) { (error) in
            print(error)
            self.isLoadData = true
            SVProgressHUD.dismiss()
        }
    }
    
    
}

extension TDetailsTableVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isLoadData == false{
            return 1
        }
        return self.arrayAllgroup.count
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isLoadData == false{
            return UIView()
        }
        let headerView = UIView()
        headerView.backgroundColor = UIColor.colorConstant.appDarkBlack
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50)
        
        let StandingViewHeader = StandingHeader.instanceFromNib() as! StandingHeader
        StandingViewHeader.frame = CGRect(x: 0, y: 0, width:tableView.bounds.size.width , height: 50)
        StandingViewHeader.bgColor.backgroundColor = UIColor.colorConstant.appDarkBlack
        StandingViewHeader.lbl_groupName.text = "   " +  self.arrayAllgroup[section].name
        
        headerView.addSubview(StandingViewHeader)
        return headerView
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoadData == false
        {
            return placeholderRow
        }
        let objLeague = self.arrayAllgroup[section]
        return objLeague.arrStandings.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadData == false || self.arrayAllgroup.count == 0
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "statndingViewCell", for: indexPath) as? statndingViewCell{
                cell.show_skelton()
                return cell
            }
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "statndingViewCell", for: indexPath) as? statndingViewCell{
            cell.hide_skelton()
            let section = indexPath.section
            let index = indexPath.row
            let ObjStanding = self.arrayAllgroup[indexPath.section]
            let objMatch = ObjStanding.arrStandings[indexPath.row]
            
            if let url = URL(string: objMatch.logoPath){
                cell.imgLogoPath.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgLogoPath.image = UIImage(named: "icon_placeholderTeam")
            }
            
            if section == 0{
                switch (index){
                case 0:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 1:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 2:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 3:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appGreenColor
                case 4:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appBlueColor
                case 5:
                    cell.viewline.backgroundColor = UIColor.colorConstant.appBlueColor
                default:
                    
                    if ObjStanding.arrStandings.count > 6 && self.arrayAllgroup.count<=1
                    {
                        if index == ObjStanding.arrStandings.count - 1 || index == ObjStanding.arrStandings.count - 2 || index == ObjStanding.arrStandings.count - 3
                        {
                            cell.viewline.backgroundColor = UIColor.red
                        }
                        else{
                            cell.viewline.backgroundColor = UIColor.clear
                        }
                    }
                    else
                    {
                        cell.viewline.backgroundColor = UIColor.clear
                    }
                }
            }else{
                cell.viewline.backgroundColor = UIColor.clear
            }
            
            if  section == self.arrayAllgroup.count-1 && section != 0{
                if index == ObjStanding.arrStandings.count - 1 || index == ObjStanding.arrStandings.count - 2 || index == ObjStanding.arrStandings.count - 3
                {
                    cell.viewline.backgroundColor = UIColor.red
                }
                else{
                    cell.viewline.backgroundColor = UIColor.clear
                }
            }
            
            
            if objAppShareData.str_team_Id == objMatch.teamId{
                cell.backgroundColor = UIColor.colorConstant.appDeepBlack
            }else{
                cell.backgroundColor = UIColor.clear
            }
            
            cell.lblTeamName.text =  objMatch.teamName
            
            cell.lblPositon.text =  String(index + 1) + "  "
            //cell.lblPositon.text =  objMatch.position + "  "
            
            cell.lblGamePlayed.text =  objMatch.gamesPlayed
            
            cell.lblWon.text =  objMatch.won
            
            cell.lbldraw.text =  objMatch.draw
            
            cell.lbllost.text =  objMatch.lost
            
            cell.lblGoalScore.text =  objMatch.goalsScored
            
            cell.lblGoalAgainst.text =  objMatch.goalsAgainst
            
            cell.lblGoalDifferecnc.text =  objMatch.goalDifference
            
            cell.lblPoints.text =  objMatch.points
            
            return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.arrayAllgroup.count == 0{
            return
        }
        let ObjStanding = self.arrayAllgroup[indexPath.section]
        let objTable = ObjStanding.arrStandings[indexPath.row]
        objAppShareData.str_team_Id = objTable.teamId
        
        let viewController = UIStoryboard(name: "TableDetails",bundle: nil).instantiateViewController(withIdentifier: "TableDetailsVC") as! TableDetailsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
extension TDetailsTableVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}
extension TDetailsTableVC: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return tblTeam
    }
}
