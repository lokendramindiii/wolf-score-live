//
//  TarnsferTableCell.swift
//  WolfScore
//
//  Created by mac on 16/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView

class TarnsferTableCell: UITableViewCell {
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var imgViewTeam: UIImageView!
    @IBOutlet weak var imgViewPlayer: UIImageView!
    @IBOutlet weak var imgViewInOut: UIImageView!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var viewProfileRight: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblTeamName ,lblPlayerName ,lblDays , lblPosition ].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        [imgViewTeam,imgViewPlayer,imgViewInOut].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
        [viewProfileRight].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
    }
    
    func hide_skelton()
    {
        [lblTeamName ,lblPlayerName ,lblDays , lblPosition].forEach { $0?.hideSkeleton()
        }
        [imgViewTeam ,imgViewPlayer,imgViewInOut].forEach { $0?.hideSkeleton()
        }
        [viewProfileRight].forEach { $0?.hideSkeleton()
        }
    }
}
