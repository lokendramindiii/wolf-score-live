//
//  PlayeInfoViewCell.swift
//  WolfScore
//
//  Created by Mindiii on 7/11/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class PlayeInfoViewCell: UITableViewCell {

    @IBOutlet weak var lblyellowCard: UILabel!
    @IBOutlet weak var lblredCard: UILabel!
    @IBOutlet weak var lblGoal: UILabel!
    @IBOutlet weak var lblAssist: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
