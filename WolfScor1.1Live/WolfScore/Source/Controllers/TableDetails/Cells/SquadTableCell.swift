//
//  SquadTableCell.swift
//  WolfScore
//
//  Created by mac on 23/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView

class SquadTableCell: UITableViewCell {
    
    @IBOutlet weak var imgViewPlayer: UIImageView!
    @IBOutlet weak var imgViewCountry: UIImageView!
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblGoals: UILabel!
    @IBOutlet weak var lblPlayerName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblCountryName ,lblGoals ,lblPlayerName ].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        [imgViewPlayer,imgViewCountry].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
    }
    
    func hide_skelton()
    {
        [lblCountryName ,lblGoals ,lblPlayerName ].forEach { $0?.hideSkeleton()
        }
        [imgViewPlayer,imgViewCountry].forEach { $0?.hideSkeleton()
        }
    }
}
