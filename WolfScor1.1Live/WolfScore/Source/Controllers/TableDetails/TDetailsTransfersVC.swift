//
//  TDetailsTransfersVC.swift
//  WolfScore
//
//  Created by mac on 16/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import CommonCrypto
import SJSegmentedScrollView
import GoogleMobileAds

class TDetailsTransfersVC: UIViewController {
    
    @IBOutlet weak var tblTransfers: UITableView!
    @IBOutlet weak var lblNoRecodFound: UILabel!
    @IBOutlet weak var viewBannerBottom: UIView!
    
    var arrTransfers = [TransferModelClass]()
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!
    var isLoadData = false
    var placeholderRow = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblTransfers.backgroundColor = UIColor.colorConstant.appDarkBlack
        
        self.bannerAdSetup()
    }
    
    func callApiSelfTDetailsTransfersVC()
    {
        self.call_Webservice_Get_Team_Details(str_team_id: objAppShareData.str_team_Id)
    }

    
}

//MARK: - tableView delegate method
extension TDetailsTransfersVC:UITableViewDelegate,UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoadData == false
        {
            return placeholderRow
        }
        return arrTransfers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoadData == false
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "TarnsferTableCell", for: indexPath) as? TarnsferTableCell{
                cell.show_skelton()
                return cell
            }
        }
        
        let cellIdentifier = "TarnsferTableCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TarnsferTableCell
        cell.hide_skelton()
        cell.lblPosition.backgroundColor = UIColor(red: 1/255.0, green: 169/255.0, blue: 244/255.0, alpha: 1.0)
        let objTransfers = arrTransfers[indexPath.row]
        
        if objTransfers.days == "-1"{
            cell.lblDays.text = "Yesterday"
        }else if  objTransfers.days == "0"{
            cell.lblDays.text = "Today"
        }else{
            cell.lblDays.text = objTransfers.days.replacingOccurrences(of: "-", with: "") + " days ago"
        }
        
        cell.lblTeamName.text = objAppShareData.str_team_Name
        cell.lblPlayerName.text = objTransfers.player_name
        cell.lblPosition.text = objTransfers.position
        
        if let url = URL(string: objTransfers.player_logo_path){
            cell.imgViewPlayer.af_setImage(withURL: url, placeholderImage: UIImage(named: "circle_goal_icon"))
        }
        
        if let url = URL(string: objAppShareData.str_team_Logo){
            cell.imgViewTeam.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
        }
        cell.btnProfile.tag = indexPath.row
        cell.btnProfile.addTarget(self, action: #selector(btnShowPlayerProfile(_:)), for: .touchUpInside)
        if objTransfers.in_out_status == "IN"{
            cell.imgViewInOut.image = UIImage.init(named: "in_ico")
        }else{
            cell.imgViewInOut.image = UIImage.init(named: "out_ico")
        }
        
        return cell
    }
    @objc func btnShowPlayerProfile(_ sender: UIButton){
        let objPlayer = self.arrTransfers[sender.tag]
        print(sender.tag)
        objAppShareData.str_player_Id = objPlayer.player_id
        let viewController = UIStoryboard(name: "TableDetails",bundle: nil).instantiateViewController(withIdentifier: "PlayerDetailVC") as! PlayerDetailVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension TDetailsTransfersVC{
    
    func call_Webservice_Get_Team_Details(str_team_id:String){
        //SVProgressHUD.show()
        
        let paramDict = ["team_id":str_team_id,"type":"transfer"] as [String:AnyObject]
        
        print(paramDict)
        
        objWebServiceManager.requestGet(strURL: webUrl.get_team_detail, params: paramDict, success: { (response) in
            self.isLoadData = true
          
            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let dataDict = response["data"] as? [String:Any] {
                    if let data = dataDict["data"] as? [String:Any] {
                        if let transfers = data["transfers"] as? [String:Any] {
                            if let data = transfers["data"] as? [[String:Any]]{
                                for  obj in data{
                                    let object =  TransferModelClass.init(dict: obj)
                                    if object.days != "" {
                                        let diff = Int(object.days) ?? 0
                                        if  diff < 1 && diff > -101 {
                                            self.arrTransfers.append(object)
                                        }
                                        
                                        //                                        if object.days == "Yesterday"{
                                        //                                            self.arrTransfers.append(object)
                                        //                                        }else if  object.days == "Today"{
                                        //                                            self.arrTransfers.append(object)
                                        //                                        }else if  object.days == "Tomorrow"{
                                        //
                                        //                                        }else{
                                        //                                            let diff = Int(object.days) ?? 0
                                        //                                            if  diff < 1 && diff > -101 {
                                        //                                                self.arrTransfers.append(object)
                                        //                                            }
                                        //                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                self.arrTransfers = self.arrTransfers.sorted(by: { Int($0.days)! > Int($1.days)! })
                if self.arrTransfers.count == 0{
                    self.lblNoRecodFound.isHidden = false
                }else{
                    self.lblNoRecodFound.isHidden = true
                }
                self.tblTransfers.reloadData()
            }
            else{
                if self.arrTransfers.count == 0{
                    self.lblNoRecodFound.isHidden = false
                }else{
                    self.lblNoRecodFound.isHidden = true
                }
            }
        }) { (error) in
            print(error)
            self.isLoadData = true
            SVProgressHUD.dismiss()
        }
    }
}

extension TDetailsTransfersVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}


extension TDetailsTransfersVC: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return tblTransfers
    }
}
