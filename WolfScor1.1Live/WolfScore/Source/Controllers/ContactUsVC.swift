//
//  ContactUsVC.swift
//  WolfScore
//
//  Created by Mindiii on 7/2/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsVC: UIViewController ,MFMailComposeViewControllerDelegate
{
    
    @IBOutlet var lblContactDesc:UILabel!
    @IBOutlet var txtFieldEmail:UITextField!

    var str_ContactDescription:String = ""
    var str_MailId:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        lblContactDesc.text = leftVC.strContactUsDescription
        txtFieldEmail.text = leftVC.strMail
    }
    
    
    @IBAction func btnContactUs(_ sender : UIButton){
        self.view.endEditing(true)
        let mailComposeViewController = configureMailComposer()
        if MFMailComposeViewController.canSendMail(){
            self.present(mailComposeViewController, animated: true, completion: nil)
        }else{
            print("Can't send email")
        }

    }
    func configureMailComposer() -> MFMailComposeViewController{
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self
        mailComposeVC.setToRecipients(["\(leftVC.strMail)"])
        mailComposeVC.setSubject("")
        //mailComposeVC.setMessageBody(self.textViewBody.text!, isHTML: false)
        return mailComposeVC
    }
    
    //MARK: - MFMail compose method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)        
        let storyboard = UIStoryboard(name: "UserTabbar", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "UserTabbarVC") as! UserTabbarVC
        
        self.slideMenuController()?.changeMainViewController(mainViewController, close: true)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.slideMenuController()?.leftPanGesture?.isEnabled = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.slideMenuController()?.leftPanGesture?.isEnabled = true
    }
    
  
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
