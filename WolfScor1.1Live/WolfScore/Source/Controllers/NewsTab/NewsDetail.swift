//
//  NewsDetail.swift
//  WolfScore
//
//  Created by Mindiii on 5/20/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SVProgressHUD

class NewsDetail: UIViewController  {
    
    var arrObjDetail = [ModelNewsList]()

    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!

    var news_id: String = ""

    @IBOutlet weak var tbl_view: UITableView!
    @IBOutlet weak var viewBannerBottom: UIView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.bannerAdSetup()
        
        tbl_view.register(UINib(nibName: "kGADAdSizeMediumCell", bundle: Bundle.main), forCellReuseIdentifier: "kGADAdSizeMediumCell")
        
        tbl_view.estimatedRowHeight = 820.0
        tbl_view.rowHeight = UITableViewAutomaticDimension
        
        self.call_Webservice_Get_NewsDetail(newsId: news_id)
    }
  
    // MARK:- IBAction
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
}


// MARK: - TableView Delegates & Datasource
extension NewsDetail: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
      
        return arrObjDetail.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NewsHighlightCell", for: indexPath) as? NewsHighlightCell
        {
            if let url = URL(string: arrObjDetail[indexPath.row].image){
                
                cell.ImgView.af_setImage(withURL: url, placeholderImage: UIImage(named: "NoImage"))
        }
            
            cell.lbl_Newstitle.text = arrObjDetail[indexPath.row].title
            
            cell.lbl_Time.text = arrObjDetail[indexPath.row].date_at_time
            
            
            bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
            bannerViewDfp.adUnitID = objAppShareData.BannerId
            bannerViewDfp.rootViewController = self
            bannerViewDfp.load(DFPRequest())
            bannerViewDfp.frame = CGRect(x:(SCREEN_WIDTH - bannerViewDfp.frame.size.width)/2,
                                         y: 5,
                                         width: bannerViewDfp.frame.size.width,
                                         height: bannerViewDfp.frame.size.height)
            cell.View_Banner.center = bannerView.center
            cell.View_Banner.addSubview(bannerViewDfp)
            
            cell.lbl_body.text = arrObjDetail[indexPath.row].body
            

            return cell
        }
        return UITableViewCell()
        
    }
    
}
// MARK: - Webservice
extension NewsDetail
{
    
    func call_Webservice_Get_NewsDetail(newsId: String) {
        SVProgressHUD.show()
        let paramDict = ["news_id":newsId] as [String:Any]

        objWebServiceManager.requestGet(strURL: webUrl.news_Detail, params: paramDict as [String : AnyObject], success: { (response) in
            
            SVProgressHUD.dismiss()
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                if let data = response["data"] as? [String:Any] {
                    
                    if let infoDic = data["\(self.news_id)"] as? [String:Any]
                    {
                        if let infoDetail = infoDic["info"] as? [String:Any]
                        {
                            let obj = ModelNewsList.init(fromDictionary: infoDetail)
                            self.arrObjDetail.append(obj)
                            self.tbl_view.reloadData()

                        }
                    }
                }
            }else{
                objAlert.showAlert(message: message, title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}

extension NewsDetail:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
        
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}




