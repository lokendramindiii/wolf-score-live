//
//  NewsVC.swift
//  WolfScore
//
//  Created by Mindiii on 28/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.

import UIKit
import SVProgressHUD
import GoogleMobileAds

class NewsVC: UIViewController {

    @IBOutlet weak var tbl_view: UITableView!
    @IBOutlet weak var viewBannerBottom: UIView!

    var arrAllNews = [ModelNewsList]()
    var arrFinalAllNews = [ModelNewsList]()

    var pullToRefreshCtrl:UIRefreshControl!
    var Ispulltorefresh = false

    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!

    weak var timer: Timer?
   
    var currentPageIndex = 1
    var totalPages = 0
    
    var isLoadData = false
    var placeholderRow = 10
    
    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tbl_view.tableFooterView = UIView()
            } else {
                self.tbl_view.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       // SVProgressHUD.show()
        setPullToRefresh()
        
        tbl_view.register(UINib(nibName: "kGADAdSizeMediumCell", bundle: Bundle.main), forCellReuseIdentifier: "kGADAdSizeMediumCell")
        
        tbl_view.estimatedRowHeight = 420.0
        tbl_view.rowHeight = UITableViewAutomaticDimension

        self.bannerAdSetup()
        self.call_Webservice_Post_NewsFeed()
        
    }
    
    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tbl_view.refreshControl = pullToRefreshCtrl
        }else{
            tbl_view.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        sender.endRefreshing()
       // SVProgressHUD.show()
        
        self.currentPageIndex = 1
        self.call_Webservice_Post_NewsFeed()
        sender.endRefreshing()
     }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        selected_my_Index = 2
        
        if APP_DELEGATE.isInterstialPresent == true
        {
            return
        }
        objAppShareData.isLonchXLPageIndex = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnMenuAction(_ sender : UIButton)
    {
        self.view.endEditing(true)
        self.slideMenuController()?.toggleLeft()
    }
}
// MARK: - TableView Delegates & Datasource
extension NewsVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isLoadData == false
        {
            return placeholderRow
        }
        else
        {
            return arrFinalAllNews.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadData == false
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as? NewsCell{
                
                cell.show_skelton()
                return cell
            }
        }
        
        let objModel = self.arrFinalAllNews[indexPath.row]
        if indexPath.row == 0
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "NewsHighlightCell", for: indexPath) as? NewsHighlightCell{
                
                cell.hide_skelton()
                cell.lbl_Newstitle.text = objModel.title
                cell.lbl_Time.text = objModel.dateTimeManual
                if let url = URL(string: objModel.image){
                    cell.ImgView.af_setImage(withURL: url, placeholderImage: UIImage(named: "NoImage"))
                }
                return cell
            }
        }
      else  if objModel.title == "" && objModel.dateTime == ""
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "kGADAdSizeMediumCell", for: indexPath) as! kGADAdSizeMediumCell
            
            for subview in cell.GoogleBannerView.subviews {
                subview.removeFromSuperview()
            }
            
            let dfrequest:DFPRequest = DFPRequest()
            dfrequest.testDevices = [kstrDeviceId,kGADSimulatorID]
            bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
            bannerViewDfp.adUnitID = objAppShareData.BannerId
            bannerViewDfp.rootViewController = self
            bannerViewDfp.load(DFPRequest())
            bannerViewDfp.frame = CGRect(x:(SCREEN_WIDTH - bannerViewDfp.frame.size.width)/2,
                                         y: 10,
                                         width: bannerViewDfp.frame.size.width,
                                         height: bannerViewDfp.frame.size.height+20)
            
            cell.GoogleBannerView.addSubview(bannerViewDfp)
            return cell

        }
        else
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as? NewsCell{
                
                    cell.hide_skelton()
                    cell.lbl_Newstitle.text = objModel.title
                    cell.lbl_Time.text = objModel.dateTimeManual
                    
                    if let url = URL(string: objModel.image){
                        cell.ImgView.af_setImage(withURL: url, placeholderImage: UIImage(named: "inactive_news_ico"))
                    }
                    else
                    {
                        cell.ImgView.image = UIImage(named: "NoImage")
                    }
                    return cell
            }
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (arrFinalAllNews.count - 1) && shouldLoadMore == true {
            self.shouldLoadMore = false
            //SVProgressHUD.show()
            self.call_Webservice_Post_NewsFeed()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isLoadData == false
        {
           return
        }
       let objNewsDetail = self.arrFinalAllNews[indexPath.row]

        if objNewsDetail.title == "" && objNewsDetail.dateTime == ""
        {
            return
        }
        
        let viewController = UIStoryboard(name: "UserTabbar",bundle: nil).instantiateViewController(withIdentifier: "NewsDetail") as! NewsDetail
        
        viewController.news_id = self.arrFinalAllNews[indexPath.row].uri_newsId
        
       self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// MARK: - Webservice
extension NewsVC{
    
    func call_Webservice_Post_NewsFeed(){
        
        self.isLoadData = false
    
        let paramDict = [ "keyword":"football" , "page":currentPageIndex] as [String:Any]

        objWebServiceManager.requestPost(strURL: webUrl.news_feed, params: paramDict, success: { (response) in
             print(response)
            self.isLoadData = true
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    if let results = data["articles"] as? [String:Any]
                    {
                        if let pages = results["pages"] as? Int
                        {
                            self.totalPages = pages
                        }
                        
                        if self.currentPageIndex == 1
                        {
                            self.arrAllNews.removeAll()
                            self.arrFinalAllNews.removeAll()
                            self.tbl_view.reloadData()
                        }
                        self.arrAllNews.removeAll()
                        
                        //NewsList
                        if let NewsList = results["results"] as? [[String: Any]]{
                            
                            for dict in NewsList{
                                let obj = ModelNewsList.init(fromDictionary: dict)
                                self.arrAllNews.append(obj)
                            }
                            
                            for  obj in self.arrAllNews
                            {
                                let results = self.arrFinalAllNews.filter { $0.title == obj.title }
                                if results.count == 0
                                {
                                    self.arrFinalAllNews.append(obj)
                                }
                            }
                        }
                        
                        
                        var intAfterThreebannerShow:Int
                        intAfterThreebannerShow = 0
                        for  i  in 0..<self.arrFinalAllNews.count{
                            intAfterThreebannerShow = intAfterThreebannerShow + 1
                            
                            if self.arrFinalAllNews[i].title != "" && self.arrFinalAllNews[0].dateTimeManual != ""
                            {
                                if intAfterThreebannerShow % 4 == 0 {
                                    let value = ModelNewsList.init(fromDictionary: [:])
                                    self.arrFinalAllNews.insert(value, at: i)
                                }
                            }
                        }

                        if self.currentPageIndex  < self.totalPages
                        {
                            self.currentPageIndex += 1
                            self.shouldLoadMore = true
                        }
                        
                        self.tbl_view.reloadData()
                    }
                }
                else{
                    
                    let message = response["message"] as? String ?? ""
                    objAlert.showAlert(message: message, title: "Alert", controller: self)
                }
                SVProgressHUD.dismiss()
            }
            else{
            SVProgressHUD.dismiss()
            }

        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}

extension NewsVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
    // In this case, we instantiate the banner with desired ad size.

        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        //request.testDevices = [kGADSimulatorID,kUnique_Device_Token.lowercased()]
        bannerView.load(request)
        

        //  bannerView.load(GADRequest())
        
        let dfrequest:DFPRequest = DFPRequest()
        bannerView.delegate = self
        dfrequest.testDevices = [kstrDeviceId,kGADSimulatorID]
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self

        bannerViewDfp.load(DFPRequest())

        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)

    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}
