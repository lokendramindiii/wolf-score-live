//
//  NewsCell.swift
//  WolfScore
//
//  Created by Mindiii on 5/20/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView
class NewsCell: UITableViewCell {

    @IBOutlet weak var lbl_Newstitle: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var ImgView: UIImageView!
   // @IBOutlet weak var GoogleBannerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func show_skelton()
    {
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))

        [lbl_Newstitle,lbl_Time].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
        [ImgView].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
    }
    
    
    func hide_skelton()
    {
        [lbl_Newstitle,lbl_Time].forEach { $0?.hideSkeleton()
        }
        [ImgView].forEach { $0?.hideSkeleton()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
