//
//  UpdateProfileVC.swift
//  WolfScore
//
//  Created by Mindiii on 5/11/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import Toaster
import SVProgressHUD

class UpdateProfileVC: UIViewController  ,UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtView_about: UITextView!

    @IBOutlet var txt_DateOfBirth: UITextField!
    
    @IBOutlet var imgProfileImage: UIImageView!
    @IBOutlet var btnImgProfile: UIButton!
    @IBOutlet var lbl_userName: UILabel!

    @IBOutlet var imgMale: UIImageView!
    @IBOutlet var imgFemale: UIImageView!
    
    @IBOutlet var datePicker: UIDatePicker!
    
    @IBOutlet var View_DatePicker: UIView!
    
    var isFromprofile:Bool = false
    
    var strUserName:String = ""
    var strAbout:String = ""
    var strGender:String = ""
    var strDateOfBirth:String = ""

    
    var imageL : UIImage = UIImage()
    var imagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self

        self.txt_PlaceholderColor()
        
        let defaults = UserDefaults.standard
        lbl_userName.text = defaults.string(forKey: UserDefaults.Keys.kUserName)
        
        txtUserName.text = defaults.string(forKey: UserDefaults.Keys.kUserName)

        
        let textAbout = defaults.string(forKey: UserDefaults.Keys.kabout)
        
        if textAbout == "Bio" ||  textAbout == ""
        {
            txtView_about.text = "Bio"
            txtView_about.textColor = UIColor.colorConstant.appLightGrayColor
        }
        else{
            txtView_about.textColor = UIColor.white
            txtView_about.text = textAbout
        }
        
        if txtView_about.text == "Bio"
        {
            txtView_about.textColor = UIColor.colorConstant.appLightGrayColor
        }
        
        if defaults.string(forKey: UserDefaults.Keys.kDOB) == "0000-00-00"
        {
           txt_DateOfBirth.text = ""
        }
        else
        {
            txt_DateOfBirth.text = defaults.string(forKey: UserDefaults.Keys.kDOB)
        }

        let strGetGender = defaults.string(forKey: UserDefaults.Keys.kGender)
        if strGetGender == "Male" {
            
            strGender = "Male"
            imgMale.image = UIImage(named: "icon_active")
            imgFemale.image = UIImage(named: "icon_inactive")
        }
        else if strGetGender == "Female"
        {
            strGender = "Female"
            imgFemale.image = UIImage(named: "icon_active")
            imgMale.image = UIImage(named: "icon_inactive")
        }
        else
        {
            strGender = ""
            imgFemale.image = UIImage(named: "icon_inactive")
            imgMale.image = UIImage(named: "icon_inactive")
        }
        

        let ProfileUrl = defaults.string(forKey: UserDefaults.Keys.kUserProfileurl)
        if let url = URL(string: ProfileUrl ?? ""){
            imgProfileImage.af_setImage(withURL: url, placeholderImage: UIImage(named: "profile_updateicon"))
        }
        
        self.setDoneOnKeyboard()
    }
    
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(UpdateProfileVC.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.txtView_about.inputAccessoryView = keyboardToolbar
    }
    
    @objc func dismissKeyboard() {
        
        if txtView_about.text == ""{
            txtView_about.text = "Bio"
            txtView_about.textColor = UIColor.colorConstant.appLightGrayColor
        }
        view.endEditing(true)
    }
    
    
    func txt_PlaceholderColor()  {
        txtUserName.attributedPlaceholder = NSAttributedString(string:"Username", attributes: [NSAttributedStringKey.foregroundColor:UIColor.colorConstant.appLightGrayColor])
        
        txt_DateOfBirth.attributedPlaceholder = NSAttributedString(string:"Date Of Birth", attributes: [NSAttributedStringKey.foregroundColor:UIColor.colorConstant.appLightGrayColor])
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Button Actions
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBirthDateAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if txtView_about.text == ""
        {
            txtView_about.text = "Bio"
            txtView_about.textColor = UIColor.colorConstant.appLightGrayColor
        }
        self.View_DatePicker.isHidden = false
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())
        
         datePicker.setDate(datePicker.maximumDate!, animated: true)

    }
    
    @IBAction func btnPickerAction(_ sender: UIButton) {
        if sender.tag == 0
        {

            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            txt_DateOfBirth.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
            self.View_DatePicker.isHidden = true
        }
        else
        {
            self.view.endEditing(true)
            self.View_DatePicker.isHidden = true
        }
    }
    
    
    @IBAction func btnGenderAction(_ sender: UIButton) {
        
        if sender.tag == 0
        {
            strGender = "Male"
            imgMale.image = UIImage(named: "icon_active")
            imgFemale.image = UIImage(named: "icon_inactive")
        }
        else
        {
            strGender = "Female"
            imgFemale.image = UIImage(named: "icon_active")
            imgMale.image = UIImage(named: "icon_inactive")
        }
        
    }
    
    @IBAction func btnImageSelectAction(_ sender: UIButton) {
        self.view .endEditing(true)
        self.isFromprofile = true
        self.imageSelection()
    }
    
    func imageSelection(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
   
}
//MARK: - UIImagePicker extension Methods
extension UpdateProfileVC{
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if self.isFromprofile == true {
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
                let img = image.fixedOrientation()
                self.imgProfileImage.image = img
                self.btnImgProfile.setImage(nil, for: .normal)
                
            }
            else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                let img = image.fixedOrientation()
                self.imgProfileImage.image = img
                self.btnImgProfile.setImage(nil, for: .normal)
                
            }
            else{  }
            self.dismiss(animated: true, completion: nil)
        }else{
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
                let img = image.fixedOrientation()
                self.imageL = img
            }
            else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                let img = image.fixedOrientation()
                self.imageL = img
            }
            else{  }
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController)
    { dismiss(animated: true, completion: nil)
    }
    
    //to Access Camera
    func openCamera()
    {  if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
    {  imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        self .present(imagePicker, animated: true, completion: nil) }
    else {  }
    }
    func openGallary() //to Access Gallery
    {    imagePicker.allowsEditing = true
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil) }
}



extension UpdateProfileVC {
    @IBAction func btnUpdateAction(_ sender: UIButton) {
        self.view.endEditing(true)
        var strMessage:String = ""
        var inValid:Bool = false
        
        let heightInPoints = self.imgProfileImage.image!.size.height
        let heightInPixels = heightInPoints * self.imgProfileImage.image!.scale
        
        let widthInPoints = self.imgProfileImage.image!.size.width
        let widthInPixels = widthInPoints * self.imgProfileImage.image!.scale

        print(heightInPixels)
        print(widthInPixels)

        
//        if self.imgProfileImage.image == nil || self.imgProfileImage.image == UIImage(named: "profile_updateicon")
//        {
//            inValid=true
//            strMessage = "Please upload your profile photo"
//            Toast(text: strMessage).show()
//        }
        
          if (txtUserName.text?.isEmpty)! {
            //objIndicator.StopIndicator()
            inValid=true
            objValidationManager.AnimationShakeTextField(textField: txtUserName)
            strMessage = "Username can't be empty"
            Toast(text: strMessage).show()
        }
        
//        else if (txtView_about.text?.isEmpty)! || txtView_about.text == "Bio"   {
//            inValid=true
//            txtView_about.text = "Bio"
//            txtView_about.textColor = UIColor.colorConstant.appLightGrayColor
//            strMessage = "Bio can't be empty"
//            Toast(text: strMessage).show()
//        }
//
//        else if (txt_DateOfBirth.text?.isEmpty)! {
//
//            inValid=true
//        objValidationManager.AnimationShakeTextField(textField: txt_DateOfBirth)
//            strMessage = "Please select Date Of Birth"
//            Toast(text: strMessage).show()
//        }
//        else if strGender == "" {
//            inValid=true
//            strMessage = "Please select Gender"
//            Toast(text: strMessage).show()
//        }
        
        if (inValid && strMessage.count==0){
            //objIndicator.StopIndicator()
            strMessage = "Please Fill All Field"
        }else if (!inValid) {
            inValid=true
            call_WebserviceFor_UpdateProfile()
        }
    }
}

// MARK: - Webservice Calls
extension UpdateProfileVC {
    
    func call_WebserviceFor_UpdateProfile(){
        
        if self.imgProfileImage.image == nil || self.imgProfileImage.image == UIImage(named: "profile_updateicon") || self.isFromprofile == false
        {
            self.call_WebserviceFor_UpdateProfileWithoutImage()
            return
        }
        SVProgressHUD.show()
        strUserName = self.txtUserName.text!
        strAbout = self.txtView_about.text!
        
        strAbout = strAbout.trimmingCharacters(in: .whitespacesAndNewlines)


        strDateOfBirth = self.txt_DateOfBirth.text!
        var param = [String:Any]()
        param = ["name":checkForNULL(obj: strUserName),
                 "about":checkForNULL(obj: strAbout),
                 "gender":strGender,
                 "birthday":checkForNULL(obj: strDateOfBirth)
        ]
        
        let img = self.imgProfileImage.image?.fixedOrientation();
        var data = UIImageJPEGRepresentation(img!,1.0)! as NSData
        
        if self.imgProfileImage.image == UIImage(named:"26722")! {
        }else{
            data = UIImageJPEGRepresentation(img!, 1.0)! as NSData
        }
        
        objWebServiceManager.uploadMultipartData(strURL: webUrl.update_profile, params: param as [String : AnyObject], imageData: data as Data, fileName: "profile_photo", mimeType: "image/jpg", success: { (response) in
            let message =  response["message"] as? String ?? ""
            
            let status = response["status"] as? String ?? ""
            
            if status == "success" || status == "SUCCESS"
            {
                let dictUserDetail = response["data"] as! Dictionary<String, Any>
                  self.saveDataInDevice ( dict:dictUserDetail )
                Toast(text: message).show()
            self.navigationController?.popViewController(animated: true)
                
            } else if status == "fail" || status == "FAIL"{
                Toast(text: message).show()
            }
            SVProgressHUD.dismiss()
        }) { (error) in
            
            SVProgressHUD.dismiss()
            if (error.localizedDescription.contains("The network connection was lost.")){
                self.call_WebserviceFor_UpdateProfile()
            }
            else
            {
            }
        }
    }
    
    
    func call_WebserviceFor_UpdateProfileWithoutImage()
    {
        
        SVProgressHUD.show()
        strUserName = self.txtUserName.text!
        strAbout = self.txtView_about.text!
        strAbout = strAbout.trimmingCharacters(in: .whitespacesAndNewlines)

        strDateOfBirth = self.txt_DateOfBirth.text!
        var param = [String:Any]()
        param = ["name":checkForNULL(obj: strUserName),
                 "about":checkForNULL(obj: strAbout),
                 "gender":strGender,
                 "birthday":checkForNULL(obj: strDateOfBirth),
                 "profile_photo":""
        ]
        
        objWebServiceManager.requestPost(strURL: webUrl.update_profile, params: param, success: { (response) in
            SVProgressHUD.dismiss()
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success" || status == "SUCCESS"{
                
                let dictUserDetail = response["data"] as! Dictionary<String, Any>
                self.saveDataInDevice ( dict:dictUserDetail )
                Toast(text: message).show()
                self.navigationController?.popViewController(animated: true)
                
            }else if status == "fail" || status == "FAIL"{
                Toast(text: message).show()
                
            }else if status == "authFail"{
                Toast(text: message).show()
                
            }
        }) { (errore) in
            SVProgressHUD.dismiss()
            if (errore.localizedDescription.contains("The network connection was lost.")){
                self.call_WebserviceFor_UpdateProfileWithoutImage()
            }else{
                //objIndicator.StopIndicator()
                //objAlertVc.showAlert(message: FailAPI , title: kAlertTitle, controller: self)
            }
        }
    }

}

// MARK: - Textfield Delegate Methods
extension UpdateProfileVC : UITextFieldDelegate {
 
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.View_DatePicker.isHidden = true
        if txtView_about.text == ""
        {
            txtView_about.text = "Bio"
            txtView_about.textColor = UIColor.colorConstant.appLightGrayColor
        }
    }
    
    // MARK: - Textfield Delegate Methods
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtUserName {
            txtView_about.becomeFirstResponder()
        }
        else
        {
            txtView_about.resignFirstResponder()
        }
        return true
    }
}

// MARK:-Save Data
extension UpdateProfileVC {
    
    func saveDataInDevice(dict: Dictionary<String, Any>) {
        
        let defaults = UserDefaults.standard
        if (defaults.string(forKey: UserDefaults.Keys.kSign) != nil)
        {
        }
        defaults.set(dict["userId"], forKey: UserDefaults.Keys.kUserId)
        defaults.set(dict["name"], forKey: UserDefaults.Keys.kUserName)
        defaults.set(dict["auth_token"], forKey: UserDefaults.Keys.kAuthToken)
        defaults.set(dict["email"], forKey: UserDefaults.Keys.kEmail)
        
        defaults.set(dict["gender"], forKey: UserDefaults.Keys.kGender)
        defaults.set(dict["about"], forKey: UserDefaults.Keys.kabout)
        defaults.set(dict["birthday"], forKey: UserDefaults.Keys.kDOB)

        
        let profleurl = dict["profile_url"]
        let profile_photo = dict["profile_photo"]
        
        let finalprofileurl = "\(profleurl ?? "")\(profile_photo ?? "")"
        
        defaults.set(finalprofileurl, forKey: UserDefaults.Keys.kUserProfileurl)
        
        defaults.set(dict["social_id"], forKey: UserDefaults.Keys.kSocialId)
        defaults.set(dict["device_token"], forKey: UserDefaults.Keys.kUnique_Device_Token)
        UserDefaults.standard.synchronize()
        
        lbl_userName.text = defaults.string(forKey: UserDefaults.Keys.kUserName)
        
    }
    
    func hideDropdownWithOutAnimation()
    {
        //  constraintsDropdownBottom.constant = -220
        view.superview?.layoutIfNeeded()
    }
}


extension UpdateProfileVC : UITextViewDelegate
{
    //MARK: - uitextview extension
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
//        if(text == "\n") {
//            textView.resignFirstResponder()
//            return false
//        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 200
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtView_about.text == "Bio"
        {
            txtView_about.text = ""
        }
        txtView_about.textColor = UIColor.white
    }
}

