//
//  StaticPagesViewController.swift
//  WolfScore
//
//  Created by Mindiii on 7/2/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class StaticPagesViewController: UIViewController,UIWebViewDelegate
{
    
    @IBOutlet weak var webView : UIWebView!
    @IBOutlet weak var loader : UIActivityIndicatorView!
    @IBOutlet weak var lblHeader : UILabel!
    
    var strUrl : String?
    var strPageType : String?
    var isfromMyProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
  
    override func viewDidAppear(_ animated: Bool) {
        
        if leftVC.PageType == "Privacy Policy"
        {
            lblHeader.text = "Privacy Policy"
            strUrl = leftVC.strPrivacyUrl
        }
        else if leftVC.PageType == "Terms & Conditions"
        {
            lblHeader.text = "Terms & Conditions"
            strUrl = leftVC.strTermsUrl
        }
        
        self.loadWebView()

    }
    
    func loadWebView(){
        guard let url = strUrl ,let finalUrl = URL(string: url) else{return}
        loader.startAnimating()
        webView.loadRequest(URLRequest(url: finalUrl))
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func webViewDidFinishLoad(_ webView : UIWebView) {
        loader.stopAnimating()
    }
    
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        
        if isfromMyProfile == true
        {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        
        let storyboard = UIStoryboard(name: "UserTabbar", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "UserTabbarVC") as! UserTabbarVC
        
    self.slideMenuController()?.changeMainViewController(mainViewController, close: true)
    }
 
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        self.navigationController?.isNavigationBarHidden = true
        self.slideMenuController()?.leftPanGesture?.isEnabled = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.slideMenuController()?.leftPanGesture?.isEnabled = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

