//
//  MatchsVC.swift
//  WolfScore
//
//  Created by Mindiii on 28/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import FSCalendar
import SlideMenuControllerSwift
import GoogleMobileAds
import SVProgressHUD

struct MatchsVcVariable{
    static var strfilter = ""
    static var strTotalRecord = ""
    static var arrayPopulerLeague = [String]()
}


class MatchsVC: ButtonBarPagerTabStripViewController, UITextFieldDelegate,FSCalendarDelegate,FSCalendarDataSource,UIGestureRecognizerDelegate {

    
    var arrAllSequenceLeauge = [ModelLeagueList]()

    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewCalendarBg: UIView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblSelectedDete: UILabel!
    @IBOutlet weak var lblDay: UILabel!

    var todayVC : TodayVC!
    var yesterdayVC : YesterdayVC!
    var tomorroowVC : TomorrowVC!
    var slide : SlideMenuController!
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        if APP_DELEGATE.isFromNotification == true
        {
            let viewController = UIStoryboard(name: "MatchesTab",bundle: nil).instantiateViewController(withIdentifier: "MatchDetailMainVC") as! MatchDetailMainVC
            self.navigationController?.pushViewController(viewController, animated: false)
        }
        
        if objAppShareData.arrLeagueForFinalSorting.count == 0
        {
            self.call_Webservice_GetleagueFilterSequencelist()
        }

        
        self.call_Webservice_For_GetPopulerLeagueAndCount()
        
        self.viewCalendarBg.isHidden = true
        self.viewSearch.layer.cornerRadius = self.viewSearch.frame.size.height/2
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Search", tableName: nil, comment: ""), attributes: [NSAttributedStringKey.foregroundColor:UIColor.white
            ])
        

        settings.style.buttonBarBackgroundColor = UIColor.colorConstant.appDarkBlack
        settings.style.buttonBarItemBackgroundColor = UIColor.colorConstant.appDarkBlack
        settings.style.buttonBarItemFont = UIFont(name: "roboto-Medium" , size: 14.0)!
        settings.style.selectedBarHeight = 3.0
        settings.style.selectedBarBackgroundColor = UIColor.colorConstant.appBlueColor
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
      
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.colorConstant.appLightBlueColor
            newCell?.label.textColor = UIColor.colorConstant.appLightBlueColor
            oldCell?.label.font = UIFont(name: "roboto-Medium" , size: 14.0)!
            newCell?.label.font = UIFont(name: "roboto-Medium" , size: 14.0)!
        }
        
        self.view.layoutIfNeeded()
       
        self.calendarHeightConstraint.constant = 300
        //self.calendar.setScope(.month, animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        if objAppShareData.isLonchXLPageIndex == true {
            self.reloadPagerTabStripView()
            objAppShareData.isLonchXLPageIndex = false
            self.moveToViewController(at: 1, animated: false)
        }
        else if objAppShareData.isAnotherDateMatchVC == true{
            objAppShareData.isAnotherDateMatchVC = false
            switch (objAppShareData.IndexOfController){
            case "0":
                self.moveToViewController(at: 0, animated: false)
            case "1":
                self.moveToViewController(at: 1, animated: false)
            case "2":
                self.moveToViewController(at: 2, animated: false)
            default:
               print("No case")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selected_my_Index = 0
        configureCalendar(SelectDate: objAppShareData.SelectedCalanderDate)
        //configureCalendar(SelectDate: Date())
        self.viewCalendarBg.isHidden = true
        //self.calendar.selectedDate.color
        let date = Date()
        let date2 = self.dateFormatter2.string(from:date)
        self.lblDay.text = date2
        self.navigationController?.isNavigationBarHidden = true
        self.slideMenuController()?.leftPanGesture?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.slideMenuController()?.leftPanGesture?.isEnabled = true
    }

    
//    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int){
//        print(fromIndex)
//        print(toIndex)
//    }
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool){
        print(fromIndex)
        print(toIndex)
        
//        if fromIndex == 2 && toIndex == 2{
//            self.view.endEditing(true)
//            configureCalendar(SelectDate: Date())
//            self.viewCalendarBg.isHidden = false
//        }else if fromIndex == 0 && toIndex == 0{
//            self.view.endEditing(true)
//            configureCalendar(SelectDate: Date())
//            self.viewCalendarBg.isHidden = false
//        }else{
//            self.view.endEditing(true)
//            configureCalendar(SelectDate: Date())
//            self.viewCalendarBg.isHidden = true
//        }
    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        self.todayVC = UIStoryboard(name: "UserTabbar", bundle: nil).instantiateViewController(withIdentifier: "TodayVC") as? TodayVC
        
        
        self.yesterdayVC = UIStoryboard(name: "UserTabbar", bundle: nil).instantiateViewController(withIdentifier: "YesterdayVC") as? YesterdayVC
        
       
        self.tomorroowVC = UIStoryboard(name: "UserTabbar", bundle: nil).instantiateViewController(withIdentifier: "TomorrowVC") as? TomorrowVC
        
        
        return [self.yesterdayVC, self.todayVC, self.tomorroowVC]
    }
    
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.slideMenuController()?.toggleLeft()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func btnFilterAction(_ sender : UIButton){
        self.view.endEditing(true)
        
        let sb = UIStoryboard(name: "UserTabbar", bundle: nil)
        let FilterNav = sb.instantiateViewController(withIdentifier: "FilterVCNav") as! UINavigationController
        
        let filterLeague = FilterNav.viewControllers[0] as! FilterLeagueVC1
        
        DispatchQueue.main.async{
            self.present(FilterNav, animated:true, completion: nil)
        }
        
    }
    @IBAction func btnCalanderAction(_ sender : UIButton){
        self.view.endEditing(true)
        //objAlert.showAlert(message: "Under Development", title: "Alert", controller: self)
        configureCalendar(SelectDate: objAppShareData.SelectedCalanderDate)
        //configureCalendar(SelectDate: Date())
        self.viewCalendarBg.isHidden = false
    }
    @IBAction func btnCalanderCancelAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.viewCalendarBg.isHidden = true
    }
    
    @IBAction func btnCalanderTodayAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.reloadPagerTabStripView()
        objAppShareData.SelectedCalanderDate = Date()
        self.moveToViewController(at: 1, animated: false)
        self.viewCalendarBg.isHidden = true
    }
    
    // UITextField Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchTeamListVC") as! SearchTeamListVC
        
        let sb = UIStoryboard(name: "UserTabbar", bundle: nil)
        let SerachListNav = sb.instantiateViewController(withIdentifier: "SerachListNav") as! UINavigationController
      //  let rootVC = SerachListNav.viewControllers[0] as! SearchTeamListVC
        DispatchQueue.main.async{
            self.present(SerachListNav, animated:true, completion: nil)
        }
 
        return true
    }
    

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        txtSearch.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }

}
extension MatchsVC{
    
    func configureCalendar(SelectDate:Date){
        
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 400
        }
        self.calendar.delegate = self
        self.calendar.dataSource = self
        self.calendar.select(SelectDate)
        self.calendar.calendarHeaderView.isHidden = true
        self.changeCalenderHeader(date: SelectDate)
        self.view.addGestureRecognizer(self.scopeGesture)
        self.calendar.scope = .week
        //self.calendar.cell(for: <#T##Date#>, at: T##FSCalendarMonthPosition)
        // For UITest
        self.calendar.accessibilityIdentifier = "calendar"
        self.calendar.calendarWeekdayView.backgroundColor = UIColor.colorConstant.appDarkBlack
        //self.calendar.minimumDate = self.calendar.maximumDate
        self.calendar.appearance.todayColor = UIColor.lightGray
        
        
    }
    
    
    
    func changeCalenderHeader(date : Date){
        let fmt = DateFormatter()
        fmt.dateFormat = "MMMM yyyy"
        let strCalenderHeader = fmt.string(from: date)
        self.lblSelectedDete.text = strCalenderHeader.uppercased()
    }
    /**
     Tells the delegate the calendar is about to change the bounding rect.
     */
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        
        UIView.animate(withDuration: 0.6, animations: {
            DispatchQueue.main.async {
                self.calendarHeightConstraint.constant = bounds.height
                self.view.layoutIfNeeded()
            }
        })
    }
    
    /**
     Tells the delegate a date in the calendar is selected by tapping.
     */
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        //self.getlatestSlotDataFor(dateSelected: newDate)
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
    }
    
    /**
     Tells the delegate the calendar is about to change the current page.
     */
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.changeCalenderHeader(date: calendar.currentPage)
    }
    /**
     Close past dates in FSCalendar
     */
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at     monthPosition: FSCalendarMonthPosition) -> Bool{
        let strDate1 = self.dateFormatter.string(from: date)
        let date1 = self.dateFormatter.date(from: strDate1)
        objAppShareData.SelectedCalanderDate = date1 ?? Date()
        let day = dayDate.dayDifference(from: (date1?.timeIntervalSince1970)!)
     
        if day == "Yesterday"{
            self.moveToViewController(at: 0, animated: false)
            objAppShareData.lastSelectedDate =  Calendar.current.date(byAdding: .day, value: -1, to: Date()) ?? Date()
            self.viewCalendarBg.isHidden = true
        }else if day == "Today"{
            self.moveToViewController(at: 1, animated: false)
             objAppShareData.lastSelectedDate = Date()
            self.viewCalendarBg.isHidden = true
        }else if day == "Tomorrow"{
            self.moveToViewController(at: 2, animated: false)
            objAppShareData.lastSelectedDate =  Calendar.current.date(byAdding: .day, value: +1, to: Date()) ?? Date()
            self.viewCalendarBg.isHidden = true
        }else{
            objAppShareData.days = Int(day) ?? 0
            let viewController = UIStoryboard(name: "UserTabbar",bundle: nil).instantiateViewController(withIdentifier: "AnotherDateMatchVC") as! AnotherDateMatchVC
            viewController.selectedDate = strDate1
            viewController.selectedDate1 = date1!
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        return true
    }
}

extension MatchsVC
{
    func call_Webservice_For_GetPopulerLeagueAndCount() {
        
        
        let dictPram = ["country":kCurrent_Country] as [String: AnyObject]
        
        objWebServiceManager.requestGet(strURL: webUrl.GetPopular_League_and_count, params: dictPram, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    
                    if let totalRecord = data["total_records"]as? String
                    {
                        MatchsVcVariable.strTotalRecord = totalRecord
                    }
                    
                    if let arrPopulerLeague = data["popular_league"] as? [[String:Any]]
                    {
                        MatchsVcVariable.arrayPopulerLeague.removeAll()
                        for i in 0..<arrPopulerLeague.count
                        {
                            MatchsVcVariable.arrayPopulerLeague.append(arrPopulerLeague[i]["league_id"] as? String ?? "")
                        }
                    }
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    
    
    
    func call_Webservice_GetleagueFilterSequencelist()
    {
        //  SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.get_leagueFilterSequenceList, params: nil, success: { (response) in
            SVProgressHUD.dismiss()
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any]
                {
                    
                    if let arrData = data["league_list"] as? [[String: Any]]{
                        
                        for dict in arrData{
                            let obj = ModelLeagueList.init(fromDictionary: dict)
                            self.arrAllSequenceLeauge.append(obj)
                        }
                        
                        objAppShareData.arrLeagueForFinalSorting.removeAll()
                        var arrSortLeague = [String]()
                        if self.arrAllSequenceLeauge.count != 0
                        {
                            for obj in self.arrAllSequenceLeauge
                            {
                                arrSortLeague.append(obj.leagueId)
                                objAppShareData.arrLeagueForFinalSorting.append(obj)
                            }
                        }
                    }
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    
}

