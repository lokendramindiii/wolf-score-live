//
//  AnotherDateMatchVC.swift
//  WolfScore
//
//  Created by Mindiii on 1/29/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVProgressHUD
import Toaster
import AlamofireImage
import FSCalendar
import GoogleMobileAds
import SkeletonView

class AnotherDateMatchVC: UIViewController,FSCalendarDelegate,FSCalendarDataSource,UIGestureRecognizerDelegate ,UIScrollViewDelegate {
    
    var arrModelLeague = [LeaugeModel]()
    var arrFinalModelLeague = [LeaugeModel]()
    var arrAllData1:[String] = []
    var offSet = 0
    var currentPageIndex = 1
    var totalPages = 0
    var isDidSelectApiCall = false
    
    @IBOutlet weak var viewBannerBottom: UIView!
    @IBOutlet weak var btnAllTab: UIButton!
    @IBOutlet weak var btnFilterTurnament: UIButton!
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var tblMatches: UITableView!
    @IBOutlet weak var lblSelectedDete: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    /// Calander
    @IBOutlet weak var viewCalendarBg: UIView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblSelectedMonth: UILabel!
    var selectedDate = ""
    var selectedDate1 = Date()
    
    var isLoadData = false
    var placeholderRow = 10

    var limit = 5
    var offset = 0
    var isnext = 0
    var isPopulerList = false
    

    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!

    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, yyyy"
        return formatter
    }()
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        return formatter
    }()
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    
    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tblMatches.tableFooterView = UIView()
            } else {
                self.tblMatches.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblMatches.backgroundColor = UIColor.colorConstant.appDarkBlack
        SVProgressHUD.setDefaultMaskType(.clear)
        self.viewCalendarBg.isHidden = true
        configureCalendar(SelectDate: objAppShareData.SelectedCalanderDate)
        let finaldate =  dayDate.dateformateYYYY_MM_DD(strDate: selectedDate)
        selectedDate =  finaldate
        
        self.viewCalendarBg.isHidden = true
        let date1 = self.dateFormatter.string(from: self.selectedDate1)
        let date2 = self.dateFormatter2.string(from: self.selectedDate1)
        self.lblSelectedDete.text = date1
        self.lblDay.text = date2
        self.calendarHeightConstraint.constant = 300
        
        tblMatches.register(UINib(nibName: "kGADAdSizeMediumCell", bundle: Bundle.main), forCellReuseIdentifier: "kGADAdSizeMediumCell")
        
        tblMatches.register(UINib(nibName: "MatchesSkeltonCell", bundle: Bundle.main), forCellReuseIdentifier: "MatchesSkeltonCell")

        
         self.bannerAdSetup()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tblMatches.isUserInteractionEnabled = true
        for i in 0..<12
        {
            let cell = tblMatches.dequeueReusableCell(withIdentifier: "MatchesSkeltonCell")as! MatchesSkeltonCell
            
            cell.hide_skelton()
        }

        if APP_DELEGATE.isInterstialPresent == true || isDidSelectApiCall == true
        {
            isDidSelectApiCall = false
            APP_DELEGATE.isInterstialPresent = false
            return
        }
        isDidSelectApiCall = false
        
        self.viewCalendarBg.isHidden = true
        self.arrFinalModelLeague.removeAll()
        self.tblMatches.reloadData()
        
        self.currentPageIndex = 1
        self.apiCallGetFixtures()
    }

}

//MARK : custom extension
extension AnotherDateMatchVC {
    
    func selectAllTab(){
        self.arrFinalModelLeague.removeAll()
        self.tblMatches.reloadData()
        self.lblNoRecord.isHidden = true
        self.btnAllTab.isHidden = true
        objAppShareData.strselctedMatchTab = "all"
    }
    
    func apiCallGetFixtures()
    {
        
        // skelton flage
        
        self.isPopulerList = false
        for i in 0..<12
        {
            let cell = tblMatches.dequeueReusableCell(withIdentifier: "MatchesSkeltonCell")as! MatchesSkeltonCell
            cell.hide_skelton()
        }
        self.isLoadData = false
        self.tblMatches.isUserInteractionEnabled = false
        if objAppShareData.strselctedMatchTab == ""
        {
            self.limit = 5
            self.offset = 0
            self.arrFinalModelLeague.removeAll()
            self.tblMatches.reloadData()
            self.apiCallGetFixturesListTypeEmpty()
            return
        }
        
        if self.currentPageIndex == 1
        {
            self.arrFinalModelLeague.removeAll()
            self.placeholderRow = 10
            self.tblMatches.reloadData()
        }
        // skelton flage
        
        self.shouldLoadMore = false
        
        let dictPram = ["type":"date",
                        "page":currentPageIndex,
                        "date":selectedDate,
                        "team_id":"",
                        "ongoing":"",
                        "sort_by":objAppShareData.strselctedByTimeTab,
                        "list_type":objAppShareData.strselctedMatchTab,
                        "league_id":objAppShareData.strFilterLeagueId,
                        "time_zone":objAppShareData.localTimeZoneName,
                        "country":"",
                        "limit":"",
                        "offset":""
            ] as [String: AnyObject]
        self.call_Webservice_Get_fixtures(dict_param: dictPram)
    }

    
    func apiCallGetFixturesListTypeEmpty()
    {
        
        self.isPopulerList = true
        
        self.tblMatches.isUserInteractionEnabled = false
        
        for i in 0..<12
        {
            let cell = tblMatches.dequeueReusableCell(withIdentifier: "MatchesSkeltonCell")as! MatchesSkeltonCell
            cell.hide_skelton()
        }
        let dictPram = ["type":"date",
                        "page":currentPageIndex,
                        "date":selectedDate,
                        "team_id":"",
                        "ongoing":"",
                        "sort_by":objAppShareData.strselctedByTimeTab,
                        "list_type":objAppShareData.strselctedMatchTab,
                        "league_id":objAppShareData.strFilterLeagueId,
                        "time_zone":objAppShareData.localTimeZoneName,
                        "country":kCurrent_Country,
                        "limit":limit,
                        "offset":offset
            ] as [String: AnyObject]

        call_Webservice_Get_fixturesListTypeEmpty(dict_param: dictPram)
    }
}

//MARK : @IBAction extension
extension AnotherDateMatchVC {
    
    
    @IBAction func btnFilterTurnamentAction(_ sender : UIButton){
        self.view.endEditing(true)
        let sb = UIStoryboard(name: "UserTabbar", bundle: nil)
        let FilterNav = sb.instantiateViewController(withIdentifier: "FilterVCNav") as! UINavigationController
        self.present(FilterNav, animated:true, completion: nil)
    }
    
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCalanderAction(_ sender : UIButton){
        self.view.endEditing(true)
     
        
        //configureCalendar(SelectDate: Date())
        configureCalendar(SelectDate: objAppShareData.SelectedCalanderDate)
       

        //objAlert.showAlert(message: "Under Development", title: "Alert", controller: self)
        self.viewCalendarBg.isHidden = false
    }
    @IBAction func btnCalanderCancelAction(_ sender : UIButton){
        self.view.endEditing(true)
        //objAlert.showAlert(message: "Under Development", title: "Alert", controller: self)
        self.viewCalendarBg.isHidden = true
    }
    @IBAction func btnTodayAction(_ sender : UIButton){
        self.view.endEditing(true)
        objAppShareData.isLonchXLPageIndex = true
        objAppShareData.IndexOfController = "1"
        objAppShareData.SelectedCalanderDate = Date()
        self.viewCalendarBg.isHidden = true
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnAllTabAction(_ sender : UIButton){
        self.view.endEditing(true)
        if btnAllTab.titleLabel?.text == "Show all matches"
        {
            self.isLoadData = false
            self.arrFinalModelLeague.removeAll()
            self.tblMatches.reloadData()
            self.lblNoRecord.isHidden = true
            self.btnAllTab.isHidden = true
            objAppShareData.strselctedMatchTab = ""
            self.limit = 5
            self.offset = 0
            self.isnext = 0
            self.apiCallGetFixturesListTypeEmpty()
        }
        else
        {
        
        self.selectAllTab()
        self.currentPageIndex = 1
        self.apiCallGetFixtures()
        }
    }
    
}


// MARK: - TableView Delegates & Datasource
extension AnotherDateMatchVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isLoadData == false
        {
            return 2
        }
        return self.arrFinalModelLeague.count + 1
      }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        
        //headerView.conten.backgroundColor = UIColor.colorConstant.appSectionHeader
        headerView.backgroundColor = UIColor.colorConstant.appDarkBlack
        if section == 0
        {
            
            let btnShowall = UIButton()
            btnShowall.setTitle(" All Matches", for: .normal)
            btnShowall.setImage(UIImage(named: "star_ico"), for: .normal)
            btnShowall.layer.cornerRadius = 5
            btnShowall.layer.borderColor = UIColor.white.cgColor
            btnShowall.layer.borderWidth = 0.4
            btnShowall.setTitleColor(UIColor.white, for: .normal)
            btnShowall.frame = CGRect(x: 10, y: 0, width: (self.view.frame.size.width - 30)/2, height: 40)
            btnShowall.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 12)
            btnShowall.addTarget(self, action: #selector(pressed_AllmatchHeader(sender:)), for: .touchUpInside)
            if objAppShareData.strselctedMatchTab == "" {
                btnShowall.backgroundColor = UIColor.colorConstant.appTblBgColor
                
             //   btnShowall.backgroundColor = UIColor.colorConstant.appBlueColor
            }else{
                btnShowall.backgroundColor = UIColor.colorConstant.appDarkBlack
            }
            headerView.addSubview(btnShowall)

            // Make button My Matches
            let btnMyMatches = UIButton()
            btnMyMatches.setTitle(" My Matches", for: .normal)
            btnMyMatches.setImage(UIImage(named: "star_ico"), for: .normal)
            btnMyMatches.layer.cornerRadius = 5
            btnMyMatches.layer.borderColor = UIColor.white.cgColor
            btnMyMatches.layer.borderWidth = 0.3
            btnMyMatches.setTitleColor(UIColor.white, for: .normal)
            btnMyMatches.frame = CGRect(x: btnShowall.frame.size.width + btnShowall.frame.origin.y + 20, y: 0, width: (self.view.frame.size.width - 30)/2, height: 40)
            btnMyMatches.clipsToBounds = true
            btnMyMatches.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 12)
            btnMyMatches.addTarget(self, action: #selector(pressed_MyMetches(sender:)), for: .touchUpInside)
            if objAppShareData.strselctedMatchTab == "my" {
                btnMyMatches.backgroundColor = UIColor.colorConstant.appTblBgColor

            }else{
                btnMyMatches.backgroundColor = UIColor.colorConstant.appDarkBlack
            }
            headerView.addSubview(btnMyMatches)
        }
        else
        {
            let myCustomView = UIImageView(frame: CGRect(x: 14, y: 12, width:
                22, height: 22))
            let myImage: UIImage = UIImage(named: "circle_transfers_icon")!
            myCustomView.image = myImage
            myCustomView.backgroundColor = UIColor.white
            myCustomView.layer.cornerRadius = 11
            myCustomView.layer.masksToBounds = true
            myCustomView.contentMode = .scaleToFill
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
            let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))

            if isLoadData != false
            {
            if let url = URL(string: self.arrFinalModelLeague[section - 1].countryFlag){
                myCustomView.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                myCustomView.image = UIImage(named: "icon_placeholderTeam")

            }
            [myCustomView].forEach { $0?.hideSkeleton()
            }
        }
            else
            {
                [myCustomView].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
                }

            }

            headerView.addSubview(myCustomView)
            
            // create Icon in Header
            let myCustomView2 = UIImageView(frame: CGRect(x: tableView.bounds.size.width - 30, y: 16, width:
                14, height: 14))
            let myImage2: UIImage = UIImage(named: "icon_back_Table")!
            myCustomView2.image = myImage2
            headerView.addSubview(myCustomView2)
            
            // Create Lable in Header
            let headerLabel = UILabel(frame: CGRect(x: 50, y: 2, width:
                tableView.frame.size.width - 80, height: 40))
            
            headerLabel.font = UIFont(name: "Roboto-Bold", size: 14)
            headerLabel.textColor = UIColor.white
            headerLabel.numberOfLines = 2
            if isLoadData != false
            {
                [headerLabel].forEach { $0?.hideSkeleton()
                }
                headerLabel.text = self.arrFinalModelLeague[section - 1].name
            }
            else
            {
                [headerLabel].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
                }

            }
            
            headerView.addSubview(headerLabel)
            
            // Create Line in Header
            let headerLine = UILabel(frame: CGRect(x: 0, y: 40, width:
                tableView.bounds.size.width, height: 0.4))
            
            headerLine.backgroundColor = UIColor.darkGray
            headerLine.alpha = 0.7
            headerView.addSubview(headerLine)
            
            // Create Line Top in Header
            let headerTopLine = UILabel(frame: CGRect(x: 0, y: 0, width:
                tableView.bounds.size.width, height: 4))
            
            if section == 0{
                headerTopLine.backgroundColor = UIColor.colorConstant.appDeepBlack
                headerTopLine.alpha = 0.7
            }else{
                headerTopLine.backgroundColor = UIColor.colorConstant.appDeepBlack
                headerTopLine.alpha = 0.7
            }
            
            headerView.addSubview(headerTopLine)
            
            let btnLeagueDetail = UIButton()
            btnLeagueDetail.tag = section
            btnLeagueDetail.frame = CGRect(x:0, y: 0, width: (self.view.frame.size.width), height: 40)
            btnLeagueDetail.addTarget(self, action: #selector(pressed_leagueDetail(sender:)), for: .touchUpInside)
            headerView.addSubview(btnLeagueDetail)
            
        }
        return headerView
    }
    
    @objc func pressed_AllmatchHeader(sender: UIButton!)
    {
        if isLoadData == false{
            return
        }
        self.isLoadData = false
        self.arrFinalModelLeague.removeAll()
        self.tblMatches.reloadData()
        self.lblNoRecord.isHidden = true
        self.btnAllTab.isHidden = true
        objAppShareData.strselctedMatchTab = ""
        self.limit = 5
        self.offset = 0
        self.isnext = 0
        self.apiCallGetFixturesListTypeEmpty()
    }
    
    @objc func pressed_leagueDetail(sender: UIButton!) {
        
        let viewController = UIStoryboard(name: "LeaguesTab",bundle: nil).instantiateViewController(withIdentifier: "LeaguesDetailsVC") as! LeaguesDetailsVC
        viewController.strleagueName = self.arrFinalModelLeague[sender.tag - 1].name
        objAppShareData.str_league_id = self.arrFinalModelLeague[sender.tag  - 1].leaugeId
        
        isDidSelectApiCall = true

    self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func pressed_MyMetches(sender: UIButton!) {
        
        if isLoadData == false{
            return
        }
        if objAppShareData.strselctedMatchTab == "my" {
            isLoadData = false
            self.arrFinalModelLeague.removeAll()
            self.tblMatches.reloadData()
            self.lblNoRecord.isHidden = true
            objAppShareData.strselctedMatchTab = ""
            self.limit = 5
            self.offset = 0
            self.isnext = 0
            self.apiCallGetFixturesListTypeEmpty()

            sender.backgroundColor = UIColor.colorConstant.appDarkBlack
        }else{
          //  self.arrFinalModelLeague.removeAll()
          //  self.tblMatches.reloadData()
            self.lblNoRecord.isHidden = true
            objAppShareData.strselctedMatchTab = "my"
            sender.backgroundColor = UIColor.colorConstant.appTblBgColor
            self.currentPageIndex = 1
            self.apiCallGetFixtures()

        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isLoadData == false
        {
            return placeholderRow
        }
        if section == 0
        {return 0}
        else
        {
            let objLeague = self.arrFinalModelLeague[section - 1]
            return objLeague.arrMatches.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadData == false
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesSkeltonCell", for: indexPath) as! MatchesSkeltonCell
            cell.show_skelton()
            return cell
        }

        let objLeague = self.arrFinalModelLeague[indexPath.section - 1]
        let objMatch = objLeague.arrMatches[indexPath.row]
        
        if objMatch.strbannerLeagueName == "La Liga 2" || objMatch.strbannerLeagueName == "La Liga"
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "kGADAdSizeMediumCell", for: indexPath) as! kGADAdSizeMediumCell
            
            for subview in cell.GoogleBannerView.subviews {
                subview.removeFromSuperview()
            }
            
            bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
            bannerViewDfp.adUnitID = objAppShareData.BannerId
            bannerViewDfp.rootViewController = self
            bannerViewDfp.load(DFPRequest())
            bannerViewDfp.frame = CGRect(x:(SCREEN_WIDTH - bannerViewDfp.frame.size.width)/2,
                                         y: 10,
                                         width: bannerViewDfp.frame.size.width,
                                         height: bannerViewDfp.frame.size.height+20)
            cell.GoogleBannerView.addSubview(bannerViewDfp)
            return cell
        }
        
        if objMatch.strbannerLeagueName != ""
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "kGADAdSizeMediumCell", for: indexPath) as! kGADAdSizeMediumCell
            
            for subview in cell.GoogleBannerView.subviews {
                subview.removeFromSuperview()
            }
            bannerViewDfp = DFPBannerView(adSize: kGADAdSizeBanner)
            bannerViewDfp.adUnitID = objAppShareData.BannerId
            bannerViewDfp.rootViewController = self
            bannerViewDfp.load(DFPRequest())
            
            bannerViewDfp.frame = CGRect(x:(SCREEN_WIDTH - bannerViewDfp.frame.size.width)/2,
                                         y: 10,
                                         width: bannerViewDfp.frame.size.width,
                                         height: 50)
            cell.GoogleBannerView.addSubview(bannerViewDfp)
            
            return cell
        }
        else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesTableViewCell", for: indexPath) as? MatchesTableViewCell{
                cell.lblLocalTeamName.text = objMatch.strNameloacalTeam
                cell.lblVisitorTeamName.text = objMatch.strNameVisitorTeam
                
                if objAppShareData.days < 0{
                    cell.viewAllStatus.isHidden = false
                    cell.lblScore.isHidden = false
                    cell.lblStartTime.isHidden = true
                    cell.lblScore.text = objMatch.strLocalTeamScore + " - " + objMatch.strVisitorTeamScore
                    if objMatch.strStatus.count >= 3{
                        let status = String(objMatch.strStatus.prefix(3))
                        cell.lblAllStatus.text = status.capitalizingFirstLetter()
                    }else{
                        cell.lblAllStatus.text =  objMatch.strStatus
                    }
                }
                else{
                    cell.lblStartTime.isHidden = false
                    cell.lblScore.isHidden = true
                    cell.viewAllStatus.isHidden = true
                    cell.lblStartTime.text = dayDate.Dateformate24_hours(strTime: objMatch.strTime)
                }
                
                if let url = URL(string: objMatch.strLogopathLocalTeam ){
                    cell.imgLocal.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgLocal.image = UIImage(named: "icon_placeholderTeam")
                }
                
                if let url = URL(string: objMatch.strLogopathVisitorTeam ){
                    cell.imgVisitor.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgVisitor.image = UIImage(named: "icon_placeholderTeam")
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if arrFinalModelLeague.count == 1
        {
            if indexPath.section == (arrFinalModelLeague.count) && shouldLoadMore == true  {
                self.shouldLoadMore = false
                if self.isPopulerList == true
                {
                    self.apiCallGetFixturesListTypeEmpty()
                }
                else
                {
                    self.apiCallGetFixtures()
                }
            }
        }
        else
        {
            if indexPath.section == (arrFinalModelLeague.count - 1) && shouldLoadMore == true  {
                self.shouldLoadMore = false
                if self.isPopulerList == true
                {
                    self.apiCallGetFixturesListTypeEmpty()
                }
                else
                {
                    self.apiCallGetFixtures()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if isLoadData == false
        {
            return 72
        }
        let objLeague = self.arrFinalModelLeague[indexPath.section - 1]
        let objMatch = objLeague.arrMatches[indexPath.row]

        if objMatch.strbannerLeagueName ==  "La Liga 2" || objMatch.strbannerLeagueName ==  "La Liga"
        {
            return 300
        }
        return 72
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isLoadData == false
        {
            return
        }
        let objLeague = self.arrFinalModelLeague[indexPath.section - 1]
        let objMatch = objLeague.arrMatches[indexPath.row]
        
        if objMatch.strbannerLeagueName ==  "La Liga 2" || objMatch.strbannerLeagueName ==  "Premier League" || objMatch.strbannerLeagueName ==  "La Liga"
        {
            return
        }
        isDidSelectApiCall = true
        
        let viewController = UIStoryboard(name: "MatchesTab",bundle: nil).instantiateViewController(withIdentifier: "MatchDetailMainVC") as! MatchDetailMainVC
        print(objMatch.strLocalTeamId)
        objAppShareData.str_Local_Team_Id = objMatch.strLocalTeamId
        objAppShareData.str_Visiter_Team_Id = objMatch.strVisitorTeamId
        objAppShareData.str_match_Id = objMatch.fixtureId
        objAppShareData.str_season_Id = objMatch.seasonId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
extension AnotherDateMatchVC
{
    func call_Webservice_Get_fixtures(dict_param:[String:AnyObject]) {
        //SVProgressHUD.show()
        
        //suresh
        self.btnFilterTurnament.isHidden = true
        self.btnAllTab.isHidden = true
        self.lblNoRecord.isHidden = true
        //
        
        objWebServiceManager.requestGet(strURL: webUrl.get_fixtures, params: dict_param, success: { (response) in
            SVProgressHUD.dismiss()
            self.isLoadData = true
            self.tblMatches.isUserInteractionEnabled = true
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    if self.currentPageIndex == 1
                    {
                        self.arrModelLeague.removeAll()
                        self.arrFinalModelLeague.removeAll()
                        self.tblMatches.reloadData()
                    }
                    
                    if let metadata = data ["meta"] as? [String:Any]
                    {
                        let data1 = metadata["pagination"] as? [String:Any]
                        let TotalPages = data1?["total_pages"] as? Int  ?? 0
                        self.totalPages = TotalPages
                    }
                    
                    if let arrData = data["data"] as? [[String: Any]]{
                        
                        for dict in arrData{
                            
                            let time = dict["time"] as? [String:Any]
                            if let startingDict = time?["starting_at"] as? [String:Any]  {
                                
                                let strDate = startingDict["date"] as? String ?? ""
                                if strDate == self.selectedDate
                                {
                                    
                                    if objAppShareData.days < 0{
                                        let obj = LeaugeModel.init(fromDictionary: dict, isPastMatches: true)
                                        self.arrModelLeague.append(obj)
                                    }else{
                                        let obj = LeaugeModel.init(fromDictionary: dict, isPastMatches: false)
                                        self.arrModelLeague.append(obj)
                                    }
                                }
                            }
                        }
                        for obj in self.arrModelLeague
                        {
                            //  let filteredArray = self.arrModelLeague.filter(){ $0.leaugeId.contains(obj.leaugeId) }
                            
                            let filteredArray = self.arrModelLeague.filter(){ $0.leaugeId == obj.leaugeId }
                            
                            for objNEW in filteredArray{
                                if objNEW.arrMatches.count > 0 {
                                    if !obj.arrMatches.contains(objNEW.arrMatches[0]){
                                        obj.arrMatches.append(objNEW.arrMatches[0])
                                    }
                                }
                            }
                            //     let arrNewCheck = self.arrFinalModelLeague.filter(){ $0.leaugeId.contains(obj.leaugeId) }
                            
                            let arrNewCheck = self.arrFinalModelLeague.filter(){ $0.leaugeId == obj.leaugeId }
                            
                            if arrNewCheck.count == 0{
                                if obj.arrMatches.count > 0{
                                    self.arrFinalModelLeague.append(obj)
                                }
                            }
                        }
                        
                        if self.currentPageIndex  < self.totalPages
                        {
                            self.currentPageIndex += 1
                            self.shouldLoadMore = true
                        }
                        
                        var arrFilter = [LeaugeModel]()
                        if objAppShareData.arrLeagueForFinalSorting.count>0{
                            for i in 0..<objAppShareData.arrLeagueForFinalSorting.count{
                                let obj = objAppShareData.arrLeagueForFinalSorting[i]
                                let filteredArray = self.arrFinalModelLeague.filter { $0.leaugeId == obj.leagueId }
                                if filteredArray.count>0{
                                    let objNew = filteredArray[0]
                                    if arrFilter.count==i{
                                        arrFilter.insert(objNew, at: i)
                                    }else{
                                        arrFilter.append(objNew)
                                    }
                                }
                            }
                        }
                        
                        if arrFilter.count>0{
                            self.arrFinalModelLeague = arrFilter
                        }
                        
                        // #MARK:  for banner show after every Third league ...
                        
                        var intAfterThreebannerShow:Int
                        intAfterThreebannerShow = 0
                        
                        for obj in self.arrFinalModelLeague{
                            
                            intAfterThreebannerShow = intAfterThreebannerShow + 1
                            var SeasionId_And_fixterId = true
                            for object in obj.arrMatches
                            {
                                if object.seasonId == ""  && object.fixtureId == ""
                                {
                                    SeasionId_And_fixterId = false
                                }
                            }
                            
                            if SeasionId_And_fixterId == true
                            {
                                if intAfterThreebannerShow % 3 == 0 {
                                    let value = MatchesModel.init(localdictionary: [:], visitordictionary: [:] , scoredict : [:], timedict:[:], fixture_Id: "", season_Id: "", bannerLeagueName:obj.bannerLeagueName )
                                    obj.arrMatches.append(value)
                                }
                                else{
                                    if obj.bannerLeagueName == "La Liga 2" || obj.bannerLeagueName == "La Liga" || obj.bannerLeagueName == "Premier League" {
                                        let value = MatchesModel.init(localdictionary: [:], visitordictionary: [:] , scoredict : [:], timedict:[:], fixture_Id: "", season_Id: "", bannerLeagueName:obj.bannerLeagueName )
                                        obj.arrMatches.append(value)
                                    }
                                }
                            }
                        }
                        // --- banner close loop
                        self.tblMatches.reloadData()
                    }
                }
                if self.arrFinalModelLeague.count == 0{
                    if objAppShareData.strselctedMatchTab == "my"{
                        self.lblNoRecord.text = "No favorites playing"
                        self.btnAllTab.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
                        self.btnAllTab.isHidden = false
                        self.btnFilterTurnament.isHidden = true
                    }else{
                        self.lblNoRecord.text = "No matches scheduled"
                        self.btnFilterTurnament.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
                        self.btnAllTab.isHidden = true
                        self.btnFilterTurnament.isHidden = false
                    }
                    self.placeholderRow = 0
                    self.tblMatches.reloadData()
                    self.lblNoRecord.isHidden = false
                }else{
                    self.btnAllTab.isHidden = true
                    self.lblNoRecord.isHidden = true
                    self.btnFilterTurnament.isHidden = true
                }
            }else{
                if self.arrFinalModelLeague.count == 0{
                    if objAppShareData.strselctedMatchTab == "my"{
                        self.lblNoRecord.text = "No favorites playing"
                        self.btnAllTab.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
                        self.btnAllTab.isHidden = false
                        self.btnFilterTurnament.isHidden = true
                    }else{
                        self.lblNoRecord.text = "No matches scheduled"
                        self.btnFilterTurnament.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
                        self.btnAllTab.isHidden = true
                        self.btnFilterTurnament.isHidden = false
                    }
                    self.placeholderRow = 0
                    self.tblMatches.reloadData()
                    self.lblNoRecord.isHidden = false
                }else
                {
                    self.btnAllTab.isHidden = true
                    self.lblNoRecord.isHidden = true
                    self.btnFilterTurnament.isHidden = true
                }
                //GlobalUtility.showToastMessage(msg: message)
            }
        }) { (error) in
            print(error)
            self.isLoadData = true
            self.tblMatches.isUserInteractionEnabled = true
            SVProgressHUD.dismiss()
        }
    }
    
    func call_Webservice_Get_fixturesListTypeEmpty(dict_param:[String:AnyObject]) {
        
       
        self.btnFilterTurnament.isHidden = true
        self.btnAllTab.isHidden = true
        self.lblNoRecord.isHidden = true
        
        objWebServiceManager.requestGet(strURL: webUrl.get_fixtures, params: dict_param, success: { (response) in
            SVProgressHUD.dismiss()
            self.isLoadData = true
            self.tblMatches.isUserInteractionEnabled = true

            if let isnext = response["is_next"] as? Int
            {
                self.isnext = isnext
                if isnext == 1
                {
                    self.shouldLoadMore = true
                }
                else
                {
                    self.shouldLoadMore = false
                }
            }
            if let limit = response["limit"] as? Int
            {
                self.limit = limit
            }
            if let offset = response["offset"] as? Int
            {
                self.offset = offset
            }

            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    self.arrModelLeague.removeAll()
                    if let arrData = data["data"] as? [[String: Any]]{
                        
                        for dict in arrData{
                            
                            let time = dict["time"] as? [String:Any]
                            if let startingDict = time?["starting_at"] as? [String:Any]  {
                                
                                let strDate = startingDict["date"] as? String ?? ""
                                if strDate == self.selectedDate
                                {
                                    
                                    if objAppShareData.days < 0{
                                        let obj = LeaugeModel.init(fromDictionary: dict, isPastMatches: true)
                                        self.arrModelLeague.append(obj)
                                    }else{
                                        let obj = LeaugeModel.init(fromDictionary: dict, isPastMatches: false)
                                        self.arrModelLeague.append(obj)
                                    }
                                }
                            }
                        }
                        for obj in self.arrModelLeague
                        {
                            
                            let filteredArray = self.arrModelLeague.filter(){ $0.leaugeId == obj.leaugeId }
                            
                            for objNEW in filteredArray{
                                if objNEW.arrMatches.count > 0 {
                                    if !obj.arrMatches.contains(objNEW.arrMatches[0]){
                                        obj.arrMatches.append(objNEW.arrMatches[0])
                                    }
                                }
                            }
                            
                            let arrNewCheck = self.arrFinalModelLeague.filter(){ $0.leaugeId == obj.leaugeId }
                            
                            if arrNewCheck.count == 0{
                                if obj.arrMatches.count > 0{
                                    self.arrFinalModelLeague.append(obj)
                                }
                            }
                        }
                        
                     
                        
                        var arrFilter = [LeaugeModel]()
                        if objAppShareData.arrLeagueForFinalSorting.count>0{
                            for i in 0..<objAppShareData.arrLeagueForFinalSorting.count{
                                let obj = objAppShareData.arrLeagueForFinalSorting[i]
                                let filteredArray = self.arrFinalModelLeague.filter { $0.leaugeId == obj.leagueId }
                                if filteredArray.count>0{
                                    let objNew = filteredArray[0]
                                    if arrFilter.count==i{
                                        arrFilter.insert(objNew, at: i)
                                    }else{
                                        arrFilter.append(objNew)
                                    }
                                }
                            }
                        }
                        
                        if arrFilter.count>0{
                            self.arrFinalModelLeague = arrFilter
                        }
                        
 // Rest of the wold filter by Poluler  List
                        if MatchsVcVariable.arrayPopulerLeague.count > 0
                        {
                            
                            var arrFilterRestOftheWold = [LeaugeModel]()
                            arrFilterRestOftheWold.removeAll()
                            for i in 0..<MatchsVcVariable.arrayPopulerLeague.count
                            {
                                let filteredArray = self.arrFinalModelLeague.filter { $0.leaugeId == MatchsVcVariable.arrayPopulerLeague[i] }
                                
                                if filteredArray.count>0{
                                    arrFilterRestOftheWold.append(filteredArray[0])
                                }
                            }
                            self.arrFinalModelLeague.removeAll()
                            self.arrFinalModelLeague = arrFilterRestOftheWold
                        }
        // mindiii
                        
                        // #MARK:  for banner show after every Third league ...
                        
                        var intAfterThreebannerShow:Int
                        intAfterThreebannerShow = 0
                        
                        for obj in self.arrFinalModelLeague{
                            
                            intAfterThreebannerShow = intAfterThreebannerShow + 1
                            var SeasionId_And_fixterId = true
                            for object in obj.arrMatches
                            {
                                if object.seasonId == ""  && object.fixtureId == ""
                                {
                                    SeasionId_And_fixterId = false
                                }
                            }
                            
                            if SeasionId_And_fixterId == true
                            {
                                if intAfterThreebannerShow % 3 == 0 {
                                    let value = MatchesModel.init(localdictionary: [:], visitordictionary: [:] , scoredict : [:], timedict:[:], fixture_Id: "", season_Id: "", bannerLeagueName:obj.bannerLeagueName )
                                    obj.arrMatches.append(value)
                                }
                                else{
                                    if obj.bannerLeagueName == "La Liga 2" || obj.bannerLeagueName == "La Liga" || obj.bannerLeagueName == "Premier League" {
                                        let value = MatchesModel.init(localdictionary: [:], visitordictionary: [:] , scoredict : [:], timedict:[:], fixture_Id: "", season_Id: "", bannerLeagueName:obj.bannerLeagueName )
                                        obj.arrMatches.append(value)
                                    }
                                }
                            }
                        }
                        // --- banner close loop
                        self.tblMatches.reloadData()
                    }
                }
                if self.arrFinalModelLeague.count == 0{
                    if objAppShareData.strselctedMatchTab == "my"{
                        self.lblNoRecord.text = "No favorites playing"
                        self.btnAllTab.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
                        self.btnAllTab.isHidden = false
                        self.btnFilterTurnament.isHidden = true
                    }else{
                        self.lblNoRecord.text = "No matches scheduled"
                        self.btnFilterTurnament.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
                        self.btnAllTab.isHidden = true
                        self.btnFilterTurnament.isHidden = false
                    }
                    self.placeholderRow = 0
                    self.tblMatches.reloadData()
                    self.lblNoRecord.isHidden = false
                }else{
                    self.btnAllTab.isHidden = true
                    self.lblNoRecord.isHidden = true
                    self.btnFilterTurnament.isHidden = true
                }
            }else{
                if self.arrFinalModelLeague.count == 0{
                    if objAppShareData.strselctedMatchTab == "my"{
                        self.lblNoRecord.text = "No favorites playing"
                        self.btnAllTab.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
                        self.btnAllTab.isHidden = false
                        self.btnFilterTurnament.isHidden = true
                    }else{
                        self.lblNoRecord.text = "No matches scheduled"
                        self.btnFilterTurnament.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
                        self.btnAllTab.isHidden = true
                        self.btnFilterTurnament.isHidden = false
                    }
                    self.placeholderRow = 0
                    self.tblMatches.reloadData()
                    self.lblNoRecord.isHidden = false
                }else
                {
                    self.btnAllTab.isHidden = true
                    self.lblNoRecord.isHidden = true
                    self.btnFilterTurnament.isHidden = true
                }
            }
        }) { (error) in
            print(error)
            self.tblMatches.isUserInteractionEnabled = true
          
            self.isLoadData = true
            SVProgressHUD.dismiss()
        }
    }
}

extension AnotherDateMatchVC{
    
    func configureCalendar(SelectDate:Date){
        
        print(SelectDate)
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 400
        }
        self.calendar.delegate = self
        self.calendar.dataSource = self
        //self.calendar.selectedDate1
       // self.calendar.select(selectedDate1)
        self.calendar.select(SelectDate)
        self.calendar.calendarHeaderView.isHidden = true
        self.changeCalenderHeader(date: SelectDate)
        self.view.addGestureRecognizer(self.scopeGesture)
        self.calendar.scope = .week
        // For UITest
        self.calendar.accessibilityIdentifier = "calendar"
        self.calendar.calendarWeekdayView.backgroundColor = UIColor.colorConstant.appDarkBlack
        self.calendar.appearance.todayColor = UIColor.lightGray
    }
    
    func changeCalenderHeader(date : Date){
        
  
        let fmt = DateFormatter()
        fmt.dateFormat = "MMMM"
        let strCalenderHeader = fmt.string(from: date)
        self.lblSelectedMonth.text = strCalenderHeader.uppercased()
       
    }
    /**
     Tells the delegate the calendar is about to change the bounding rect.
     */
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        
        UIView.animate(withDuration: 0.6, animations: {
            DispatchQueue.main.async {
                self.calendarHeightConstraint.constant = bounds.height
                self.view.layoutIfNeeded()
            }
        })
    }
    
    /**
     Tells the delegate a date in the calendar is selected by tapping.
     */
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        //self.getlatestSlotDataFor(dateSelected: newDate)
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
    }
    
    /**
     Tells the delegate the calendar is about to change the current page.
     */
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.changeCalenderHeader(date: calendar.currentPage)
    }
    
    /**
      Close past dates in FSCalendar
     */
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at     monthPosition: FSCalendarMonthPosition) -> Bool{
        
        let strDate1 = self.dateFormatter.string(from: date)
        let date1 = self.dateFormatter.date(from: strDate1)
        self.selectedDate = strDate1
        self.selectedDate1 = date1 ?? Date()
        objAppShareData.SelectedCalanderDate = date1 ?? Date()
        let day = dayDate.dayDifference(from: (date1?.timeIntervalSince1970) ?? 0)
        if day == "Yesterday"{
            objAppShareData.IndexOfController = "0"
            objAppShareData.isAnotherDateMatchVC = true
            objAppShareData.lastSelectedDate =  Calendar.current.date(byAdding: .day, value: -1, to: Date()) ?? Date()
            self.navigationController?.popViewController(animated: false)
        }else if day == "Today"{
            objAppShareData.IndexOfController = "1"
            objAppShareData.isAnotherDateMatchVC = true
            objAppShareData.lastSelectedDate = Date()
            self.navigationController?.popViewController(animated: false)
        }else if day == "Tomorrow"{
            objAppShareData.IndexOfController = "2"
            objAppShareData.isAnotherDateMatchVC = true
            objAppShareData.lastSelectedDate =  Calendar.current.date(byAdding: .day, value: +1, to: Date()) ?? Date()
            self.navigationController?.popViewController(animated: false)
        }else{
            objAppShareData.days = Int(day) ?? 0
            let date11 = self.dateFormatter.string(from: date1!)
            let date22 = self.dateFormatter2.string(from: date1!)
            self.lblSelectedDete.text = date11
            self.lblDay.text = date22
            
            let finaldate = dayDate.dateformateMMMM_dd_yyy(strDate: strDate1)
            selectedDate =  finaldate
            
            self.currentPageIndex = 1
            self.shouldLoadMore = false
            self.arrModelLeague.removeAll()
            self.arrFinalModelLeague.removeAll()
            self.tblMatches.reloadData()
            self.apiCallGetFixtures()
            
            self.viewCalendarBg.isHidden = true
        }
        
        return true
    }
    
 

}

extension AnotherDateMatchVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }}
