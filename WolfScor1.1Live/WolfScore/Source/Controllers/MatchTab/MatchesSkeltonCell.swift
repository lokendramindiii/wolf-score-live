//
//  MatchesSkeltonCell.swift
//  WolfScore
//
//  Created by Mindiii on 7/17/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView
class MatchesSkeltonCell: UITableViewCell {

    @IBOutlet var lblLocalTeamName:UILabel!
    @IBOutlet var lblVisitorTeamName:UILabel!
    @IBOutlet var lblmiddle:UILabel!

    @IBOutlet var imgLocal:UIImageView!
    @IBOutlet var imgVisitor:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))

        [lblLocalTeamName , lblVisitorTeamName ,lblmiddle].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
//        [imgLocal,imgVisitor].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
//        }
       
    }
    
    func hide_skelton()
    {
        [lblLocalTeamName , lblVisitorTeamName ,lblmiddle ].forEach { $0?.hideSkeleton()
        }
        [imgLocal,imgVisitor].forEach { $0?.hideSkeleton()
        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
