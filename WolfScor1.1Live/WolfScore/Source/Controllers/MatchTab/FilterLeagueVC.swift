//
//  FilterLeagueVC.swift
//  WolfScore
//
//  Created by Mindiii on 07/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import Toaster
class FilterLeagueVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblLeagues: UITableView!
    var isSearch : Bool!
    var arrAllLeauge = [ModelLeagueList]()
    var Filterleaguelist = [ModelLeagueList]()
    var arrSelectedLeague = [String]()
    
    var arrSortRecived = [String]()
    
    var closerfilterbyleagueId:((_ leaugeId:String ,_ arrFilter: [String]) ->())?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSearch.layer.cornerRadius = self.viewSearch.frame.size.height/2
        isSearch = false
        self.txtSearch.text = ""
        self.call_Webservice_Get_LeaugeList()

        self.txtSearch.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Filter tournaments", tableName: nil, comment: ""), attributes: [NSAttributedStringKey.foregroundColor:UIColor.white
            ])
        
        self.modifyClearButtonWithImage(image: UIImage(named: "icon_clearTxtfild")!, txtfiled: txtSearch)

    }
    
    func modifyClearButtonWithImage(image : UIImage ,txtfiled : UITextField) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(sender:)), for: .touchUpInside)
        txtfiled.rightView = clearButton
        txtfiled.rightViewMode = .whileEditing
    }
    
    @objc func clear(sender : AnyObject) {
        
        self.txtSearch.text = ""
        self.txtSearch.sendActions(for: .editingChanged)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func btnDoneAction(_ sender : UIButton){
        self.view.endEditing(true)
        
        if arrSelectedLeague.count != 0
        {
            let strLeagueId = arrSelectedLeague.joined(separator: ",")
            print(strLeagueId)
            objAppShareData.strFilterLeagueId = strLeagueId
            closerfilterbyleagueId?(strLeagueId, self.arrSortRecived)
            
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnSortinglAction(_ sender : UIButton)
    {
        
        self.view.endEditing(true)
        let viewController = UIStoryboard(name: "UserTabbar",bundle: nil).instantiateViewController(withIdentifier: "SortLeaguesVC") as! SortLeaguesVC
        viewController.arrSelectedFilterLeague = arrSelectedLeague
        
        viewController.sortleagueCloser = {(arrSort)in
            self.arrSortRecived = arrSort
        }
        
    self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    @IBAction func btnSelectAllAction(_ sender : UIButton){
        self.view.endEditing(true)
        for i in 0..<self.arrAllLeauge.count {
            let objModel: ModelLeagueList = self.arrAllLeauge[i]
            print(objModel.leagueId)
        }
        self.tblLeagues.reloadData()
        
    }
    @IBAction func btnDeSelectAllAction(_ sender : UIButton){
        self.view.endEditing(true)
        objAppShareData.strFilterLeagueId = ""
        call_Webservice_Post_addRemoveFilterLeague(request_type: "remove_all", league_id:"")
        arrSelectedLeague.removeAll()
        for dic in self.Filterleaguelist
        {
            dic.isSelected = "0"
        }
        for dic in self.arrAllLeauge
        {
            dic.isSelected = "0"
        }
        self.tblLeagues.reloadData()
    }
    
    func getSearchArrayContains(_ text : String) {
        
        //   print(text)
        if arrAllLeauge.count == 0 {
            
        } else {
            // Filterleaguelist = arrAllLeauge.filter{ $0.leagueName.contains(text) }
            
            Filterleaguelist = self.arrAllLeauge.filter { $0.leagueName.localizedCaseInsensitiveContains(text) }
            
            if(Filterleaguelist.count == 0){
                isSearch = false;
                print(arrAllLeauge.count)
                
            } else {
                isSearch = true;
                print(Filterleaguelist.count)
            }
            tblLeagues.reloadData()
        }
    }
}
// UITextField Delegates

extension FilterLeagueVC{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        print(substring)
        getSearchArrayContains(substring)
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true;
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        txtSearch.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
}

// MARK: - TableView Delegates & Datasource

extension FilterLeagueVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch == true{
            return Filterleaguelist.count
        }
        else
        {
            return arrAllLeauge.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TeamTableViewCell", for: indexPath) as? TeamTableViewCell{
            
            if isSearch == true
            {
                let objModel = self.Filterleaguelist[indexPath.row]
                cell.objleague = objModel
                cell.lblTeamName.text = objModel.leagueName
                cell.lblCountryName.text = objModel.countryName
                
                if let url = URL(string: objModel.leagueFlage ?? ""){
                    cell.imgFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "circle_goal_icon"))
                }

           
                if arrSelectedLeague.contains(objModel.leagueId) || objModel.isSelected == "1"
                {
                    cell.imgSelection.image = UIImage(named: "selct_icon")
                }
                else{
                    cell.imgSelection.image = UIImage(named: "white_circle")
                }
            }
            else
            {
                let objModel = self.arrAllLeauge[indexPath.row]
                cell.objleague = objModel
                cell.lblTeamName.text = objModel.leagueName
                cell.lblCountryName.text = objModel.countryName

                if let url = URL(string: objModel.leagueFlage ?? ""){
                    cell.imgFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "circle_goal_icon"))
                }
                
                if arrSelectedLeague.contains(objModel.leagueId) || objModel.isSelected == "1"
                {
                    cell.imgSelection.image = UIImage(named: "selct_icon")
                }
                else{
                    cell.imgSelection.image = UIImage(named: "white_circle")
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearch == true
        {
            let objModel = self.Filterleaguelist[indexPath.row]
            print(objModel.leagueName)
            if arrSelectedLeague.contains(objModel.leagueId) || objModel.isSelected == "1"
            {
                if let index = arrSelectedLeague.index(of:objModel.leagueId) {
                    arrSelectedLeague.remove(at: index)
                }
                objModel.isSelected = "0"
                self.call_Webservice_Post_addRemoveFilterLeague(request_type: "remove", league_id: objModel.leagueId)

            }
            else
            {
                objModel.isSelected = "1"
                self.call_Webservice_Post_addRemoveFilterLeague(request_type: "add", league_id: objModel.leagueId)
              arrSelectedLeague.append(objModel.leagueId)
            }
        }
        else
        {
            let objModel = self.arrAllLeauge[indexPath.row]
            if arrSelectedLeague.contains(objModel.leagueId) || objModel.isSelected == "1"
            {
                objModel.isSelected = "0"
                
                if let index = arrSelectedLeague.index(of:objModel.leagueId) {
                    arrSelectedLeague.remove(at: index)
                }
                call_Webservice_Post_addRemoveFilterLeague(request_type: "remove", league_id: objModel.leagueId)

            }
            else
            {
                objModel.isSelected = "1"
                call_Webservice_Post_addRemoveFilterLeague(request_type: "add", league_id: objModel.leagueId)
                arrSelectedLeague.append(objModel.leagueId)
            }
        }
        tblLeagues.reloadData()
    }
}
// MARK: - Webservice
extension FilterLeagueVC{
    
    func call_Webservice_Get_LeaugeList() {
        SVProgressHUD.show()
        
        objWebServiceManager.requestGet(strURL: webUrl.get_league_list, params: nil, success: { (response) in
            SVProgressHUD.dismiss()
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    if let arrData = data["league_list"] as? [[String: Any]]{
                        
                        for dict in arrData{
                            let obj = ModelLeagueList.init(fromDictionary: dict)
                            self.arrAllLeauge.append(obj)
                            if obj.isSelected == "1"
                            {
                                self.arrSelectedLeague.append(obj.leagueId)
                            }
                        }
                        self.tblLeagues.reloadData()
                    }
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    
    //
    func call_Webservice_Post_addRemoveFilterLeague(request_type:String,league_id:String){
        
        let paramDict = ["type":request_type,
                         "league_id":league_id] as [String:Any]
        print(paramDict)
        
        objWebServiceManager.requestPost(strURL: webUrl.add_removeFilterleague, params: paramDict, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"{
                //  self.tblLeagues.reloadData()
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}

