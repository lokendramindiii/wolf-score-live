//
//  LiveTickerVC.swift
//  WolfScore
//
//  Created by Mindiii on 17/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import Alamofire
import SVProgressHUD
import GoogleMobileAds

class LiveTickerVC: UIViewController {
    
    @IBOutlet weak var lblNoRecodFound:UILabel!
    @IBOutlet weak var tblLiveTicker:UITableView!
    @IBOutlet weak var viewBannerBottom: UIView!

    var arrLiveTicker = [ModelLiveTicker]()
    
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!

    var isLoadData = false
    var placeholderRow = 10

    //var objectTicker =  ModelLiveTicker.init(dict: [:])
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
         self.bannerAdSetup()
    }
    func callApiSelfLiveTickerVC()
    {
        self.call_Webservice_Get_LiveTicker(str_matchId:objAppShareData.str_match_Id)
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tblLiveTicker.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
    }
    
    
    func call_Webservice_Get_LiveTicker(str_matchId:String) {
       // SVProgressHUD.show()
        let paramDict = ["match_id":str_matchId] as [String:AnyObject]
        objWebServiceManager.requestGet(strURL: webUrl.get_match_commentary, params: paramDict, success: { (response) in
            
           self.isLoadData = true
            SVProgressHUD.dismiss()
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    if let dicArray = data["data"] as? [[String: Any]]{
                        self.arrLiveTicker.removeAll()
                        for dic in dicArray{
                            let objectTicker =  ModelLiveTicker.init(dict:dic)
                            self.arrLiveTicker.append(objectTicker)
                        }
                        if self.arrLiveTicker.count == 0{
                            self.lblNoRecodFound.isHidden = false
                        }else{
                            self.lblNoRecodFound.isHidden = true
                        }
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                           self.tblLiveTicker.reloadData()
                        })
                    }
                }
            }
        }) { (error) in
            print(error)
            self.isLoadData = true
            SVProgressHUD.dismiss()
        }
    }
}

//MARK: - tableView delegate method
extension LiveTickerVC:UITableViewDelegate,UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoadData == false{
            return placeholderRow
        }
        return arrLiveTicker.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadData == false{
            let cellIdentifier = "LiveTickerCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! LiveTickerCell
            cell.show_skelton()
            return cell
        }
        let cellIdentifier = "LiveTickerCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! LiveTickerCell
        cell.hide_skelton()
        let objTicker = arrLiveTicker[indexPath.row]
        cell.lblTitle.isHidden = true
        if objTicker.isGoal == true{
            cell.lblTitle.isHidden = false
            cell.lblTitle.text = "GOAL"
            cell.btnTime.setTitle(objTicker.strMinute, for: .normal)
            cell.lblTitle.textColor = UIColor.colorConstant.appGreenColor
            cell.btnTime.layer.borderColor = UIColor.clear.cgColor
            cell.btnTime.backgroundColor = UIColor.colorConstant.appGreenColor
        }else if objTicker.isImportant == true {
            cell.lblTitle.isHidden = false
            cell.lblTitle.text = "IMPORTANT"
            cell.btnTime.setTitle(objTicker.strMinute, for: .normal)
            cell.lblTitle.textColor  = UIColor.colorConstant.appBlueColor
            cell.btnTime.layer.borderColor = UIColor.clear.cgColor
            cell.btnTime.backgroundColor = UIColor.colorConstant.appBlueColor
        }else{
            cell.btnTime.setTitle(objTicker.strMinute, for: .normal)
            cell.btnTime.backgroundColor = UIColor.clear
            cell.btnTime.layer.borderWidth = 1
            cell.btnTime.layer.borderColor = UIColor.colorConstant.appTimeLineColor.cgColor
        }
        if indexPath.row == 0{
            cell.btnTime.setTitle("", for: .normal)
            cell.btnTime.setImage(UIImage.init(named: "live_watch"), for: .normal)
            cell.btnTime.layer.borderWidth = 0
            cell.btnTime.backgroundColor = UIColor.clear
        }else{
            cell.btnTime.setImage(nil, for: .normal)
        }
        cell.lblCommentry.text = objTicker.strComment
        return cell
        
      }
}

extension LiveTickerVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}
extension LiveTickerVC: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return tblLiveTicker
    }
}
