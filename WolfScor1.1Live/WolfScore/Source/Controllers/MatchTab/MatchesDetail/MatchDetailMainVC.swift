//
//  MatchDetailMainVC.swift
//  WolfScore
//
//  Created by Mindiii on 17/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import SkeletonView
import SJSegmentedScrollView


struct MatchDetailMainStruct {
    
    static var isheadToHeadLoad = 0
    static var isMatchMediaLoad = 0
    static var isLineUpLoad = 0
    static var isLiveTickerLoad = 0
    static var isMatchTableLoad = 0
    static var isStatisticsLoad = 0

}


class MatchDetailMainVC: SJSegmentedViewController {
    
    
    var selectedSegment: SJSegmentTab?
    
    var matchFactsVC : MatchFactsVC!
    var headToHeadVC : HeadToHeadVC!
    var MatchMediaVC : MatchMediaVC!
    var LineUpVC : LineUpVC!
    var LiveTickerVC : LiveTickerVC!
    var MatchTableVC : MatchTableVC!
    var StatisticsVC : StatisticsVC!
    
    
    // skelton outlet
    @IBOutlet weak var imageLocalTeam:UIImageView!
    @IBOutlet weak var imageVisiterTeam:UIImageView!
    @IBOutlet weak var lblLocalTeamName:UILabel!
    @IBOutlet weak var lblVisiterTeamName:UILabel!
    @IBOutlet weak var lblMatchStatus:UILabel!
    @IBOutlet weak var lblscore:UILabel!

    @IBOutlet weak var lblfist:UILabel!
    @IBOutlet weak var lblsecond:UILabel!
    @IBOutlet weak var lblthird:UILabel!
    @IBOutlet weak var viewHeaderSkelton:UIView!
    
    // skelton outlet end
    
    var dict:[String:Any] = [:]
    
    var timer: Timer?
    var totalTime = 0
    var strLocalTeamId: String = ""
    var strVisitorTeamId: String = ""
    
    var isloadPager = false
    var SkeltonBgview:UIView!
    
    
    override func viewDidLoad() {
        //  super.viewDidLoad()
        
        if isloadPager == false
        {
            
            show_skelton()
            SkeltonBgview = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT))
            
            viewHeaderSkelton.frame = CGRect (x: 0, y: 44, width: SCREEN_WIDTH, height: 250)
            SkeltonBgview.addSubview(viewHeaderSkelton)
            SkeltonBgview.backgroundColor = UIColor.colorConstant.appDarkBlack
            self.view.addSubview(SkeltonBgview)
            
            if APP_DELEGATE.isFromNotification
            {
                APP_DELEGATE.isFromNotification = false
            }
            
            self.call_Webservice_Get_Detail(str_matchId: objAppShareData.str_match_Id)
        }
        
        if isloadPager == true
        {
            
            
            MatchDetailMainStruct.isheadToHeadLoad = 0
            MatchDetailMainStruct.isMatchMediaLoad = 0
            MatchDetailMainStruct.isLineUpLoad = 0
            MatchDetailMainStruct.isLiveTickerLoad = 0
            MatchDetailMainStruct.isMatchTableLoad = 0
            MatchDetailMainStruct.isStatisticsLoad = 0

            hide_skelton()
            viewHeaderSkelton.removeFromSuperview()
            SkeltonBgview.removeFromSuperview()
            
            var arrController = [UIViewController]()
            if let response = self.dict["tab_status"] as? [String: Any]{
                
                let lineup = response["lineup"] as? Int ?? 0
                let statistics = response["statistics"] as? Int ?? 0
                let commentry = response["commentry"] as? Int ?? 0
                let highlights = response["highlights"] as? Int ?? 0
                let head2head = response["head2head"] as? Int ?? 0
                let standings = response["standings"] as? Int ?? 0
                let matchfact = response["matchfact"] as? Int ?? 0
                
                if matchfact == 1{
                    self.matchFactsVC = UIStoryboard(name: "MatchesTab", bundle: nil).instantiateViewController(withIdentifier: "MatchFactsVC") as? MatchFactsVC
                    self.matchFactsVC.isLoadapi = true
                    self.matchFactsVC.title = "   MATCH FACTS   "
                    arrController.append(self.matchFactsVC)
                }
                if head2head == 1{
                    self.headToHeadVC = UIStoryboard(name: "MatchesTab", bundle: nil).instantiateViewController(withIdentifier: "HeadToHeadVC") as? HeadToHeadVC
                    self.headToHeadVC.title = "   HEAD TO HEAD   "
                    arrController.append(self.headToHeadVC)
                    
                }
                if lineup == 1{
                    self.LineUpVC = UIStoryboard(name: "MatchesTab", bundle: nil).instantiateViewController(withIdentifier: "LineUpVC") as? LineUpVC
                    self.LineUpVC.title = " LINEUP "
                    arrController.append(self.LineUpVC)
                    
                }
                
                if commentry == 1{
                    self.LiveTickerVC = UIStoryboard(name: "MatchesTab", bundle: nil).instantiateViewController(withIdentifier: "LiveTickerVC") as? LiveTickerVC
                    self.LiveTickerVC.title = "   LIVE TICKER   "
                    arrController.append(self.LiveTickerVC)
                    
                }
                
                if standings == 1{
                    self.MatchTableVC = UIStoryboard(name: "MatchesTab", bundle: nil).instantiateViewController(withIdentifier: "MatchTableVC") as? MatchTableVC
                    self.MatchTableVC.title = " TABLE "
                    arrController.append(self.MatchTableVC)
                    
                }
                
                if statistics == 1{
                    self.StatisticsVC = UIStoryboard(name: "MatchesTab", bundle: nil).instantiateViewController(withIdentifier: "StatisticsVC") as? StatisticsVC
                    self.StatisticsVC.title = "   STATISTICS   "
                    arrController.append(self.StatisticsVC)
                    
                }
                
                if highlights == 1{
                    self.MatchMediaVC = UIStoryboard(name: "MatchesTab", bundle: nil).instantiateViewController(withIdentifier: "MatchMediaVC") as? MatchMediaVC
                    
                    self.MatchMediaVC.title = " MEDIA "
                    arrController.append(self.MatchMediaVC)
                    
                }
                
                let headerController =     UIStoryboard(name: "MatchesTab", bundle: nil).instantiateViewController(withIdentifier: "MatchDetailHeaderVC") as? MatchDetailHeaderVC
                
                headerController!.dict = self.dict
                self.headerViewController = headerController
                
                self.segmentControllers = arrController
                
                self.segmentTitleColor = UIColor.colorConstant.appLightBlueColor
                
                self.selectedSegmentViewColor  = UIColor.colorConstant.appBlueColor
                
                self.segmentSelectedTitleColor = UIColor.colorConstant.appLightBlueColor
                
                self.segmentBackgroundColor = UIColor.colorConstant.appDarkBlack
                self.headerViewHeight = 250
                self.selectedSegmentViewHeight = 3.0
                
                if UIScreen.main.bounds.size.height >= 812.0
                {
                    self.headerViewOffsetHeight = 80.0
                }
                else
                {
                    self.headerViewOffsetHeight = 35.0
                }
                self.segmentTitleFont = UIFont(name: "roboto-Medium" , size: 14.0)!
                
                self.segmentedScrollViewColor = UIColor.colorConstant.appDarkBlack
                self.segmentShadow = SJShadow.dark()
                self.showsHorizontalScrollIndicator = false
                self.showsVerticalScrollIndicator = false
                self.segmentBounces = true
                self.delegate = self
                
            }
        }
        super.viewDidLoad()
        
    }
    
    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblfist,lblsecond,lblthird, lblLocalTeamName ,lblscore,lblVisiterTeamName ,lblMatchStatus].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
        [imageLocalTeam ,lblscore, imageVisiterTeam].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
        
    }
    
    func hide_skelton()
    {
        [lblfist,lblsecond,lblthird, lblLocalTeamName ,lblVisiterTeamName ,lblMatchStatus].forEach { $0?.hideSkeleton()
        }
        [imageLocalTeam , imageVisiterTeam].forEach { $0?.hideSkeleton()
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        print("viewWillAppear")
        super.viewWillAppear(animated)
        
        if APP_DELEGATE.isInterstialPresent == true
        {
            APP_DELEGATE.isInterstialPresent = false
            return
        }
        self.slideMenuController()?.leftPanGesture?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if APP_DELEGATE.isBannerDisappearShow == true
        {
            return
        }
        self.timer?.invalidate()
        self.timer = nil
        self.slideMenuController()?.leftPanGesture?.isEnabled = true
    }
    
    
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func call_Webservice_Get_Detail(str_matchId:String) {
        
        let paramDict = ["match_id":objAppShareData.str_match_Id,
                         "time_zone":objAppShareData.localTimeZoneName] as [String:AnyObject]
        
        objWebServiceManager.requestGet(strURL: webUrl.get_match_details, params: paramDict, success: { (response) in
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    self.dict = data
                    self.isloadPager = true
                    self.viewDidLoad()
                }
            }
            else{
                
                self.hide_skelton()
                self.viewHeaderSkelton.removeFromSuperview()
                self.SkeltonBgview.removeFromSuperview()
                GlobalUtility.showToastMessage(msg: message)
            }
            
        }) { (error) in
            
            self.hide_skelton()
            self.viewHeaderSkelton.removeFromSuperview()
            self.SkeltonBgview.removeFromSuperview()
            
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}


extension MatchDetailMainVC: SJSegmentedViewControllerDelegate {
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        

        if controller == self.headToHeadVC
        {
            if  MatchDetailMainStruct.isheadToHeadLoad == 0
            {
                print("self.headToHeadVC")
                 self.headToHeadVC.callApiSelfHeadToHeadVC()
                MatchDetailMainStruct.isheadToHeadLoad = 1
            }
        }
        if controller == self.MatchMediaVC
        {
            if  MatchDetailMainStruct.isMatchMediaLoad == 0
            {
                print("self.MatchMediaVC")
                self.MatchMediaVC.callApiSelfMatchMediaVC()
                MatchDetailMainStruct.isMatchMediaLoad = 1
            }
        }
        if controller == self.LineUpVC
        {
            if  MatchDetailMainStruct.isLineUpLoad == 0
            {
                print("self.LineUpVC")
                self.LineUpVC.callApiSelfLineUpVC()
                MatchDetailMainStruct.isLineUpLoad = 1
            }
        }
        if controller == self.LiveTickerVC
        {
            if  MatchDetailMainStruct.isLiveTickerLoad == 0
            {
                print("self.LiveTickerVC")
                self.LiveTickerVC.callApiSelfLiveTickerVC()
                MatchDetailMainStruct.isLiveTickerLoad = 1
            }
        }
        if controller == self.MatchTableVC
        {
            if  MatchDetailMainStruct.isMatchTableLoad == 0
            {
                print("self.MatchTableVC")
                self.MatchTableVC.callApiSelfMatchTableVC()
               MatchDetailMainStruct.isMatchTableLoad = 1
            }
        }
        
        if controller == self.StatisticsVC
        {
            if  MatchDetailMainStruct.isStatisticsLoad == 0
            {
                print("self.StatisticsVC")
                self.StatisticsVC.callApiSelfStatisticsVC()
                MatchDetailMainStruct.isStatisticsLoad = 1
            }
        }

        
        if selectedSegment != nil {
            selectedSegment?.titleColor(UIColor.colorConstant.appLightBlueColor)
        }
        
        if segments.count > 0 {
            
            selectedSegment = segments[index]
            selectedSegment?.titleColor(UIColor.colorConstant.appLightBlueColor)
        }
    }
}






