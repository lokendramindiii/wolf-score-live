//
//  statndingViewCell.swift
//  WolfScore
//
//  Created by Mindiii on 2/23/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView

class statndingViewCell: UITableViewCell {
    
    
    @IBOutlet var lblPositon:UILabel!
    @IBOutlet var lblTeamName:UILabel!
    @IBOutlet var viewStandings:UIView!
    @IBOutlet var lblGamePlayed:UILabel!
    @IBOutlet var lblWon:UILabel!
    @IBOutlet var lbldraw:UILabel!
    @IBOutlet var lbllost:UILabel!
    @IBOutlet var lblGoalScore:UILabel!
    @IBOutlet var lblGoalAgainst:UILabel!
    @IBOutlet var lblGoalDifferecnc:UILabel!
    @IBOutlet var lblPoints:UILabel!
    
    @IBOutlet var viewline:UIView!
    
    @IBOutlet var imgLogoPath:UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    
    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblPositon , lblTeamName ,lblGamePlayed , lblWon , lbldraw,lbllost,lblGoalScore,lblGoalAgainst,lblGoalDifferecnc,lblPoints].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
    }
    
    func hide_skelton()
    {
        [lblPositon , lblTeamName ,lblGamePlayed , lblWon , lbldraw,lbllost,lblGoalScore,lblGoalAgainst,lblGoalDifferecnc,lblPoints ].forEach { $0?.hideSkeleton()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
