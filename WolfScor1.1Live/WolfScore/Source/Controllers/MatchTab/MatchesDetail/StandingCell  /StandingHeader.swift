//
//  StandingHeader.swift
//  WolfScore
//
//  Created by Mindiii on 2/23/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
class StandingHeader: UIView {
    @IBOutlet   weak var lbl_groupName: UILabel!
    @IBOutlet   weak var bgColor: UIView!
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "StandingHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
