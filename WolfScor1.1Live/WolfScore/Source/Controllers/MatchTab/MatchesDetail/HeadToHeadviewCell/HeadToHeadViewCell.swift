//
//  HeadToHeadViewCell.swift
//  WolfScore
//
//  Created by Mindiii on 5/7/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView
class HeadToHeadViewCell: UITableViewCell {

    @IBOutlet   weak var lblWins: UILabel!
    @IBOutlet   weak var lblDraws: UILabel!
    @IBOutlet   weak var lblWins2: UILabel!
    
    @IBOutlet   weak var lblwintext: UILabel!
    @IBOutlet   weak var lblDrawstext: UILabel!
    @IBOutlet   weak var lblWins2text: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblWins , lblDraws ,lblWins2,lblwintext,lblDrawstext,lblWins2text].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
    }
    
    func hide_skelton()
    {
        [lblWins , lblDraws ,lblWins2,lblwintext,lblDrawstext,lblWins2text ].forEach { $0?.hideSkeleton()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
