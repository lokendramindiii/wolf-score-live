//
//  HeadToHeadVC.swift
//  WolfScore
//
//  Created by Mindiii on 17/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import SVProgressHUD
import AlamofireImage
import GoogleMobileAds

class HeadToHeadVC: UIViewController {
    
    var currentPageIndex = 1
    var totalPages = 0
    var pullToRefreshCtrl:UIRefreshControl!
    var Ispulltorefresh = false
    var arrMatches = [MatchesModelHeadToHead]()
    
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!

    @IBOutlet weak var viewBannerBottom: UIView!

    @IBOutlet var lblLocalTeamWinCount:UILabel!
    @IBOutlet var lblVisiterTeamWinCount:UILabel!
    @IBOutlet var lblDrowCount:UILabel!
    
    @IBOutlet weak var tblMatches: UITableView!
    var isLoadData = false
    var placeholderRow = 10

    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tblMatches.tableFooterView = UIView()
            } else {
                self.tblMatches.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        SVProgressHUD.setDefaultMaskType(.clear)
        setPullToRefresh()
        self.bannerAdSetup()
        tblMatches.register(UINib(nibName: "MatchesSkeltonCell", bundle: Bundle.main), forCellReuseIdentifier: "MatchesSkeltonCell")
    }
    
    func callApiSelfHeadToHeadVC()
    {
       self.apiCallGetHeadToHead()
    }

    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tblMatches.refreshControl  = pullToRefreshCtrl
        } else {
            tblMatches.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        sender.endRefreshing()
        if Ispulltorefresh == false
        {
            currentPageIndex = 1
            shouldLoadMore = false
            self.apiCallGetHeadToHead()
            sender.endRefreshing()
        }
    }
    
  
    
  
    func apiCallGetHeadToHead()
    {
        let dictPram = ["team_one_id":objAppShareData.str_Local_Team_Id,"page":currentPageIndex,"team_two_id":objAppShareData.str_Visiter_Team_Id] as [String: AnyObject]
        print(dictPram)
        call_Webservice_Get_HeadToHead_List(dict_param: dictPram)
    }
    
    
    
}
extension HeadToHeadVC{
    
    func call_Webservice_Get_HeadToHead_List(dict_param:[String:AnyObject]) {
        
      //  SVProgressHUD.show()
        self.Ispulltorefresh = true
        
        objWebServiceManager.requestGet(strURL: webUrl.get_head_to_head, params: dict_param, success: { (response) in
            print(response)
            self.isLoadData = true
            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    if let metadata = data ["meta"] as? [String:Any]
                    {
                        let data1 = metadata["pagination"] as? [String:Any]
                        let TotalPages = data1?["total_pages"] as? Int  ?? 0
                        self.totalPages = TotalPages
                    }
                    
                    if let arrData = data["data"] as? [[String: Any]]{
                        
                        if self.currentPageIndex == 1
                        {
                            self.arrMatches.removeAll()
                            self.tblMatches.reloadData()
                        }
                        var localWin:Int = 0
                        var visiterWin:Int = 0
                        var Drow:Int = 0
                        for dict in arrData{
                            let obj = MatchesModelHeadToHead.init(fromDictionary: dict)
                            let intLTScore = (obj.strLocalTeamScore as NSString).integerValue
                            let intVTScore = (obj.strVisitorTeamScore as NSString).integerValue
                            if (intLTScore > intVTScore) {
                                if objAppShareData.str_Local_Team_Id == obj.strLocalTeamId{
                                    localWin = localWin + 1
                                }else{
                                    visiterWin = visiterWin + 1
                                }
                            }else if(intLTScore < intVTScore){
                                if objAppShareData.str_Visiter_Team_Id == obj.strVisitorTeamId{
                                    visiterWin = visiterWin + 1
                                }else{
                                    localWin = localWin + 1
                                }
                            }else if(intLTScore == intVTScore ){
                                Drow = Drow + 1
                            }
                            self.arrMatches.append(obj)
                        }
                        if self.currentPageIndex  < self.totalPages
                        {
                            self.currentPageIndex += 1
                            self.shouldLoadMore = true
                        }
                        
                        self.lblLocalTeamWinCount.text = "\(localWin)"
                        self.lblDrowCount.text = "\(Drow)"
                        self.lblVisiterTeamWinCount.text = "\(visiterWin)"
                        
                        self.tblMatches.reloadData()
                        
                        print("drow == \(Drow)")
                        print("localWin == \(localWin)")
                        print("visiterWin == \(visiterWin)")
                    }
                }
            }
            self.Ispulltorefresh = false
            
        }) { (error) in
            print(error)
            self.isLoadData = true
            SVProgressHUD.dismiss()
        }
    }
}

// MARK: - TableView Delegates & Datasource

extension HeadToHeadVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isLoadData == false
        {
            return placeholderRow
        }
        return self.arrMatches.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeadToHeadViewCell", for: indexPath) as! HeadToHeadViewCell
           
            if isLoadData == false
            {
               cell.show_skelton()
            }
            else
            {
            cell.hide_skelton()
            cell.lblWins.text =  lblLocalTeamWinCount.text
            cell.lblDraws.text = lblDrowCount.text
            cell.lblWins2.text = lblVisiterTeamWinCount.text
            }
            return  cell
        }
    
        if isLoadData == false
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesSkeltonCell", for: indexPath) as! MatchesSkeltonCell
            cell.show_skelton()
            return cell
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesTableViewCell", for: indexPath) as? MatchesTableViewCell{
            
            cell.viewStatus.layer.cornerRadius = 4.0
            cell.viewStatus.layer.masksToBounds = true
            
            let objMatch = self.arrMatches[indexPath.row - 1]
            cell.lblLocalTeamName.text = objMatch.strNameloacalTeam
            cell.lblVisitorTeamName.text = objMatch.strNameVisitorTeam
            
            cell.lblScore.text = objMatch.strLocalTeamScore + " - " + objMatch.strVisitorTeamScore
            
            if objMatch.strStatus == "FT"
            {
                cell.lblStatus.text = ""
                cell.viewStatus.isHidden = true
            }
            else
            {
                cell.lblStatus.text = objMatch.strStatus
                cell.viewStatus.isHidden = false
            }

            cell.stackView.isHidden = false
            cell.lblStartTime.isHidden = true
            cell.lblDate.text = objMatch.strDate
            
            if let url = URL(string: objMatch.strLogopathLocalTeam ){
                cell.imgLocal.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else{
                cell.imgLocal.image = UIImage(named: "icon_placeholderTeam")
            }
            
            if let url = URL(string: objMatch.strLogopathVisitorTeam ){
                cell.imgVisitor.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgVisitor.image = UIImage(named: "icon_placeholderTeam")

            }
            
            cell.lblLeagueName.layer.cornerRadius = 10
            cell.lblLeagueName.layer.masksToBounds = true
            
            cell.lblLeagueName.text = "  " + objMatch.strLeagueName + "     "

          //  cell.lblLeagueName.text = "  " + objMatch.strLeagueName + " - Round " + objMatch.strRoundName + "  "
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == (arrMatches.count - 1) && shouldLoadMore == true {
            self.shouldLoadMore = false
            self.apiCallGetHeadToHead()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 || isLoadData == false
        {
            return
        }
        let objMatch = arrMatches[indexPath.row - 1]
        let viewController = UIStoryboard(name: "MatchesTab",bundle: nil).instantiateViewController(withIdentifier: "MatchDetailMainVC") as! MatchDetailMainVC
        objAppShareData.str_Local_Team_Id = objMatch.strLocalTeamId
        objAppShareData.str_Visiter_Team_Id = objMatch.strVisitorTeamId
        objAppShareData.str_match_Id = objMatch.fixtureId
        objAppShareData.str_season_Id = objMatch.seasonId
    self.navigationController?.pushViewController(viewController, animated: true)

    }
}


extension HeadToHeadVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}

extension HeadToHeadVC: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return tblMatches
    }
}
