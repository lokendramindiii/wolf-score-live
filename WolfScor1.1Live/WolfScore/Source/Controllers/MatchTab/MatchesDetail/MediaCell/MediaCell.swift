//
//  MediaCell.swift
//  WolfScore
//
//  Created by mac on 28/02/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView
class MediaCell: UITableViewCell {
    
    
    @IBOutlet   weak var imgViewVedio: UIImageView!
    @IBOutlet   weak var lblTime: UILabel!
    @IBOutlet   weak var lblTitle: UILabel!
    @IBOutlet   weak var btnPlay: UIButton!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }

    func show_skelton()
    {
       
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)

        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))

        [lblTitle,lblTime].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
        [imgViewVedio].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
    }
    
    func hide_skelton()
    {
        [lblTitle,lblTime].forEach { $0?.hideSkeleton()
        }
        [imgViewVedio].forEach { $0?.hideSkeleton()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
