//
//  LiveTickerCell.swift
//  WolfScore
//
//  Created by mac on 26/02/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView
class LiveTickerCell: UITableViewCell {
    
    @IBOutlet   weak var lblCommentry: UILabel!
    @IBOutlet   weak var lblTitle: UILabel!
    @IBOutlet   weak var btnTime: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblCommentry , lblTitle ].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
        [btnTime].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
    }
    
    func hide_skelton()
    {
        [lblCommentry , lblTitle].forEach { $0?.hideSkeleton()
        }
        [btnTime].forEach { $0?.hideSkeleton()
        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
