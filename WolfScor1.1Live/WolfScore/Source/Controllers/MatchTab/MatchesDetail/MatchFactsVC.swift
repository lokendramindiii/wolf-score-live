//
//  MatchFactsVC.swift
//  WolfScore
//
//  Created by Mindiii on 17/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import Alamofire
import SVProgressHUD
import GoogleMobileAds

class MatchFactsVC: UIViewController {

    var strFixtureId = ""
    var isLoadapi = false

    var closerfilterbyleagueId:((_ leaugeId:String ) ->())?
    
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!

    
    var  visitorRecentFormation : String = ""
    var  localRecentFormation  : String = ""
    


    @IBOutlet weak var lblNoRecodFound:UILabel!
    
    @IBOutlet weak var tblMatchEvents:UITableView!
    @IBOutlet weak var viewBannerBottom: UIView!

    var objectMatchDetails =  ModelMatchDetail.init(fromDictionary: [:])
    //Suresh
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.setDefaultMaskType(.clear)
        MatchsVcVariable.strfilter = "MatchFactsVC"
        
        tblMatchEvents.register(UINib(nibName: "kGADAdSizeMediumCell", bundle: Bundle.main), forCellReuseIdentifier: "kGADAdSizeMediumCell")
        
        tblMatchEvents.register(UINib(nibName: "MatchfactDetailCell", bundle: Bundle.main), forCellReuseIdentifier: "MatchfactDetailCell")
        
        tblMatchEvents.register(UINib(nibName: "TeamFormViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TeamFormViewCell")

        tblMatchEvents.estimatedRowHeight = 350.0
        tblMatchEvents.rowHeight = UITableViewAutomaticDimension
        
        self.bannerAdSetup()
        
        if isLoadapi == true{
            
            self.call_Webservice_Get_Detail(str_matchId: objAppShareData.str_match_Id)
            
            if objAppShareData.str_season_Id != ""{
               self.call_Webservice_Get_standings(str_seasonId: objAppShareData.str_season_Id)
                }
        }
        else{
            self.lblNoRecodFound.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
        if APP_DELEGATE.isInterstialPresent == true
        {
            self.viewDidAppear(true)
            return
        }
        MatchsVcVariable.strfilter = "MatchFactsVC"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if APP_DELEGATE.isInterstialPresent == true
        {
            APP_DELEGATE.isInterstialPresent = false
            return
        }
        MatchsVcVariable.strfilter = "MatchFactsVC"
    }
    
    
    func filterByLeaugeId(leaugeId:String ){
        
        strFixtureId = leaugeId
    }
   
    @IBAction func btnAction(_ sender : UIButton){
        self.view.endEditing(true)
    }
    
  
}

//MARK: - tableView delegate method
extension MatchFactsVC:UITableViewDelegate,UITableViewDataSource  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        let string : String = self.localRecentFormation
        if string == ""
        {
            return 3
        }
        else
        {
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        if indexPath.section == 3
        {
            return 100
        }
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        let string : String = self.localRecentFormation
        
        if string == ""
        {
            if section == 1 || section == 2
            {
                return 1
            }
            else
            {
                return objectMatchDetails.arrMatchFactModel.count
            }
        }
        else
        {
            if section == 1 || section == 2 || section == 3
            {
                return 1
            }
            else
            {
                return objectMatchDetails.arrMatchFactModel.count
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
    
        if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "kGADAdSizeMediumCell", for: indexPath) as! kGADAdSizeMediumCell
            
            bannerViewDfp.frame = CGRect(x:(SCREEN_WIDTH - bannerViewDfp.frame.size.width)/2,
                                         y: 10,
                                         width: bannerViewDfp.frame.size.width,
                                         height: bannerViewDfp.frame.size.height)
            cell.GoogleBannerView.addSubview(bannerViewDfp)
            return cell
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MatchfactDetailCell", for: indexPath) as! MatchfactDetailCell
            
            if self.objectMatchDetails.strVenueName == ""{
                cell.lblStaduiam.text = "NA"
            }else{
                cell.lblStaduiam.text = self.objectMatchDetails.strVenueName
            }
            
            if self.objectMatchDetails.strLeagueName == ""{
                cell.lblLeagueName.text = "NA"
            }else{
                cell.lblLeagueName.text = self.objectMatchDetails.strLeagueName
            }
            
            if self.objectMatchDetails.strRefereeCommonName == ""{
                cell.lblRaferees.text = "NA"
            }else{
                cell.lblRaferees.text = self.objectMatchDetails.strRefereeCommonName
            }
            
            if self.objectMatchDetails.strAttendance == ""{
                cell.lblAttedance.text = "NA"
            }else{
                cell.lblAttedance.text = self.objectMatchDetails.strAttendance
            }
            
            if self.objectMatchDetails.strDateTime == nil
            {
                cell.lblDateTime.text = "NA"
            }
            else
            {
                cell.lblDateTime.text = dayDate.dateFormatedWithTime(strDate: self.objectMatchDetails.strDateTime)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                
                cell.viewStadium.layer.masksToBounds = true
                cell.viewStadium.layer.cornerRadius = cell.viewStadium.frame.size.height / 2
                
                cell.viewTournament.layer.masksToBounds = true
                cell.viewTournament.layer.cornerRadius = cell.viewTournament.frame.size.height / 2
            })
            
            return cell
        }
       else if indexPath.section == 3
        {
          
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamFormViewCell", for: indexPath) as! TeamFormViewCell
            
            let string : String = self.localRecentFormation
            let localRecentFormationArr = Array(string)
            
            for i in 0..<localRecentFormationArr.count
            {
                if i == 0
                {
                    cell.lbl_LocalFirst.text = "\(localRecentFormationArr[i])"
                    cell.lbl_LocalFirst.layer.masksToBounds = true
                    cell.lbl_LocalFirst.layer.cornerRadius = 5
                    
                    if localRecentFormationArr[i] == "W"
                    {
                        cell.lbl_LocalFirst.backgroundColor = UIColor(red: 9/255.0, green: 161/255.0, blue: 40/255.0, alpha: 1.0)
                    }
                    
                    if localRecentFormationArr[i] == "D"
                    {
                        cell.lbl_LocalFirst.backgroundColor = UIColor(red: 122/255.0, green: 122/255.0, blue: 122/255.0, alpha: 1.0)
                    }
                    if localRecentFormationArr[i] == "L"
                    {
                        cell.lbl_LocalFirst.backgroundColor = UIColor(red: 206/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
                    }
                }
                
                else if i == 1
                {
                    cell.lbl_LocalSecond.text = "\(localRecentFormationArr[i])"
                    cell.lbl_LocalSecond.layer.masksToBounds = true
                    cell.lbl_LocalSecond.layer.cornerRadius = 5
                    
                    if localRecentFormationArr[i] == "W"
                    {
                        cell.lbl_LocalSecond.backgroundColor = UIColor(red: 9/255.0, green: 161/255.0, blue: 40/255.0, alpha: 1.0)
                    }
                    if localRecentFormationArr[i] == "D"
                    {
                        cell.lbl_LocalSecond.backgroundColor = UIColor(red: 122/255.0, green: 122/255.0, blue: 122/255.0, alpha: 1.0)
                    }
                    if localRecentFormationArr[i] == "L"
                    {
                        cell.lbl_LocalSecond.backgroundColor = UIColor(red: 206/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
                    }
                    
                }
                else if i == 2
                {
                    cell.lbl_LocalThird.text = "\(localRecentFormationArr[i])"
                    cell.lbl_LocalThird.layer.masksToBounds = true
                    cell.lbl_LocalThird.layer.cornerRadius = 5
                    
                    if localRecentFormationArr[i] == "W"
                    {
                        cell.lbl_LocalThird.backgroundColor = UIColor(red: 9/255.0, green: 161/255.0, blue: 40/255.0, alpha: 1.0)
                    }
                    if localRecentFormationArr[i] == "D"
                    {
                        cell.lbl_LocalThird.backgroundColor = UIColor(red: 122/255.0, green: 122/255.0, blue: 122/255.0, alpha: 1.0)
                    }
                    if localRecentFormationArr[i] == "L"
                    {
                        cell.lbl_LocalThird.backgroundColor = UIColor(red: 206/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
                    }
                }
                else if i == 3
                {
                    cell.lbl_LocalForth.text = "\(localRecentFormationArr[i])"
                    cell.lbl_LocalForth.layer.masksToBounds = true
                    cell.lbl_LocalForth.layer.cornerRadius = 5
                    
                    if localRecentFormationArr[i] == "W"
                    {
                        cell.lbl_LocalForth.backgroundColor = UIColor(red: 9/255.0, green: 161/255.0, blue: 40/255.0, alpha: 1.0)
                    }
                    if localRecentFormationArr[i] == "D"
                    {
                        cell.lbl_LocalForth.backgroundColor = UIColor(red: 122/255.0, green: 122/255.0, blue: 122/255.0, alpha: 1.0)
                    }
                    if localRecentFormationArr[i] == "L"
                    {
                        cell.lbl_LocalForth.backgroundColor = UIColor(red: 206/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
                    }
                }
                else if i == 4
                {
                    cell.lbl_LocalFive.text = "\(localRecentFormationArr[i])"
                    cell.lbl_LocalFive.layer.masksToBounds = true
                    cell.lbl_LocalFive.layer.cornerRadius = 5
                    
                    if localRecentFormationArr[i] == "W"
                    {
                        cell.lbl_LocalFive.backgroundColor = UIColor(red: 9/255.0, green: 161/255.0, blue: 40/255.0, alpha: 1.0)
                    }
                    if localRecentFormationArr[i] == "D"
                    {
                        cell.lbl_LocalFive.backgroundColor = UIColor(red: 122/255.0, green: 122/255.0, blue: 122/255.0, alpha: 1.0)
                    }
                    if localRecentFormationArr[i] == "L"
                    {
                        cell.lbl_LocalFive.backgroundColor = UIColor(red: 206/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
                    }
                }
            }
            
            
  // -----------     visitor team ------------
            
            
            let string1 : String = self.visitorRecentFormation
            
            if string1 == ""
            {
                cell.ImageVisitor.isHidden = true
            }
            let VisitorRecentFormationArr = Array(string1)
            for i in 0..<VisitorRecentFormationArr.count
            {
                
                if i == 0
                {
                    cell.lblVisitorFirst.text = "\(VisitorRecentFormationArr[i])"
                    
                    cell.lblVisitorFirst.layer.masksToBounds = true
                    cell.lblVisitorFirst.layer.cornerRadius = 5
                    if VisitorRecentFormationArr[i] == "W"
                    {
                        cell.lblVisitorFirst.backgroundColor = UIColor(red: 9/255.0, green: 161/255.0, blue: 40/255.0, alpha: 1.0)
                    }
                    if VisitorRecentFormationArr[i] == "D"
                    {
                        cell.lblVisitorFirst.backgroundColor = UIColor(red: 122/255.0, green: 122/255.0, blue: 122/255.0, alpha: 1.0)
                    }
                    if VisitorRecentFormationArr[i] == "L"
                    {
                        cell.lblVisitorFirst.backgroundColor = UIColor(red: 206/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
                    }
                }
                    
                else if i == 1
                {
                    
                    cell.lblVisitorSecond.text = "\(VisitorRecentFormationArr[i])"
                    cell.lblVisitorSecond.layer.masksToBounds = true
                    cell.lblVisitorSecond.layer.cornerRadius = 5
                    
                    if VisitorRecentFormationArr[i] == "W"
                    {
                        cell.lblVisitorSecond.backgroundColor = UIColor(red: 9/255.0, green: 161/255.0, blue: 40/255.0, alpha: 1.0)
                    }
                    if VisitorRecentFormationArr[i] == "D"
                    {
                        cell.lblVisitorSecond.backgroundColor = UIColor(red: 122/255.0, green: 122/255.0, blue: 122/255.0, alpha: 1.0)
                    }
                    if VisitorRecentFormationArr[i] == "L"
                    {
                        cell.lblVisitorSecond.backgroundColor = UIColor(red: 206/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
                    }
                    
                }
                else if i == 2
                {
                    cell.lblVisitorThird.text = "\(VisitorRecentFormationArr[i])"
                    cell.lblVisitorThird.layer.masksToBounds = true
                    cell.lblVisitorThird.layer.cornerRadius = 5
                    
                    if VisitorRecentFormationArr[i] == "W"
                    {
                        cell.lblVisitorThird.backgroundColor = UIColor(red: 9/255.0, green: 161/255.0, blue: 40/255.0, alpha: 1.0)
                    }
                    if VisitorRecentFormationArr[i] == "D"
                    {
                        cell.lblVisitorThird.backgroundColor = UIColor(red: 122/255.0, green: 122/255.0, blue: 122/255.0, alpha: 1.0)
                    }
                    if VisitorRecentFormationArr[i] == "L"
                    {
                        cell.lblVisitorThird.backgroundColor = UIColor(red: 206/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
                    }
                    
                }
                else if i == 3
                {
                  
                    cell.lblVisitorForth.text = "\(VisitorRecentFormationArr[i])"
                    cell.lblVisitorForth.layer.masksToBounds = true
                    cell.lblVisitorForth.layer.cornerRadius = 5
                    
                    if VisitorRecentFormationArr[i] == "W"
                    {
                        cell.lblVisitorForth.backgroundColor = UIColor(red: 9/255.0, green: 161/255.0, blue: 40/255.0, alpha: 1.0)
                    }
                    if VisitorRecentFormationArr[i] == "D"
                    {
                        cell.lblVisitorForth.backgroundColor = UIColor(red: 122/255.0, green: 122/255.0, blue: 122/255.0, alpha: 1.0)
                    }
                    if VisitorRecentFormationArr[i] == "L"
                    {
                        cell.lblVisitorForth.backgroundColor = UIColor(red: 206/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
                    }
                    
                }
                else if i == 4
                {
                    
                    cell.lblVisitorFive.text = "\(VisitorRecentFormationArr[i])"
                    cell.lblVisitorFive.layer.masksToBounds = true
                    cell.lblVisitorFive.layer.cornerRadius = 5
                    
                    if VisitorRecentFormationArr[i] == "W"
                    {
                        cell.lblVisitorFive.backgroundColor =  UIColor(red: 9/255.0, green: 161/255.0, blue: 40/255.0, alpha: 1.0)
                    }
                    if VisitorRecentFormationArr[i] == "D"
                    {
                        cell.lblVisitorFive.backgroundColor = UIColor(red: 122/255.0, green: 122/255.0, blue: 122/255.0, alpha: 1.0)
                    }
                    if VisitorRecentFormationArr[i] == "L"
                    {
                        cell.lblVisitorFive.backgroundColor = UIColor(red: 206/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
                    }
                }
            }
            
            return cell
        }
      
        else{
        let objMatch = objectMatchDetails.arrMatchFactModel[indexPath.row]
        if objMatch.strTeamId == objectMatchDetails.strLocalTeamId {
            
            switch (objMatch.strType){
            case "yellowcard":
                print("yellowcard")
                let cellIdentifier = "LocalTeamCardCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! LocalTeamCardCell
                cell.hide_skelton()
                cell.lblPlayerName.text = objMatch.strPlayerName
                cell.imageCard.image = UIImage.init(named: "yellow_card")
                cell.lblMinuts.text = objMatch.strMinute
                return cell
            case "redcard":
                print("redcard")
                let cellIdentifier = "LocalTeamCardCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! LocalTeamCardCell
                cell.lblPlayerName.text = objMatch.strPlayerName
                cell.imageCard.image = UIImage.init(named: "red_card")
                cell.lblMinuts.text = objMatch.strMinute
                return cell
            case "substitution":
                print("substitution")
                let cellIdentifier = "LocalTeamSubsituteCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! LocalTeamSubsituteCell
                cell.imagePlayerIn.image = UIImage.init(named: "in_icon")
                cell.imagePlayerOut.image = UIImage.init(named: "out_icon")
                cell.lblPlayerInName.text = objMatch.strPlayerName
                cell.lblPlayerOutName.text = objMatch.strRelatedPlayerName
                cell.lblMinuts.text = objMatch.strMinute
                return cell
            case "goal":
                print("goal")
                let cellIdentifier = "LocalTeamGoalCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! LocalTeamGoalCell
                cell.lblPlayerName.text = objMatch.strPlayerName
                cell.lblMinuts.text = objMatch.strMinute
                return cell
            default:
                print("no case")
                return UITableViewCell()
            }
        }
        else{
            switch (objMatch.strType){
            case "yellowcard":
                print("yellowcard")
                let cellIdentifier = "VisitorTeamCardCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! VisitorTeamCardCell
                cell.hide_skelton()
                cell.lblPlayerName.text = objMatch.strPlayerName
                cell.imageCard.image = UIImage.init(named: "yellow_card")
                cell.lblMinuts.text = objMatch.strMinute
                return cell
            case "redcard":
                print("redcard")
                let cellIdentifier = "VisitorTeamCardCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! VisitorTeamCardCell
                cell.lblPlayerName.text = objMatch.strPlayerName
                cell.imageCard.image = UIImage.init(named: "red_card")
                cell.lblMinuts.text = objMatch.strMinute
                return cell
            case "substitution":
                print("substitution")
                let cellIdentifier = "VisitorTeamSubsituteCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! VisitorTeamSubsituteCell

                cell.imagePlayerIn.image = UIImage.init(named: "in_iconVisitor")
                cell.imagePlayerOut.image = UIImage.init(named: "out_iconVisitor")
                cell.lblPlayerInName.text = objMatch.strPlayerName
                cell.lblPlayerOutName.text = objMatch.strRelatedPlayerName
                cell.lblMinuts.text = objMatch.strMinute
                return cell
            case "goal":
                print("goal")
                let cellIdentifier = "VisitorTeamGoalCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! VisitorTeamGoalCell
                cell.viewSkeltonBg.isHidden = true
                cell.lblPlayerName.text = objMatch.strPlayerName
                cell.lblMinuts.text = objMatch.strMinute
                return cell
            default:
                 print("no case")
                return UITableViewCell()
            }
        }
      }
   }
}

extension MatchFactsVC

{
    
    func call_Webservice_Get_standings(str_seasonId:String) {
       // SVProgressHUD.show()
        let paramDict = ["season_id":str_seasonId] as [String:AnyObject]
        objWebServiceManager.requestGet(strURL: webUrl.get_standings_detail, params: paramDict, success: { (response) in
            print("--Statnding----------\(response)")
            SVProgressHUD.dismiss()
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                if let dataresponse = response["data"] as? [String:Any]
                {
                    
                    if let data = dataresponse ["data"] as? [[String:Any]] {
                        
                        if data.count == 0
                        {
                            return
                        }
                        for j in 0..<data.count
                        {
                            
                            if let standingDic = data[j]["standings"] as? [String:Any] {
                                var standingArray = [[String:Any]]()
                                standingArray = standingDic["data"] as! [[String:Any]]
                                
                                for i in 0..<standingArray.count
                                {
                                    let  teamid = standingArray[i]["team_id"] as? Int ?? 0
                                    let  StrTeamId:String = String(teamid)
                                    
                                    print(StrTeamId)
                                    
                                    if objAppShareData.str_Visiter_Team_Id  ==  StrTeamId
                                    {
                                        self.visitorRecentFormation = standingArray[i]["recent_form"] as? String ?? ""
                                    }
                                    if objAppShareData.str_Local_Team_Id  == StrTeamId
                                    {
                                        self.localRecentFormation = standingArray[i]["recent_form"] as? String ?? ""
                                    }
                                }
                            }
                        }
                        self.tblMatchEvents.reloadData()
                    }
                }
            }
            else{
                GlobalUtility.showToastMessage(msg: message)
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    

    func call_Webservice_Get_Detail(str_matchId:String) {
        let paramDict = ["match_id":str_matchId,"time_zone":objAppShareData.localTimeZoneName] as [String:AnyObject]
        objWebServiceManager.requestGet(strURL: webUrl.get_match_details, params: paramDict, success: { (response) in
            print(response)
            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    if let dic = data["data"] as? [String: Any]{
                        self.objectMatchDetails =  ModelMatchDetail.init(fromDictionary: dic)
                        self.tblMatchEvents.reloadData()
                        self.tblMatchEvents.isHidden = false
                    }
                }
            }else{
                self.lblNoRecodFound.isHidden = false
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }

}


extension MatchFactsVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
      //  request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}



extension MatchFactsVC: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return tblMatchEvents
    }
}
