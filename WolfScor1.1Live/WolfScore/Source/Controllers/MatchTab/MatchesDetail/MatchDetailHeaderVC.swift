//
//  MatchDetailHeaderVC.swift
//  WolfScore
//
//  Created by Macbook-Lokendra on 26/07/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SJSegmentedScrollView

class MatchDetailHeaderVC: UIViewController {

    @IBOutlet weak var lblScore:UILabel!
    @IBOutlet weak var imageLocalTeam:UIImageView!
    @IBOutlet weak var imageVisiterTeam:UIImageView!
    @IBOutlet weak var lblLocalTeamName:UILabel!
    @IBOutlet weak var lblVisiterTeamName:UILabel!
    @IBOutlet weak var lblMatchStatus:UILabel!
    
    
    var dict:[String:Any] = [:]
    
    var timer: Timer?
    var totalTime = 0
    var strLocalTeamId: String = ""
    var strVisitorTeamId: String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
   
        if let dic = self.dict["data"] as? [String: Any]{
            let object =  ModelMatchDetail.init(fromDictionary: dic)
            
            self.lblLocalTeamName.text = object.strLocalTeamName
            self.lblVisiterTeamName.text = object.strVisitorTeamName
            
            objAppShareData.str_Local_Team_Name =
                object.strLocalTeamName
            objAppShareData.str_Visiter_Team_Name = object.strVisitorTeamName
            
            
            self.strLocalTeamId = object.strLocalTeamId
            self.strVisitorTeamId = object.strVisitorTeamId
            
            objAppShareData.str_Local_Team_Id =
                self.strLocalTeamId
            objAppShareData.str_Visiter_Team_Id = self.strVisitorTeamId
            
            // Get time diffrence from current utc time to starting match
            var diffrence =   self.TimeDifferenceBetweenCurrentUtcToYourDate(yourDate: object.strDateTime ?? "")
            
            diffrence = diffrence.replacingOccurrences(of: "-", with: "")
            diffrence = diffrence.replacingOccurrences(of: "day:", with: "")
            diffrence = diffrence.replacingOccurrences(of: "hour:", with: ",")
            diffrence = diffrence.replacingOccurrences(of: "minute:", with: ",")
            diffrence = diffrence.replacingOccurrences(of: "second:", with: ",")
            diffrence = diffrence.replacingOccurrences(of: "isLeapMonth:", with: ",")
            
            if diffrence == ""
            {
                self.lblMatchStatus.text = object.strStatus
            }
            else
            {
                let DayMinutesSecond = diffrence.components(separatedBy: ",")
                // Today match check for - DayMinutesSecond[0] == " 0 "
                if DayMinutesSecond[0] == " 0 "
                {
                    // tomorrow match less then 24 hours  will be show tomarrow status
                    //current time compare to starting match
                    let CurrentDate = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                    let result = formatter.string(from: CurrentDate)
                    if result.compare(object.strDate ?? "") == ComparisonResult.orderedAscending {
                        self.lblMatchStatus.text =  "Tomorrow"
                    }
                    else{
                        
                        // Today match  time show will be Hours Minuets and second
                        var RemainingTime = DayMinutesSecond[1] + ":" +  DayMinutesSecond[2] + ":" + DayMinutesSecond[3]
                        RemainingTime = RemainingTime.replacingOccurrences(of: " ", with: "")
                        let HoursMinuteSecond = RemainingTime.components(separatedBy: ":")
                        
                        self.totalTime = Int(HoursMinuteSecond[0])! * 3600 + Int(HoursMinuteSecond[1])! * 60 + Int(HoursMinuteSecond[2])!
                        
                        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
                    }
                }
                    // Tomorrow match and upcoming date  for else condition
                else
                {
                    if DayMinutesSecond[0] == " 1 " && DayMinutesSecond[1] == " 0 "
                    {
                        self.lblMatchStatus.text =  "Tomorrow"
                    }
                    else
                    {
                        
                        var GetDay  = self.GetDiffrenceDate(yourDate: object.strDate ?? "")
                        
                        GetDay = GetDay.replacingOccurrences(of: "-", with: "")
                        
                        GetDay = GetDay.replacingOccurrences(of: "day:", with: "")
                        GetDay = GetDay.replacingOccurrences(of: "isLeapMonth:", with: ",")
                        
                        let day = GetDay.components(separatedBy: ",")
                        
                        self.lblMatchStatus.text = day[0] + "days"
                        
                    }
                }
            }
            
            if object.strStatus == "LIVE"{
                self.lblScore.text = object.strLocalScore + " - " + object.strVisitorScore
                self.lblMatchStatus.text = object.strStatus
            }
                
            else if object.strStatus == "CANCL"{
                self.lblMatchStatus.text = "CANCEL"
            }
                
            else if object.strStatus == "FT_PEN" || object.strStatus == "FT" || object.strStatus == "HF"{
                self.lblScore.text = object.strLocalScore + " - " + object.strVisitorScore
                self.lblMatchStatus.text = object.strStatus
            }else{
                self.lblScore.text =  dayDate.Dateformate24_hours(strTime: object.strTime)
            }
            
            if let url = URL(string: object.strLocalLogoPath ?? ""){
                self.imageLocalTeam.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            
            if let url = URL(string: object.strVisitorLogoPath ?? ""){
                self.imageVisiterTeam.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if APP_DELEGATE.isBannerDisappearShow == true
        {
            return
        }
        self.timer?.invalidate()
        self.timer = nil
    }

    
    @objc func updateTimer()
    {
        self.lblMatchStatus.text = self.timeFormatted(self.totalTime) // will show timer
        if totalTime != 0
        {
            totalTime -= 1
        }
        else {
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
            }
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let hours: Int = (totalSeconds / 3600)
        let minutes: Int = (totalSeconds / 60) % 60
        let seconds: Int = totalSeconds % 60
        return String(format: "%02d:%02d:%02d",hours ,minutes, seconds)
        
    }
    
    func TimeDifferenceBetweenCurrentUtcToYourDate(yourDate:String) -> String
    {
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //  formatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        let CurrentTimeUTC = formatter.string(from: date as Date)
        
        if yourDate.compare( CurrentTimeUTC) == ComparisonResult.orderedDescending {
            var userCalendar = Calendar.current
            userCalendar.timeZone = TimeZone.current
            let requestedComponent: Set<Calendar.Component> = [.day,.hour,.minute,.second]
            
            let timeDifference = userCalendar.dateComponents(requestedComponent, from:formatter.date(from: yourDate)! , to:formatter.date(from: CurrentTimeUTC)!)
            
            print(timeDifference)
            
            return String("\(timeDifference)")
        }
        return String("")
    }
    
    func GetDiffrenceDate(yourDate:String) -> String
    {
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let CurrentTimeUTC = formatter.string(from: date as Date)
        
        if yourDate.compare( CurrentTimeUTC) == ComparisonResult.orderedDescending {
            var userCalendar = Calendar.current
            userCalendar.timeZone = TimeZone.current
            let requestedComponent: Set<Calendar.Component> = [.day]
            
            let timeDifference = userCalendar.dateComponents(requestedComponent, from:formatter.date(from: yourDate)! , to:formatter.date(from: CurrentTimeUTC)!)
            
            return String("\(timeDifference)")
        }
        return String("")
    }

    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        objAppShareData.isLonchXLPageIndex = false
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTeamDetailAction(_ sender : UIButton){
        self.view.endEditing(true)
        if sender.tag == 0
        {
            objAppShareData.str_team_Id = self.strLocalTeamId
        }
        else
        {
            objAppShareData.str_team_Id = self.strVisitorTeamId
        }
        
        let viewController = UIStoryboard(name: "TableDetails",bundle: nil).instantiateViewController(withIdentifier: "TableDetailsVC") as! TableDetailsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
