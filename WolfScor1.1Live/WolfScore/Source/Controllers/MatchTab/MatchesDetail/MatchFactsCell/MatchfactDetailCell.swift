//
//  MatchfactDetailCell.swift
//  WolfScore
//
//  Created by Mindiii on 5/28/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class MatchfactDetailCell: UITableViewCell {

    @IBOutlet weak var lblLeagueName:UILabel!
    @IBOutlet weak var lblStaduiam:UILabel!
    @IBOutlet weak var lblRaferees:UILabel!
    @IBOutlet weak var lblAttedance:UILabel!
    @IBOutlet weak var lblDateTime:UILabel!
    @IBOutlet weak var viewStadium:UIView!
    @IBOutlet weak var viewTournament:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
