//
//  LocalTeamGoalCell.swift
//  WolfScore
//
//  Created by Mindiii on 2/12/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class LocalTeamGoalCell: UITableViewCell {

    @IBOutlet var lblPlayerName:UILabel!
    @IBOutlet var lblTypeName:UILabel!
    @IBOutlet var lblMinuts:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
