//
//  VisitorGoalCell.swift
//  WolfScore
//
//  Created by Mindiii on 2/12/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView
class VisitorTeamGoalCell: UITableViewCell {

    @IBOutlet var lblPlayerName:UILabel!
    @IBOutlet var lblTypeName:UILabel!
    @IBOutlet var lblMinuts:UILabel!
    
    @IBOutlet var lblfirst:UILabel!
    @IBOutlet var lblsecond:UILabel!
    @IBOutlet var imgPlaceholder:UIImageView!
    @IBOutlet var viewSkeltonBg:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblfirst , lblsecond  ].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        [imgPlaceholder ].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
    }
    
    func hide_skelton()
    {
        [lblfirst , lblsecond ].forEach { $0?.hideSkeleton()
        }
        [imgPlaceholder].forEach { $0?.hideSkeleton()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
