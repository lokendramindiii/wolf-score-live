//
//  TeamFormViewCell.swift
//  WolfScore
//
//  Created by Mindiii on 6/7/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class TeamFormViewCell: UITableViewCell {

    @IBOutlet weak var lbl_LocalFirst:UILabel!
    @IBOutlet weak var lbl_LocalSecond:UILabel!
    @IBOutlet weak var lbl_LocalThird:UILabel!
    @IBOutlet weak var lbl_LocalForth:UILabel!
    @IBOutlet weak var lbl_LocalFive:UILabel!
    @IBOutlet weak var ImageLocal:UIImageView!

    @IBOutlet weak var lblVisitorFirst:UILabel!
    @IBOutlet weak var lblVisitorSecond:UILabel!
    @IBOutlet weak var lblVisitorThird:UILabel!
    @IBOutlet weak var lblVisitorForth:UILabel!
    @IBOutlet weak var lblVisitorFive:UILabel!
    @IBOutlet weak var ImageVisitor:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
