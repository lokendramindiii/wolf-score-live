//
//  VisitorTeamSubsituteCell.swift
//  WolfScore
//
//  Created by Mindiii on 2/12/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView
class VisitorTeamSubsituteCell: UITableViewCell {

    @IBOutlet var lblPlayerInName:UILabel!
    @IBOutlet var lblPlayerOutName:UILabel!
    @IBOutlet var lblMinuts:UILabel!
    @IBOutlet var imagePlayerIn:UIImageView!
    @IBOutlet var imagePlayerOut:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
