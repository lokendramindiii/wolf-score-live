//
//  VisitorTeamCardCell.swift
//  WolfScore
//
//  Created by Mindiii on 2/12/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView
class VisitorTeamCardCell: UITableViewCell {

    @IBOutlet var lblMinuts:UILabel!
    @IBOutlet var lblPlayerName:UILabel!
    @IBOutlet var imageCard:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblMinuts , lblPlayerName ].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
        [imageCard ].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
        
    }
    
    func hide_skelton()
    {
        [lblMinuts , lblPlayerName ].forEach { $0?.hideSkeleton()
        }
        [imageCard].forEach { $0?.hideSkeleton()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
