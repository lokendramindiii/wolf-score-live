//
//  MatchMediaVC.swift
//  WolfScore
//
//  Created by Mindiii on 17/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SJSegmentedScrollView
import SVProgressHUD
import AlamofireImage
import GoogleMobileAds


class MatchMediaVC: UIViewController,AVPlayerViewControllerDelegate {
    @IBOutlet weak var viewBannerBottom: UIView!

    @IBOutlet weak var lblNoRecodFound:UILabel!
    @IBOutlet weak var tblMedia:UITableView!
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!
    var MediaLocalTeamName: String =  ""
    var MediaVisitorTeamName: String =  ""

    var arrMedia = [ModelMedia]()
    
    var isLoadData = false
    var placeholderRow = 10

    override func viewDidLoad() {
        super.viewDidLoad()
        self.bannerAdSetup()
        
    tblMedia.register(UINib(nibName: "kGADAdSizeMediumCell", bundle: Bundle.main), forCellReuseIdentifier: "kGADAdSizeMediumCell")
        // Do any additional setup after loading the view.
    }
    
    func callApiSelfMatchMediaVC()
    {
        self.call_Webservice_Get_Match_Highlight(str_matchId:objAppShareData.str_match_Id)
    }
    


    @objc func btnPlayPressed(sender: UIButton){
        let objTag = self.arrMedia[sender.tag]
        //let url = NSURL(string: urlstring)
        if objTag.strlocationUrl != "" {
           
            let url = URL(string: objTag.strlocationUrl)
             //let url = URL(string:  "https://cc.sporttube.com/embed/CDUCCCG")
            let player = AVPlayer(url:url!)
            let playerController = AVPlayerViewController()
            playerController.player = player
            playerController.allowsPictureInPicturePlayback = true
            playerController.delegate = self
            playerController.player?.play()
            self.present(playerController,animated:true,completion:nil)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tblMedia.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
    }
}

//MARK :- call_Webservice
extension MatchMediaVC{
    
    func call_Webservice_Get_Match_Highlight(str_matchId:String) {
      //  SVProgressHUD.show()
        let paramDict = ["match_id":str_matchId] as [String:AnyObject]
        objWebServiceManager.requestGet(strURL: webUrl.get_match_highlight, params: paramDict, success: { (response) in
            
            self.isLoadData = true

            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    if let dict = data["data"] as? [String: Any]{
                        if let dictHighlights = dict["highlights"] as? [String: Any]{
                          if let dicArray = dictHighlights["data"] as? [[String: Any]]{
                            self.arrMedia.removeAll()
                            
                            var intAfterThreebannerShow:Int
                            intAfterThreebannerShow = 0

                            for dic in dicArray{
                                
                                intAfterThreebannerShow = intAfterThreebannerShow + 1

                                if intAfterThreebannerShow % 3 == 0 {
                                    let objectMedia =  ModelMedia.init(dict: [:])
                                    self.arrMedia.append(objectMedia)
                                }

                                let objectMedia =  ModelMedia.init(dict: dic)
                                self.arrMedia.append(objectMedia)
                            }
                            
                            if self.arrMedia.count == 0{
                                self.lblNoRecodFound.isHidden = false
                            }else{
                                self.lblNoRecodFound.isHidden = true
                            }
                            self.tblMedia.reloadData()
                           }
                        }
                    }
                }
            }
        }) { (error) in
            print(error)
            self.isLoadData = true
            SVProgressHUD.dismiss()
        }
    }
}

//MARK: - tableView delegate method
extension MatchMediaVC:UITableViewDelegate,UITableViewDataSource  {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isLoadData  == false
        {
            return placeholderRow
        }
        
        return arrMedia.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadData  == false
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MediaCell", for: indexPath) as! MediaCell
            cell.show_skelton()
            
            return cell
        }
        
        let objMedia = arrMedia[indexPath.row]
        
        if objMedia.strDate == "" && objMedia.strlocationUrl == ""
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "kGADAdSizeMediumCell", for: indexPath) as! kGADAdSizeMediumCell
            
            for subview in cell.GoogleBannerView.subviews {
                subview.removeFromSuperview()
            }
            
            bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
            bannerViewDfp.adUnitID = objAppShareData.BannerId
            bannerViewDfp.rootViewController = self
            bannerViewDfp.load(DFPRequest())
            bannerViewDfp.frame = CGRect(x:(SCREEN_WIDTH - bannerViewDfp.frame.size.width)/2,
                                         y: 10,
                                         width: bannerViewDfp.frame.size.width,
                                         height: bannerViewDfp.frame.size.height+20)
            cell.GoogleBannerView.addSubview(bannerViewDfp)
            return cell
        }
        else
        {
            
            let cellIdentifier = "MediaCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MediaCell
            cell.hide_skelton()
            cell.imgViewVedio.image = objMedia.thumbImage
            
            cell.lblTitle.text = objAppShareData.str_Local_Team_Name + "  vs  " + objAppShareData.str_Visiter_Team_Name

            cell.lblTime.text = objMedia.strDate
            cell.btnPlay.tag = indexPath.row
            cell.btnPlay.addTarget(self, action: #selector(btnPlayPressed(sender:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = UIStoryboard(name: "UserTabbar",bundle: nil).instantiateViewController(withIdentifier: "WebViewNewVC") as! WebViewNewVC
        let objTag = self.arrMedia[indexPath.row]
        
        viewController.strUrl = objTag.strlocationUrl
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}


extension MatchMediaVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))

    }
}
extension MatchMediaVC: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return tblMedia
    }
}
