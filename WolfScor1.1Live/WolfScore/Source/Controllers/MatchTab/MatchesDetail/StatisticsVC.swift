//
//  StatisticsVC.swift
//  WolfScore
//
//  Created by mac on 25/02/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage
import MBCircularProgressBar
import GoogleMobileAds
import SkeletonView
import SJSegmentedScrollView
class StatisticsVC: UIViewController {
    
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!
    
    @IBOutlet var mbProgressBar: MBCircularProgressBarView!
    @IBOutlet weak var viewBannerBottom: UIView!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var viewOnTarget: UIView!
    @IBOutlet weak var viewOffTarget: UIView!
    @IBOutlet weak var viewBlocked: UIView!
    @IBOutlet weak var viewInsideBox: UIView!
    @IBOutlet weak var viewOutsideBox: UIView!
    @IBOutlet weak var lblProcession: UILabel!
    
    @IBOutlet weak var lblFullVislOutside: UILabel!
    @IBOutlet weak var lblFullLocalOutside: UILabel!
    
    @IBOutlet weak var lblFullVislInside: UILabel!
    @IBOutlet weak var lblFullLocalInside: UILabel!
    
    @IBOutlet weak var lblFullVisBlocked: UILabel!
    @IBOutlet weak var lblFullLocalBlocked: UILabel!
    
    @IBOutlet weak var lblFullVisOnTarget: UILabel!
    @IBOutlet weak var lblFullLocalOnTarget: UILabel!
    
    @IBOutlet weak var lblFullVisOffTarget: UILabel!
    @IBOutlet weak var lblFullLocalOffTarget: UILabel!
    
    @IBOutlet weak var lblFullVisTotal: UILabel!
    @IBOutlet weak var lblFullLocalTotal: UILabel!
    
    @IBOutlet weak var sliderLocTotal: customSlider!
    @IBOutlet weak var sliderVisTotal: customSlider!
    @IBOutlet weak var sliderLocOnTarget: customSlider!
    @IBOutlet weak var sliderVisOnTarget: customSlider!
    @IBOutlet weak var sliderLocOffTarget: customSlider!
    @IBOutlet weak var sliderVisOffTarget: customSlider!
    @IBOutlet weak var sliderLocBlocked: customSlider!
    @IBOutlet weak var sliderVisBlocked: customSlider!
    @IBOutlet weak var sliderLocInsideBox: customSlider!
    @IBOutlet weak var sliderVisInsideBox: customSlider!
    @IBOutlet weak var sliderLocOutsideBox: customSlider!
    @IBOutlet weak var sliderVisOutsideBox: customSlider!
    
    @IBOutlet weak var viewMain:UIView!
    @IBOutlet weak var lblNoRecordFound:UILabel!
    
    @IBOutlet weak var viewProgess:UIView!
    @IBOutlet weak var lblLocPossessionTime:UILabel!
    @IBOutlet weak var lblVstPossessionTime:UILabel!
    
    @IBOutlet weak var lblLocTotalShots:UILabel!
    @IBOutlet weak var lblLocOnShots:UILabel!
    @IBOutlet weak var lblLocOffShots:UILabel!
    @IBOutlet weak var lblLocBlockedShots:UILabel!
    @IBOutlet weak var lblLocInsideShots:UILabel!
    @IBOutlet weak var lblLocOutsideShots:UILabel!
    
    @IBOutlet weak var lblVstTotalShots:UILabel!
    @IBOutlet weak var lblVstOnShots:UILabel!
    @IBOutlet weak var lblVstOffShots:UILabel!
    @IBOutlet weak var lblVstBlockedShots:UILabel!
    @IBOutlet weak var lblVstInsideShots:UILabel!
    @IBOutlet weak var lblVstOutsideShots:UILabel!
    
    ////Skelton
    @IBOutlet weak var viewForSkelton: UIView!
    @IBOutlet weak var lbl1:UILabel!
    @IBOutlet weak var lbl2:UILabel!
    @IBOutlet weak var lbl3:UILabel!
    @IBOutlet weak var lbl4:UILabel!
    @IBOutlet weak var lbl5:UILabel!
    @IBOutlet weak var lbl6:UILabel!
    @IBOutlet weak var lbl7:UILabel!
    @IBOutlet weak var lbl8:UILabel!
    @IBOutlet weak var lbl9:UILabel!
    @IBOutlet weak var lbl10:UILabel!
    @IBOutlet weak var lbl11:UILabel!
    @IBOutlet weak var lbl12:UILabel!
    @IBOutlet weak var lbl13:UILabel!
    @IBOutlet weak var lbl14:UILabel!
    @IBOutlet weak var lbl15:UILabel!
    @IBOutlet weak var lbl16:UILabel!
    @IBOutlet weak var lbl17:UILabel!
    @IBOutlet weak var lbl18:UILabel!
    @IBOutlet weak var img1:UIImageView!
    ////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewMain.isHidden = false
        self.viewForSkelton.isHidden = false
        self.show_skelton()
       
        self.bannerAdSetup()
    }
    
    
    func callApiSelfStatisticsVC()
    {
         self.call_Webservice_Get_Team_Statistics(str_matchId: objAppShareData.str_match_Id)
    }

    
    func show_skelton(){
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        [lbl1,lbl2,lbl3,lbl4,lbl5,lbl6,lbl7,lbl8,lbl9,lbl10,lbl11,lbl12,lbl13,lbl14,lbl15,lbl16,lbl17,lbl18].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
            [img1].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
            }
        }
    }
    
    func hide_skelton(){
        [lbl1,lbl2,lbl3,lbl4,lbl5,lbl6,lbl7,lbl8,lbl9,lbl10,lbl11,lbl12,lbl13,lbl14,lbl15,lbl16,lbl17,lbl18].forEach { $0?.hideSkeleton()
        }
        [img1].forEach { $0?.hideSkeleton()
        }
    }
}


//MARK :- call_Webservice
extension StatisticsVC{
    
    func call_Webservice_Get_Team_Statistics(str_matchId:String) {
        //SVProgressHUD.show()
        let paramDict = ["match_id":str_matchId] as [String:AnyObject]
        objWebServiceManager.requestGet(strURL: webUrl.get_team_statistics, params: paramDict, success: { (response) in
            self.hide_skelton()
            self.viewForSkelton.isHidden = true
            print(response)
            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    if let dict = data["data"] as? [String: Any]{
                        let objStats = ModelStatistics.init(dict: dict)
                        
                        if objStats.arrCount > 0{
                            self.viewMain.isHidden = false
                        }else{
                            self.viewMain.isHidden = true
                            self.lblNoRecordFound.isHidden = false
                        }
                        
                        if objStats.LocPossessionTime != nil || objStats.LocPossessionTime != nil{
                            self.viewProgess.isHidden = false
                            //                            self.progressBar.progress = objStats.LocPossessionTime!
                            //                            self.progressBar.trackTintColor = UIColor.colorConstant.appBlueColor
                            //                            self.progressBar.progressTintColor = UIColor.colorConstant.appGreenColor
                        }else{
                            self.viewProgess.isHidden = true
                        }
                        
                        
                        let progressPercent:Int? = Int(objStats.strVstPossessionTime)
                        
                        self.mbProgressBar.value = CGFloat(progressPercent ?? 0)
                        
                        
                        self.lblLocPossessionTime.text = objStats.strLocPossessionTime + "%"
                        
                        if objStats.strLocTotalShots == ""{
                            self.lblLocTotalShots.text = "0"
                        }else{
                            self.lblLocTotalShots.text = objStats.strLocTotalShots
                        }
                        
                        if objStats.strLocOnTarget == ""{
                            self.lblLocOnShots.text = "0"
                        }else{
                            self.lblLocOnShots.text = objStats.strLocOnTarget
                        }
                        
                        if objStats.strLocOffTarget == ""{
                            self.lblLocOffShots.text = "0"
                        }else{
                            self.lblLocOffShots.text = objStats.strLocOffTarget
                        }
                        
                        if objStats.strLocBlocked == ""{
                            self.lblLocBlockedShots.text = "0"
                        }else{
                            self.lblLocBlockedShots.text = objStats.strLocBlocked
                        }
                        
                        if objStats.strLocInsidebox == ""{
                            self.lblLocInsideShots.text = "0"
                        }else{
                            self.lblLocInsideShots.text = objStats.strLocInsidebox
                        }
                        
                        if objStats.strLocOutsidebox == ""{
                            self.lblLocOutsideShots.text = "0"
                        }else{
                            self.lblLocOutsideShots.text = objStats.strLocOutsidebox
                        }
                        
                        
                        self.lblVstPossessionTime.text = objStats.strVstPossessionTime + "%"
                        
                        if objStats.strVstTotalShots == ""{
                            self.lblVstTotalShots.text = "0"
                        }else{
                            self.lblVstTotalShots.text = objStats.strVstTotalShots
                        }
                        
                        if objStats.strVstOnTarget == ""{
                            self.lblVstOnShots.text = "0"
                        }else{
                            self.lblVstOnShots.text = objStats.strVstOnTarget
                        }
                        
                        if objStats.strVstOffTarget == ""{
                            self.lblVstOffShots.text = "0"
                        }else{
                            self.lblVstOffShots.text = objStats.strVstOffTarget
                        }
                        
                        if objStats.strVstBlocked == ""{
                            self.lblVstBlockedShots.text = "0"
                        }else{
                            self.lblVstBlockedShots.text = objStats.strVstBlocked
                        }
                        
                        if objStats.strVstInsidebox == ""{
                            self.lblVstInsideShots.text = "0"
                        }else{
                            self.lblVstInsideShots.text = objStats.strVstInsidebox
                        }
                        
                        if objStats.strVstOutsidebox == ""{
                            self.lblVstOutsideShots.text = "0"
                        }else{
                            self.lblVstOutsideShots.text = objStats.strVstOutsidebox
                        }
                        
                        ////// ViewTotal
                        if objStats.totalOfTotalShots>0{
                            
                            self.viewTotal.isHidden = false
                            self.sliderLocTotal.minimumTrackTintColor = UIColor.white
                            self.sliderLocTotal.maximumTrackTintColor = UIColor.colorConstant.sliderLocalColorGrren
                            self.sliderVisTotal.minimumTrackTintColor = UIColor.colorConstant.sliderVisitorColorSkyBlue
                            self.sliderVisTotal.maximumTrackTintColor = UIColor.white
                            self.sliderLocTotal.minimumValue = 0.0
                            self.sliderVisTotal.minimumValue = 0.0
                            self.sliderLocTotal.maximumValue = Float(objStats.totalOfTotalShots)
                            self.sliderVisTotal.maximumValue = Float(objStats.totalOfTotalShots)
                            self.sliderLocTotal.value = Float(objStats.unfilledLocalOfTotalShots)
                            self.sliderVisTotal.value = Float(objStats.filledVisitorOfTotalShots)
                            self.sliderLocTotal.thumbTintColor = UIColor.clear
                            self.sliderVisTotal.thumbTintColor = UIColor.clear
                            if objStats.unfilledLocalOfTotalShots == 0{
                                self.lblFullLocalTotal.isHidden = false
                                self.lblFullVisTotal.isHidden = false
                                
                            }else{
                                self.lblFullLocalTotal.isHidden = true
                                self.lblFullVisTotal.isHidden = true
                            }
                        }else{
                            self.viewTotal.isHidden = true
                        }
                        
                        ////// ViewOnTarget
                        if objStats.totalOfShotsOnTarget>0{
                            
                            self.viewOnTarget.isHidden = false
                            self.sliderLocOnTarget.minimumTrackTintColor = UIColor.white
                            self.sliderLocOnTarget.maximumTrackTintColor = UIColor.colorConstant.sliderLocalColorGrren
                            self.sliderVisOnTarget.minimumTrackTintColor = UIColor.colorConstant.sliderVisitorColorSkyBlue
                            self.sliderVisOnTarget.maximumTrackTintColor = UIColor.white
                            self.sliderLocOnTarget.minimumValue = 0.0
                            self.sliderVisOnTarget.minimumValue = 0.0
                            self.sliderLocOnTarget.maximumValue = Float(objStats.totalOfShotsOnTarget)
                            self.sliderVisOnTarget.maximumValue = Float(objStats.totalOfShotsOnTarget)
                            self.sliderLocOnTarget.value = Float(objStats.unfilledLocalOfShotsOnTarget)
                            self.sliderVisOnTarget.value = Float(objStats.filledVisitorOfShotsOnTarget)
                            self.sliderLocOnTarget.thumbTintColor = UIColor.clear
                            self.sliderVisOnTarget.thumbTintColor = UIColor.clear
                            if objStats.unfilledLocalOfShotsOnTarget == 0{
                                self.lblFullLocalOnTarget.isHidden = false
                                self.lblFullVisOnTarget.isHidden = false
                                
                            }else{
                                self.lblFullLocalOnTarget.isHidden = true
                                self.lblFullVisOnTarget.isHidden = true
                            }
                        }else{
                            self.viewOnTarget.isHidden = true
                        }
                        
                        ////// ViewOffTarget
                        if objStats.totalOfShotsOffTarget>0{
                            
                            self.viewOffTarget.isHidden = false
                            self.sliderLocOffTarget.minimumTrackTintColor = UIColor.white
                            self.sliderLocOffTarget.maximumTrackTintColor = UIColor.colorConstant.sliderLocalColorGrren
                            self.sliderVisOffTarget.minimumTrackTintColor = UIColor.colorConstant.sliderVisitorColorSkyBlue
                            self.sliderVisOffTarget.maximumTrackTintColor = UIColor.white
                            self.sliderLocOffTarget.minimumValue = 0.0
                            self.sliderVisOffTarget.minimumValue = 0.0
                            self.sliderLocOffTarget.maximumValue = Float(objStats.totalOfShotsOffTarget)
                            self.sliderVisOffTarget.maximumValue = Float(objStats.totalOfShotsOffTarget)
                            self.sliderLocOffTarget.value = Float(objStats.unfilledLocalOfShotsOffTarget)
                            self.sliderVisOffTarget.value = Float(objStats.filledVisitorOfShotsOffTarget)
                            self.sliderLocOffTarget.thumbTintColor = UIColor.clear
                            self.sliderVisOffTarget.thumbTintColor = UIColor.clear
                            if objStats.unfilledLocalOfShotsOffTarget == 0{
                                self.lblFullLocalOffTarget.isHidden = false
                                self.lblFullVisOffTarget.isHidden = false
                                
                            }else{
                                self.lblFullLocalOffTarget.isHidden = true
                                self.lblFullVisOffTarget.isHidden = true
                            }
                        }else{
                            self.viewOffTarget.isHidden = true
                        }
                        
                        ////// ViewBlocked
                        if objStats.totalOfBlockedShots>0{
                            
                            self.viewBlocked.isHidden = false
                            self.sliderLocBlocked.minimumTrackTintColor = UIColor.white
                            self.sliderLocBlocked.maximumTrackTintColor = UIColor.colorConstant.sliderLocalColorGrren
                            self.sliderVisBlocked.minimumTrackTintColor = UIColor.colorConstant.sliderVisitorColorSkyBlue
                            self.sliderVisBlocked.maximumTrackTintColor = UIColor.white
                            self.sliderLocBlocked.minimumValue = 0.0
                            self.sliderVisBlocked.minimumValue = 0.0
                            self.sliderLocBlocked.maximumValue = Float(objStats.totalOfBlockedShots)
                            self.sliderVisBlocked.maximumValue = Float(objStats.totalOfBlockedShots)
                            self.sliderLocBlocked.value = Float(objStats.unfilledLocalOfBlockedShots)
                            self.sliderVisBlocked.value = Float(objStats.filledVisitorOfBlockedShots)
                            self.sliderLocBlocked.thumbTintColor = UIColor.clear
                            self.sliderVisBlocked.thumbTintColor = UIColor.clear
                            if objStats.unfilledLocalOfBlockedShots == 0{
                                self.lblFullLocalBlocked.isHidden = false
                                self.lblFullVisBlocked.isHidden = false
                                
                            }else{
                                self.lblFullLocalBlocked.isHidden = true
                                self.lblFullVisBlocked.isHidden = true
                            }
                        }else{
                            self.viewBlocked.isHidden = true
                        }
                        
                        ////// ViewInsideBox
                        if objStats.totalOfShotsInsideBox>0{
                            
                            self.viewInsideBox.isHidden = false
                            self.sliderLocInsideBox.minimumTrackTintColor = UIColor.white
                            self.sliderLocInsideBox.maximumTrackTintColor = UIColor.colorConstant.sliderLocalColorGrren
                            self.sliderVisInsideBox.minimumTrackTintColor = UIColor.colorConstant.sliderVisitorColorSkyBlue
                            self.sliderVisInsideBox.maximumTrackTintColor = UIColor.white
                            self.sliderLocInsideBox.minimumValue = 0.0
                            self.sliderVisInsideBox.minimumValue = 0.0
                            self.sliderLocInsideBox.maximumValue = Float(objStats.totalOfShotsInsideBox)
                            self.sliderVisInsideBox.maximumValue = Float(objStats.totalOfShotsInsideBox)
                            self.sliderLocInsideBox.value = Float(objStats.unfilledLocalOfShotsInsideBox)
                            self.sliderVisInsideBox.value = Float(objStats.filledVisitorOfShotsInsideBox)
                            self.sliderLocInsideBox.thumbTintColor = UIColor.clear
                            self.sliderVisInsideBox.thumbTintColor = UIColor.clear
                            
                            if objStats.unfilledLocalOfShotsInsideBox == 0{
                                self.lblFullLocalInside.isHidden = false
                                self.lblFullVislInside.isHidden = false
                                
                            }else{
                                self.lblFullLocalInside.isHidden = true
                                self.lblFullVislInside.isHidden = true
                            }
                        }else{
                            self.viewInsideBox.isHidden = true
                        }
                        
                        ////// ViewOutsideBox
                        if objStats.totalOfShotsOutsideBox>0{
                            
                            self.viewOutsideBox.isHidden = false
                            self.sliderLocOutsideBox.minimumTrackTintColor = UIColor.white
                            self.sliderLocOutsideBox.maximumTrackTintColor = UIColor.colorConstant.sliderLocalColorGrren
                            self.sliderVisOutsideBox.minimumTrackTintColor = UIColor.colorConstant.sliderVisitorColorSkyBlue
                            self.sliderVisOutsideBox.maximumTrackTintColor = UIColor.white
                            self.sliderLocOutsideBox.minimumValue = 0.0
                            self.sliderVisOutsideBox.minimumValue = 0.0
                            self.sliderLocOutsideBox.maximumValue = Float(objStats.totalOfShotsOutsideBox)
                            self.sliderVisOutsideBox.maximumValue = Float(objStats.totalOfShotsOutsideBox)
                            self.sliderLocOutsideBox.value = Float(objStats.unfilledLocalOfShotsOutsideBox)
                            self.sliderVisOutsideBox.value = Float(objStats.filledVisitorOfShotsOutsideBox)
                            self.sliderLocOutsideBox.thumbTintColor = UIColor.clear
                            self.sliderVisOutsideBox.thumbTintColor = UIColor.clear
                            self.sliderVisOutsideBox.thumbRect(forBounds: CGRect.init(x: 0, y: 0, width: 0, height: 0), trackRect: CGRect.init(x: 0, y: 0, width: 0, height: 0), value: 0.0)
                            
                            if objStats.unfilledLocalOfShotsOutsideBox == 0{
                                self.lblFullLocalOutside.isHidden = false
                                self.lblFullVislOutside.isHidden = false
                                
                            }else{
                                self.lblFullLocalOutside.isHidden = true
                                self.lblFullVislOutside.isHidden = true
                            }
                            
                        }else{
                            self.viewOutsideBox.isHidden = true
                        }
                    }
                }
            }
        }) { (error) in
            print(error)
            self.viewForSkelton.isHidden = true
            self.hide_skelton()
            SVProgressHUD.dismiss()
        }
    }
}

extension StatisticsVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}


extension StatisticsVC: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return scrollView
    }
}
