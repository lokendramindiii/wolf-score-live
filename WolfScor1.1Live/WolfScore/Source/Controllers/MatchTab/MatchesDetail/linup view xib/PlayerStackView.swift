//
//  PlayerStackView.swift
//  WolfScore
//
//  Created by Mindiii on 2/13/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class PlayerStackView: UIView {

    @IBOutlet   weak var stakView: UIStackView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "PlayerStackView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
