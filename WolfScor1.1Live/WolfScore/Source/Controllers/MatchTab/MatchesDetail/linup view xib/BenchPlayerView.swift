//
//  BenchView.swift
//  WolfScore
//
//  Created by Mindiii on 2/21/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class BenchPlayerView: UIView {

    @IBOutlet   weak var lbl_playerName: UILabel!
    @IBOutlet   weak var lbl_Position: UILabel!
    @IBOutlet   weak var lbl_In: UILabel!
    @IBOutlet   weak var lbl_Out: UILabel!

    @IBOutlet   weak var btn_player: UIButton!
    @IBOutlet   weak var imagProfile: UIImageView!
    @IBOutlet   weak var imgCard: UIImageView!
    @IBOutlet   weak var imgIn: UIImageView!
    @IBOutlet   weak var imgOut: UIImageView!

   class func instanceFromNib()-> UIView
   {
    return UINib(nibName: "BenchPlayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    
    }
}
