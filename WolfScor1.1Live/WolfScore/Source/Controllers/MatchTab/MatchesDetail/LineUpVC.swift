//
//  LineUpVC.swift
//  WolfScore
//
//  Created by Mindiii on 17/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import SVProgressHUD
import GoogleMobileAds
import SkeletonView
class LineUpVC: UIViewController

{
      var modelLinup = [ModelLineup]()
      var arrayLineup = [ArrayLinup]()
      var arrayBench = [ArrayBenchModel]()
      var arrayBenchlocal = [ArrayBenchModel]()
      var arrayBenchVisitor = [ArrayBenchModel]()

    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!

    @IBOutlet weak var viewBannerBottom: UIView!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var View_LineupGround:UIView!
    @IBOutlet weak var benchView:UIView!
    @IBOutlet weak var lbl_Nodata:UILabel!
    
    // skelton lable outlet
    @IBOutlet weak var lblfirst:UILabel!
    @IBOutlet weak var lblsecod:UILabel!
    @IBOutlet weak var lblthird:UILabel!
    @IBOutlet weak var lblfour:UILabel!
    @IBOutlet weak var lblfive:UILabel!
    @IBOutlet weak var lblsix:UILabel!
    @IBOutlet weak var lblseven:UILabel!
    @IBOutlet weak var lbleight:UILabel!
    @IBOutlet weak var lblNine:UILabel!
    @IBOutlet weak var lblTen:UILabel!
    @IBOutlet weak var skeltonView:UIView!

    // Linup section
    // local
    @IBOutlet weak var lblLocalformation:UILabel!
    @IBOutlet weak var lblLocalTeamName:UILabel!
    @IBOutlet weak var localTeamView:UIView!
    @IBOutlet weak var localViewHeight:NSLayoutConstraint!

    // visitor
    @IBOutlet weak var VisitorTeamView:UIView!
    @IBOutlet weak var lblVisitorTeamName:UILabel!
    @IBOutlet weak var lblVisitorformation:UILabel!
    @IBOutlet weak var visitorViewHeight:NSLayoutConstraint!

    //Bench Section
    @IBOutlet weak var lblbenchLocalT_Name:UILabel!
    @IBOutlet weak var lblbenchVisitorT_Name:UILabel!
    @IBOutlet weak var benchViewHeight:NSLayoutConstraint!


   
    override func viewDidLoad()
       {
        super.viewDidLoad()
        self.View_LineupGround.isHidden = true
        benchView.isHidden = true
        
        self.show_skelton()
        self.skeltonView.isHidden = false
         self.bannerAdSetup()
      }


    func callApiSelfLineUpVC()
    {
        self.call_Webservice_Get_Linup(str_matchId:objAppShareData.str_match_Id)
    }

    
    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblfirst , lblsecod ,lblthird,lblfour,lblfive,lblsix,lblseven,lbleight,lblNine,lblTen].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
    }
    func hide_skelton()
    {
        [lblfirst , lblsecod ,lblthird,lblfour,lblfive,lblsix,lblseven,lbleight,lblNine,lblTen ].forEach { $0?.hideSkeleton()
        }
        
    }

// MARK: ------------ Webservice call ----------------
func call_Webservice_Get_Linup(str_matchId:String) {
   // SVProgressHUD.show()
    //10421375
    let paramDict = ["match_id":str_matchId] as [String:AnyObject]
    objWebServiceManager.requestGet(strURL: webUrl.get_team_lineup, params: paramDict, success: { (response) in
        print("--Linup----------\(response)")
        SVProgressHUD.dismiss()
        self.hide_skelton()
        self.skeltonView.isHidden = true
        let message = response["message"] as? String ?? ""
        let status = response["status"] as? String ?? ""
        if status == "success"{
            if let data = response["data"] as? [String:Any] {
                
                if let dic = data["data"] as? [String: Any]{
                    
                    let object =  ModelLineup.init(localdictionary: dic)
                    
                    // lineup
                    if object.arrLinup.count != 0
                    {
                        self.View_LineupGround.isHidden = false
                        
                        self.lblLocalformation.text = object.localTeamFormation
                        self.lblVisitorformation.text = object.visitorTeamFormation
                        
                        for obj in object.arrLinup
                        {
                            for objSubstitution in object.arrSubstitution
                            {
                                if obj.teamId == objSubstitution.teamId
                                {
                                    obj.playerIn_Id = objSubstitution.playerIn_Id
                                    obj.playerOut_Id = objSubstitution.playerOut_Id
                                    obj.minute = objSubstitution.minute
                                }
                            }
                        }
                        
                        self.arrayLineup = object.arrLinup
                        self.setUi_linupLocalSection(teamName: object.localTeamName)
                        self.setUi_linupVisitorSection(teamName: object.visitorTeamName)
                        
                    }
                    else
                    {
                        self.View_LineupGround.isHidden = true
                        self.localViewHeight.constant = 0
                        self.visitorViewHeight.constant = 0
                    }
                    
                    // Bench
                    self.arrayBench = object.arrBench
                    if object.arrBench.count != 0
                    {
                        self.benchView.isHidden = false
                        var arrlocalBench = [ArrayBenchModel]()
                        var arrVisitorBench = [ArrayBenchModel]()
                        
                        for obj in object.arrBench
                        {
                            for objSubstitution in object.arrSubstitution
                            {
                                if obj.teamId == objSubstitution.teamId
                                {
                                    obj.playerIn_Id = objSubstitution.playerIn_Id
                                    obj.playerOut_Id = objSubstitution.playerOut_Id
                                    obj.minute = objSubstitution.minute
                                }
                            }
                            
                            if obj.teamId == object.localTeamId
                            {
                                arrlocalBench.append(obj)
                            }
                            else
                            {
                                arrVisitorBench.append(obj)
                            }
                        }
                        
                        self.lblbenchLocalT_Name.text = object.localTeamName
                        self.lblbenchVisitorT_Name.text = object.visitorTeamName
                        self.setUi_benchview(localTeamArray: arrlocalBench, visitorTeamArray: arrVisitorBench)
                    }
                    else
                    {
                        self.benchView.isHidden = true
                        self.benchViewHeight.constant = 0
                    }
                    
                    if object.arrLinup.count == 0  && object.arrBench.count == 0
                    {
                 // self.lbl_Nodata.bringSubview(toFront: self.View_LineupGround)
                   self.lbl_Nodata.isHidden = false
                    }
                    
                }
            }
        }
        else{
            GlobalUtility.showToastMessage(msg: message)
        }
        
    }) { (error) in
        print(error)
         self.hide_skelton()
         self.skeltonView.isHidden = true
        SVProgressHUD.dismiss()
    }
 }
    
    
// MARK: ----------------------lineup Section ---------------------------------
    
    func setUi_linupLocalSection(teamName:String)
    {
        
        self.lblLocalTeamName.text = teamName
        
        let formationrow = "1-\(self.lblLocalformation.text ?? "")"
        
        let localFormationRow  = formationrow.components(separatedBy: "-")
        
        print(localFormationRow)
        
        var yAxis:Int = 40
        var Indexformation:Int = 0
        var playerformation = 0
        
        for i in  0..<localFormationRow.count
        {
            
            let Playerstack = PlayerStackView.instanceFromNib() as! PlayerStackView
            Playerstack.tag = i
            
            let myView = UIView(frame: CGRect(x: 0, y: yAxis , width:Int(localTeamView.frame.size.width) , height: 80))
            
            Playerstack.frame = CGRect(x: 0, y: 0, width: Int(localTeamView.frame.size.width), height: 80)
            localTeamView.addSubview(myView)
            
            let playerInRow:Int = Int(localFormationRow[i]) ?? 0
            
            for j in 0..<playerInRow
            {
                let Playerxib = player.instanceFromNib() as! player
                
                
                Playerxib.btn_player.tag = 50 + playerformation
                
                Playerxib.btn_player.addTarget(self, action: #selector(btnLocalGroudPlayer(_:)), for: .touchUpInside)

                Playerxib.lbl_playerName.text = self.arrayLineup[playerformation].number + " " + self.arrayLineup[playerformation].playerName
                
                if let url = URL(string: self.arrayLineup[playerformation].imagePath ?? ""){
                    Playerxib.imagProfile.af_setImage(withURL: url , placeholderImage: UIImage(named: "circle_goal_icon"))
                }else{
                    //suresh
                    Playerxib.imagProfile.image = UIImage.init(named: "placegolder")
                    //suresh
                }
                
                if self.arrayLineup[playerformation].yellowCards == "1"
                {
                    Playerxib.imgCard.image = UIImage(named: "yellow_Card")

                    Playerxib.imgCard.isHidden = false
                    
                }
                else if self.arrayLineup[playerformation].redCards == "1"
                {
                    Playerxib.imgCard.image = UIImage(named: "red_Card")
                    Playerxib.imgCard.isHidden = false
                }
                
                if self.arrayLineup[playerformation].playerId == self.arrayLineup[playerformation].playerIn_Id
                {
                    Playerxib.lbl_In.text = self.arrayLineup[playerformation].minute + "'"
                    Playerxib.lbl_In.isHidden = false
                    Playerxib.imgIn.isHidden = false
                    
                }
                else if self.arrayLineup[playerformation].playerId == self.arrayLineup[playerformation].playerOut_Id
                {
                    Playerxib.lbl_Out.text = self.arrayLineup[playerformation].minute + "'"
                    Playerxib.lbl_Out.isHidden = false
                    Playerxib.imgOut.isHidden = false
                    
                }
                
                playerformation = playerformation + 1
                Playerstack.stakView.addArrangedSubview(Playerxib)
            }
            myView.addSubview(Playerstack)
            Indexformation = Indexformation + 1
            
            yAxis  = yAxis + 5 + Int(Playerstack.frame.size.height + Playerstack.frame.origin.y)
        }
        localViewHeight.constant = CGFloat(yAxis)
    }

    
    
    @objc func btnLocalGroudPlayer(_ sender: UIButton) {
        
        //Perform actions here
        let section = sender.tag - 50
        
        if self.arrayLineup.count == 0
        {
            return
        }
        objAppShareData.str_player_Id =  self.arrayLineup[section].playerId

        let viewController = UIStoryboard(name: "TableDetails",bundle: nil).instantiateViewController(withIdentifier: "PlayerDetailVC") as! PlayerDetailVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    
   // visitorn linup setup
    func setUi_linupVisitorSection(teamName:String)
    {
        self.lblVisitorTeamName.text = teamName
        
        let formationrow = "1-\(self.lblVisitorformation.text ?? "")"
        
        var visitorFormationRow  = formationrow.components(separatedBy: "-")
        visitorFormationRow = visitorFormationRow.reversed()
        print(visitorFormationRow)
        
        var yAxis:Int = 20
        var Indexformation:Int = 0
        var playerformation = self.arrayLineup.count - 1
        
        for i in  0..<visitorFormationRow.count
        {
            
            let Playerstack = PlayerStackView.instanceFromNib() as! PlayerStackView
            Playerstack.tag = i
            
            let myView = UIView(frame: CGRect(x: 0, y: yAxis , width:Int(VisitorTeamView.frame.size.width) , height: 80))
            VisitorTeamView.addSubview(myView)
            Playerstack.frame = CGRect(x: 0, y: 0, width: Int(VisitorTeamView.frame.size.width), height: 80)

            let playerInRow:Int = Int(visitorFormationRow[i]) ?? 0
            for j in 0..<playerInRow
            {
                let Playerxib = player.instanceFromNib() as! player
                
                
                Playerxib.btn_player.tag = 100 + playerformation
                
                Playerxib.btn_player.addTarget(self, action: #selector(btnVisitorGroudPlayer(_:)), for: .touchUpInside)

                
                Playerxib.lbl_playerName.text = self.arrayLineup[playerformation].number + " " + self.arrayLineup[playerformation].playerName
                
                if let url = URL(string: self.arrayLineup[playerformation].imagePath ?? ""){
                Playerxib.imagProfile.af_setImage(withURL: url , placeholderImage: UIImage(named: "circle_goal_icon"))
                }else{
                    //suresh
                    Playerxib.imagProfile.image = UIImage.init(named: "placegolder")
                    //suresh
                }
                
                if self.arrayLineup[playerformation].yellowCards == "1"
                {
                    Playerxib.imgCard.image = UIImage(named: "yellow_Card")

                    Playerxib.imgCard.isHidden = false
                    
                }
                else if self.arrayLineup[playerformation].redCards == "1"
                {
                    Playerxib.imgCard.image = UIImage(named: "red_Card")
                    
                    Playerxib.imgCard.isHidden = false
                }
                
                if self.arrayLineup[playerformation].playerId == self.arrayLineup[playerformation].playerIn_Id
                {
                    Playerxib.lbl_In.text = self.arrayLineup[playerformation].minute + "'"
                    Playerxib.lbl_In.isHidden = false
                    Playerxib.imgIn.isHidden = false
                    
                }
                else if self.arrayLineup[playerformation].playerId == self.arrayLineup[playerformation].playerOut_Id
                {
                    Playerxib.lbl_Out.text = self.arrayLineup[playerformation].minute + "'"
                    Playerxib.lbl_Out.isHidden = false
                    Playerxib.imgOut.isHidden = false
                    
                }
                
                
            playerformation = playerformation - 1
            Playerstack.stakView.addArrangedSubview(Playerxib)
            }
            myView.addSubview(Playerstack)
            Indexformation = Indexformation + 1
            
            yAxis  = yAxis + 5 + Int(Playerstack.frame.size.height + Playerstack.frame.origin.y)
        }
        visitorViewHeight.constant = CGFloat(yAxis + 30)
    }
    @objc func btnVisitorGroudPlayer(_ sender: UIButton) {
        
        //Perform actions here
        let section = sender.tag - 100
        
        if self.arrayLineup.count == 0
        {
            return
        }
        objAppShareData.str_player_Id =  self.arrayLineup[section].playerId

        let viewController = UIStoryboard(name: "TableDetails",bundle: nil).instantiateViewController(withIdentifier: "PlayerDetailVC") as! PlayerDetailVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    
// MARK:  ----------------------Bench Section ---------------------------------
    func setUi_benchview(localTeamArray:[ArrayBenchModel] , visitorTeamArray:[ArrayBenchModel])
    {
        
         arrayBenchlocal = localTeamArray
         arrayBenchVisitor = visitorTeamArray

        var  yAxis:Int = Int(self.lblbenchVisitorT_Name.frame.size.height + 2 + self.lblbenchVisitorT_Name.frame.origin.y)
        for i in 0..<localTeamArray.count
        {
            let myView = UIView(frame: CGRect(x: 0, y: yAxis , width:Int((benchView.frame.size.width/2)-0.5) , height: 100))

            let BenchPlayerViewXib = BenchPlayerView.instanceFromNib() as! BenchPlayerView
            
            BenchPlayerViewXib.frame = CGRect(x: 0, y: 0, width: (benchView.frame.size.width/2)-0.5, height: 100)
            
            BenchPlayerViewXib.tag = i
            
            BenchPlayerViewXib.lbl_playerName.text = localTeamArray[i].number + " " + localTeamArray[i].playerName
            
            if localTeamArray[i].playerId == localTeamArray[i].playerIn_Id
            {
                BenchPlayerViewXib.lbl_In.text = localTeamArray[i].minute + "'"
                BenchPlayerViewXib.lbl_In.isHidden = false
                BenchPlayerViewXib.imgIn.isHidden = false

            }
            else if localTeamArray[i].playerId == localTeamArray[i].playerOut_Id
            {
                BenchPlayerViewXib.lbl_Out.text = localTeamArray[i].minute + "'"
                BenchPlayerViewXib.lbl_Out.isHidden = false
                BenchPlayerViewXib.imgOut.isHidden = false

            }
            
            switch localTeamArray[i].position
            {
            case "G":
                BenchPlayerViewXib.lbl_Position.text = "Keeper"
                break
            case "D":
                BenchPlayerViewXib.lbl_Position.text = "Defender"
                break
            case "F" :
                BenchPlayerViewXib.lbl_Position.text = "Attacker"
                break
            case "M" :
                BenchPlayerViewXib.lbl_Position.text = "Midfielder"
                break
            default:
                
                break
            }
            
            
            if let url = URL(string: localTeamArray[i].imagePath ?? ""){
            BenchPlayerViewXib.imagProfile.af_setImage(withURL: url , placeholderImage: UIImage(named: "circle_goal_icon"))
            }else{
                BenchPlayerViewXib.imagProfile.image = UIImage.init(named: "placegolder")
            }
            
            if localTeamArray[i].yellowCards == "1"
            {
            BenchPlayerViewXib.imgCard.image = UIImage(named: "yellow_Card")
            
            BenchPlayerViewXib.imgCard.isHidden = false

            }
            else if localTeamArray[i].redCards == "1"
            {
                BenchPlayerViewXib.imgCard.image = UIImage(named: "red_Card")
                BenchPlayerViewXib.imgCard.isHidden = false
            }
            myView.addSubview(BenchPlayerViewXib)
            
            
            BenchPlayerViewXib.btn_player.tag = 100 + i
            
            BenchPlayerViewXib.btn_player.addTarget(self, action: #selector(btnBenchLocalPlayer(_:)), for: .touchUpInside)


            benchView.addSubview(myView)
            yAxis = yAxis + Int(myView.frame.size.height) + 5
        }
        
        // Visitor bench
        var  yAxis_V:Int = Int(self.lblbenchVisitorT_Name.frame.size.height + self.lblbenchVisitorT_Name.frame.origin.y )
        for i in 0..<visitorTeamArray.count
        {
            
            let myView = UIView(frame: CGRect(x: Int(benchView.frame.size.width/2), y: yAxis_V , width:Int((benchView.frame.size.width/2)-0.5) , height: 100))
            
            let BenchPlayerViewXib = BenchPlayerView.instanceFromNib() as! BenchPlayerView
            
            BenchPlayerViewXib.btn_player.tag = 1000 + i
            BenchPlayerViewXib.btn_player.addTarget(self, action: #selector(btnBenchVisitorPlayer(_:)), for: .touchUpInside)

            
            BenchPlayerViewXib.frame = CGRect(x: 0, y: 0, width: (benchView.frame.size.width/2)-0.5, height: 100)
            
            BenchPlayerViewXib.tag = i
            
            BenchPlayerViewXib.lbl_playerName.text = visitorTeamArray[i].number + " " + visitorTeamArray[i].playerName
            
            if visitorTeamArray[i].playerId == visitorTeamArray[i].playerIn_Id
            {
                BenchPlayerViewXib.lbl_In.text = visitorTeamArray[i].minute  + "'"
                BenchPlayerViewXib.lbl_In.isHidden = false
                BenchPlayerViewXib.imgIn.isHidden = false
            }
            else if visitorTeamArray[i].playerId == visitorTeamArray[i].playerOut_Id
            {
                BenchPlayerViewXib.lbl_Out.text = visitorTeamArray[i].minute + "'"
                BenchPlayerViewXib.lbl_Out.isHidden = false
                BenchPlayerViewXib.imgOut.isHidden = false
            }
            
            switch visitorTeamArray[i].position
            {
            case "G":
                BenchPlayerViewXib.lbl_Position.text = "Keeper"
                break
            case "D":
                BenchPlayerViewXib.lbl_Position.text = "Defender"
                break
            case "F" :
                BenchPlayerViewXib.lbl_Position.text = "Attacker"
                break
            case "M" :
                BenchPlayerViewXib.lbl_Position.text = "Midfielder"
                break
            default:
                break
            }
            
            if let url = URL(string: visitorTeamArray[i].imagePath ?? ""){
                BenchPlayerViewXib.imagProfile.af_setImage(withURL: url , placeholderImage: UIImage(named: "circle_goal_icon"))
            }else{
                //suresh
                BenchPlayerViewXib.imagProfile.image = UIImage.init(named: "placegolder")
                //suresh
            }
            
            if visitorTeamArray[i].yellowCards == "1"
            {
                BenchPlayerViewXib.imgCard.image = UIImage(named: "yellow_Card")

                BenchPlayerViewXib.imgCard.isHidden = false
                
            }
            else if visitorTeamArray[i].redCards == "1"
            {
                BenchPlayerViewXib.imgCard.image = UIImage(named: "red_Card")
                BenchPlayerViewXib.imgCard.isHidden = false
            }
            
            myView.addSubview(BenchPlayerViewXib)
            benchView.addSubview(myView)
            yAxis_V = yAxis_V + Int(myView.frame.size.height) + 5
        }
        
        if yAxis>yAxis_V
        {
            benchViewHeight.constant = CGFloat(yAxis)
        }
        else
        {
            benchViewHeight.constant = CGFloat(yAxis_V)
        }
    }
    
    @objc func btnBenchLocalPlayer(_ sender: UIButton) {
        //Perform actions here
        let section = sender.tag - 100
        
        if arrayBenchlocal.count == 0
        {
            return
        }
        
        objAppShareData.str_player_Id =  arrayBenchlocal[section].playerId
        
        let viewController = UIStoryboard(name: "TableDetails",bundle: nil).instantiateViewController(withIdentifier: "PlayerDetailVC") as! PlayerDetailVC
        self.navigationController?.pushViewController(viewController, animated: true)

    }
   @objc func btnBenchVisitorPlayer(_ sender: UIButton) {
        //Perform actions here
        let section = sender.tag - 1000
    
        if arrayBenchVisitor.count == 0
        {
            return
        }
    
        objAppShareData.str_player_Id =  arrayBenchVisitor[section].playerId
        
        let viewController = UIStoryboard(name: "TableDetails",bundle: nil).instantiateViewController(withIdentifier: "PlayerDetailVC") as! PlayerDetailVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}

extension LineUpVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
     bannerView.translatesAutoresizingMaskIntoConstraints = false
        
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}

extension LineUpVC: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return scrollView
    }
}
