//
//  SearchTeamListVC.swift
//  WolfScore
//
//  Created by Mindiii on 03/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage

class SearchTeamListVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblFavorite: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    // create callback function for get string on back
    var selectedTeambyfilterId:((_ ids:String ) -> ())?
    
    var offSet = 0
    var Limit = 20
    var str_Selected_Team_Id = ""
    
    var arrAllData = [ModelTeam]()
    
    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tblFavorite.tableFooterView = UIView()
            } else {
                self.tblFavorite.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewSearch.layer.cornerRadius = self.viewSearch.frame.size.height/2
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Search", tableName: nil, comment: ""), attributes: [NSAttributedStringKey.foregroundColor:UIColor.white
            ])
        
        self.modifyClearButtonWithImage(image: UIImage(named: "icon_clearTxtfild")!, txtfiled: txtSearch)

        self.tblFavorite.tableFooterView = UIView()
    }
    
    
    func modifyClearButtonWithImage(image : UIImage ,txtfiled : UITextField) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(sender:)), for: .touchUpInside)
        txtfiled.rightView = clearButton
        txtfiled.rightViewMode = .whileEditing
    }
    
    @objc func clear(sender : AnyObject) {
        
        self.view.endEditing(true)
        self.txtSearch.text = ""
        self.loadDataWithPageCount(page: 0, strSearchText: "")
        self.txtSearch.sendActions(for: .editingChanged)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if APP_DELEGATE.isInterstialPresent == true
        {
            APP_DELEGATE.isInterstialPresent = false
            return
        }
        self.txtSearch.text = ""
        self.lblNoData.isHidden = true
        //self.call_Webservice_Demo()
        if self.arrAllData.count == 0{
            //cellCount = 0
            let dictPram = ["search_term":"",
                            "limit":String(self.Limit),
                            "offset":self.offSet] as [String: AnyObject]
            self.call_Webservice_Get_Popular(dict_param: dictPram)
        }
    }
    @IBAction func btnCancelAction(_ sender : UIButton){
        self.view.endEditing(true)
        dismiss(animated: true, completion: nil)
        
    }
}

// MARK: - UITextField Delegates

extension SearchTeamListVC{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //searchActive = false
        return true;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //searchActive = false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //searchActive = false
    }
    
    /*  @objc func textFieldDidChange(_ textField: UITextField) {
     
     let searchText  = textField.text
     arrFilterd = self.arrAllData.filter { $0.name.localizedCaseInsensitiveContains(searchText!)}
     if(arrFilterd.count == 0){
     searchActive = false;
     } else {
     searchActive = true;
     }
     self.tblFavorite.reloadData()
     } */
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        txtSearch.resignFirstResponder()
        //searchActive = false
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        //searchActive = false
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        if (text.length == 1)  && (string == "") {
            self.loadDataWithPageCount(page: 0, strSearchText: "")
        }
        
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        //        // Suresh
        //        self.arrAllData.removeAll()
        //        self.tblFavorite.reloadData()
        //        self.offSet = 0
        //        // Suresh
        searchAutocompleteEntries(withSubstring: substring)
        return true
    }
    
    func searchAutocompleteEntries(withSubstring substring: String) {
        if !(substring == "") {
            self.loadDataWithPageCount(page: 0, strSearchText: substring)
        }
    }
    func loadDataWithPageCount(page: Int, strSearchText : String = "") {
        //        self.txtSearch.text = strSearchText.lowercased()
        //        self.txtSearch.text = self.txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        // Suresh
        self.arrAllData.removeAll()
        self.tblFavorite.reloadData()
        //self.tblFavorite.setContentOffset(.zero, animated: true)
        self.offSet = 0
        // Suresh
        let strName = strSearchText
        let dicParam = ["search_term":strName,
                        "limit":String(self.Limit),
                        "offset":offSet
            ] as [String : AnyObject]
        self.call_Webservice_Get_Popular(dict_param: dicParam)
    }
}
// MARK: - Webservice
extension SearchTeamListVC{
    
   /* func call_Webservice_Demo() {
        let url = Bundle.main.url(forResource: "DemoTeam", withExtension: "plist")!
        let soundsData = try! Data(contentsOf: url)
        
        if let myPlist = try! PropertyListSerialization.propertyList(from: soundsData, options: [], format: nil) as? [String:Any] {
            print(myPlist)
            if let arrData = myPlist["data"] as? [[String: Any]]{
                self.arrAllData.removeAll()
                for dict in arrData{
                    let obj = ModelTeam.init(dict_Demo: dict)
                    self.arrAllData.append(obj)
                }
                self.tblFavorite.reloadData()
            }
        }
    } */
    //
    func call_Webservice_Post_Favorite(request_type:String,request_id:String){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"team"] as [String:Any]
        print(paramDict)
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"{
                self.tblFavorite.reloadData()
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
        
    }
    func call_Webservice_Get_Popular(dict_param:[String:AnyObject]) {
        SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.get_Popular_Teams, params: dict_param, success: { (response) in
             print(response)
            SVProgressHUD.dismiss()
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    if let arrData = data["team_list"] as? [[String: Any]]{
                        var arrAllDataLocal = [ModelTeam]()
                        //suresh
                        if self.offSet == 0{
                            self.arrAllData.removeAll()
                        }
                        //suresh
                        for dict in arrData{
                            let obj = ModelTeam.init(dict: dict)
                            //self.arrAllData.append(obj)
                            arrAllDataLocal.append(obj)
                        }
                        if self.offSet == 0{
                            self.arrAllData = arrAllDataLocal
                        }
                        else{
                            self.arrAllData += arrAllDataLocal
                        }
                        
                        var strTotalRecords = "0"
                        if let strTotal = data["total_records"] as? String {
                            strTotalRecords = strTotal
                        }
                        let myInt1 = Int(strTotalRecords)
                        if self.arrAllData.count < myInt1!{
                            self.shouldLoadMore = true
                            
                            //suresh
                            if self.arrAllData.count > 19{
                                self.offSet += self.Limit
                            }else{
                                self.shouldLoadMore = false
                            }
                            //suresh
                        }
                        else{
                            self.shouldLoadMore = false
                        }
                        if self.arrAllData.count == 0{
                            self.tblFavorite.isHidden = true
                            self.lblNoData.isHidden = false
                        }else{
                            self.tblFavorite.isHidden = false
                            self.lblNoData.isHidden = true
                        }
                        self.tblFavorite.reloadData()
                        //                        let hi = self.tblFavorite.contentSize.height
                        //                        self.heightOfTable.constant = hi
                    }
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}
// MARK: - TableView Delegates & Datasource

extension SearchTeamListVC: UITableViewDelegate, UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAllData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TeamTableViewCell", for: indexPath) as? TeamTableViewCell{
            
            let objModel = self.arrAllData[indexPath.row]
            cell.objTeams = objModel
            cell.lblTeamName.text = objModel.name
            let strUrl = objModel.logo_path
            if let url = URL(string: strUrl){
                cell.imgFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgFlag.image = UIImage(named: "icon_placeholderTeam")

            }
            
            if objModel.is_favorite == "1"{
                cell.imgSelection.image = UIImage(named: "active_star")
            }else{
                cell.imgSelection.image = UIImage(named: "white_star")
            }
            
            
            cell.actionSelectHandler = { [weak self] (objTeam) in
                guard let weakSelf = self else {return}
                guard let team = objTeam else {return}
                if team.is_favorite == "0"{
                    team.is_favorite = "1"
                    self?.call_Webservice_Post_Favorite(request_type: "1", request_id: team.team_id)
                }else{
                    team.is_favorite = "0"
                    self?.call_Webservice_Post_Favorite(request_type: "0", request_id: team.team_id)
                }
                weakSelf.tblFavorite.reloadData()
            }
            
            
            ///////////////////////////////////
                return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (arrAllData.count - 1) && shouldLoadMore == true {
            let dictPram = ["search_term":self.txtSearch.text!,
                            "limit":String(self.Limit),
                            "offset":self.offSet] as [String: AnyObject]
            //suresh
            if self.offSet > 0{
                call_Webservice_Get_Popular(dict_param: dictPram)
            }
            //suresh
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let obj = self.arrAllData[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterTeamListVC") as! FilterTeamListVC
        vc.strTeamId = obj.team_id
        vc.strTeamName = obj.name
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
