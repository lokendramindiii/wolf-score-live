//
//  FilterTeamListVC.swift
//  WolfScore
//
//  Created by Mindiii on 1/31/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVProgressHUD
import Toaster
import AlamofireImage

class FilterTeamListVC: UIViewController {

    var arrModelLeague = [LeaugeModel]()
    var arrFinalModelLeague = [LeaugeModel]()
    var arrAllData1:[String] = []
    var currentPageIndex = 1
    var totalPages = 0
    
    var strTeamId = ""
    var strTeamName = ""

    @IBOutlet weak var tblMatches: UITableView!
    @IBOutlet weak var lblSelectedTeam: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        shouldLoadMore = false
        
        lblSelectedTeam.text = strTeamName
        
        self.apiCallGetFixtures()
    }
    func apiCallGetFixtures()
    {
        
        let dictPram = ["type":"team","page":currentPageIndex,"date":"","team_id":strTeamId] as [String: AnyObject]
        call_Webservice_SearchMatches(dict_param: dictPram)
    }
    
    
    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tblMatches.tableFooterView = UIView()
            } else {
                self.tblMatches.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }
    
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
       dismiss(animated: true, completion: nil)
    }
}

extension FilterTeamListVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrFinalModelLeague.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        //headerView.conten.backgroundColor = UIColor.colorConstant.appSectionHeader
        headerView.backgroundColor = UIColor.colorConstant.appDarkBlack
        
        // create Image in Header
        let myCustomView = UIImageView(frame: CGRect(x: 14, y: 10, width:
            26, height: 26))
        let myImage: UIImage = UIImage(named: "circle_transfers_icon")!
        myCustomView.image = myImage
        headerView.addSubview(myCustomView)
        
        // create Icon in Header
        let myCustomView2 = UIImageView(frame: CGRect(x: tableView.bounds.size.width - 30, y: 15, width:
            14, height: 14))
        let myImage2: UIImage = UIImage(named: "icon_back_Table")!
        myCustomView2.image = myImage2
        headerView.addSubview(myCustomView2)
        
        // Create Lable in Header
        let headerLabel = UILabel(frame: CGRect(x: 50, y: 16, width:
            tableView.bounds.size.width - 80, height: tableView.bounds.size.height-2))
        
        headerLabel.font = UIFont(name: "Roboto-Bold", size: 14)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.arrFinalModelLeague[section].name.uppercased()

        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        // Create Line in Header
        let headerLine = UILabel(frame: CGRect(x: 0, y: 40, width:
            tableView.bounds.size.width, height: 0.6))
        
        headerLine.backgroundColor = UIColor.darkGray
        headerLine.alpha = 0.7
        headerLabel.font = UIFont(name: "Roboto-Medium", size: 14)
        headerLabel.textColor = UIColor.white
        //headerLine.sizeToFit()
        headerView.addSubview(headerLine)
        
        return headerView
        
    }
    //     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    //     {
    //     //return self.arrBusinessTypeList[section].businessName.capitalized
    //     //return "Nations League"
    //        if section == 0 {
    //            return "Nations League"
    //        }else if (section == 1){
    //            return "International Friendly"
    //        }else{
    //            return "Friendly League"
    //        }
    //     }
    

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let objLeague = self.arrFinalModelLeague[section]
        return objLeague.arrMatches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesTableViewCell", for: indexPath) as? MatchesTableViewCell{
            cell.viewStatus.layer.cornerRadius = 4.0
            cell.viewStatus.layer.masksToBounds = true
            
            let objLeague = self.arrFinalModelLeague[indexPath.section]
            let objMatch = objLeague.arrMatches[indexPath.row]
            
            cell.lblLocalTeamName.text = objMatch.strNameloacalTeam
            cell.lblVisitorTeamName.text = objMatch.strNameVisitorTeam
            cell.lblDate.text =  dayDate.dateformatedd_MMMM_yyyyy(strDate: objMatch.strDate)
            if objMatch.strStatus == "LIVE"
            {
                cell.lblScore.text = objMatch.strLocalTeamScore + " - " + objMatch.strVisitorTeamScore
                cell.stackView.isHidden = false
                cell.lblStartTime.isHidden = true
            }
            else
            {
                cell.lblStartTime.text = dayDate.Dateformate24_hours(strTime: objMatch.strTime)
                cell.lblStartTime.isHidden = false
                cell.stackView.isHidden = true
            }
            if let url = URL(string: objMatch.strLogopathLocalTeam ){
                cell.imgLocal.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            if let url = URL(string: objMatch.strLogopathVisitorTeam ){
                cell.imgVisitor.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == (arrFinalModelLeague.count - 1) && shouldLoadMore == true {
            self.shouldLoadMore = false
            self.apiCallGetFixtures()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let viewController = UIStoryboard(name: "MatchesTab",bundle: nil).instantiateViewController(withIdentifier: "MatchDetailMainVC") as! MatchDetailMainVC
//        self.navigationController?.pushViewController(viewController, animated: true)
    }
}


extension FilterTeamListVC
{
    
    func call_Webservice_SearchMatches(dict_param:[String:AnyObject]) {
        SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.search_matches, params: dict_param, success: { (response) in
            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    if let metadata = data ["meta"] as? [String:Any]
                    {
                        let data1 = metadata["pagination"] as? [String:Any]
                        let TotalPages = data1?["total_pages"] as? Int  ?? 0
                        self.totalPages = TotalPages
                    }
                    
                    if let arrData = data["data"] as? [[String: Any]]{
                        
                        for dict in arrData{
                            let obj = LeaugeModel.init(fromDictionary: dict, isPastMatches: false)
                            self.arrModelLeague.append(obj)
                        }
                        for obj in self.arrModelLeague
                        {
                            let filteredArray = self.arrModelLeague.filter(){ $0.leaugeId.contains(obj.leaugeId) }
                            for objNEW in filteredArray{
                                if !obj.arrMatches.contains(objNEW.arrMatches[0]){
                                    obj.arrMatches.append(objNEW.arrMatches[0])
                                }
                            }
                            let arrNewCheck = self.arrFinalModelLeague.filter(){ $0.leaugeId.contains(obj.leaugeId) }
                            if arrNewCheck.count == 0{
                                self.arrFinalModelLeague.append(obj)
                            }
                        }
                        
                        if self.currentPageIndex  < self.totalPages
                        {
                            self.currentPageIndex += 1
                            self.shouldLoadMore = true
                        }
                        self.tblMatches.reloadData()
                    }
                   
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}
