//
//  FilterLeagueVC1.swift
//  WolfScore
//
//  Created by mac on 26/03/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster
import AlamofireImage
import GoogleMobileAds
import SkeletonView

struct FilterLeague {
    static var arrSelectedLeague = [String]()
    static var isFilterd = false
}
class FilterLeagueVC1: UIViewController {
    
    @IBOutlet weak var lblSelectedLeagues: UILabel!
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var viewPopuler: UIView!
    @IBOutlet weak var viewRestOfThe: UIView!
    @IBOutlet weak var viewBannerBottom: UIView!
    
    @IBOutlet weak var tblPopulerLeague: UITableView!
    @IBOutlet weak var tblCountryLeague: UITableView!
    
    @IBOutlet weak var lblDeselectAll: UILabel!
    
    @IBOutlet weak var lblPopuler: UILabel!
    @IBOutlet weak var lblRestOfthewold: UILabel!
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var tblPopulerHightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblCountryHightConstraint: NSLayoutConstraint!
    
    //  var arrFevoriteLeagues = [ModelLeagueList]()
    var arrPopulerLeague = [ModelLeagueListGroupByCountry]()
    var arrCountryLeague = [ModelLeagueListGroupByCountry]()
    
    var arrSelectedLeague = [String]()
    
    var arrSortRecived = [String]()
    var closerfilterbyleagueId:((_ leaugeId:String ,_ arrFilter: [String]) ->())?
    
    var arrFilterFevoriteLeagues = [ModelLeagueList]()
    
    var arrFilterPopulerLeague = [ModelLeagueListGroupByCountry]()
    var arrFilterCountryLeague = [ModelLeagueListGroupByCountry]()
    
    var isSearching = false
    var currentHight = 0.0
    
    var bannerView: GADBannerView!
    var bannerViewDfp: DFPBannerView!
    var arrPopularlocal = [String]()
    
    var isLoadData = false
    var placeholderRow = 10
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.show_skelton()
        arrPopularlocal = ["England","Spain","Germany","Italy","France","United State Of America","World","Europe","South America","Asia","North America","Oceania"]
        
        if arrPopularlocal.contains(kCurrent_Country){
            let index = arrPopularlocal.index(of: kCurrent_Country)
            if index != nil {
                arrPopularlocal.remove(at: index!)
            }
            arrPopularlocal.insert(kCurrent_Country, at: 0)
        }else{
            arrPopularlocal.insert(kCurrent_Country, at: 0)
        }
        
        
        self.tblCountryLeague.rowHeight = UITableViewAutomaticDimension
        self.txtSearch.delegate = self
        
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Filter tournaments", tableName: nil, comment: ""), attributes: [NSAttributedStringKey.foregroundColor:UIColor.white
            ])
        
        self.modifyClearButtonWithImage(image: UIImage(named: "icon_clearTxtfild")!, txtfiled: txtSearch)
        
        lblDeselectAll.text = "Deselect all"
        self.call_Webservice_Get_LeaugeList()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForDetailCell), name: NSNotification.Name(rawValue: KNotifiationFavouriteLeague), object: nil)
        activity.isHidden = true
        activity.stopAnimating()
        
        self.bannerAdSetup()
        
    }
    
    func show_skelton()
    {
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblPopuler,lblRestOfthewold].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
    }
    
    func hide_skelton()
    {
        [lblPopuler,lblRestOfthewold].forEach { $0?.hideSkeleton()
        }
    }
    
    func modifyClearButtonWithImage(image : UIImage ,txtfiled : UITextField) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(sender:) ), for: .touchUpInside)
        txtfiled.rightView = clearButton
        txtfiled.rightViewMode = .whileEditing
    }
    
    @objc func clear(sender : AnyObject) {
        
        self.isSearching = false
        self.arrFilterCountryLeague = self.arrCountryLeague
        self.arrFilterPopulerLeague = self.arrPopulerLeague
        self.tblPopulerLeague.reloadData()
        self.tblCountryLeague.reloadData()
        activity.isHidden = true
        activity.stopAnimating()
        
        self.txtSearch.text = ""
        self.txtSearch.sendActions(for: .editingChanged)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        objAppShareData.isLonchXLPageIndex = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - UITextField Delegates
extension FilterLeagueVC1:UITextFieldDelegate,UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        self.isSearching = false
        self.tblCountryHightConstraint.constant = self.tblCountryLeague.contentSize.height
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        self.txtSearch.resignFirstResponder()
        self.isSearching = false
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.isSearching = false
        // NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.loadDataWithPageCount), object: nil)
        // self.arrFilterFevoriteLeagues = self.arrFevoriteLeagues
        self.arrFilterCountryLeague = self.arrCountryLeague
        self.arrFilterPopulerLeague = self.arrPopulerLeague
        
        self.tblPopulerLeague.reloadData()
        self.tblCountryLeague.reloadData()
        activity.isHidden = true
        activity.stopAnimating()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if isLoadData == false
        {
           return false
        }
        
        let text = textField.text! as NSString
        if (text.length == 1)  && (string == "") {
            
            //NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.loadDataWithPageCount), object: nil)
            
            //self.arrFilterFevoriteLeagues = self.arrFevoriteLeagues
            
            self.arrFilterPopulerLeague = self.arrPopulerLeague
            self.tblPopulerLeague.reloadData()
            
            self.arrFilterCountryLeague = self.arrCountryLeague
            self.tblCountryLeague.reloadData()
            activity.isHidden = true
            activity.stopAnimating()
            return true
        }
        
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if !(substring == "") {
            activity.isHidden = false
            activity.startAnimating()
        }
        searchAutocompleteEntries(withSubstring: substring)
        return true
    }
    
    func searchAutocompleteEntries(withSubstring substring: String) {
        if !(substring == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.loadDataWithPageCount), object: nil)
            self.perform(#selector(self.loadDataWithPageCount), with: nil, afterDelay: 0.4)
            //self.loadDataWithPageCount(page: 0, strSearchText: substring)
        }
    }
    
    @objc func loadDataWithPageCount() {
        // activity.isHidden = false
        // activity.startAnimating()
        self.isSearching = true
        //    self.arrFilterFevoriteLeagues = self.arrFevoriteLeagues.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
        //search country leagues
        /*  //        DispatchQueue.main.async {
         self.arrFilterCountryLeague.removeAll()
         for objCountry in self.arrCountryLeague {
         
         let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
         objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
         objmodel.countryName = objCountry.countryName
         objmodel.countryId = objCountry.countryId
         objmodel.countryFlage = objCountry.countryFlage
         objmodel.isExpandation = true
         if objmodel.arrLeagueList.count > 0{
         self.arrFilterCountryLeague.append(objmodel)
         }else{
         //let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
         //self.arrFilterCountryLeague = self.arrCountryLeague.filter { $0.countryName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
         }
         }
         //        if self.arrFilterCountryLeague.count == 0{
         //            self.arrFilterCountryLeague = self.arrCountryLeague.filter { $0.countryName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
         //        } */
        
        // searching with country name
        self.arrFilterCountryLeague.removeAll()
        self.arrFilterPopulerLeague.removeAll()
        
        self.arrFilterCountryLeague = self.arrCountryLeague.filter { $0.countryName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
        self.arrFilterPopulerLeague = self.arrPopulerLeague.filter { $0.countryName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
        //------------Populer leage list ------------------------------------
        var  arrTemp1 = [ModelLeagueListGroupByCountry]()
        for objCountry in self.arrPopulerLeague {
            
            if self.arrFilterPopulerLeague.count == 0 {
                
                let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                objmodel.countryName = objCountry.countryName
                objmodel.countryId = objCountry.countryId
                objmodel.countryFlage = objCountry.countryFlage
                objmodel.isExpandation = true
                if objmodel.arrLeagueList.count > 0{
                    //self.arrFilterCountryLeague.append(objmodel)
                    arrTemp1.append(objmodel)
                }
                
            }else{
                
                for objFilterCountry in self.arrFilterPopulerLeague{
                    if objCountry.countryId != objFilterCountry.countryId {
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                        objmodel.countryName = objCountry.countryName
                        objmodel.countryId = objCountry.countryId
                        objmodel.countryFlage = objCountry.countryFlage
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            //self.arrFilterCountryLeague.append(objmodel)
                            arrTemp1.append(objmodel)
                        }
                        break
                    }else{
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.countryName = objFilterCountry.countryName
                        objmodel.countryId = objFilterCountry.countryId
                        objmodel.countryFlage = objFilterCountry.countryFlage
                        objmodel.arrLeagueList = objFilterCountry.arrLeagueList
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            arrTemp1.append(objmodel)
                        }
                        break
                    }
                }
            }
        }
        
        self.arrFilterPopulerLeague.removeAll()
        self.arrFilterPopulerLeague = arrTemp1
        // searching with country name
        
        
        // ---------Rest of the wold filter start-------------
        var  arrTemp = [ModelLeagueListGroupByCountry]()
        for objCountry in self.arrCountryLeague {
            
            if self.arrFilterCountryLeague.count == 0 {
                
                let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                objmodel.countryName = objCountry.countryName
                objmodel.countryId = objCountry.countryId
                objmodel.countryFlage = objCountry.countryFlage
                objmodel.isExpandation = true
                if objmodel.arrLeagueList.count > 0{
                    //self.arrFilterCountryLeague.append(objmodel)
                    arrTemp.append(objmodel)
                }
                
            }else{
                
                for objFilterCountry in self.arrFilterCountryLeague{
                    if objCountry.countryId != objFilterCountry.countryId {
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                        objmodel.countryName = objCountry.countryName
                        objmodel.countryId = objCountry.countryId
                        objmodel.countryFlage = objCountry.countryFlage
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            //self.arrFilterCountryLeague.append(objmodel)
                            arrTemp.append(objmodel)
                        }
                        break
                    }else{
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.countryName = objFilterCountry.countryName
                        objmodel.countryId = objFilterCountry.countryId
                        objmodel.countryFlage = objFilterCountry.countryFlage
                        objmodel.arrLeagueList = objFilterCountry.arrLeagueList
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            arrTemp.append(objmodel)
                        }
                        break
                    }
                }
            }
        }
        
        self.arrFilterCountryLeague.removeAll()
        self.arrFilterCountryLeague = arrTemp
        // searching with country name
        
        
        if self.txtSearch.text! == ""{
            //  self.arrFilterFevoriteLeagues = self.arrFevoriteLeagues
            self.arrFilterPopulerLeague = self.arrPopulerLeague
            self.arrFilterCountryLeague = self.arrCountryLeague
        }
        if  self.arrFilterCountryLeague.count == 0{
            self.isSearching = false
        }
        if self.arrFilterFevoriteLeagues.count == 0 && self.arrFilterPopulerLeague.count == 0 && self.arrFilterCountryLeague.count == 0 {
            self.lblNoRecord.isHidden = false
        }else{
            self.lblNoRecord.isHidden = true
        }
        self.tblPopulerLeague.reloadData()
        self.tblCountryLeague.reloadData()
        activity.isHidden = true
        activity.stopAnimating()
        //        self.tblCountryHightConstraint.constant = self.tblCountryLeague.contentSize.height
        //        self.view.layoutIfNeeded()
        //        }
    }
}


// MARK: - extension IBAction
extension FilterLeagueVC1{
    
    @IBAction func btnDoneAction(_ sender : UIButton){
        self.view.endEditing(true)
        //
        //        if arrSelectedLeague.count != 0
        //        {
        //
        //            //            if self.arrSortRecived.count != 0
        //            //            {
        //            //                let strLeagueId =  self.arrSortRecived.joined(separator: ",")
        //            //                print(strLeagueId)
        //            //                objAppShareData.strFilterLeagueId = strLeagueId
        //            //                closerfilterbyleagueId?(strLeagueId, self.arrSortRecived)
        //            //            }
        //            //
        //            //            else
        //            //            {
        //
        //            let strLeagueId = arrSelectedLeague.joined(separator: ",")
        //            print(strLeagueId)
        //            objAppShareData.strFilterLeagueId = strLeagueId
        //            objAppShareData.strselctedMatchTab = "all"
        //
        //            //    }
        //        }
        let strLeagueId = arrSelectedLeague.joined(separator: ",")
        print(strLeagueId)
        objAppShareData.strFilterLeagueId = strLeagueId
        objAppShareData.strselctedMatchTab = "all"
        objAppShareData.strselctedOngoingTab = ""
        FilterLeague.isFilterd = true
        APP_DELEGATE.isInterstialPresent = false
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnSortinglAction(_ sender : UIButton)
    {
        self.view.endEditing(true)
        let viewController = UIStoryboard(name: "UserTabbar",bundle: nil).instantiateViewController(withIdentifier: "SortLeaguesVC") as! SortLeaguesVC
        viewController.arrSelectedFilterLeague = arrSelectedLeague
        
        viewController.sortleagueCloser = {(arrSort)in
            self.arrSortRecived = arrSort
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnSelectAllAction(_ sender : UIButton){
        self.view.endEditing(true)
        
    }
    @IBAction func btnDeSelectAllAction(_ sender : UIButton){
        self.view.endEditing(true)
        objAppShareData.strFilterLeagueId = ""
        self.txtSearch.text = ""
        self.call_Webservice_Post_addRemoveFilterLeague(request_type: "remove_all", league_id: "", index: -1, tableName: "")
        
    }
    
    @IBAction func btnMenuAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.slideMenuController()?.toggleLeft()
    }
    
    
    @objc func reloadDataForDetailCell(notification: Notification){
        
        
        self.lblSelectedLeagues.text = "Selected " + String(FilterLeague.arrSelectedLeague.count) + "/25"
        
        if FilterLeague.arrSelectedLeague.count > 0
        {
            lblDeselectAll.text = "Deselect all"
        }
        
        
        self.tblPopulerLeague.reloadData()
        self.tblCountryLeague.reloadData()
        /*
         if let objLeague = notification.object as? ModelLeagueList{
         
         if objLeague.isSelected == "0"{
         
         if objAppShareData.arrLeagueForFinalSorting.count>0{
         let filteredArray = objAppShareData.arrLeagueForFinalSorting.filter { $0.leagueId == objLeague.leagueId }
         if filteredArray.count>0{
         let obj = filteredArray[0]
         let index = objAppShareData.arrLeagueForFinalSorting.index(where: {$0.leagueId == obj.leagueId})
         objAppShareData.arrLeagueForFinalSorting.remove(at: index ?? 0)
         }
         }
         
         
         // unSelecte FevoriteLeagues
         for objFevorite in self.arrFilterFevoriteLeagues{
         if objFevorite.leagueId == objLeague.leagueId {
         objFevorite.isSelected = "0"
         break
         }
         }
         
         //remove from selected
         for objSelected in self.arrSelectedLeague{
         if objSelected == objLeague.leagueId{
         let index = self.arrSelectedLeague.index(where: {$0 == objSelected})
         if index != nil {
         self.arrSelectedLeague.remove(at: index!)
         }
         break
         }
         }
         self.lblSelectedLeagues.text = "Selected " + String(self.arrSelectedLeague.count) + "/25"
         
         self.tblPopulerLeague.reloadData()
         
         }else{
         objAppShareData.arrLeagueForFinalSorting.append(objLeague)
         
         // Selecte from fevorite
         for objFevorite in self.arrFilterFevoriteLeagues{
         if objFevorite.leagueId == objLeague.leagueId {
         objFevorite.isSelected = "1"
         break
         }
         }
         self.arrSelectedLeague.append(objLeague.leagueId)
         self.arrSelectedLeague = self.arrSelectedLeague.uniqued()
         self.lblSelectedLeagues.text = "Selected " + String(self.arrSelectedLeague.count) + "/25"
         
         self.tblPopulerLeague.reloadData()
         }
         }
         */
    }
    
    
    
    @objc func btnPopulerDropdownPressed(sender: UIButton){
        //let indexPath = IndexPath(row: sender.tag, section: 0)
        //let cell = self.tblCountryLeague.cellForRow(at: indexPath) as! LeagueFilterCell
        
        if self.arrFilterPopulerLeague.count == 0
        {
            return
        }
        
        let objModel = self.arrFilterPopulerLeague[sender.tag]
        print(  objModel.arrLeagueList.count)
        if objModel.isExpandation == false{
            objModel.isExpandation = true
        }else{
            objModel.isExpandation = false
        }
        
        self.tblPopulerLeague.reloadData()
    }
    
    @objc func btnCountryDropdownPressed(sender: UIButton){
        //let indexPath = IndexPath(row: sender.tag, section: 0)
        //let cell = self.tblCountryLeague.cellForRow(at: indexPath) as! LeagueFilterCell
        if self.arrFilterCountryLeague.count == 0
        {
            return
        }
        let objModel = self.arrFilterCountryLeague[sender.tag]
        print(  objModel.arrLeagueList.count)
        if objModel.isExpandation == false{
            objModel.isExpandation = true
        }else{
            objModel.isExpandation = false
        }
        
        //let indexPosition = IndexPath(row: sender.tag, section: 0)
        //self.tblCountryLeague.reloadRows(at: [indexPosition], with: .none)
        // self.tblCountryHightConstraint.constant = self.tblCountryLeague.contentSize.height
        //  self.view.layoutIfNeeded()
        
        self.tblCountryLeague.reloadData()
    }
}


// MARK: - TableView Delegates & Datasource
extension FilterLeagueVC1: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isLoadData == false
        {
            return placeholderRow
        }
        
        if tableView == tblPopulerLeague{
            if arrFilterPopulerLeague.count > 0{
                self.viewPopuler.isHidden = false
            }else{
                self.viewPopuler.isHidden = true
            }
            //  self.tblPopulerHightConstraint.constant = CGFloat((self.arrFilterPopulerLeague.count)*50)
            return self.arrFilterPopulerLeague.count
        }else{
            if arrFilterCountryLeague.count > 0{
                self.viewRestOfThe.isHidden = false
            }else{
                self.viewRestOfThe.isHidden = true
            }
            //self.tblCountryHightConstraint.constant = self.tblCountryLeague.contentSize.height
            return self.arrFilterCountryLeague.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if isLoadData == false
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueFilterCell", for: indexPath) as? LeagueFilterCell{
                cell.show_skelton()
                
                DispatchQueue.main.async(execute: {() -> Void in
                    self.tblPopulerHightConstraint.constant = self.tblPopulerLeague.contentSize.height
                })
                return cell
            }
        }
        
        if tableView == tblPopulerLeague {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueFilterCell", for: indexPath) as? LeagueFilterCell
            {
                cell.hide_skelton()
                let objModel = self.arrFilterPopulerLeague[indexPath.row]
                cell.lblLeagueName.text = objModel.countryName
                
                if let url = URL(string: objModel.countryFlage){
                    cell.imgLeague.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgLeague.image =  UIImage(named: "icon_placeholderTeam")
                }
                
                
                if objModel.isExpandation == true
                {
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi)
                    cell.arrPopulerLeagegs = objModel.arrLeagueList
                    cell.tblPopulerLeagues.reloadData()
                }else{
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi*2)
                    cell.arrPopulerLeagegs.removeAll()
                    cell.tblPopulerLeagues.reloadData()
                }
                
                cell.btnAddFavorite.tag = indexPath.row
                cell.btnAddFavorite.addTarget(self, action: #selector(btnPopulerDropdownPressed(sender:)), for: .touchUpInside)
                
                
                DispatchQueue.main.async(execute: {() -> Void in
                    self.tblPopulerHightConstraint.constant = self.tblPopulerLeague.contentSize.height
                })
                return cell
            }
                
                
            else{
                return UITableViewCell()
            }
            
        }
        else
        {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueFilterCell", for: indexPath) as? LeagueFilterCell{
                
                cell.hide_skelton()
                let objModel = self.arrFilterCountryLeague[indexPath.row]
                cell.lblLeagueName.text = objModel.countryName
                
                if let url = URL(string: objModel.countryFlage){
                    cell.imgLeague.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgLeague.image =  UIImage(named: "icon_placeholderTeam")
                }
                
                
                if objModel.isExpandation == true
                {
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi)
                    cell.arrCountryLeagegs = objModel.arrLeagueList
                    cell.tblCountryLeagues.reloadData()
                }else{
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi*2)
                    cell.arrCountryLeagegs.removeAll()
                    cell.tblCountryLeagues.reloadData()
                }
                
                
                cell.btnAddFavorite.tag = indexPath.row
                cell.btnAddFavorite.addTarget(self, action: #selector(btnCountryDropdownPressed(sender:)), for: .touchUpInside)
                
                DispatchQueue.main.async(execute: {() -> Void in
                    self.tblCountryHightConstraint.constant = self.tblCountryLeague.contentSize.height
                })
                return cell
            }
            else{
                return UITableViewCell()
            }
            
        }
        
    }
}

// MARK: - Webservice
extension FilterLeagueVC1{
    
    func call_Webservice_Post_addRemoveFilterLeague(request_type:String,league_id:String, index:Int,tableName:String){
        
        self.view.isUserInteractionEnabled = false
        
        let paramDict = ["type":request_type,
                         "league_id":league_id] as [String:Any]
        
        print(paramDict)
        objWebServiceManager.requestPost(strURL: webUrl.add_removeFilterleague, params: paramDict, success: { (response) in
            
            if request_type == "remove_all"
            {
                self.lblDeselectAll.text = "All league are deselected"
            }
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                objAppShareData.strFilterLeagueId = ""
                self.call_Webservice_Get_LeaugeList()
                
                self.view.isUserInteractionEnabled = true
            }else{
                self.view.isUserInteractionEnabled = true
                if message == "Can't select more than 25 leagues"{
                    
                    objAlert.showAlert(message: message, title: "Alert", controller: self)
                }
            }
            
        }) { (error) in
            print(error)
            self.view.isUserInteractionEnabled = true
            SVProgressHUD.dismiss()
            
        }
    }
    
    func call_Webservice_Get_LeaugeList() {
       // SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.get_country_league_list, params: nil, success: { (response) in
            self.isLoadData = true
            self.hide_skelton()
            SVProgressHUD.dismiss()
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                //
                self.arrPopulerLeague.removeAll()
                self.arrFilterPopulerLeague.removeAll()
                //  self.arrFevoriteLeagues.removeAll()
                self.arrFilterFevoriteLeagues.removeAll()
                self.arrCountryLeague.removeAll()
                self.arrFilterCountryLeague.removeAll()
                self.arrSelectedLeague.removeAll()
                //
                
                if let data = response["data"] as? [String:Any] {
                    
                    //country_list
                    if let country_list = data["country_list"] as? [[String: Any]]{
                        
                        
                        // Popular
                        var arr_poplulerleague = [ModelLeagueListGroupByCountry]()
                        
                        for dict in country_list{
                            let obj = ModelLeagueListGroupByCountry.init(fromDictionary: dict)
                            if  self.arrPopularlocal.contains(where: { $0 == obj.countryName }) {
                                if obj.arrLeagueList.count > 0{
                                    arr_poplulerleague.append(obj)
                                }
                            }
                        }
                        
                        for i in 0..<self.arrPopularlocal.count
                        {
                            let filteredArray = arr_poplulerleague.filter(){ $0.countryName.contains(self.arrPopularlocal[i]) }
                            for objNEW in filteredArray{
                                self.arrPopulerLeague.append(objNEW)
                                self.arrFilterPopulerLeague.append(objNEW)
                            }
                        }
                        
                        self.tblPopulerLeague.reloadData()
                        
                        FilterLeague.arrSelectedLeague.removeAll()
                        
                        for dict in country_list{
                            let obj = ModelLeagueListGroupByCountry.init(fromDictionary: dict)
                            if obj.arrLeagueList.count > 0{
                                
                                for objSelected in obj.arrLeagueList{
                                    if objSelected.isSelected == "1"{
                                        self.arrSelectedLeague.append(objSelected.leagueId)
                                    }
                                }
                                self.arrSelectedLeague = self.arrSelectedLeague.uniqued()
                                self.lblSelectedLeagues.text = "Selected " + String(self.arrSelectedLeague.count) + "/25"
                                
                                // obj.isExpandation = true
                                
                                for i in 0..<obj.arrLeagueList.count
                                {
                                    if obj.arrLeagueList[i].isSelected == "1"
                                    {
                                        FilterLeague.arrSelectedLeague.append(obj.arrLeagueList[i].leagueId)
                                    }
                                }
                                
                                self.arrCountryLeague.append(obj)
                                self.arrFilterCountryLeague.append(obj)
                            }
                            
                        }
                        self.tblCountryLeague.reloadData()
                    }
                    if  self.arrPopulerLeague.count == 0 && self.arrCountryLeague.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                    
                }
            }else{
                if self.arrPopulerLeague.count == 0 && self.arrCountryLeague.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
                objAlert.showAlert(message: message, title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            self.hide_skelton()
            self.isLoadData = true
            SVProgressHUD.dismiss()
        }
    }
}


extension FilterLeagueVC1:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        
        
        bannerViewDfp = DFPBannerView(adSize: kGADAdSizeMediumRectangle)
        bannerViewDfp.adUnitID = objAppShareData.BannerId
        bannerViewDfp.rootViewController = self
        bannerViewDfp.load(DFPRequest())
        
        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
}

