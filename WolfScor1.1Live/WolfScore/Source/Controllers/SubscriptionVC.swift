//
//  SubscriptionVC.swift
//  WolfScore
//
//  Created by Mindiii on 6/25/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SVProgressHUD
import ImageSlideshow

class SubscriptionVC: UIViewController {
    
    var arrSubscriptionList = [ModelSubscriptionList]()
    
    @IBOutlet var slideshow: ImageSlideshow!
    
    let localSource = [BundleImageSource(imageString: "85"), BundleImageSource(imageString: "293"), BundleImageSource(imageString: "939")]

    override func viewDidLoad() {
        super.viewDidLoad()

        
        slideshow.slideshowInterval = 4.0
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideshow.contentScaleMode = UIViewContentMode.scaleToFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        pageControl.pageIndicatorTintColor = UIColor.black
        slideshow.pageIndicator = pageControl
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        slideshow.setImageInputs(localSource)
        

        self.call_Webservice_For_GetSubscriptionPlanList()
    }
    
  
    
    @objc func didTap() {
//        let fullScreenController = slideshow.presentFullScreenController(from: self)
//        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
//        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    // MARK: Get subscription list api -----

    func call_Webservice_For_GetSubscriptionPlanList() {
        
        objWebServiceManager.requestGet(strURL: webUrl.Get_subscription_plan_list, params: nil, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [[String:Any]] {
                    
                    self.arrSubscriptionList.removeAll()
                    for obj in data
                    {
                     let object = ModelSubscriptionList(fromdictionary: obj)
                        
                        self.arrSubscriptionList.append(object)
                    }
                }
            }
            SVProgressHUD.dismiss()
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
    }
    
// MARK: Subscription transaction detail api for send infor  server -----
    func call_Webservice_Post_SubscriptionTransactionDetail(purchaseDetails: [String:String],SubscriptionPlanId:String,encodedString:String){
        
        
        let itunesTransactionDetail = try! JSONSerialization.data(withJSONObject:purchaseDetails, options: [ ])
        let jsonItunesTransactionDetail = String(data: itunesTransactionDetail, encoding: .utf8)
        
        
        var subscription_detailDict = [String: Any]()
        subscription_detailDict[WsParams.product_id] =  purchaseDetails["product_id"]
        subscription_detailDict[WsParams.quantity] = purchaseDetails["quantity"]
        subscription_detailDict[WsParams.purchase_date] = purchaseDetails["purchase_date"]
        subscription_detailDict[WsParams.expires_date] = purchaseDetails["expires_date"]
        subscription_detailDict[WsParams.amount] = purchaseDetails["amount"]
        subscription_detailDict[WsParams.transaction_id] = purchaseDetails["transaction_id"]
        subscription_detailDict[WsParams.origional_transaction_id] = purchaseDetails["origional_transaction_id"]
        subscription_detailDict[WsParams.receipt_key] = encodedString
        
        
        let subscription_detail = try! JSONSerialization.data(withJSONObject:subscription_detailDict, options: [ ])
        let jsonStringSubscription_detail = String(data: subscription_detail, encoding: .utf8)
        
        
        
        let paramDict = ["plan_id":SubscriptionPlanId,
                         "subscription_platform":"2" ,
                         "subscription_detail":jsonStringSubscription_detail ?? "",
                         "receipt_key":encodedString,
                         "subscription_info":jsonItunesTransactionDetail ?? ""] as [String:Any]
        
        objWebServiceManager.requestPost(strURL: webUrl.SubscriptionUserTransactionDetail, params: paramDict, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
    self.navigationController?.popViewController(animated: true)
                
            }else{
                
            }
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            SVProgressHUD.dismiss()
            
        }) { (error) in
            print(error)
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            SVProgressHUD.dismiss()
        }
    }
    
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        print("current page:", page)
    }
    
    @IBAction func action_Back(sender: UIButton){
    self.navigationController?.popViewController(animated: true)

    }
    @IBAction func action_PurchasePlan(sender: UIButton){
        
        
        if sender.tag == 100
        {
        }
        else if sender.tag == 101
        {
            GlobalUtility.showActivityIndi(viewContView: self.view)
            PurchaseHelper.payForPackage(packageId: "com.mindiii.wolfScore.autoRenew3", onSuccss: {[weak self] (PurchaseDetails, encodedString , receipt_Data) in
                
                guard let weakSelf = self else{return}
                weakSelf.upload(receipt: receipt_Data, SubscriptionPlanId: "1")
                })
            { (errorAlert) in
                
                GlobalUtility.hideActivityIndi(viewContView: self.view)
                self.showAlert(errorAlert)
            }
        }
        else if sender.tag == 102
        {
            
        }
        
    }
    func showAlert(_ alert: UIAlertController) {
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
}


extension SubscriptionVC
{
    func upload(receipt data: Data , SubscriptionPlanId : String) {
        
        let receiptdataBase64 = data.base64EncodedString()
        let body = [
            "receipt-data":receiptdataBase64 ,
            "password": WsParams.itunesSharedSecret
        ]
        let bodyData = try! JSONSerialization.data(withJSONObject: body, options: [])
        
        let url = URL(string:  webUrl.verifyReceiptUrl)!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = bodyData
        
        let task = URLSession.shared.dataTask(with: request) { (responseData, response, error) in
            
            DispatchQueue.main.async {
                
                if let responseData = responseData {
                    let json = try! JSONSerialization.jsonObject(with: responseData, options: []) as! Dictionary<String, Any>
                    
                    if let receiptInfo: NSArray = json["latest_receipt_info"] as? NSArray {
                        print("latestReceiptInfoList----\(receiptInfo)")
                        //LatestRecipt
                        let lastReceipt = receiptInfo.lastObject as! NSDictionary
                        
                        print("latestReciptData \(lastReceipt)")
                        self.call_Webservice_Post_SubscriptionTransactionDetail(purchaseDetails: lastReceipt as! [String : String], SubscriptionPlanId: SubscriptionPlanId, encodedString: receiptdataBase64)
                    }
                }
            }
        }
        task.resume()
    }
    
   
}



