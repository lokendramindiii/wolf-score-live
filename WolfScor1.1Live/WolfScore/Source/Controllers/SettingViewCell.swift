//
//  SettingViewCell.swift
//  WolfScore
//
//  Created by Mindiii on 5/13/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class SettingViewCell: UITableViewCell {

    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var img:UIImageView!
    @IBOutlet var imgRightArrow:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
