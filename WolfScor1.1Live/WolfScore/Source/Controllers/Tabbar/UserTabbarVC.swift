//
//  UserTabbarVC.swift
//  WolfScore
//
//  Created by Mindiii on 27/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
var viewOverTabBar = UIView()
var lblUnreadChat = UILabel()
var lblUnreadPro = UILabel()
let SEPARATOR_WIDTH = 1.0
var selected_my_Index : Int = 0

class UserTabbarVC: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for viewController in self.viewControllers! {
            _ = viewController.view
            
        }
        self.tabBar.backgroundColor = UIColor.colorConstant.tabBarBackgroundColor
        self.selectedIndex = selected_my_Index
        UITabBar.appearance().layer.borderWidth = 0.0
        UITabBar.appearance().clipsToBounds = true
        self.tabBar.setValue(true, forKey: "_hidesShadow")
        drawTabbar()
        //self.setTabarShadow()
    }
    
    func setTabarShadow() {
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        tabBar.layer.shadowRadius = 5
        tabBar.layer.shadowOpacity = 0.3
        tabBar.layer.masksToBounds = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - Navigation
    func drawTabbar() {
        self.tabBar.items?[0].image = #imageLiteral(resourceName: "inactive_matche_ico").withRenderingMode(.alwaysOriginal)
        //self.tabBar.items?[1].image = #imageLiteral(resourceName: "inactive_live_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[1].image = #imageLiteral(resourceName: "inactive_leagues_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[2].image = #imageLiteral(resourceName: "inactive_news_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[3].image = #imageLiteral(resourceName: "inactive_highlight_ico").withRenderingMode(.alwaysOriginal)
        
        self.tabBar.items?[0].selectedImage = #imageLiteral(resourceName: "active_matche_ico").withRenderingMode(.alwaysOriginal)
        //self.tabBar.items?[1].selectedImage = #imageLiteral(resourceName: "active_live_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[1].selectedImage = #imageLiteral(resourceName: "active_leagues_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[2].selectedImage = #imageLiteral(resourceName: "active_news_ico").withRenderingMode(.alwaysOriginal)
        self.tabBar.items?[3].selectedImage = #imageLiteral(resourceName: "active_highlight_ico").withRenderingMode(.alwaysOriginal)
        // addUnderlinInTabBarItem()
    }
    
    func setupTabBarSeparators(){
        let itemWidth = floor(self.tabBar.frame.size.width / CGFloat(self.tabBar.items!.count))
        let bgView = UIView(frame: CGRect(x: 0, y: 0, width: tabBar.frame.size.width, height: tabBar.frame.size.height))
        
        for i in 0..<(tabBar.items?.count)! - 1{
            let separator = UIView(frame: CGRect(x: itemWidth * CGFloat(i + 1) - CGFloat(SEPARATOR_WIDTH / 2), y: 10, width: CGFloat(SEPARATOR_WIDTH), height: self.tabBar.frame.size.height-20))
            separator.backgroundColor = UIColor(red: 98.0 / 255.0, green: 98.0 / 255.0, blue: 98.0 / 255.0, alpha: 1)
            bgView.addSubview(separator)
        }
        
        UIGraphicsBeginImageContext(bgView.bounds.size)
        bgView.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let tabBarBackground = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UITabBar.appearance().backgroundImage = tabBarBackground
    }
    
    func addUnderlinInTabBarItem() {
        let tabBar: UITabBar? = self.tabBar
        let divide = 4.0
        
        var view = UIView ()
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                view = UIView(frame: CGRect(x: (tabBar?.frame.origin.x)!, y: (tabBar?.frame.origin.y)!, width: self.view.frame.size.width/CGFloat(divide), height: 58))
            case 1334:
                view = UIView(frame: CGRect(x: (tabBar?.frame.origin.x)!, y: (tabBar?.frame.origin.y)!, width: self.view.frame.size.width/CGFloat(divide), height: 58))
            case 1920, 2208:
                view = UIView(frame: CGRect(x: (tabBar?.frame.origin.x)!, y: (tabBar?.frame.origin.y)!, width: self.view.frame.size.width/CGFloat(divide), height: 58))
            case 2436:
                view = UIView(frame: CGRect(x: (tabBar?.frame.origin.x)!, y: (tabBar?.frame.origin.y)!, width: self.view.frame.size.width/CGFloat(divide), height: 90))
            default:
                view = UIView(frame: CGRect(x: (tabBar?.frame.origin.x)!, y: (tabBar?.frame.origin.y)!, width: self.view.frame.size.width/CGFloat(divide), height: 58))
            }
        }
        
        
        let border = UIImageView(frame: CGRect(x: view.frame.origin.x + 5, y: 5, width: self.view.frame.size.width / CGFloat(divide) - 10, height: 2))
        border.backgroundColor = #colorLiteral(red: 0.951467216, green: 0.6705550551, blue: 0.2112716436, alpha: 1)
        
        view.addSubview(border)
        
        UIGraphicsBeginImageContext(view.bounds.size)
        
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let tabBarBackground = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        //  UITabBar.appearance().backgroundImage = tabBarBackground
        
        self.tabBar.tintColor = #colorLiteral(red: 0.951467216, green: 0.6705550551, blue: 0.2112716436, alpha: 1)
        // self.tabBar.backgroundImage = #imageLiteral(resourceName: "tab_bar_bg")
        
        //bottom line
        tabBar?.selectionIndicatorImage = tabBarBackground
        //////////////////// Custom View on Tabbar
        viewOverTabBar = UIView(frame: CGRect(x: 0 , y: 0, width: (tabBar?.frame.size.width)!, height: 100))
        self.tabBar.addSubview(viewOverTabBar)
        viewOverTabBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        viewOverTabBar.alpha = 0.6
        //viewOverTabBar.isOpaque
        viewOverTabBar.isHidden = true
        //////////////////// Custom View ForBadge
        //************ Profile Badge **************//
        lblUnreadPro = UILabel(frame: CGRect(x: ((tabBar?.frame.size.width)! - ((tabBar?.frame.size.width)!/5) - 25) , y: -10, width: 20, height: 20))
        lblUnreadPro.font = UIFont.systemFont(ofSize: 12)
        lblUnreadPro.textAlignment = .center
        lblUnreadPro.layer.cornerRadius = lblUnreadPro.frame.size.height/2
        lblUnreadPro.textColor = UIColor.white
        lblUnreadPro.layer.masksToBounds = true
        self.tabBar.addSubview(lblUnreadPro)
        //lblUnreadPro.backgroundColor = UIColor.colorConstant.appRedLightColor
        lblUnreadPro.isHidden = true
        //************ Chat Badge **************//
        lblUnreadChat = UILabel(frame: CGRect(x: ((tabBar?.frame.size.width)! - 2*((tabBar?.frame.size.width)!/5) - 25) , y: -10, width: 20, height: 20))
        lblUnreadChat.font = UIFont.systemFont(ofSize: 12)
        lblUnreadChat.textAlignment = .center
        lblUnreadChat.layer.cornerRadius = lblUnreadChat.frame.size.height/2
        lblUnreadChat.textColor = UIColor.white
        lblUnreadChat.layer.masksToBounds = true
        self.tabBar.addSubview(lblUnreadChat)
        //lblUnreadChat.backgroundColor = UIColor.colorConstant.appRedLightColor
        lblUnreadChat.isHidden = true
    }
    
    func repositionBadge(tabIndex: Int){
        
        for badgeView in self.tabBar.subviews[4].subviews {
            
            print(NSStringFromClass(badgeView.classForCoder))
            
            if NSStringFromClass(badgeView.classForCoder) == "BadgeView" {
                badgeView.layer.transform = CATransform3DIdentity
                badgeView.layer.transform = CATransform3DMakeTranslation(-117.0, -10.0, 1.0)
            }
        }
    }
}
