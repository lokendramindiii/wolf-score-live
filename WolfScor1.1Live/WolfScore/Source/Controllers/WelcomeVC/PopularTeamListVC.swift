//
//  PopularTeamListVC.swift
//  WolfScore
//
//  Created by Mindiii on 25/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster
import AlamofireImage

class PopularTeamListVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tblFavorite: UITableView!
    @IBOutlet weak var lblSelectedTeam: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewDot1: UIView!
    @IBOutlet weak var viewDot2: UIView!
    @IBOutlet weak var viewDot3: UIView!
    @IBOutlet weak var viewDot4: UIView!
    @IBOutlet weak var viewDot5: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var scrollViewMain: UIScrollView!
    
    @IBOutlet weak var tblHightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var stackViewDots: UIStackView!
    var arrAllData = [ModelTeam]()
    
    var searchActive = false
    var strTotalRecords = "0"
    var offSet = 0
    var Limit = 20
    var isFromMyFav = false
    //var searchActive:Bool = false
    var isLoadData = false
    var placeholderRow = 10

    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tblFavorite.tableFooterView = UIView()
            } else {
                self.tblFavorite.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblFavorite.register(UINib(nibName: "TeamSkeltonViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TeamSkeltonViewCell")

        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnBg))
        self.view.addGestureRecognizer(tap)
        SVProgressHUD.setDefaultMaskType(.clear)
        
        self.scrollViewMain.isScrollEnabled = true
        self.configureView()
    }
    func configureView() {
        
        self.lblSelectedTeam.isHidden = true
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Search", tableName: nil, comment: ""), attributes: [NSAttributedStringKey.foregroundColor:UIColor.white
            ])
        
        self.modifyClearButtonWithImage(image: UIImage(named: "icon_clearTxtfild")!, txtfiled: txtSearch)

        self.viewDot1.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot2.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot3.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot4.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot5.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        
        self.tblFavorite.tableFooterView = UIView()
    }
    
    func modifyClearButtonWithImage(image : UIImage ,txtfiled : UITextField) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(sender:)), for: .touchUpInside)
        txtfiled.rightView = clearButton
        txtfiled.rightViewMode = .whileEditing
    }
    
    @objc func clear(sender : AnyObject) {
        
        self.view.endEditing(true)
        self.txtSearch.text = ""
        self.loadDataWithPageCount(page: 0, strSearchText: "")
        self.txtSearch.sendActions(for: .editingChanged)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        for i in 0..<12
        {
            let cell = self.tblFavorite.dequeueReusableCell(withIdentifier: "TeamSkeltonViewCell")as! TeamSkeltonViewCell
            cell.hide_skelton()
        }

        self.txtSearch.text = ""
        self.scrollViewMain.contentOffset.y = 0
        self.btnNext.isUserInteractionEnabled = true
        self.lblNoData.isHidden = true
        
        if self.isFromMyFav == true {
            self.btnSkip.isHidden = true
            self.stackViewDots.isHidden = true
            self.btnNext.setTitle("DONE", for: .normal)
            
        }
        else{
            self.btnSkip.isHidden = false
            self.stackViewDots.isHidden = false
            self.btnNext.setTitle("FINISH", for: .normal)
        }
        if self.arrAllData.count == 0{
            self.offSet = 0
            let dictPram = ["search_term":"",
                            "limit":String(self.Limit),
                            "offset":self.offSet] as [String: AnyObject]
            self.call_Webservice_Get_Popular(dict_param: dictPram)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc func tapOnBg() {
        self.view.endEditing(true)
    }
}

// MARK: - IBActions

extension PopularTeamListVC{
    
    @IBAction func btnNextAction(_ sender : UIButton){
        
        self.view.endEditing(true)
        SVProgressHUD.show(withStatus: "Please wait")
        
        /* self.arrSelectedData.removeAll()
         self.str_Selected_Team_Id = ""
         for i in 0..<self.arrAllData.count {
         let objModel: ModelTeam = self.arrAllData[i]
         objModel.isAddedToFav = false
         let strIsFav = objModel.is_favorite
         if strIsFav == "1"{
         //self.str_Selected_Team_Id = objModel.str_Team_id
         self.arrSelectedData.append(objModel.team_id)
         self.str_Selected_Team_Id = (self.arrSelectedData.joined(separator: ","))
         print(self.arrSelectedData)
         }
         
         } */
        if self.isFromMyFav == true {
            self.dismiss_Presented_View()
        }else{
            self.btnNext.isUserInteractionEnabled = true
//            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "TopLeaguesListVC") as! TopLeaguesListVC
//            self.navigationController?.pushViewController(viewController, animated: true)
            
        self.call_Webservice_add_popular_league_favorite_league()

        }
        
        
        
    }
    
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        if self.isFromMyFav == true {
            dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnSkipAction(_ sender : UIButton){
        self.view.endEditing(true)
        
        self.call_Webservice_add_popular_league_favorite_league()
      //  objAppDelegate.goToTabBar()
    }
    func dismiss_Presented_View() {
        dismiss(animated: true, completion: nil)
    }
}
// MARK: - UITextField Delegates

extension PopularTeamListVC{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.searchActive = true
        return true;
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.searchActive = false
        return true;
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        txtSearch.resignFirstResponder()
        //searchActive = false
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if isLoadData == false
        {
            return false
        }
        
        let text = textField.text! as NSString
        if (text.length == 1)  && (string == "") {
            self.loadDataWithPageCount(page: 0, strSearchText: "")
        }
        
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        searchAutocompleteEntries(withSubstring: substring)
        return true
    }
    
    func searchAutocompleteEntries(withSubstring substring: String) {
        if !(substring == "") {
            self.loadDataWithPageCount(page: 0, strSearchText: substring)
        }
    }
    func loadDataWithPageCount(page: Int, strSearchText : String = "") {
        self.offSet = 0
        let strName = strSearchText
        let dicParam = ["search_term":strName,
                        "limit":String(self.Limit),
                        "offset":offSet
            ] as [String : AnyObject]
        self.call_Webservice_Get_Popular(dict_param: dicParam)
    }
}
// MARK: - Webservice

extension PopularTeamListVC {
    
    func call_Webservice_add_popular_league_favorite_league() {
        self.view.isUserInteractionEnabled = false
        let paramsDict = ["country":kCurrent_Country] as [String:AnyObject]

        objWebServiceManager.requestGet(strURL: webUrl.add_popular_league_favorite_league, params: paramsDict, success: { (response) in
            // print(response)
            SVProgressHUD.dismiss()
            self.view.isUserInteractionEnabled = true
            _ = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                print("success api")
                  objAppDelegate.goToTabBar()
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    
    func call_Webservice_Get_Popular(dict_param:[String:AnyObject]) {
        if self.searchActive == false {
          //  SVProgressHUD.show(withStatus: "Please wait")
        }
        objWebServiceManager.requestGet(strURL: webUrl.get_Popular_Teams, params: dict_param, success: { (response) in
            // print(response)
            SVProgressHUD.dismiss()
            self.isLoadData = true
            // self.view.isUserInteractionEnabled = true
            _ = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                //suresh
                self.shouldLoadMore = false
                //suresh
                if let data = response["data"] as? [String:Any] {
                    
                    if let arrData = data["team_list"] as? [[String: Any]]{
                        var arrAllDataLocal = [ModelTeam]()
                        //suresh
                        if self.offSet == 0{
                            self.arrAllData.removeAll()
                        }
                        //suresh
                        for dict in arrData{
                            let obj = ModelTeam.init(dict: dict)
                            
                            arrAllDataLocal.append(obj)
                        }
                        if self.offSet == 0{
                            self.arrAllData = arrAllDataLocal
                        }
                        else{
                            self.arrAllData += arrAllDataLocal
                        }
                        if let strTotal = data["total_records"] as? String {
                            self.strTotalRecords = strTotal
                        }
                      
                        if self.arrAllData.count == 0{
                            self.tblFavorite.isHidden = true
                            self.lblNoData.isHidden = false
                        }else{
                            self.tblFavorite.isHidden = false
                            self.lblNoData.isHidden = true
                        }
                        
                        self.tblFavorite.reloadData()
                        //                        let hi = self.tblFavorite.contentSize.height
                        //                        self.heightOfTable.constant = hi
                    }
                }
            }
        }) { (error) in
            print(error)
            self.isLoadData = true
            SVProgressHUD.dismiss()
        }
    }
    
    
    func call_Webservice_Post_Favorite(request_type:String,request_id:String, index:Int){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"team"] as [String:Any]
        print(paramDict)
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
 //             let objTag = self.arrAllData[index]
//                if objTag.is_favorite == "0"{
//                    self.lblSelectedTeam.isHidden = false
//                    self.lblSelectedTeam.text = objTag.name + " added as favorite"
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
//                        self.lblSelectedTeam.isHidden = true
//                    }
//                    objTag.is_favorite = "1"
//                }else{
//                    objTag.is_favorite = "0"
//                    self.lblSelectedTeam.isHidden = true
//                }
//                let indexPosition = IndexPath(row: index, section: 0)
//                self.tblFavorite.reloadRows(at: [indexPosition], with: .none)
//                self.tblFavorite.reloadData()
            }else{
                
                if message == "Cannot select more than 100 teams"{
                    let objTag = self.arrAllData[index]
                    objTag.is_favorite = "0"
                    self.tblFavorite.reloadData()
                    let window = UIApplication.shared.delegate?.window
                    let visibleVC = window??.visibleViewController
                    
                    if  visibleVC?.title == "Alert"{
                        return
                    }
                    objAlert.showAlert(message: message, title: "Alert", controller: self)
                }
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
    }
}
// MARK: - TableView Delegates & Datasource

extension PopularTeamListVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isLoadData == false
        {
            self.tblHightConstraint.constant = CGFloat((placeholderRow)*55)
            return placeholderRow

        }
        
        self.tblHightConstraint.constant = CGFloat((self.arrAllData.count)*55)
        return arrAllData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if isLoadData == false
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamSkeltonViewCell", for: indexPath) as! TeamSkeltonViewCell
            cell.show_skelton()
            return cell
        }

        if let cell = tableView.dequeueReusableCell(withIdentifier: "TeamTableViewCell", for: indexPath) as? TeamTableViewCell{
            
            let objModel = self.arrAllData[indexPath.row]
            cell.objTeams = objModel
            //cell.loadCellData(obj: objModel)
            cell.lblTeamName.text = objModel.name
            let strUrl = objModel.logo_path
            if let url = URL(string: strUrl){
                cell.imgFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgFlag.image = UIImage(named: "icon_placeholderTeam")
            }

            if objModel.is_favorite == "1"{
                cell.imgSelection.image = UIImage(named: "pin_favourite")
            }else{
                cell.imgSelection.image = UIImage(named: "pin_unfavourite")
            }

            cell.actionSelectHandler = { [weak self] (objTeam) in
                guard self != nil else {return}
                guard let team = objTeam else {return}
                if team.is_favorite == "0"{
                    team.is_favorite = "1"
                    self?.call_Webservice_Post_Favorite(request_type: "1", request_id: team.team_id, index: indexPath.row)
                    
                }else{

                    team.is_favorite = "0"
                    self?.call_Webservice_Post_Favorite(request_type: "0", request_id: team.team_id, index: indexPath.row)
                }
                let indexPosition = IndexPath(row: indexPath.row, section: 0)
                self?.tblFavorite.reloadRows(at: [indexPosition], with: .none)
//                weakSelf.tblFavorite.reloadData()
            }
            return cell
            //}
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (arrAllData.count - 1) && shouldLoadMore == true {
            let dictPram = ["search_term":self.txtSearch.text!,
                            "limit":String(self.Limit),
                            "offset":self.offSet] as [String: AnyObject]
            //suresh
            if self.offSet > 0{
                call_Webservice_Get_Popular(dict_param: dictPram)
            }
            //suresh
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    //    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
    //
    //    }
    //suresh
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 10.0 {
            let myInt1 = Int(strTotalRecords)
            if self.arrAllData.count < myInt1!{
                //suresh
                if self.arrAllData.count > 19{
                    self.shouldLoadMore = true
                    self.offSet += self.Limit
                }else{
                    self.shouldLoadMore = false
                }
            }
            else{
                self.shouldLoadMore = false
            }
            self.tblFavorite.reloadData()
        }
    }
}

