//
//  TeamSkeltonViewCell.swift
//  WolfScore
//
//  Created by Mindiii on 7/19/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView

class TeamSkeltonViewCell: UITableViewCell {
    
    @IBOutlet var lblTeamName:UILabel!
    @IBOutlet var imgTeam:UIImageView!
    @IBOutlet var imgPlus:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblTeamName].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
        [imgPlus,imgTeam].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
        
    }
    
    func hide_skelton()
    {
        [lblTeamName].forEach { $0?.hideSkeleton()
        }
        [imgPlus,imgTeam].forEach { $0?.hideSkeleton()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

