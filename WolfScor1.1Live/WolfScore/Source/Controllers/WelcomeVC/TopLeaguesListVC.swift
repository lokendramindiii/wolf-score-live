//
//  LeaguesVC.swift
//  WolfScore
//
//  Created by Mindiii on 28/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster
import AlamofireImage
import SkeletonView
struct TopLeaguesVc {
    static var arrSelectedLeague = [String]()
}
class TopLeaguesListVC: UIViewController {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var viewPopuler: UIView!
    @IBOutlet weak var viewRestOfThe: UIView!
    @IBOutlet weak var lblSelectedTeam: UILabel!
    @IBOutlet weak var lblPolulerleague: UILabel!
    @IBOutlet weak var lblRestOfthWold: UILabel!

    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewDot1: UIView!
    @IBOutlet weak var viewDot2: UIView!
    @IBOutlet weak var viewDot3: UIView!
    @IBOutlet weak var viewDot4: UIView!
    @IBOutlet weak var viewDot5: UIView!
    @IBOutlet weak var stackViewDots: UIStackView!
    
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var tblPopulerLeague: UITableView!
    @IBOutlet weak var tblCountryLeague: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var tblPopulerHightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblCountryHightConstraint: NSLayoutConstraint!
    var isFromMyFav = false
    //   var arrFevoriteLeagues = [ModelLeagueList]()
    var arrPopulerLeague = [ModelLeagueListGroupByCountry]()
    var arrCountryLeague = [ModelLeagueListGroupByCountry]()
    
    var arrFilterPopulerLeague = [ModelLeagueListGroupByCountry]()
    var arrFilterCountryLeague = [ModelLeagueListGroupByCountry]()
    
    var isSearching = false
    var currentHight = 0.0
    
    var isLoadData = false
    var placeholderRow = 10

    var arrPopularlocal = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.show_skelton()
        arrPopularlocal = ["England","Spain","Germany","Italy","France","United State Of America","World","Europe","South America","Asia","North America","Oceania"]
        
        if arrPopularlocal.contains(kCurrent_Country){
            let index = arrPopularlocal.index(of: kCurrent_Country)
            if index != nil {
                arrPopularlocal.remove(at: index!)
            }
            arrPopularlocal.insert(kCurrent_Country, at: 0)
        }else{
            arrPopularlocal.insert(kCurrent_Country, at: 0)
        }
        
        self.tblCountryLeague.rowHeight = UITableViewAutomaticDimension
        self.txtSearch.delegate = self
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Search leagues", tableName: nil, comment: ""), attributes: [NSAttributedStringKey.foregroundColor:UIColor.white
            ])
        
        self.modifyClearButtonWithImage(image: UIImage(named: "icon_clearTxtfild")!, txtfiled: txtSearch)

        self.call_Webservice_Get_LeaugeList()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForDetailCell), name: NSNotification.Name(rawValue: KNotifiationFavouriteLeague), object: nil)
        
        activity.isHidden = true
        activity.stopAnimating()
        
        self.configureView()
    }
   
    func show_skelton()
    {
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblRestOfthWold,lblPolulerleague].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
    }

    func hide_skelton()
    {
        [lblRestOfthWold,lblPolulerleague].forEach { $0?.hideSkeleton()
        }
    }
    
    func modifyClearButtonWithImage(image : UIImage ,txtfiled : UITextField) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(sender:)), for: .touchUpInside)
        txtfiled.rightView = clearButton
        txtfiled.rightViewMode = .whileEditing
    }
    
    @objc func clear(sender : AnyObject) {
        
        self.arrFilterPopulerLeague = self.arrPopulerLeague
        self.arrFilterCountryLeague = self.arrCountryLeague
        self.tblPopulerLeague.reloadData()
        self.tblCountryLeague.reloadData()
        activity.isHidden = true
        activity.stopAnimating()

        self.txtSearch.text = ""
        self.txtSearch.sendActions(for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        objAppShareData.isLonchXLPageIndex = true
        self.btnNext.isUserInteractionEnabled = true
        if self.isFromMyFav == true {
            self.btnSkip.isHidden = true
            self.stackViewDots.isHidden = true
            self.btnNext.setTitle("DONE", for: .normal)
        }else{
            self.btnSkip.isHidden = false
            self.stackViewDots.isHidden = false
            self.btnNext.setTitle("NEXT", for: .normal)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func flag(country: String) -> String {
        let base: UInt32 = 127397
        return String(String.UnicodeScalarView(
            country.unicodeScalars.compactMap({ UnicodeScalar(base + $0.value) })
        ))
    }
    
    func locale(for fullCountryName : String) -> String {
        let locales : String = ""
        for localeCode in NSLocale.isoCountryCodes {
            let identifier = NSLocale(localeIdentifier: localeCode)
            let countryName = identifier.displayName(forKey: NSLocale.Key.countryCode, value: localeCode)
            if fullCountryName.lowercased() == countryName?.lowercased() {
                return localeCode
            }
        }
        return locales
    }
    func configureView(){
        self.viewDot1.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot2.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot3.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot4.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot5.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
    }
}


// MARK: - UITextField Delegates
extension TopLeaguesListVC:UITextFieldDelegate,UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        // self.view.endEditing(true)
        self.isSearching = false
        self.tblCountryHightConstraint.constant = self.tblCountryLeague.contentSize.height
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        self.txtSearch.resignFirstResponder()
        self.isSearching = false
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        // NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.loadDataWithPageCount), object: nil)
        //  self.arrFilterFevoriteLeagues = self.arrFevoriteLeagues
        self.arrFilterPopulerLeague = self.arrPopulerLeague
        self.arrFilterCountryLeague = self.arrCountryLeague
        self.tblPopulerLeague.reloadData()
        self.tblCountryLeague.reloadData()
        activity.isHidden = true
        activity.stopAnimating()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if isLoadData == false
        {
            return  false
        }

        let text = textField.text! as NSString
        if (text.length == 1)  && (string == "") {
            // NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.loadDataWithPageCount), object: nil)
            
            //  self.arrFilterFevoriteLeagues = self.arrFevoriteLeagues
            self.arrFilterPopulerLeague = self.arrPopulerLeague
            self.arrFilterCountryLeague = self.arrCountryLeague
           
            self.tblPopulerLeague.reloadData()
            self.tblCountryLeague.reloadData()
            activity.isHidden = true
            activity.stopAnimating()
            return true
        }
        
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if !(substring == "") {
            activity.isHidden = false
            activity.startAnimating()
        }
        searchAutocompleteEntries(withSubstring: substring)
        return true
    }
    
    func searchAutocompleteEntries(withSubstring substring: String) {
        if !(substring == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.loadDataWithPageCount), object: nil)
            self.perform(#selector(self.loadDataWithPageCount), with: nil, afterDelay: 0.4)
            //self.loadDataWithPageCount(page: 0, strSearchText: substring)
        }
    }
    
    @objc func loadDataWithPageCount() {
        //        activity.isHidden = false
        //        activity.startAnimating()
        self.isSearching = true
        //         self.arrFilterFevoriteLeagues = self.arrFevoriteLeagues.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
        //         self.arrFilterPopulerLeague = self.arrPopulerLeague.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
        
        self.arrFilterCountryLeague.removeAll()
        self.arrFilterPopulerLeague.removeAll()
        self.arrFilterCountryLeague = self.arrCountryLeague.filter { $0.countryName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
        self.arrFilterPopulerLeague = self.arrPopulerLeague.filter { $0.countryName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
        
      //-------------Populer country search by league -start --------
        
        var  arrTemp1 = [ModelLeagueListGroupByCountry]()
        for objCountry in self.arrPopulerLeague {
            
            if self.arrFilterPopulerLeague.count == 0 {
                
                let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                objmodel.countryName = objCountry.countryName
                objmodel.countryId = objCountry.countryId
                objmodel.countryFlage = objCountry.countryFlage
                objmodel.isExpandation = true
                if objmodel.arrLeagueList.count > 0{
                    //self.arrFilterCountryLeague.append(objmodel)
                    arrTemp1.append(objmodel)
                }
                
            }else{
                
                for objFilterCountry in self.arrFilterPopulerLeague{
                    if objCountry.countryId != objFilterCountry.countryId {
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                        objmodel.countryName = objCountry.countryName
                        objmodel.countryId = objCountry.countryId
                        objmodel.countryFlage = objCountry.countryFlage
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            //self.arrFilterCountryLeague.append(objmodel)
                            arrTemp1.append(objmodel)
                        }
                        break
                    }else{
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.countryName = objFilterCountry.countryName
                        objmodel.countryId = objFilterCountry.countryId
                        objmodel.countryFlage = objFilterCountry.countryFlage
                        objmodel.arrLeagueList = objFilterCountry.arrLeagueList
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            arrTemp1.append(objmodel)
                        }
                        break
                    }
                }
                
            }
        }
        self.arrFilterPopulerLeague.removeAll()
        self.arrFilterPopulerLeague = arrTemp1

        // -------- Country league filter by search - Start--------------
        var  arrTemp = [ModelLeagueListGroupByCountry]()
        for objCountry in self.arrCountryLeague {
            
            if self.arrFilterCountryLeague.count == 0 {
                
                let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                objmodel.countryName = objCountry.countryName
                objmodel.countryId = objCountry.countryId
                objmodel.countryFlage = objCountry.countryFlage
                objmodel.isExpandation = true
                if objmodel.arrLeagueList.count > 0{
                    //self.arrFilterCountryLeague.append(objmodel)
                    arrTemp.append(objmodel)
                }
                
            }else{
                
                for objFilterCountry in self.arrFilterCountryLeague{
                    if objCountry.countryId != objFilterCountry.countryId {
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.arrLeagueList = objCountry.arrLeagueList.filter { $0.leagueName.localizedCaseInsensitiveContains(self.txtSearch.text!) }
                        objmodel.countryName = objCountry.countryName
                        objmodel.countryId = objCountry.countryId
                        objmodel.countryFlage = objCountry.countryFlage
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            //self.arrFilterCountryLeague.append(objmodel)
                            arrTemp.append(objmodel)
                        }
                        break
                    }else{
                        let objmodel = ModelLeagueListGroupByCountry(fromDictionary: [:])
                        objmodel.countryName = objFilterCountry.countryName
                        objmodel.countryId = objFilterCountry.countryId
                        objmodel.countryFlage = objFilterCountry.countryFlage
                        objmodel.arrLeagueList = objFilterCountry.arrLeagueList
                        objmodel.isExpandation = true
                        if objmodel.arrLeagueList.count > 0{
                            arrTemp.append(objmodel)
                        }
                        break
                    }
                }
                
            }
        }
        self.arrFilterCountryLeague.removeAll()
        self.arrFilterCountryLeague = arrTemp
        
        if self.txtSearch.text! == ""{
            self.arrFilterPopulerLeague = self.arrPopulerLeague
            self.arrFilterCountryLeague = self.arrCountryLeague
        }
        if  self.arrFilterCountryLeague.count == 0{
            self.isSearching = false
        }
        if  self.arrFilterPopulerLeague.count == 0 && self.arrFilterCountryLeague.count == 0 {
            self.lblNoRecord.isHidden = false
        }else{
            self.lblNoRecord.isHidden = true
        }
        self.tblPopulerLeague.reloadData()
        self.tblCountryLeague.reloadData()
        activity.isHidden = true
        activity.stopAnimating()
        
    }
}


// MARK: - extension IBAction
extension TopLeaguesListVC{
    func dismiss_Presented_View() {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnNextAction(_ sender : UIButton){
        
        self.view.endEditing(true)
        SVProgressHUD.show(withStatus: "Please wait")
        
        if self.isFromMyFav == true {
            self.dismiss_Presented_View()
            
        }else{
            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "NotificationSettingVC") as! NotificationSettingVC
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        
    }
    @IBAction func btnSkipAction(_ sender : UIButton){
        self.view.endEditing(true)
        objAppDelegate.goToTabBar()
    }
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        if self.isFromMyFav == true {
            dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnMenuAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.slideMenuController()?.toggleLeft()
    }
    
    
    //     @objc func LeguageDetails(notification: Notification){
    //         if let objLeague = notification.object as? ModelLeagueList{
    //            let viewController = UIStoryboard(name: "LeaguesTab",bundle: nil).instantiateViewController(withIdentifier: "LeaguesDetailsVC") as! LeaguesDetailsVC
    //            viewController.strleagueName = objLeague.leagueName
    //             objAppShareData.str_league_id = objLeague.leagueId
    //        self.navigationController?.pushViewController(viewController, animated: true)
    //        }
    //
    //    }
    
    
    @objc func reloadDataForDetailCell(notification: Notification){
        
        self.tblPopulerLeague.reloadData()
        self.tblCountryLeague.reloadData()
        
    }
    
    @objc func btnPopulerDropdownPressed(sender: UIButton){
        
        let objModel = self.arrFilterPopulerLeague[sender.tag]
        
        if objModel.isExpandation == false{
            objModel.isExpandation = true
        }else{
            objModel.isExpandation = false
        }
        self.tblPopulerLeague.reloadData()
    }
    
    @objc func btnCountryDropdownPressed(sender: UIButton){
        //let indexPath = IndexPath(row: sender.tag, section: 0)
        //let cell = self.tblCountryLeague.cellForRow(at: indexPath) as! LeagueTableViewCell
        
        let objModel = self.arrFilterCountryLeague[sender.tag]
        
        
        if objModel.isExpandation == false{
            objModel.isExpandation = true
        }else{
            objModel.isExpandation = false
        }
        
        //let indexPosition = IndexPath(row: sender.tag, section: 0)
        //self.tblCountryLeague.reloadRows(at: [indexPosition], with: .none)
        // self.tblCountryHightConstraint.constant = self.tblCountryLeague.contentSize.height
        //  self.view.layoutIfNeeded()
        
        self.tblCountryLeague.reloadData()
    }
}


// MARK: - TableView Delegates & Datasource
extension TopLeaguesListVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if isLoadData == false
        {
            return placeholderRow
        }
        
        
        if tableView == tblPopulerLeague{
            if arrFilterPopulerLeague.count > 0{
                self.viewPopuler.isHidden = false
            }else{
                self.viewPopuler.isHidden = true
            }
            //self.tblPopulerHightConstraint.constant = CGFloat((self.arrFilterPopulerLeague.count)*50)
            return self.arrFilterPopulerLeague.count
        }else{
            if arrFilterCountryLeague.count > 0{
                self.viewRestOfThe.isHidden = false
            }else{
                self.viewRestOfThe.isHidden = true
            }
            //self.tblCountryHightConstraint.constant = self.tblCountryLeague.contentSize.height
            return self.arrFilterCountryLeague.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadData == false
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCellNew", for: indexPath) as? LeagueTableViewCellNew{
                cell.show_skelton()
                
                DispatchQueue.main.async(execute: {() -> Void in
                    self.tblPopulerHightConstraint.constant = self.tblPopulerLeague.contentSize.height
                })
                return cell
            }
        }
        
        if tableView == tblPopulerLeague {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCellNew", for: indexPath) as? LeagueTableViewCellNew{
                
                let objModel = self.arrFilterPopulerLeague[indexPath.row]
                cell.lblLeagueName.text = objModel.countryName
                cell.hide_skelton()
                if let url = URL(string: objModel.countryFlage){
                    cell.imgLeague.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgLeague.image =  UIImage(named: "icon_placeholderTeam")
                }
                
                
                if objModel.isExpandation == true
                {
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi)
                    cell.arrPopulerLeagegs = objModel.arrLeagueList
                    cell.tblPopulerLeagues.reloadData()
                }else{
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi*2)
                    cell.arrPopulerLeagegs.removeAll()
                    cell.tblPopulerLeagues.reloadData()
                }
                
                cell.btnAddFavorite.tag = indexPath.row
                cell.btnAddFavorite.addTarget(self, action: #selector(btnPopulerDropdownPressed(sender:)), for: .touchUpInside)
                
                
                DispatchQueue.main.async(execute: {() -> Void in
                    self.tblPopulerHightConstraint.constant = self.tblPopulerLeague.contentSize.height
                })
                return cell
                
            }
            else
            {
                return UITableViewCell()
            }
        }
        else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCellNew", for: indexPath) as? LeagueTableViewCellNew{
                let objModel = self.arrFilterCountryLeague[indexPath.row]
                cell.lblLeagueName.text = objModel.countryName
                cell.hide_skelton()
                if let url = URL(string: objModel.countryFlage){
                    cell.imgLeague.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgLeague.image = UIImage(named: "icon_placeholderTeam")
                }
                
                if objModel.isExpandation == true{
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi)
                    cell.arrCountryLeagegs = objModel.arrLeagueList
                    cell.tblCountryLeagues.reloadData()
                }else{
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi*2)
                    cell.arrCountryLeagegs.removeAll()
                    cell.tblCountryLeagues.reloadData()
                }
                
                
                cell.btnAddFavorite.tag = indexPath.row
                cell.btnAddFavorite.addTarget(self, action: #selector(btnCountryDropdownPressed(sender:)), for: .touchUpInside)
                
                DispatchQueue.main.async(execute: {() -> Void in
                    self.tblCountryHightConstraint.constant = self.tblCountryLeague.contentSize.height
                })
                return cell
            }
            else{
                return UITableViewCell()
            }
        }
    }
}

// MARK: - Webservice
extension TopLeaguesListVC{
    
    
    func call_Webservice_Post_Favorite(request_type:String,request_id:String, index:Int,tableName:String){
        
        self.view.isUserInteractionEnabled = false
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"league"] as [String:Any]
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if tableName == "Fevorite"{
                    
                    //let indexPosition = IndexPath(row: index, section: 0)
                    self.tblPopulerLeague.reloadData()
                    self.tblCountryLeague.reloadData()
                    
                }else if tableName == "Populer"{
                    let objTag = self.arrFilterPopulerLeague[index]
                    
                    
                    // unFevoreite from populer
                    //  objTag.isFavorite = "0"
                    let indexPosition = IndexPath(row: index, section: 0)
                    self.tblPopulerLeague.reloadRows(at: [indexPosition], with: .none)
                    
                    self.tblCountryLeague.reloadData()
                }
                self.view.isUserInteractionEnabled = true
            }else{
                self.view.isUserInteractionEnabled = true
                if message == "Cannot select more than 25 leagues"{
                    if tableName == "Fevorite"{
                        //  let objTag = self.arrFilterFevoriteLeagues[index]
                        //  objTag.isFavorite = "0"
                        let window = UIApplication.shared.delegate?.window
                        let visibleVC = window??.visibleViewController
                        if  visibleVC?.title == "Alert"{
                            return
                        }
                    }else if tableName == "Populer"{
                        let objTag = self.arrFilterPopulerLeague[index]
                        // objTag.isFavorite = "0"
                        self.tblPopulerLeague.reloadData()
                        let window = UIApplication.shared.delegate?.window
                        let visibleVC = window??.visibleViewController
                        if  visibleVC?.title == "Alert"{
                            return
                        }
                    }
                    objAlert.showAlert(message: message, title: "Alert", controller: self)
                }
            }
            
        }) { (error) in
            print(error)
            self.view.isUserInteractionEnabled = true
            SVProgressHUD.dismiss()
            
        }
    }
    
    func call_Webservice_Get_LeaugeList() {
      //  SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.get_country_league_list, params: nil, success: { (response) in
            SVProgressHUD.dismiss()
            self.isLoadData = true
            self.hide_skelton()
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    //country_list
                    if let country_list = data["country_list"] as? [[String: Any]]{
                        
                        // Popular
                        var arr_poplulerleague = [ModelLeagueListGroupByCountry]()
                        
                        for dict in country_list{
                            let obj = ModelLeagueListGroupByCountry.init(fromDictionary: dict)
                            if  self.arrPopularlocal.contains(where: { $0 == obj.countryName }) {
                                if obj.arrLeagueList.count > 0{
                                    arr_poplulerleague.append(obj)
                                }
                            }
                        }
                        
                        for i in 0..<self.arrPopularlocal.count
                        {
                            let filteredArray = arr_poplulerleague.filter(){ $0.countryName.contains(self.arrPopularlocal[i]) }
                            for objNEW in filteredArray{
                                self.arrPopulerLeague.append(objNEW)
                                self.arrFilterPopulerLeague.append(objNEW)
                            }
                        }
                        
                        self.tblPopulerLeague.reloadData()
                        TopLeaguesVc.arrSelectedLeague.removeAll()
                        
                        for dict in country_list{
                            let obj = ModelLeagueListGroupByCountry.init(fromDictionary: dict)
                            if obj.arrLeagueList.count > 0{
                                
                                for i in 0..<obj.arrLeagueList.count
                                {
                                    if obj.arrLeagueList[i].isFavorite == "1"
                                    {
                                        TopLeaguesVc.arrSelectedLeague.append(obj.arrLeagueList[i].leagueId)
                                    }
                                }
                                self.arrCountryLeague.append(obj)
                                self.arrFilterCountryLeague.append(obj)
                            }
                            
                        }
                        self.tblCountryLeague.reloadData()
                    }
                    if self.arrPopulerLeague.count == 0 && self.arrCountryLeague.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                    
                }
            }else{
                if  self.arrPopulerLeague.count == 0 && self.arrCountryLeague.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
                objAlert.showAlert(message: message, title: "Alert", controller: self)
            }
        }) { (error) in
            print(error)
            self.isLoadData = true
            self.hide_skelton()
            SVProgressHUD.dismiss()
        }
    }
}


