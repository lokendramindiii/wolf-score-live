//
//  LocalTeamListVC.swift
//  WolfScore
//
//  Created by Mindiii on 20/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster
import Alamofire
import SJSegmentedScrollView
import AlamofireImage

struct LocalTeamList {
   static var arrSelectedTeam = [String]()
}

class LocalTeamListVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tblFavorite: UITableView!
    @IBOutlet weak var lblSelectedTeam: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewBack: UIView!

    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var viewDot1: UIView!
    @IBOutlet weak var viewDot2: UIView!
    @IBOutlet weak var viewDot3: UIView!
    @IBOutlet weak var viewDot4: UIView!
    @IBOutlet weak var viewDot5: UIView!
    
    @IBOutlet weak var scrollViewMain: UIScrollView!
    @IBOutlet weak var tblHightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblFavBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblGroupBottomConstraint: NSLayoutConstraint!

    @IBOutlet weak var tblGroupByTeam: UITableView!
    @IBOutlet weak var tblHightGroupByConstraint: NSLayoutConstraint!
    
    var arrAllData = [ModelTeam]()
    var arrGroupByTeam = [ModelLeagueListGroupByTeam]()
    
    var offSet = 0
    var Limit = 20
    var str_Selected_Team_Id = ""
    var strTotalRecords = "0"
    var searchActive = false
    var isLoginUser = false
    
    var isLoadData = false
    var placeholderRow = 10

    
    @IBOutlet weak var txtSearch: UITextField!
    
    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tblFavorite.tableFooterView = UIView()
            } else {
                self.tblFavorite.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.tblGroupByTeam.register(UINib(nibName: "TeamSkeltonViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TeamSkeltonViewCell")

        self.tblGroupByTeam.rowHeight = UITableViewAutomaticDimension
        self.scrollViewMain.contentOffset.y = 0
        self.lblNoData.isHidden = true
        self.btnNext.isUserInteractionEnabled = true
        self.txtSearch.text = ""
        if kCurrent_Country == ""{
            objAppDelegate.get_Current_Country()
        }
        
        SVProgressHUD.setDefaultMaskType(.clear)
        
        if isLoginUser == true
        {
          self.viewBack.isHidden = true
        }
        
        let userId = UserDefaults.standard.string(forKey: UserDefaults.Keys.kUserId)
        if userId == nil{
            self.call_Webservice()
        }
        else
        {
            let dictPram = ["search_term":"",
                            "country":kCurrent_Country,
                            "limit":String(self.Limit),
                            "offset":self.offSet
                             ] as [String: AnyObject]

            DispatchQueue.main.async(execute: {() -> Void in
                self.call_Webservice_For_GetCountryName()
            self.call_Webservice_Get_LocalTeam(dict_param: dictPram)
            })
        }
        
        self.lblSelectedTeam.isHidden = true
        self.btnSkip.isHidden = false
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnBg))
        
        self.view.addGestureRecognizer(tap)
        
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Search", tableName: nil, comment: ""), attributes: [NSAttributedStringKey.foregroundColor:UIColor.white
            ])
        
        self.modifyClearButtonWithImage(image: UIImage(named: "icon_clearTxtfild")!, txtfiled: txtSearch)
        
        
        self.tblFavorite.tableFooterView = UIView()
        
        self.configureView()
    }
    
    func modifyClearButtonWithImage(image : UIImage ,txtfiled : UITextField) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(sender:)), for: .touchUpInside)
        txtfiled.rightView = clearButton
        txtfiled.rightViewMode = .whileEditing
    }
    
    @objc func clear(sender : AnyObject) {
        
     //   self.tblGroupByTeam.reloadData()
        self.view.endEditing(true)
        self.txtSearch.text = ""
        self.loadDataWithPageCount(page: 0, strSearchText: "")
        self.txtSearch.sendActions(for: .editingChanged)
    }
    
    
    
    func configureView() {
        self.viewDot1.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot2.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot3.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot4.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot5.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
    }
    
    @objc func btnDropdownPressed(sender: UIButton){
       
        let objModel = self.arrGroupByTeam[sender.tag]
        if objModel.isExpandation == false{
            objModel.isExpandation = true
        }else{
            objModel.isExpandation = false
        }
        self.tblGroupByTeam.reloadData()
    }
    
    
    @objc func tapOnBg() {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.endEditing(true)
        for i in 0..<12
        {
            let cell = self.tblGroupByTeam.dequeueReusableCell(withIdentifier: "TeamSkeltonViewCell")as! TeamSkeltonViewCell
            cell.hide_skelton()
        }
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

// MARK: - UITextField Delegates

extension LocalTeamListVC{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.searchActive = true
        return true;
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.searchActive = false
        return true;
    }
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        txtSearch.resignFirstResponder()
        //searchActive = false
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
         self.tblGroupByTeam.reloadData()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if isLoadData == false
        {
            return  false
        }

        self.scrollViewMain.contentOffset.y = 0.0
        self.scrollViewMain.contentSize.height = 0.0
        let text = textField.text! as NSString
        if (text.length == 1)  && (string == "") {
            self.tblGroupByTeam.reloadData()
            self.loadDataWithPageCount(page: 0, strSearchText: "")
        }
        
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        searchAutocompleteEntries(withSubstring: substring)
        return true
    }
    
    func searchAutocompleteEntries(withSubstring substring: String) {
        if !(substring == "") {
            //self.tblGroupByTeam.reloadData()
            self.loadDataWithPageCount(page: 0, strSearchText: substring)
        }
    }


    func loadDataWithPageCount(page: Int, strSearchText : String = "") {

        self.offSet = 0
        let strName = strSearchText
        let dicParam = ["search_term":strName,
                        "country": kCurrent_Country,
                        "limit":String(self.Limit),
                        "offset":self.offSet
            ] as [String : AnyObject]
        self.call_Webservice_Get_LocalTeam(dict_param: dicParam)
    }
}
// MARK: - IBActions

extension LocalTeamListVC{
    
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
    self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func btnNextAction(_ sender : UIButton){
        self.view.endEditing(true)
     //   SVProgressHUD.show(withStatus: "Please wait")
        
        self.btnNext.isUserInteractionEnabled = true
        let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "PopularTeamListVC") as! PopularTeamListVC
        viewController.isFromMyFav = false
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnSkipAction(_ sender : UIButton){
        self.view.endEditing(true)
         self.call_Webservice_add_popular_league_favorite_league()
    }
}
public extension Array where Element: Hashable {
    func uniqued() -> [Element] {
        var seen = Set<Element>()
        return filter{ seen.insert($0).inserted }
    }
}
// MARK: - Webservice

extension LocalTeamListVC{
    
    func call_Webservice_add_popular_league_favorite_league() {
        self.view.isUserInteractionEnabled = false
        
        let paramsDict = ["country":kCurrent_Country] as [String:AnyObject]

        objWebServiceManager.requestGet(strURL: webUrl.add_popular_league_favorite_league, params: paramsDict, success: { (response) in
            // print(response)
            SVProgressHUD.dismiss()
            self.view.isUserInteractionEnabled = true
            _ = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                objAppDelegate.goToTabBar()
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func call_Webservice_Get_LocalTeam(dict_param:[String:AnyObject]) {
        
        if self.searchActive == false {
         //  SVProgressHUD.show(withStatus: "Please wait")
        }
        
        //SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.get_country_league_team, params: dict_param, success: { (response) in
            print("dict_param === \(dict_param)")
            
           self.isLoadData = true
            SVProgressHUD.dismiss()
            _ = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                self.shouldLoadMore = false
                if (self.txtSearch.text?.isEmpty)! {
                    self.tblFavorite.isHidden = true
                    self.tblGroupByTeam.isHidden = false
                    if let data = response["data"] as? [String:Any] {

                        if let fav_team = data["fav_team"] as? [[String: Any]]{
                            LocalTeamList.arrSelectedTeam.removeAll()
                            for dict in fav_team{
                                let team_id = dict["team_id"] as? String ?? ""
                                LocalTeamList.arrSelectedTeam.append(team_id)
                            }
                        }
                        
                        if let league_list = data["league_list"] as? [[String: Any]]{
                            
                            self.arrGroupByTeam.removeAll()
                            for dict in league_list{
                                let obj = ModelLeagueListGroupByTeam.init(fromDictionary: dict)
                                obj.isExpandation = true
                                self.arrGroupByTeam.append(obj)
                            }
                        }
                        if self.arrGroupByTeam.count == 0{
                            self.tblGroupByTeam.isHidden = true
                            self.lblNoData.isHidden = false
                        }else{
                            self.tblGroupByTeam.isHidden = false
                            self.lblNoData.isHidden = true
                        }
                        self.tblGroupByTeam.hideLoader()
                        self.tblGroupByTeam.reloadData()
                    }else{
                        if self.arrGroupByTeam.count == 0{
                            self.tblGroupByTeam.isHidden = true
                            self.lblNoData.isHidden = false
                        }else{
                            self.tblGroupByTeam.isHidden = false
                            self.lblNoData.isHidden = true
                        }
                    }
                }else{
                    //suresh
                    if let data = response["data"] as? [String:Any] {
                        if let arrData = data["data"] as? [[String: Any]]{
                            var arrAllDataLocal = [ModelTeam]()
                            self.tblFavorite.isHidden = false
                            self.tblGroupByTeam.isHidden = true
                            if self.offSet == 0{
                                self.arrAllData.removeAll()
                            }
                            
                            //suresh
                            for dict in arrData{
                                let obj = ModelTeam.init(dict: dict)
                                arrAllDataLocal.append(obj)
                            }
                            
                            if self.offSet == 0{
                                self.arrAllData = arrAllDataLocal
                            }
                            else{
                                self.arrAllData += arrAllDataLocal
                            }
                            
                            if let strTotal = data["total_records"] as? String {
                                self.strTotalRecords = strTotal
                                print(strTotal)
                            }
                            
                            if self.arrAllData.count == 0{
                                self.tblFavorite.isHidden = true
                                self.lblNoData.isHidden = false
                            }else{
                                self.tblFavorite.isHidden = false
                                self.lblNoData.isHidden = true
                            }
                            self.tblFavorite.hideLoader()
                            self.tblFavorite.reloadData()
                            
                        }
                    }
                }
            }
        }) { (error) in
            print(error)
            self.isLoadData = true
            self.tblFavorite.hideLoader()
            SVProgressHUD.dismiss()
        }
    }
    
    func call_Webservice() {
        
      //  SVProgressHUD.show(withStatus: "Please wait")
        var dictParam = [String:Any] ()
        if kDevice_Tocken ==  ""
        {
            dictParam = ["device_token":kUnique_Device_Token,
          "device_type":"2"] as [String: AnyObject]

        }
        else
        {
            
            dictParam = ["device_token":kDevice_Tocken,
                         "device_type":"2"] as [String: AnyObject]
        }
        
        objWebServiceManager.request_Geust_User_Post(strURL: webUrl.guest_signup, params: dictParam, success: { (response) in
            print(response)
            _ = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            
            if status == "fail"
            {
                SVProgressHUD.dismiss()
            }
            
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    strAuthToken = data["auth_token"] as? String ?? ""
                    UserDefaults.standard.setValue(data["auth_token"] as? String ?? "", forKey: UserDefaults.Keys.kAuthToken)
                    UserDefaults.standard.setValue(data["userId"] as? String ?? "", forKey: UserDefaults.Keys.kGuestUserId)
                    
                    UserDefaults.standard.setValue("GuestUser", forKey: UserDefaults.Keys.kGuestLogin)
                    
                    UserDefaults.standard.synchronize()
                    let dictPram = ["search_term":"",
                                    "country":kCurrent_Country,
                                    "limit":String(self.Limit),
                                    "offset":self.offSet] as [String: AnyObject]
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.call_Webservice_For_GetCountryName()
                        self.call_Webservice_Get_LocalTeam(dict_param: dictPram)
                    })
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    
    func call_Webservice_For_GetCountryName() {
        
        objWebServiceManager.requestGet(strURL: webUrl.get_Country_Name, params: nil, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    let arrCountry = data["country_list"] as? [[String:Any]] ?? []
                    UserDefaults.standard.setValue(arrCountry, forKey: UserDefaults.Keys.kCountryArr)
                    UserDefaults.standard.synchronize()
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
    }
    
    // Add remove favrouite api
    func call_Webservice_Post_Favorite(request_type:String,request_id:String, index:Int){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"team"] as [String:Any]
        print(paramDict)
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
            }else{
                if message == "Cannot select more than 100 teams"{
                    let objTag = self.arrAllData[index]
                    objTag.is_favorite = "0"
                    self.tblFavorite.reloadData()
                    let window = UIApplication.shared.delegate?.window
                    let visibleVC = window??.visibleViewController
                    
                    if  visibleVC?.title == "Alert"{
                        return
                    }
                    objAlert.showAlert(message: message, title: "Alert", controller: self)
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
    }
    
}
// MARK: - TableView Delegates & Datasource

extension LocalTeamListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isLoadData == false
        {
            self.tblHightConstraint.constant = CGFloat((placeholderRow)*55)
            self.scrollViewMain.contentSize.height = self.tblHightConstraint.constant + 170
            return placeholderRow
        }
        
        if tableView == tblFavorite{
            self.tblHightConstraint.constant = CGFloat((self.arrAllData.count)*55)
            self.scrollViewMain.contentSize.height = self.tblHightConstraint.constant + 170
          //  self.scrollViewMain.contentSize.height = self.tblHightGroupByConstraint.constant + 170
            return arrAllData.count
        }else{
            return arrGroupByTeam.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if isLoadData == false
        {
            let cell = tblGroupByTeam.dequeueReusableCell(withIdentifier: "TeamSkeltonViewCell", for: indexPath) as! TeamSkeltonViewCell
            cell.show_skelton()
            return cell
        }
        
        if tableView == tblFavorite{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "TeamTableViewCell", for: indexPath) as? TeamTableViewCell{
                
                
                let objModel = self.arrAllData[indexPath.row]
                cell.objTeams = objModel
                cell.lblTeamName.text = objModel.name
                let strUrl = objModel.logo_path
                if let url = URL(string: strUrl){
                    cell.imgFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }else{
                    cell.imgFlag.image = UIImage(named: "icon_placeholderTeam")
                }
                
                if objModel.is_favorite == "1"
                {
                    cell.imgSelection.image = UIImage(named: "pin_favourite")
                }
                else
                {
                    cell.imgSelection.image = UIImage(named: "pin_unfavourite")
                }
                
                
                cell.actionSelectHandler = { [weak self] (objTeam) in
                    guard self != nil else {return}
                    guard let team = objTeam else {return}
                    if team.is_favorite == "0"{
                        team.is_favorite = "1"
                        if LocalTeamList.arrSelectedTeam.count < 100 {
                           LocalTeamList.arrSelectedTeam.append(team.team_id)
                        }
                        self?.call_Webservice_Post_Favorite(request_type: "1", request_id: team.team_id, index: indexPath.row)
                        
                    }else{
                        team.is_favorite = "0"
                        if LocalTeamList.arrSelectedTeam.contains(team.team_id)
                        {
                            let Indexobj = LocalTeamList.arrSelectedTeam.index(of: team.team_id)
                            LocalTeamList.arrSelectedTeam.remove(at: Indexobj!)
                        }
                        self?.call_Webservice_Post_Favorite(request_type: "0", request_id: team.team_id, index: indexPath.row)
                    }
                    let indexPosition = IndexPath(row: indexPath.row, section: 0)
                    self?.tblFavorite.reloadRows(at: [indexPosition], with: .none)
                    // weakSelf.tblFavorite.reloadData()
                }
                
                return cell
            }
            return UITableViewCell()
        }else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "TeamGroupByLeagueCell", for: indexPath) as? TeamGroupByLeagueCell{
                
                let objModel = self.arrGroupByTeam[indexPath.row]
                cell.lblName.text = objModel.leagueName
                let strUrl = objModel.leagueFlage
                if let url = URL(string: strUrl){
                    cell.imgLeague.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }else{
                    cell.imgLeague.image = UIImage(named: "icon_placeholderTeam")
                }
                
                if objModel.isExpandation == true{
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi)
                    cell.arrTeam = objModel.arrTeamList
                    cell.tblTeam.reloadData()
                }else
                {
                    
                    cell.imgSelection.transform = CGAffineTransform(rotationAngle: .pi*2)
                    cell.arrTeam.removeAll()
                    cell.tblTeam.reloadData()
                }
                
                cell.btnDropDown.tag = indexPath.row
                cell.btnDropDown.addTarget(self, action: #selector(btnDropdownPressed(sender:)), for: .touchUpInside)
                
                DispatchQueue.main.async(execute: {() -> Void in
                    self.tblHightGroupByConstraint.constant = self.tblGroupByTeam.contentSize.height
                    self.scrollViewMain.contentSize.height = self.tblHightGroupByConstraint.constant + 170
                })
                
                return cell
            }
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (arrAllData.count - 1) && shouldLoadMore == true {
           
            let dictPram = ["search_term":self.txtSearch.text!,
                            "country":kCurrent_Country,
                            "limit":String(self.Limit),
                            "offset":self.offSet] as [String: AnyObject]
            
            if self.offSet > 0{
                self.call_Webservice_Get_LocalTeam(dict_param: dictPram)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblFavorite || isLoadData == false{
            return 55
        }else{
            return UITableViewAutomaticDimension
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    //suresh
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if tblGroupByTeam.isHidden == false{
         return
        }

        if maximumOffset - currentOffset <= 10.0 {
            let myInt1 = Int(strTotalRecords)
            if self.arrAllData.count < myInt1!{
                if self.arrAllData.count > 19{
                    self.shouldLoadMore = true
                    self.offSet += self.Limit
                }else{
                    self.shouldLoadMore = false
                }
            }
            else{
                self.shouldLoadMore = false
            }
            self.tblFavorite.reloadData()
        }
    }
}

