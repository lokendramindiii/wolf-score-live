//
//  TopLeaguesListVC.swift
//  WolfScore
//
//  Created by mac on 08/03/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster
import AlamofireImage

class OldTopLeagueVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var tblPopulerLeague: UITableView!
    @IBOutlet weak var tblLeague: UITableView!
    @IBOutlet weak var viewPopulerLeagues: UIView!
    @IBOutlet weak var viewRestOfLeagues: UIView!
    
    @IBOutlet weak var lblSelectedTeam: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewDot1: UIView!
    @IBOutlet weak var viewDot2: UIView!
    @IBOutlet weak var viewDot3: UIView!
    @IBOutlet weak var viewDot4: UIView!
    @IBOutlet weak var viewDot5: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var scrollViewMain: UIScrollView!
    
    @IBOutlet weak var tblHightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblHightPopulerLeagueConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var stackViewDots: UIStackView!
    var arrAllLeagues = [ModelLeagueList]()
    //var arrFilterLeagues = [ModelLeagueList]()
    var arrAllPopulerLeagues = [ModelLeagueList]()
    //var arrPopulerFilterLeagues = [ModelLeagueList]()
    
    var strTotalRecords = "0"
    var offSet = 0
    var Limit = 20
    var isFromMyFav = false
    var searchActive = false
    
    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tblLeague.tableFooterView = UIView()
            } else {
                self.tblLeague.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnBg))
        self.view.addGestureRecognizer(tap)
        SVProgressHUD.setDefaultMaskType(.clear)
        //self.viewHeaderBasicMoreInfo.isHidden = true
        //self.tblFavorite.isScrollEnabled = false
        self.scrollViewMain.isScrollEnabled = true
        self.configureView()
//        let dictPram = ["search_term":"",
//                        "limit":Limit,
//                        "offset":offSet] as [String: AnyObject]
//        self.call_Webservice_Get_league_list(dict_param: dictPram)
    }
    func configureView() {
        self.txtSearch.delegate = self
        self.lblSelectedTeam.isHidden = true
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Search", tableName: nil, comment: ""), attributes: [NSAttributedStringKey.foregroundColor:UIColor.white
            ])
        
        self.modifyClearButtonWithImage(image: UIImage(named: "icon_clearTxtfild")!, txtfiled: txtSearch)

        self.viewDot1.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot2.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot3.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot4.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot5.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        //self.tblFavorite.tableFooterView = UIView()
    }
    
    
    func modifyClearButtonWithImage(image : UIImage ,txtfiled : UITextField) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(sender:)), for: .touchUpInside)
        txtfiled.rightView = clearButton
        txtfiled.rightViewMode = .whileEditing
    }
    
    @objc func clear(sender : AnyObject) {
        
        self.txtSearch.text = ""
        self.txtSearch.sendActions(for: .editingChanged)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.txtSearch.text = ""
        self.scrollViewMain.contentOffset.y = 0
        self.btnNext.isUserInteractionEnabled = true
        self.lblNoData.isHidden = true
        
        if self.isFromMyFav == true {
            self.btnSkip.isHidden = true
            self.stackViewDots.isHidden = true
            self.btnNext.setTitle("DONE", for: .normal)
            
        }
        else{
            self.btnSkip.isHidden = false
            self.stackViewDots.isHidden = false
            self.btnNext.setTitle("NEXT", for: .normal)
        }
        if self.arrAllLeagues.count == 0 && arrAllPopulerLeagues.count == 0{
           self.offSet = 0
           let dictPram = ["search_term":"",
                            "limit":Limit,
                            "offset":offSet] as [String: AnyObject]
           self.call_Webservice_Get_league_list(dict_param: dictPram)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func tapOnBg() {
        self.view.endEditing(true)
    }
}

// MARK: - IBActions

extension OldTopLeagueVC{
    
    
    @objc func btnPopulerLeaguePressed(sender: UIButton){
        
        let objTag = self.arrAllPopulerLeagues[sender.tag]
        if objTag.isFavorite == "0"{
             objTag.isFavorite = "1"
            self.call_Webservice_Post_FavoritePopulerLeagues(request_type: "1", request_id: objTag.leagueId, index: sender.tag)
        }else{
             objTag.isFavorite = "0"
            self.call_Webservice_Post_FavoritePopulerLeagues(request_type: "0", request_id: objTag.leagueId, index: sender.tag)
        }
        let indexPosition = IndexPath(row: sender.tag, section: 0)
        self.tblPopulerLeague.reloadRows(at: [indexPosition], with: .none)
    }
    
    @objc func btnRestLeaguePressed(sender: UIButton){
        
        let objTag = self.arrAllLeagues[sender.tag]
        if objTag.isFavorite == "0"{
            objTag.isFavorite = "1"
            self.call_Webservice_Post_FavoriteRestOfLeagues(request_type: "1", request_id: objTag.leagueId, index: sender.tag)
        }else{
            objTag.isFavorite = "0"
            self.call_Webservice_Post_FavoriteRestOfLeagues(request_type: "0", request_id: objTag.leagueId, index: sender.tag)
          
        }
        let indexPosition = IndexPath(row: sender.tag, section: 0)
        self.tblLeague.reloadRows(at: [indexPosition], with: .none)
    }
    
    
    @IBAction func btnNextAction(_ sender : UIButton){
        
        self.view.endEditing(true)
        SVProgressHUD.show(withStatus: "Please wait")
     
        if self.isFromMyFav == true {
            // dismiss(animated: true, completion: nil)
            self.dismiss_Presented_View()
            
        }else{
            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "NotificationSettingVC") as! NotificationSettingVC
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        
    }
    
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        if self.isFromMyFav == true {
            dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnSkipAction(_ sender : UIButton){
        self.view.endEditing(true)
        objAppDelegate.goToTabBar()
    }
    func dismiss_Presented_View() {
        dismiss(animated: true, completion: nil)
    }
}
// MARK: - UITextField Delegates

extension OldTopLeagueVC{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.searchActive = true
        return true;
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.searchActive = false
        return true;
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        txtSearch.resignFirstResponder()
        //searchActive = false
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        if (text.length == 1)  && (string == "") {
            self.loadDataWithPageCount(page: 0, strSearchText: "")
        }
        
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        //        // Suresh
        //        self.arrAllData.removeAll()
        //        self.tblFavorite.reloadData()
        //        self.offSet = 0
        //        // Suresh
        searchAutocompleteEntries(withSubstring: substring)
        //self.txtSearch.text = substring
        return true
    }
    
    func searchAutocompleteEntries(withSubstring substring: String) {
        if !(substring == "") {
            self.loadDataWithPageCount(page: 0, strSearchText: substring)
        }
    }
    func loadDataWithPageCount(page: Int, strSearchText : String = "") {
        
        
        //self.arrAllLeagues.removeAll()
        //self.arrAllPopulerLeagues.removeAll()
      //  self.tblLeague.reloadData()
       // self.tblPopulerLeague.reloadData()
        self.offSet = 0
        // Suresh
         let strName = strSearchText
        let dictPram = ["search_term":strName,
                        "limit":Limit,
                        "offset":offSet] as [String: AnyObject]
        self.call_Webservice_Get_league_list(dict_param: dictPram)
        
    }
}
// MARK: - Webservice

extension OldTopLeagueVC {
    
    
    func call_Webservice_Get_league_list(dict_param:[String:AnyObject]) {
        
        if self.searchActive == false {
            SVProgressHUD.show(withStatus: "Please wait")
        }
        
        objWebServiceManager.requestGet(strURL: webUrl.get_league_list, params: dict_param, success: { (response) in
            SVProgressHUD.dismiss()
            _ = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                self.shouldLoadMore = false
                if let data = response["data"] as? [String:Any] {
                    
                    if let arrData = data["popular_league"] as? [[String: Any]]{
                        self.arrAllPopulerLeagues.removeAll()
                        for dict in arrData{
                            let obj = ModelLeagueList.init(fromDictionary: dict)
                            self.arrAllPopulerLeagues.append(obj)
                        }
                      
                        if self.arrAllPopulerLeagues.count > 0{
                            self.viewPopulerLeagues.isHidden = false
                        }else{
                            self.viewPopulerLeagues.isHidden = true
                        }
                        self.tblPopulerLeague.reloadData()
                    }
                    
                    if let arrData = data["league_list"] as? [[String: Any]]{
                        
                        var arrAllDataLocal = [ModelLeagueList]()
                        
                        //suresh
                        if self.offSet == 0{
                            self.arrAllLeagues.removeAll()
                        }
                        //suresh
                        
                        for dict in arrData{
                            let obj = ModelLeagueList.init(fromDictionary: dict)
                             arrAllDataLocal.append(obj)
                        }
                        if self.offSet == 0{
                            self.arrAllLeagues = arrAllDataLocal
                        }
                        else{
                            self.arrAllLeagues += arrAllDataLocal
                        }
                        if let strTotal = data["total_records"] as? String {
                            self.strTotalRecords = strTotal
                        }
                       
                        if self.arrAllLeagues.count > 0{
                           self.viewRestOfLeagues.isHidden = false
                        }else{
                          self.viewRestOfLeagues.isHidden = true
                        }
                        self.tblLeague.reloadData()
                    }
                    
                    if self.arrAllPopulerLeagues.count == 0 && self.arrAllLeagues.count == 0 {
                        self.lblNoData.isHidden = false
                    }else{
                        self.lblNoData.isHidden = true
                    }
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            //self.view.isUserInteractionEnabled = true
        }
    }
    
    
    func call_Webservice_Post_FavoriteRestOfLeagues(request_type:String,request_id:String, index:Int){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"league"] as [String:Any]
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
//                let objTag = self.arrAllLeagues[index]
//                if objTag.isFavorite == "0"{
//                    self.lblSelectedTeam.isHidden = false
//                    self.lblSelectedTeam.text = objTag.leagueName + " added as favorite"
//                    self.btnSkip.isHidden = true
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
//                        self.lblSelectedTeam.isHidden = true
//                    }
//                    objTag.isFavorite = "1"
//                    let indexPosition = IndexPath(row: index, section: 0)
//                    self.tblLeague.reloadRows(at: [indexPosition], with: .none)
//                }else{
//                    objTag.isFavorite = "0"
//                    self.lblSelectedTeam.isHidden = true
//                    let indexPosition = IndexPath(row: index, section: 0)
//                    self.tblLeague.reloadRows(at: [indexPosition], with: .none)
//                }
            }else{
                if message == "Cannot select more than 25 leagues"{
                    let objTag = self.arrAllLeagues[index]
                    objTag.isFavorite = "0"
                    self.tblLeague.reloadData()
                    let window = UIApplication.shared.delegate?.window
                    let visibleVC = window??.visibleViewController
                    if  visibleVC?.title == "Alert"{
                        return
                    }
                    objAlert.showAlert(message: message, title: "Alert", controller: self)
                }
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
    }
    
    func call_Webservice_Post_FavoritePopulerLeagues(request_type:String,request_id:String, index:Int){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"league"] as [String:Any]
        print(paramDict)
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
//                let objTag = self.arrAllPopulerLeagues[index]
//                if objTag.isFavorite == "0"{
//                    self.lblSelectedTeam.isHidden = false
//                    self.lblSelectedTeam.text = objTag.leagueName + " added as favorite"
//                    self.btnSkip.isHidden = true
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
//                        self.lblSelectedTeam.isHidden = true
//                    }
//                    objTag.isFavorite = "1"
//                    let indexPosition = IndexPath(row: index, section: 0)
//                    self.tblPopulerLeague.reloadRows(at: [indexPosition], with: .none)
//                }else{
//                    objTag.isFavorite = "0"
//                    self.lblSelectedTeam.isHidden = true
//                    let indexPosition = IndexPath(row: index, section: 0)
//                    self.tblPopulerLeague.reloadRows(at: [indexPosition], with: .none)
//                }
            }else{
                if message == "Cannot select more than 25 leagues"{
                    let objTag = self.arrAllPopulerLeagues[index]
                    objTag.isFavorite = "0"
                    self.tblPopulerLeague.reloadData()
                    let window = UIApplication.shared.delegate?.window
                    let visibleVC = window??.visibleViewController
                    
                    if  visibleVC?.title == "Alert"{
                       return
                    }
                    objAlert.showAlert(message: message, title: "Alert", controller: self)
                }
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
    }
    
}
// MARK: - TableView Delegates & Datasource

extension OldTopLeagueVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //var count = 0
        if tableView == tblLeague {
            self.tblHightConstraint.constant = CGFloat((self.arrAllLeagues.count)*55)
             return self.arrAllLeagues.count
        }else{
            self.tblHightPopulerLeagueConstraint.constant = CGFloat((self.arrAllPopulerLeagues.count)*55)
            return self.arrAllPopulerLeagues.count
        }
        
        //return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCell", for: indexPath) as? LeagueTableViewCell{
            
            if tableView == tblLeague {
                let objModel = self.arrAllLeagues[indexPath.row]
                cell.lblLeagueName.text = objModel.leagueName
                if objModel.leagueFlage != ""{
                    if let url = URL(string: objModel.leagueFlage){
                        cell.imgLeague.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                    }
                }
                if objModel.isFavorite == "1"{
                    cell.imgSelection.image = UIImage(named: "selct_icon")
                }else{
                    cell.imgSelection.image = UIImage(named: "plus_icon")
                }
                cell.btnAddFavorite.tag = indexPath.row
                cell.btnAddFavorite.addTarget(self, action: #selector(btnRestLeaguePressed(sender:)), for: .touchUpInside)
                
            }else{
                let objModel = self.arrAllPopulerLeagues[indexPath.row]
                cell.lblLeagueName.text = objModel.leagueName
                if objModel.leagueFlage != ""{
                    if let url = URL(string: objModel.leagueFlage){
                        cell.imgLeague.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                    }
                }
                if objModel.isFavorite == "1"{
                    cell.imgSelection.image = UIImage(named: "selct_icon")
                }else{
                    cell.imgSelection.image = UIImage(named: "plus_icon")
                }
                cell.btnAddFavorite.tag = indexPath.row
                cell.btnAddFavorite.addTarget(self, action: #selector(btnPopulerLeaguePressed(sender:)), for: .touchUpInside)
        
            }
            return cell
            //}
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (arrAllLeagues.count - 1) && shouldLoadMore == true {
            let dictPram = ["search_term":self.txtSearch.text!,
                            "limit":String(self.Limit),
                            "offset":self.offSet] as [String: AnyObject]
            //suresh
            if self.offSet > 0{
                self.call_Webservice_Get_league_list(dict_param: dictPram)
            }
            //suresh
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 10.0 {
            let myInt1 = Int(strTotalRecords)
            if self.arrAllLeagues.count < myInt1!{
                //suresh
                if self.arrAllLeagues.count > 19{
                    self.shouldLoadMore = true
                    self.offSet += self.Limit
                }else{
                    self.shouldLoadMore = false
                }
            }
            else{
                self.shouldLoadMore = false
            }
            self.tblLeague.reloadData()
        }
    }
   
}

extension String {
    
    var stripped: String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=().!_")
        return self.filter {okayChars.contains($0) }
    }
}
