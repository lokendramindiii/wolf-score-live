//
//  WelcomeVC.swift
//  WolfScore
//
//  Created by Mindiii on 20/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController
{
    
    @IBOutlet weak var viewNew: UIView!
    @IBOutlet weak var lblNew: UILabel!
    @IBOutlet weak var imgNew: UIImageView!
    
    
    @IBOutlet weak var viewLogMein: UIView!
    @IBOutlet weak var lblLogMein: UILabel!
    @IBOutlet weak var imgLogMein: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func configureView() {
        self.viewNew.backgroundColor = UIColor.colorConstant.appBlueColor
        self.lblNew.textColor = UIColor.white
        //self.imgNew.image = UIImage(named: "")
        self.viewLogMein.backgroundColor = UIColor.clear
        self.lblLogMein.textColor = UIColor.white
        //self.imgLogMein.image = UIImage(named: "")
    }
    @IBAction func btnSelectUserAction(_ sender : UIButton){
        
        if sender.tag == 1
        {
            self.viewNew.backgroundColor = UIColor.colorConstant.appBlueColor
            self.lblNew.textColor = UIColor.white
            //self.imgNew.image = UIImage(named: "")
            self.viewLogMein.backgroundColor = UIColor.clear
            self.lblLogMein.textColor = UIColor.white
            //self.imgLogMein.image = UIImage(named: "")
            self.method_Go_To_FavoriteVC()
            
        }
        else if sender.tag == 2 {
            
            self.viewNew.backgroundColor = UIColor.clear
            self.lblNew.textColor = UIColor.white
            //self.imgNew.image = UIImage(named: "")
            self.viewLogMein.backgroundColor = UIColor.colorConstant.appBlueColor
            self.lblLogMein.textColor = UIColor.white
            self.method_Go_To_LogInVC()
        }
    }
    
    func method_Go_To_FavoriteVC(){
        
        let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "LocalTeamListVC") as! LocalTeamListVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func method_Go_To_LogInVC(){
        
        let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
