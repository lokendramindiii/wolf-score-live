//
//  TopPlayersListVC.swift
//  WolfScore
//
//  Created by Mindiii on 25/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage

class TopPlayersListVC: UIViewController {
    
    @IBOutlet weak var tblFavorite: UITableView!
    //
    @IBOutlet weak var lblSelectedTeam: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var viewDot1: UIView!
    @IBOutlet weak var viewDot2: UIView!
    @IBOutlet weak var viewDot3: UIView!
    @IBOutlet weak var viewDot4: UIView!
    @IBOutlet weak var viewDot5: UIView!
    //
    
    @IBOutlet weak var stackViewDots: UIStackView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblNoData: UILabel!
    //var arrAllData:[ModelTeamDemo] = []
    var arrAllData = [ModelTopPlayer]()
    //var arrSelectedData = [String]()
    var cellCount = 0
    var offSet = 0
    var Limit = 20
    //var str_Selected_Team_Id = ""
    var isFromMyFav = false
    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tblFavorite.tableFooterView = UIView()
            } else {
                self.tblFavorite.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.setDefaultMaskType(.clear)
        self.configureView()
        self.tblFavorite.reloadData()
        // Do any additional setup after loading the view.
    }
    
    func configureView() {
        self.lblSelectedTeam.isHidden = true
        self.viewDot1.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot2.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot3.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot4.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
        self.viewDot5.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.isUserInteractionEnabled = true
        self.lblNoData.isHidden = true
        if self.isFromMyFav == true {
            self.btnSkip.isHidden = true
            self.stackViewDots.isHidden = true
            self.btnNext.setTitle("DONE", for: .normal)
            
        }else{
            self.btnSkip.isHidden = false
            self.stackViewDots.isHidden = false
            self.btnNext.setTitle("NEXT", for: .normal)
        }
        //self.arrAllData.removeAll()
        if self.arrAllData.count == 0{
            cellCount = 0
            let dictPram = ["limit":String(self.Limit),
                            "offset":self.offSet] as [String: AnyObject]
            self.call_Webservice_Get_Popular(dict_param: dictPram)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
// MARK: - IBActions

extension TopPlayersListVC{
   
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        if self.isFromMyFav == true {
            dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnNextAction(_ sender : UIButton){
        self.view.endEditing(true)
               
        if self.isFromMyFav == true {
            self.dismiss_Presented_View()
        }else{
            self.btnNext.isUserInteractionEnabled = true

            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "TopLeaguesListVC") as! TopLeaguesListVC
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
        
//        self.arrSelectedData.removeAll()
//        self.str_Selected_Team_Id = ""
//        for i in 0..<self.arrAllData.count {
//            let objModel: ModelTopPlayer = self.arrAllData[i]
//            let strIsFav = objModel.is_favorite
//            if strIsFav == "1"{
//                //self.str_Selected_Team_Id = objModel.str_Team_id
//                self.arrSelectedData.append(objModel.player_id)
//                self.str_Selected_Team_Id = (self.arrSelectedData.joined(separator: ","))
//                print(self.arrSelectedData)
//            }
//        }
//        self.call_Webservice_Add_To_Favorite()
//        if self.str_Selected_Team_Id != ""{
//            self.call_Webservice_Add_To_Favorite()
//        }else{
//            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "NotificationSettingVC") as! NotificationSettingVC
//            self.navigationController?.pushViewController(viewController, animated: true)
//        }
    }
    @IBAction func btnSkipAction(_ sender : UIButton){
        self.view.endEditing(true)
        
     objAppDelegate.goToTabBar()
    }
    func dismiss_Presented_View() {
        dismiss(animated: true, completion: nil)
    }
}
// MARK: - Webservice

extension TopPlayersListVC{
   
    func call_Webservice_Get_Popular(dict_param:[String:AnyObject]) {
       // SVProgressHUD.show(withStatus: "Please wait")
        self.view.isUserInteractionEnabled = false
        objWebServiceManager.requestGet(strURL: webUrl.get_popular_players, params: dict_param, success: { (response) in
           // print(response)
            SVProgressHUD.dismiss()
            self.view.isUserInteractionEnabled = true
            _ = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    if let arrData = data["player_list"] as? [[String: Any]]{
                        var arrAllDataLocal = [ModelTopPlayer]()
                        for dict in arrData{
                            let obj = ModelTopPlayer.init(dict: dict)
                            //self.arrAllData.append(obj)
                            arrAllDataLocal.append(obj)
                        }
                        var strTotalRecords = "0"
                        if let strTotal = data["total_records"] as? String {
                            strTotalRecords = strTotal
                        }
                        let myInt1 = Int(strTotalRecords)
                        if self.arrAllData.count < myInt1!{
                            self.shouldLoadMore = true
                            self.offSet += self.Limit
                        }
                        else{
                            self.shouldLoadMore = false
                        }
                        
                        if self.offSet == 0{
                            self.arrAllData = arrAllDataLocal
                        }
                        else{
                            self.arrAllData += arrAllDataLocal
                        }
                        if self.arrAllData.count == 0{
                            self.tblFavorite.isHidden = true
                            self.lblNoData.isHidden = false
                        }else{
                            self.tblFavorite.isHidden = false
                            self.lblNoData.isHidden = true
                        }
                        //self.lblNoData.isHidden = self.arrAllData.count > 0
                        if myInt1 == 0{
                            self.tblFavorite.isHidden = true
                            self.lblNoData.isHidden = false
                        }else{
                            self.tblFavorite.isHidden = false
                            self.lblNoData.isHidden = true
                        }
                        self.tblFavorite.reloadData()
                    }
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            self.view.isUserInteractionEnabled = true
        }
    }
    
   /* func call_Webservice_Add_To_Favorite() {
        
        var dictParam = [String:Any] ()
        dictParam = ["favourite_ids":self.str_Selected_Team_Id,"favourite_type":"player"]
        print(dictParam)
        
        objWebServiceManager.requestPost(strURL: webUrl.add_favourites, params: dictParam, success: { (response) in
            print(response)
            let msg = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            
            if status == "success"{
                
                if self.isFromMyFav == true {
                   // dismiss(animated: true, completion: nil)
                    self.dismiss_Presented_View()
                    
                }else{
                let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "NotificationSettingVC") as! NotificationSettingVC
                self.navigationController?.pushViewController(viewController, animated: true)
                }
            }else if status == "fail"{
                SVProgressHUD.dismiss()
                objAlert.showAlert(message: msg, title: "Alert", controller: self)
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            self.view.isUserInteractionEnabled = true
        }
    }*/
    
    func call_Webservice_Post_Favorite(request_type:String,request_id:String, index:Int){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"player"] as [String:Any]
        print(paramDict)
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
//                let objTag = self.arrAllData[index]
//                if objTag.is_favorite == "0"{
//                    self.lblSelectedTeam.isHidden = false
//                    self.lblSelectedTeam.text = objTag.common_name + " added as favorite"
//                    self.btnSkip.isHidden = true
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
//                        self.lblSelectedTeam.isHidden = true
//                    }
//                    objTag.is_favorite = "1"
//                }else{
//                    objTag.is_favorite = "0"
//                    self.lblSelectedTeam.isHidden = true
//                }
//                let indexPosition = IndexPath(row: index, section: 0)
//                self.tblFavorite.reloadRows(at: [indexPosition], with: .none)
            }else{
                if message == "Cannot select more than 100 players"{
                    let objTag = self.arrAllData[index]
                    objTag.is_favorite = "0"
                    self.tblFavorite.reloadData()
                    let window = UIApplication.shared.delegate?.window
                    let visibleVC = window??.visibleViewController
                    
                    if  visibleVC?.title == "Alert"{
                        return
                    }
                    objAlert.showAlert(message: message, title: "Alert", controller: self)
                }
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
    }
    
}
// MARK: - TableView Delegates & Datasource

extension TopPlayersListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAllData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TopPlayerTableViewCell", for: indexPath) as? TopPlayerTableViewCell{
            
            let objModel = self.arrAllData[indexPath.row]
            cell.objTeams = objModel
            cell.imgPlayer.layer.cornerRadius = cell.imgPlayer.frame.size.height/2
            cell.imgPlayer.layer.masksToBounds = true
            //cell.loadCellData(obj: objModel)
            cell.lblPlayerName.text = objModel.full_name
            cell.lblTeamName.text = objModel.country_name
            let strUrl = objModel.player_image
            if let url = URL(string: strUrl){
                cell.imgPlayer.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else{
                cell.imgPlayer.image = UIImage(named: "icon_placeholderTeam")
            }
            
            if let url = URL(string: objModel.country_flag){
                cell.imgCountryFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgCountryFlag.image = UIImage(named: "icon_placeholderTeam")
            }
            
            
            if objModel.is_favorite == "1"{
                cell.imgSelection.image = UIImage(named: "selct_icon")
            }else{
                cell.imgSelection.image = UIImage(named: "plus_icon")
            }
            cell.actionSelectHandler = { [weak self] (objTeam) in
                guard self != nil else {return}
                guard let team = objTeam else {return}
                if team.is_favorite == "0"{
                    team.is_favorite = "1"
                    self?.call_Webservice_Post_Favorite(request_type: "1", request_id: team.player_id, index: indexPath.row)
                }else{
                    team.is_favorite = "0"
                    self?.call_Webservice_Post_Favorite(request_type: "0", request_id: team.player_id, index: indexPath.row)
                }
                let indexPosition = IndexPath(row: indexPath.row, section: 0)
                self?.tblFavorite.reloadRows(at: [indexPosition], with: .none)
            }
            return cell
            //}
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (arrAllData.count - 1) && shouldLoadMore == true {
            let dictPram = ["limit":String(self.Limit),
                            "offset":self.offSet] as [String: AnyObject]
            self.call_Webservice_Get_Popular(dict_param: dictPram)
        }
    }
}
