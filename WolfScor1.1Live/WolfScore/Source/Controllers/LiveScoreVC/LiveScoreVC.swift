//
//  LiveScoreVC.swift
//  WolfScore
//
//  Created by Mindiii on 28/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster
import AlamofireImage

class LiveScoreVC: UIViewController {

    

    var arrModelLeague = [LeaugeModel]()
    var arrFinalModelLeague = [LeaugeModel]()
    var strLeagueId = ""
    
    var arrSortLeagueByFilter = [String]()
    var pullToRefreshCtrl:UIRefreshControl!
    var Ispulltorefresh = false
    var isSortFilter = false
    
  
    
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var tblLiveMatches: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblLiveMatches.backgroundColor = UIColor.colorConstant.appDarkBlack
        SVProgressHUD.setDefaultMaskType(.clear)
        isSortFilter = false
        setPullToRefresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Ispulltorefresh = false
        objAppShareData.isLonchXLPageIndex = true
        self.apiCallGetLiveScore()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnMenuAction(_ sender : UIButton){
        self.view.endEditing(true)
        
        self.slideMenuController()?.toggleLeft()
    }
}


//MARK : custom extension
extension LiveScoreVC {
    
    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tblLiveMatches.refreshControl  = pullToRefreshCtrl
        } else {
            tblLiveMatches.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        sender.endRefreshing()
        if Ispulltorefresh == false
        {
            self.apiCallGetLiveScore()
            sender.endRefreshing()
        }
    }
    
    func apiCallGetLiveScore()
    {
        let dictPram = ["league_id":strLeagueId] as [String: AnyObject]
        call_Webservice_Get_LiveScore(dict_param: dictPram)
    }
}


// MARK: - TableView Delegates & Datasource
extension LiveScoreVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrFinalModelLeague.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        
        //headerView.conten.backgroundColor = UIColor.colorConstant.appSectionHeader
        headerView.backgroundColor = UIColor.colorConstant.appDarkBlack
        // create Image in Header
        let myCustomView = UIImageView(frame: CGRect(x: 14, y: 12, width:
            22, height: 22))
        let myImage: UIImage = UIImage(named: "circle_transfers_icon")!
        myCustomView.image = myImage
        headerView.addSubview(myCustomView)
        
        // create Icon in Header
        let myCustomView2 = UIImageView(frame: CGRect(x: tableView.bounds.size.width - 30, y: 16, width:
            14, height: 14))
        let myImage2: UIImage = UIImage(named: "icon_back_Table")!
        myCustomView2.image = myImage2
        headerView.addSubview(myCustomView2)
        
        // Create Lable in Header
        let headerLabel = UILabel(frame: CGRect(x: 50, y: 2, width:
            tableView.frame.size.width - 80, height: 40))
        
        headerLabel.font = UIFont(name: "Roboto-Bold", size: 14)
        headerLabel.textColor = UIColor.white
        headerLabel.numberOfLines = 2
        headerLabel.text = self.arrFinalModelLeague[section].name
        headerView.addSubview(headerLabel)
        
        // Create Line in Header
        let headerLine = UILabel(frame: CGRect(x: 0, y: 40, width:
            tableView.bounds.size.width, height: 0.4))
        
        headerLine.backgroundColor = UIColor.darkGray
        headerLine.alpha = 0.7
        headerView.addSubview(headerLine)
        
        // Create Line Top in Header
        let headerTopLine = UILabel(frame: CGRect(x: 0, y: 0, width:
            tableView.bounds.size.width, height: 4))
        
        if section == 0{
            headerTopLine.backgroundColor = UIColor.colorConstant.appDeepBlack
            headerTopLine.alpha = 0.7
        }else{
            headerTopLine.backgroundColor = UIColor.colorConstant.appDeepBlack
            headerTopLine.alpha = 0.7
        }
        
        headerView.addSubview(headerTopLine)
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let objLeague = self.arrFinalModelLeague[section]
        return objLeague.arrMatches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MatchesTableViewCell", for: indexPath) as? MatchesTableViewCell{
            cell.viewStatus.layer.cornerRadius = 4.0
            cell.viewStatus.layer.masksToBounds = true
            let objLeague = self.arrFinalModelLeague[indexPath.section]
            
            let objMatch = objLeague.arrMatches[indexPath.row]
            cell.lblLocalTeamName.text = objMatch.strNameloacalTeam
            cell.lblVisitorTeamName.text = objMatch.strNameVisitorTeam
            if objMatch.strStatus == "LIVE"{
                cell.stackView.isHidden = false
                cell.stackViewTimeStatus.isHidden = true
                //
                cell.viewStatus.backgroundColor = UIColor.colorConstant.appGreenColor
                cell.lblStatus.textColor = UIColor.white
                cell.lblStatus.text = objMatch.strStatus
                cell.lblStatus.font = UIFont(name: "Roboto-Bold", size: 12.0)
                //
                cell.lblScore.text = objMatch.strLocalTeamScore + " - " + objMatch.strVisitorTeamScore
            }else if objMatch.strStatus == "FT"{
                
                cell.stackView.isHidden = false
                cell.stackViewTimeStatus.isHidden = true
                cell.lblScore.text = objMatch.strLocalTeamScore + " - " + objMatch.strVisitorTeamScore
                cell.viewStatus.backgroundColor = UIColor.colorConstant.appRedColor
                cell.lblStatus.textColor = UIColor.white
                cell.lblStatus.text = objMatch.strStatus
                cell.lblStatus.font = UIFont(name: "Roboto-Bold", size: 12.0)
                
            }else if objMatch.strStatus == "DELAYED" || objMatch.strStatus == "POSTP"{
                cell.stackViewTimeStatus.isHidden = false
                cell.stackView.isHidden = true
                cell.lblStartTime.text = dayDate.Dateformate24_hours(strTime: objMatch.strTime)
                cell.lblStatusTime.text = objMatch.strStatus
                cell.lblStatusTime.isHidden = false
                
            }else{
                cell.stackViewTimeStatus.isHidden = false
                cell.stackView.isHidden = true
                cell.lblStartTime.text = dayDate.Dateformate24_hours(strTime: objMatch.strTime)
                
                cell.lblStatusTime.text = objMatch.strStatus
                cell.lblStatusTime.isHidden = false
            }
            
            if let url = URL(string: objMatch.strLogopathLocalTeam ){
                cell.imgLocal.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgLocal.image  = UIImage(named: "icon_placeholderTeam")

            }
            if let url = URL(string: objMatch.strLogopathVisitorTeam ){
                cell.imgVisitor.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                
                cell.imgVisitor.image = UIImage(named: "icon_placeholderTeam")

            }
            
            return cell
        }
        return UITableViewCell()
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let objLeague = self.arrFinalModelLeague[indexPath.section]
        let objMatch = objLeague.arrMatches[indexPath.row]
        let viewController = UIStoryboard(name: "MatchesTab",bundle: nil).instantiateViewController(withIdentifier: "MatchDetailMainVC") as! MatchDetailMainVC
        print(objMatch.strLocalTeamId)
        objAppShareData.str_Local_Team_Id = objMatch.strLocalTeamId
        objAppShareData.str_Visiter_Team_Id = objMatch.strVisitorTeamId
        objAppShareData.str_match_Id = objMatch.fixtureId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

//MARK:- extension call_Webservice_Get_LiveScore
extension LiveScoreVC{
    func call_Webservice_Get_LiveScore(dict_param:[String:AnyObject]) {
        self.Ispulltorefresh = true
        
        SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.get_live_score, params: dict_param, success: { (response) in
            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                self.arrModelLeague.removeAll()
                self.arrFinalModelLeague.removeAll()
                self.tblLiveMatches.reloadData()
                if let data = response["data"] as? [String:Any] {
                    
                    if let arrData = data["data"] as? [[String: Any]]{
                        
                        print("---------- \(arrData.count)")
                      
                        for dict in arrData{
                            let obj = LeaugeModel.init(fromDictionary: dict, isPastMatches: false)
                            self.arrModelLeague.append(obj)
                        }
                        
                        for obj in self.arrModelLeague
                        {
                            let filteredArray = self.arrModelLeague.filter(){ $0.leaugeId.contains(obj.leaugeId) }
                            for objNEW in filteredArray{
                                if !obj.arrMatches.contains(objNEW.arrMatches[0]){
                                    obj.arrMatches.append(objNEW.arrMatches[0])
                                }
                            }
                            let arrNewCheck = self.arrFinalModelLeague.filter(){ $0.leaugeId.contains(obj.leaugeId) }
                            if arrNewCheck.count == 0{
                                self.arrFinalModelLeague.append(obj)
                            }
                        }
                 
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.tblLiveMatches.reloadData()
                        })
                        self.tblLiveMatches.setContentOffset(CGPoint.zero, animated:true)
                        
                        if self.arrFinalModelLeague.count == 0{
                            self.lblNoRecord.isHidden = false
                        }else{
                            self.lblNoRecord.isHidden = true
                        }
                    }
                    
                }
            }
            else{
                if self.arrFinalModelLeague.count == 0{
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
               // GlobalUtility.showToastMessage(msg: message)
            }
            self.Ispulltorefresh = false
            
        }) { (error) in
            print(error)
            self.Ispulltorefresh = false
            SVProgressHUD.dismiss()
        }
    }
   
}

