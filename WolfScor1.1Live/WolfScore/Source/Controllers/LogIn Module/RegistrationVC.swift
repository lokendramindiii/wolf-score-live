//
//  RegistrationVC.swift
//  CarA2V
//
//  Created by Mindiii on 06/12/17.
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster
import GooglePlaces
var objRegistration = RegistrationVC()

class RegistrationVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,CLLocationManagerDelegate {
    
    //for firebase database
    var image = ""
    var emails = ""
    var ids = ""
    var userName = ""
    var userTypes = ""
    var f_id = ""
    var strPlaceId = ""
    //
    var placesClient: GMSPlacesClient!
    var dictForSocialSignUp = [String:Any]()
    var imgDataForSocialSignUp : Data? = nil
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var strLatForSend = ""
    var strLongForSend = ""
    var check:Int?
    var strChack:String = ""
    var strBusinessName:String = ""
    var strUserName:String = ""
    var strEmail:String = ""
    var strPassword:String = ""
    var strConfirmPassword:String = ""
    var strLocation:String = ""
    var isFromprofile:Bool = false
    
    @IBOutlet weak var viewForProfileImage: UIView!
    @IBOutlet weak var viewForBusinessName: UIView!
    @IBOutlet var btnRegistration: UIButton!
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    @IBOutlet var btnCountry: UIButton!
    @IBOutlet var imgProfileImage: UIImageView!
    @IBOutlet var btnImgProfile: UIButton!
    @IBOutlet var lbl_SignIn: UILabel!
    
    
    var imageL : UIImage = UIImage()
    @IBOutlet weak var constraintsDropdownBottom: NSLayoutConstraint!
    let objLogin = LoginVC ()
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var viewPassword: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideDropdownWithOutAnimation()
        imagePicker.delegate = self
        self.txtUserName.delegate = self
        self.txtEmail.delegate = self
        self.txtConfirmPassword.delegate = self
        self.txtPassword.delegate = self
        self.addAccesorryToKeyBoard()
        
        let text = "Sign In"
        let textRange = NSMakeRange(0, text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        lbl_SignIn.attributedText = attributedText
        
        txt_PlaceholderColor()
        
        configourView()
    }
    
    
    func txt_PlaceholderColor()  {
        txtUserName.attributedPlaceholder = NSAttributedString(string:"Username", attributes: [NSAttributedStringKey.foregroundColor:UIColor.colorConstant.appLightGrayColor])
        txtPassword.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSAttributedStringKey.foregroundColor:UIColor.colorConstant.appLightGrayColor])
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Email", attributes: [NSAttributedStringKey.foregroundColor:UIColor.colorConstant.appLightGrayColor])
        txtConfirmPassword.attributedPlaceholder = NSAttributedString(string:"Confirm Password", attributes: [NSAttributedStringKey.foregroundColor:UIColor.colorConstant.appLightGrayColor])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func autoFillSocialData(){
        self.txtEmail.text = self.dictForSocialSignUp["email"] as? String
        self.imgProfileImage.image = UIImage(data:self.imgDataForSocialSignUp!,scale:1.0)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Button Action
    
    @IBAction func btnTermsAction(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    @IBAction func btnGoToLogin(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func imageSelection(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func btnLicenseSelectAction(_ sender: UIButton) {
        self.view .endEditing(true)
        self.isFromprofile = false
        self.imageSelection()
    }
    
    @IBAction func btnImageSelectAction(_ sender: UIButton) {
        self.view .endEditing(true)
        self.isFromprofile = true
        self.imageSelection()
    }
    
    func configourView() {
        //        txtBusinessName.setValue(colorTheame, forKeyPath: "_placeholderLabel.textColor")
        //        txtUserName.setValue(colorTheame, forKeyPath: "_placeholderLabel.textColor")
        //        txtEmail.setValue(colorTheame, forKeyPath: "_placeholderLabel.textColor")
        //        txtPassword.setValue(colorTheame, forKeyPath: "_placeholderLabel.textColor")
        //        txtContactNo.setValue(colorTheame, forKeyPath: "_placeholderLabel.textColor")
        //        txtLocation.setValue(colorTheame, forKeyPath: "_placeholderLabel.textColor")
    }
}

extension RegistrationVC {
    func addAccesorryToKeyBoard(){
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        txtConfirmPassword.inputAccessoryView = keyboardDoneButtonView
        txtUserName.inputAccessoryView = keyboardDoneButtonView
        txtEmail.inputAccessoryView = keyboardDoneButtonView
        txtPassword.inputAccessoryView = keyboardDoneButtonView
    }
    @objc func resignKeyBoard(){
        txtConfirmPassword.endEditing(true)
        txtUserName.endEditing(true)
        txtEmail.endEditing(true)
        txtPassword.endEditing(true)
    }}


//MARK: - UIImagePicker extension Methods
extension RegistrationVC{
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if self.isFromprofile == true {
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
                let img = image.fixedOrientation()
                self.imgProfileImage.image = img
                self.btnImgProfile.setImage(nil, for: .normal)
            }
            else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                let img = image.fixedOrientation()
                self.imgProfileImage.image = img
                self.btnImgProfile.setImage(nil, for: .normal)
                
            }
            else{  }
            self.dismiss(animated: true, completion: nil)
        }else{
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
                let img = image.fixedOrientation()
                self.imageL = img
            }
            else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                let img = image.fixedOrientation()
                self.imageL = img
            }
            else{  }
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController)
    { dismiss(animated: true, completion: nil)
    }
    
    //to Access Camera
    func openCamera()
    {  if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
    {  imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        self .present(imagePicker, animated: true, completion: nil) }
    else {  }
    }
    func openGallary() //to Access Gallery
    {    imagePicker.allowsEditing = true
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil) }
}

// MARK: - Registration Action

extension RegistrationVC {
    @IBAction func btnSignUpAction(_ sender: UIButton) {
        self.view.endEditing(true)
        //objIndicator.StartIndicator()
        var strMessage:String = ""
        var inValid:Bool = false
        
//
//        if self.imgProfileImage.image == nil
//        {
//            inValid=true
//            strMessage = "Please upload your profile photo"
//            Toast(text: strMessage).show()
//        }
        
          if (txtUserName.text?.isEmpty)! {
            //objIndicator.StopIndicator()
            inValid = true
            objValidationManager.AnimationShakeTextField(textField: txtUserName)
            strMessage = "Username can't be empty"
            Toast(text: strMessage).show()
         }
            
        else if (txtEmail.text?.isEmpty)! {
            //objIndicator.StopIndicator()
            inValid = true
         objValidationManager.AnimationShakeTextField(textField: txtEmail)
            strMessage = "Email address can't be empty"
            Toast(text: strMessage).show()

        }
        else if !objValidationManager.isValidateEmail(strEmail: txtEmail.text!){
            //objIndicator.StopIndicator()
            inValid = true
            strMessage = "Please Enter valid Email Id."
            //showAlertVC(title: "Alert", message: strMessage, controller: self)
            Toast(text: strMessage).show()
        }
        else if (txtPassword.text?.isEmpty)! && !self.viewPassword.isHidden{
            //objIndicator.StopIndicator()
            inValid = true
            objValidationManager.AnimationShakeTextField(textField: txtPassword)
            strMessage = "Password can't be empty"
            Toast(text: strMessage).show()
            
        }
        else if !objValidationManager.isPasswordLength(password: txtPassword.text!)
        {
            inValid = true
            strMessage = "Password should be 6 characters long"
            Toast(text: strMessage).show()
        }
            
        else if (txtConfirmPassword.text?.isEmpty)! && !self.viewPassword.isHidden{
            //objIndicator.StopIndicator()
            inValid=true
            objValidationManager.AnimationShakeTextField(textField: txtConfirmPassword)
            strMessage = "Password can't be empty"
            Toast(text: strMessage).show()
            
        }
            
        else if !(txtConfirmPassword.text == txtPassword?.text) && !self.viewPassword.isHidden{
            //objIndicator.StopIndicator()
            inValid=true
            objValidationManager.AnimationShakeTextField(textField: txtConfirmPassword)
            
            strMessage = "Password can't be match"
            Toast(text: strMessage).show()
            
        }
        
        if (inValid && strMessage.count==0){
            //objIndicator.StopIndicator()
            strMessage = "Please Fill All Field"
            //objAlertVc.showAlert(message: strMessage, title: kAlertTitle, controller: self)
        }else if (!inValid) {
            inValid=true
            call_WebserviceFor_SignUp()
        }
    }
}

// MARK: - Webservice Calls
extension RegistrationVC {
    
    
    func call_WebserviceFor_SignUp(){
        
        if self.imgProfileImage.image == nil
        {
            self.call_WebserviceFor_SignupWithoutImage()
            return
        }
        
        SVProgressHUD.show()
        strUserName = self.txtUserName.text!
        strEmail = self.txtEmail.text!
        strPassword = self.txtPassword.text!
        strConfirmPassword = self.txtConfirmPassword.text!
      
        let strSocialId = ""
        let strSocialType = ""
        
        var param = [String:Any]()
        
        param = ["name":checkForNULL(obj: strUserName),
                 "email":checkForNULL(obj: strEmail),
                 "password":checkForNULL(obj: strPassword),
                 "device_type":"2",
                 "device_token":kDevice_Tocken ,
                 "social_type":strSocialType,
                 "social_id":strSocialId
        ]
        
        let img = self.imgProfileImage.image?.fixedOrientation();
        let  data = UIImageJPEGRepresentation(img!,1.0)! as NSData
        
        objWebServiceManager.uploadMultipartData(strURL: webUrl.signup, params: param as [String : AnyObject], imageData:data as Data , fileName: "profile_photo", mimeType: "image/jpg", success: { (response) in
            
            let message =  response["message"] as? String ?? ""
            let status =  response["status"] as? String ?? ""
            if status == "success" || status == "SUCCESS"
            {
                let dictUserDetail = response["data"] as! Dictionary<String, Any>
                self.saveDataInDevice ( dict:dictUserDetail )
                self.method_Go_To_FavoriteVC()
            }
            else if status == "fail" || status == "FAIL"{
                
                Toast(text: message).show()
            }
            SVProgressHUD.dismiss()
        }) { (error) in
            
            SVProgressHUD.dismiss()
            if (error.localizedDescription.contains("The network connection was lost.")){
                self.call_WebserviceFor_SignUp()
            }else{
                
                //objIndicator.StopIndicator()
                //objAlertVc.showAlert(message: FailAPI, controller: self)
            }
        }
    }
    
    func method_Go_To_FavoriteVC(){
        
        let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "LocalTeamListVC") as! LocalTeamListVC
        viewController.isLoginUser = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func call_WebserviceFor_SignupWithoutImage()
    {
        
        SVProgressHUD.show()
        strUserName = self.txtUserName.text!
        strEmail = self.txtEmail.text!
        strPassword = self.txtPassword.text!
        strConfirmPassword = self.txtConfirmPassword.text!
       
        let strSocialId = ""
        let strSocialType = ""
        
        var param = [String:Any]()
        
        param = ["name":checkForNULL(obj: strUserName),
                 "email":checkForNULL(obj: strEmail),
                 "password":checkForNULL(obj: strPassword),
                 "device_type":"2",
                 "device_token":kDevice_Tocken ,
                 "social_type":strSocialType,
                 "social_id":strSocialId,
                  "profile_photo":""
        ]
        
        objWebServiceManager.requestPost(strURL: webUrl.signup, params: param, success: { (response) in
            SVProgressHUD.dismiss()
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success" || status == "SUCCESS"{
                let dictUserDetail = response["data"] as! Dictionary<String, Any>
                self.saveDataInDevice (dict:dictUserDetail)
                self.method_Go_To_FavoriteVC()
                
            }else if status == "fail" || status == "FAIL"{
                Toast(text: message).show()
                
            }else if status == "authFail"{
                Toast(text: message).show()
                
            }
        }) { (errore) in
            SVProgressHUD.dismiss()
            if (errore.localizedDescription.contains("The network connection was lost.")){
                self.call_WebserviceFor_SignupWithoutImage()
            }else{
                //objIndicator.StopIndicator()
                //objAlertVc.showAlert(message: FailAPI , title: kAlertTitle, controller: self)
            }
        }
    }
    
    
}

// MARK: - Textfield Delegate Methods
extension RegistrationVC : UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtUserName {
            txtEmail.becomeFirstResponder()
        }
        else if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        }
        else if textField == txtPassword {
            txtConfirmPassword.becomeFirstResponder()
        }
            //        else if textField == txtContactNo {
            //            resignFirstResponder()
            //        }
        else{
        }
        return true
    }
    public func textFieldDidBeginEditing(_ textField: UITextField) {
    }
}

// MARK:-Save Data
extension RegistrationVC {
    
    func saveDataInDevice(dict: Dictionary<String, Any>) {
        
        let defaults = UserDefaults.standard
        
        defaults.setValue(txtEmail.text, forKey: UserDefaults.Keys.kEmail)
        defaults.setValue(txtPassword.text, forKey: UserDefaults.Keys.kRMPassword)
        
        defaults.setValue(nil, forKey: UserDefaults.Keys.kGuestLogin)
        defaults.set(dict["userId"], forKey: UserDefaults.Keys.kUserId)
        defaults.set(dict["name"], forKey: UserDefaults.Keys.kUserName)
        defaults.set(dict["auth_token"], forKey: UserDefaults.Keys.kAuthToken)
        defaults.set(dict["email"], forKey: UserDefaults.Keys.kEmail)
        
        defaults.set(dict["gender"], forKey: UserDefaults.Keys.kGender)
        defaults.set(dict["about"], forKey: UserDefaults.Keys.kabout)
        defaults.set(dict["birthday"], forKey: UserDefaults.Keys.kDOB)
        
        
        let profleurl = dict["profile_url"]
        let profile_photo = dict["profile_photo"]
        
        let finalprofileurl = "\(profleurl ?? "")\(profile_photo ?? "")"
        
        defaults.set(finalprofileurl, forKey: UserDefaults.Keys.kUserProfileurl)
        
        defaults.set(dict["social_id"], forKey: UserDefaults.Keys.kSocialId)
        defaults.set(dict["device_token"], forKey: UserDefaults.Keys.kUnique_Device_Token)
        UserDefaults.standard.setValue(self.txtPassword.text!, forKey: UserDefaults.Keys.kPassword)
        UserDefaults.standard.synchronize()
    }
    
    
    func hideDropdownWithOutAnimation() {
        //  constraintsDropdownBottom.constant = -220
        view.superview?.layoutIfNeeded()
    }
}


