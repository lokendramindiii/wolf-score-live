//
//  ForgotPasswordVC.swift
//  CarA2V
//
//  Created by Mindiii on 09/12/17.
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit
import Toaster
import SVProgressHUD

class ForgotPasswordVC: UIViewController, UITextFieldDelegate {
    var userType = ""
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = .black

        txt_PlaceholderColor()
    }
    
    func txt_PlaceholderColor()  {
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Enter Email", attributes: [NSAttributedStringKey.foregroundColor:UIColor.colorConstant.appLightGrayColor])
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Button Actions

    @IBAction func btnBackAction(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        //objAlertVc.showAlert(message: "Under Development", controller: self)
        
        self.view .endEditing(true)
        var strMessage:String = ""
        var inValid:Bool = false
        
        if (txtEmail.text?.isEmpty)! {
            inValid=true
    objValidationManager.AnimationShakeTextField(textField: txtEmail)
            strMessage = "Please enter email address"
            //showAlertVC(title: "Alert", message: strMessage, controller: self)
            Toast(text: strMessage).show()
        }else if !objValidationManager.isValidateEmail(strEmail: txtEmail.text!){
            inValid=true
            strMessage = "Please enter valid email id."
            //showAlertVC(title: "Alert", message: strMessage, controller: self)
            Toast(text: strMessage).show()
        }
        if (inValid && strMessage.count==0){
            //strMessage = "Please Fill All Field"
            //objAlertVc.showAlert(message: strMessage, title: kAlertTitle, controller: self)
        }else if (!inValid) {
            inValid=true
            call_Webservice_Forgot_Password()
        }
    }
    
    func call_Webservice_Forgot_Password(){
        
        SVProgressHUD.show()
        self.txtEmail.text = self.txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        var dictParam = [String: Any]()
        dictParam = ["email": self.txtEmail.text!]
        objWebServiceManager.requestPost(strURL: webUrl.forgot_password, params: dictParam, success: { (response) in
            SVProgressHUD.dismiss()
            let message = response["message"] as? String ?? ""
            let status =  response["status"] as? String ?? ""
            if status == "success"{
                self.showSuccessAlert(message: message)
            }else if status == "fail"{
                SVProgressHUD.dismiss()
                
                //objAlertVc.showAlert(message: message, controller: self)
            }
            

        }) { (error) in
            
            SVProgressHUD.dismiss()
            //objIndicator.StopIndicator()
            //objAlertVc.showAlert(message: FailAPI, controller: self)
        }
        
    }
    
    func showSuccessAlert(message: String) {
        let alert = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(yesButton)
        present(alert, animated: true) {() -> Void in }
    }
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            txtEmail.resignFirstResponder()
        }
        return true
    }
}

