//
//  LoginVC.swift
//  CarA2V
//
//  Created by Mindiii on 05/12/17.
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import FBSDKLoginKit
import GooglePlaces
import Toaster
var RMCheck:Bool = false
var strRMEmail:String = ""
var strRMPassword:String = ""
//
class LoginVC: UIViewController,UITextFieldDelegate {
    
    //for firebase database
    var image = ""
    var email = ""
    var ids = ""
    var userName = ""
    var f_id = ""
    //
    var Remembercheck:Int?
    // var strEmail:String = ""
    var strChack:String = ""
    var strPassword:String = ""
    //  var userType = ""
    var strRememberMeChack:Bool = false

    @IBOutlet weak var imageCheckBox: UIImageView!
    @IBOutlet var btnRememberData: UIButton!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var lbl_SignUp: UILabel!

    //Facebook data
    var strName:String = ""
    var strSocialType:String = ""
    var strSocialId:String = ""
    var strEmail:String = ""
    var strFullName:String = ""
    var imgProfileImage:String = ""
    
    var facebookDict : [String : AnyObject]!
    //gmail variable
    var strGmailSocialID = ""
    var strGmailPick = ""
    var strGmailName = ""
    var strGmailEmail = ""
    
    //Current Point
    var strLatForSend = ""
    var strLongForSend = ""
    var strLocation:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let text = "Sign Up"
        let textRange = NSMakeRange(0, text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        lbl_SignUp.attributedText = attributedText
        
        Remembercheck = UserDefaults.standard.integer(forKey: UserDefaults.Keys.kIsRemember)
        
        // remeber me
        if Remembercheck == 1
        {
            txtEmail.text = UserDefaults.standard.string(forKey: UserDefaults.Keys.kEmail)
            txtPassword.text = UserDefaults.standard.string(forKey: UserDefaults.Keys.kRMPassword)
            self.imageCheckBox.image = #imageLiteral(resourceName: "checkBox_icon")
            btnRememberData.isSelected=true
        }
        else if Remembercheck == 2
        {
            self.imageCheckBox.image = #imageLiteral(resourceName: "uncheckBox_icon")
            btnRememberData.isSelected=false
        }
        
        txt_PlaceholderColor()
        
        
    }
    
    
    func txt_PlaceholderColor()  {
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Email", attributes: [NSAttributedStringKey.foregroundColor:UIColor.colorConstant.appLightGrayColor])
        txtPassword.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSAttributedStringKey.foregroundColor:UIColor.colorConstant.appLightGrayColor])
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    @IBAction func btnAlwaysAction(_ sender: UIButton) {
        
        if btnRememberData.isSelected
        {
          //  UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.kSign)
          //  UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.kRMUserName)
         //   UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.kRMPassword)
          //  UserDefaults.standard.synchronize()
            self.Remembercheck = 2
            self.imageCheckBox.image = #imageLiteral(resourceName: "uncheckBox_icon")
            btnRememberData.isSelected=false
        }
        else
        {
            self.imageCheckBox.image = #imageLiteral(resourceName: "checkBox_icon")
            self.Remembercheck = 1
            btnRememberData.isSelected=true
        }
    }
    // MARK: - Facebook Login
    
    @IBAction func btnFacebookLogin(_ sender: UIButton) {
        strSocialType = "facebook"
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func btnBackAction(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnLoginAction(_ sender: UIButton) {
        
        self.view.endEditing(true)
        var strMessage:String = ""
        var inValid:Bool = false
        
        if (txtEmail.text?.isEmpty)! {
            inValid=true
            objValidationManager.AnimationShakeTextField(textField: txtEmail)
            strMessage = "Please Enter Email address"
            Toast(text: strMessage).show()
        }
        else if !objValidationManager.isValidateEmail(strEmail: txtEmail.text!){
            inValid=true
            strMessage = "Please Enter valid Email Id."
            Toast(text: strMessage).show()
            
        }
        else if (txtPassword.text?.isEmpty)! {
            inValid=true
        objValidationManager.AnimationShakeTextField(textField: txtPassword)
            strMessage = "Please Enter Password"
            Toast(text: strMessage).show()
        }
        if (inValid && strMessage.count==0){
            strMessage = "Please Enter Email & Password for Login"
        }else if (!inValid) {
            inValid=true
            call_WebserviceFor_Login()
        }
    }
    
    
    @IBAction func btnForgotAction(_ sender: UIButton) {
        
        let objForgotInVC = self.storyboard!.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController!.pushViewController(objForgotInVC, animated : true)
    }
    
    @IBAction func btnGoToRegistration(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
        viewController.strChack = strChack
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    // MARK: - Textfield Delegate Methods
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        }else {
            txtPassword.resignFirstResponder()
        }
        return true
    }
}

// MARK: - Login Webservice Calls
extension LoginVC {
    
    func call_WebserviceFor_Login()
    {
        SVProgressHUD.show()

        strEmail = self.txtEmail.text!
        strPassword = self.txtPassword.text!
        let param = ["email":checkForNULL(obj: strEmail),
                     "password":checkForNULL(obj: strPassword),
                     "device_type":"2",
                     "device_token":kDevice_Tocken
        ]
        
        objWebServiceManager.requestPost(strURL: webUrl.login, params: param, success: { (response) in
             SVProgressHUD.dismiss()
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success" || status == "SUCCESS"{
                let dictUserDetail = response["data"] as! Dictionary<String, Any>
                self.saveDataInDevice (dict:dictUserDetail)
             //   self.method_Go_To_FavoriteVC()
                objAppDelegate.goToTabBar()
                self.call_Webservice_For_GetCountryName()
                Toast(text: message).show()
              

            }else if status == "fail" || status == "FAIL"{
                Toast(text: message).show()
                
            }else if status == "authFail"{
                Toast(text: message).show()

            }
        }) { (errore) in
            SVProgressHUD.dismiss()
            if (errore.localizedDescription.contains("The network connection was lost.")){
                self.call_WebserviceFor_Login()
            }else{
                //objIndicator.StopIndicator()
                //objAlertVc.showAlert(message: FailAPI , title: kAlertTitle, controller: self)
            }
        }
    }
    
    
    
    func call_Webservice_For_GetCountryName() {
        
        objWebServiceManager.requestGet(strURL: webUrl.get_Country_Name, params: nil, success: { (response) in
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    let arrCountry = data["country_list"] as? [[String:Any]] ?? []
                    UserDefaults.standard.setValue(arrCountry, forKey: UserDefaults.Keys.kCountryArr)
                    UserDefaults.standard.synchronize()
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    
    func method_Go_To_FavoriteVC(){
        
        let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "LocalTeamListVC") as! LocalTeamListVC
        viewController.isLoginUser = true
     self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension LoginVC {
    // MARK:-Save Data in User Default
    func saveDataInDevice(dict: Dictionary<String, Any>) {
        let defaults = UserDefaults.standard
       
        defaults.setValue(txtEmail.text, forKey: UserDefaults.Keys.kEmail)
        defaults.setValue(txtPassword.text, forKey: UserDefaults.Keys.kRMPassword)

        defaults.setValue(nil, forKey: UserDefaults.Keys.kGuestLogin)
        defaults.set(dict["userId"], forKey: UserDefaults.Keys.kUserId)
        defaults.set(dict["name"], forKey: UserDefaults.Keys.kUserName)
        defaults.set(dict["auth_token"], forKey: UserDefaults.Keys.kAuthToken)
        defaults.set(dict["email"], forKey: UserDefaults.Keys.kEmail)
        
        defaults.set(dict["gender"], forKey: UserDefaults.Keys.kGender)
        defaults.set(dict["about"], forKey: UserDefaults.Keys.kabout)
        defaults.set(dict["birthday"], forKey: UserDefaults.Keys.kDOB)
        defaults.set(self.Remembercheck, forKey: UserDefaults.Keys.kIsRemember)
        
        var finalprofileurl:String = ""
        if dict["is_photo_url"] as? String ?? ""  == "1"
        {
            finalprofileurl =  dict["profile_photo"] as? String ?? ""
        }
        else
        {
            let profleurl = dict["profile_url"]
            let profile_photo = dict["profile_photo"]
            
            finalprofileurl = "\(profleurl ?? "")\(profile_photo ?? "")"
        }
        
        defaults.set(finalprofileurl, forKey: UserDefaults.Keys.kUserProfileurl)

        defaults.set(dict["social_id"], forKey: UserDefaults.Keys.kSocialId)
        defaults.set(dict["device_token"], forKey: UserDefaults.Keys.kUnique_Device_Token)
        UserDefaults.standard.setValue(self.txtPassword.text!, forKey: UserDefaults.Keys.kPassword)
            UserDefaults.standard.synchronize()
    }
    
    //Mark: Get Facebook Data
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.facebookDict = result as! [String : AnyObject]
                    self.strName = self.facebookDict["first_name"] as! String
                    self.strFullName = self.facebookDict["name"] as! String
                    self.strSocialId = self.facebookDict["id"] as! String
                    self.strEmail = self.facebookDict["email"] as! String
                    self.imgProfileImage = "http://graph.facebook.com/" + self.strSocialId + "/picture?type=large" //type=normal
                self.call_WebserviceFor_check_is_user_social_registered()
                }
            })
        }
    }
}

//MARK: - Registration Webservice
extension LoginVC{
   
    func call_WebserviceFor_check_is_user_social_registered(){
        
        SVProgressHUD.show()
        var param = [String:Any]()
        
        param = [
                 "device_type":"2",
                 "device_token":kDevice_Tocken ,
                 "social_id":strSocialId
        ]
        
        objWebServiceManager.requestPost(strURL: webUrl.check_is_user_social_registered, params: param, success: { (response) in
            SVProgressHUD.dismiss()
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success" || status == "SUCCESS"{
                
                let dictUserDetail = response["data"] as! Dictionary<String, Any>

                self.saveDataInDevice (dict:dictUserDetail)
                objAppDelegate.goToTabBar()

            }
            else if status == "fail" || status == "FAIL"{
               // Toast(text: message).show()
                self.CallWebservice_SocialSignup()
                
            }
            else if status == "authFail"{
                Toast(text: message).show()
            }
        }) { (errore) in
            SVProgressHUD.dismiss()
            if (errore.localizedDescription.contains("The network connection was lost.")){
            }
        }
    }
    
    
    func CallWebservice_SocialSignup()
    {
        SVProgressHUD.show()
        var param = [String:Any]()
        param = ["name":strName,
                 "email":strEmail,
                 "password":"",
                 "device_type":"2",
                 "device_token":kDevice_Tocken,
                 "social_type":strSocialType,
                 "social_id":strSocialId,
                 "profile_photo": self.imgProfileImage
        ]
        
        
        objWebServiceManager.requestPost(strURL: webUrl.signup, params: param, success: { (response) in
            SVProgressHUD.dismiss()
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success" || status == "SUCCESS"{
                let dictUserDetail = response["data"] as! Dictionary<String, Any>
                self.saveDataInDevice (dict:dictUserDetail)
                self.method_Go_To_FavoriteVC()
                
            }else if status == "fail" || status == "FAIL"{
                Toast(text: message).show()
                
            }else if status == "authFail"{
                Toast(text: message).show()
                
            }
        }) { (errore) in
            SVProgressHUD.dismiss()
            if (errore.localizedDescription.contains("The network connection was lost.")){
                self.CallWebservice_SocialSignup()
            }else{
                
                //objIndicator.StopIndicator()
                //objAlertVc.showAlert(message: FailAPI , title: kAlertTitle, controller: self)
            }
        }
    }
}

func checkForNULL(obj: Any) -> Any {
    return (obj == nil) ? "" : obj
}

