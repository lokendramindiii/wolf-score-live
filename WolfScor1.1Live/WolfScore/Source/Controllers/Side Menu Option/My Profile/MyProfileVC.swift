//
//  MyProfileVC.swift
//  WolfScore
//
//  Created by Mindiii on 1/25/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage
import Toaster
class MyProfileVC: UIViewController {

    
    var navController:UINavigationController?

    @IBOutlet weak var viewBtnSignUp: UIView!
    @IBOutlet weak var viewImgProfile: UIView!
    
    @IBOutlet weak var viewBgChangePsw: UIView!
    @IBOutlet var btnUpdate: UIButton!
    @IBOutlet var txtCurrentPassword: UITextField!
    @IBOutlet var txtNewPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var viewPassword: UIView!
    
    
    @IBOutlet var imgProfileImage: UIImageView!
    @IBOutlet var lbl_username: UILabel!
    @IBOutlet var lbl_userEmail: UILabel!


    var strPassword:String = ""
    var strNewPassword:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewBtnSignUp.layer.cornerRadius = 22
        self.viewImgProfile.layer.cornerRadius = self.viewImgProfile.frame.size.height/2
        self.viewImgProfile.layer.masksToBounds = true
        self.viewImgProfile.layer.borderWidth = 2
        self.viewImgProfile.layer.borderColor = UIColor.colorConstant.appBlueColor.cgColor

        let defaults = UserDefaults.standard
        lbl_userEmail.text = defaults.string(forKey: UserDefaults.Keys.kEmail)
        lbl_username.text = defaults.string(forKey: UserDefaults.Keys.kUserName)
        let ProfileUrl = defaults.string(forKey: UserDefaults.Keys.kUserProfileurl)
        if let url = URL(string: ProfileUrl ?? ""){
            imgProfileImage.af_setImage(withURL: url, placeholderImage: UIImage(named: "profile_updateicon"))
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewBgChangePsw.addGestureRecognizer(tap)
        viewBgChangePsw.isUserInteractionEnabled = true
        
       txt_PlaceholderColor ()
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func txt_PlaceholderColor()  {
        
        txtCurrentPassword.attributedPlaceholder = NSAttributedString(string:"Current Password", attributes: [NSAttributedStringKey.foregroundColor:UIColor.white])
        
        txtNewPassword.attributedPlaceholder = NSAttributedString(string:"New Password", attributes: [NSAttributedStringKey.foregroundColor:UIColor.white])
        
        txtConfirmPassword.attributedPlaceholder = NSAttributedString(string:"Confirm Password", attributes: [NSAttributedStringKey.foregroundColor:UIColor.white])
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let ProfileUrl = UserDefaults.standard.string(forKey: UserDefaults.Keys.kUserProfileurl)
        if let url = URL(string: ProfileUrl ?? ""){
            imgProfileImage.af_setImage(withURL: url, placeholderImage: UIImage(named: "profile_updateicon"))
         
            lbl_username.text = UserDefaults.standard.string(forKey: UserDefaults.Keys.kUserName)
        }
        
        self.navigationController?.isNavigationBarHidden = true
        self.slideMenuController()?.leftPanGesture?.isEnabled = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.slideMenuController()?.leftPanGesture?.isEnabled = true
    }
    
    
    @IBAction func btnLogoutAction(_ sender : UIButton){
        self.view.endEditing(true)
        
        
        let alert = UIAlertController(title: "WolfScore", message: "Are you sure you want to log out?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Logout", style: UIAlertAction.Style.destructive, handler: { action in
            
            self.viewBgChangePsw.isHidden = true
            SVProgressHUD.show()
            objWebServiceManager.requestGet(strURL: webUrl.logout, params: nil, success: { (response) in
                SVProgressHUD.dismiss()
                let message = response["message"] as? String ?? ""
                let status = response["status"] as? String ?? ""
                if status == "success"{
                    print("scucess")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let aVariable = appDelegate.Go_ToWelcomeScreen()
                    self.resetDefaults()
                }
                else
                {
                    Toast(text: message).show()
                }
            }) { (error) in
                print(error)
                SVProgressHUD.dismiss()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func resetDefaults() {
        
        let defaults = UserDefaults.standard
        
        let UserEmail = defaults.string(forKey: UserDefaults.Keys.kEmail)
        let UserPassword = defaults.string(forKey: UserDefaults.Keys.kRMPassword)
        let rememberMe = defaults.string(forKey: UserDefaults.Keys.kIsRemember)
        let arrCountry = UserDefaults.standard.array(forKey: UserDefaults.Keys.kCountryArr) as? [[String:Any]] ?? []

        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        
        defaults.setValue(rememberMe , forKey: UserDefaults.Keys.kIsRemember)
        defaults.setValue(UserEmail , forKey: UserDefaults.Keys.kEmail)
        defaults.setValue(UserPassword , forKey: UserDefaults.Keys.kRMPassword)
        defaults.setValue(arrCountry , forKey: UserDefaults.Keys.kCountryArr)

    }
    
    @IBAction func btnCloseAction(_ sender : UIButton){
        self.view.endEditing(true)
        
        txtNewPassword.text = ""
        txtCurrentPassword.text = ""
        txtConfirmPassword.text = ""
        viewBgChangePsw.isHidden = true
        
    }
    
    @IBAction func btn_ChangePswPopupAction(_ sender : UIButton){
        
        self.view.endEditing(true)
        viewBgChangePsw.isHidden = false
    }
    
    @IBAction func btn_UpdateProfileAction(_ sender : UIButton){
        self.view.endEditing(true)
        
        
        let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
        self.navigationController?.pushViewController(viewController, animated: true)

        
    }
    @IBAction func btnUpdatePasswordAction(_ sender : UIButton){
        self.view.endEditing(true)
        var strMessage:String = ""
        var inValid:Bool = false
        
        if (txtCurrentPassword.text?.isEmpty)!{
            //objIndicator.StopIndicator()
            inValid=true
            objValidationManager.AnimationShakeTextField(textField: txtCurrentPassword)
            strMessage = "Password can't be empty"
            Toast(text: strMessage).show()

        }
           
            
        else if !objValidationManager.isPasswordLength(password: txtCurrentPassword.text!)
        {
            inValid=true
            strMessage = "Password should be 6 characters long"
            Toast(text: strMessage).show()
        }
            
        else if (txtNewPassword.text?.isEmpty)! {
            //objIndicator.StopIndicator()
            inValid=true
        objValidationManager.AnimationShakeTextField(textField: txtNewPassword)
            strMessage = "Password can't be empty"
            Toast(text: strMessage).show()

        }
            
        else if !(txtNewPassword.text == txtConfirmPassword?.text) {
            //objIndicator.StopIndicator()
            inValid=true
            objValidationManager.AnimationShakeTextField(textField: txtConfirmPassword)
            strMessage = "Password can't be match"
            Toast(text: strMessage).show()

        }
        
        
        if (inValid && strMessage.count==0){
            //objIndicator.StopIndicator()
            strMessage = "Please Fill All Field"
            //objAlertVc.showAlert(message: strMessage, title: kAlertTitle, controller: self)
        }else if (!inValid) {
            inValid=true
            call_WebserviceFor_ChangePassword()
        }
        
    }
    
    @IBAction func btnBackAction(_ sender : UIButton){
        self.view.endEditing(true)
        
        objAppShareData.isLonchXLPageIndex = true
        
        let storyboard = UIStoryboard(name: "UserTabbar", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "UserTabbarVC") as! UserTabbarVC
        self.slideMenuController()?.changeMainViewController(mainViewController, close: true)
    }
    
    
    @IBAction func btnMyFavAction(_ sender : UIButton){
        
        let viewController = UIStoryboard(name: "UserTabbar",bundle: nil).instantiateViewController(withIdentifier: "MyFavoritesMainVC") as! MyFavoritesMainVC
        viewController.isfromMyProfile = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
 
    @IBAction func btnPrivacy(_ sender : UIButton){
        
        let staticPagesController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
        
        leftVC.PageType = "Privacy Policy"

        staticPagesController.isfromMyProfile = true
    self.navigationController?.pushViewController(staticPagesController, animated: true)
    }

}
extension MyProfileVC {
    func call_WebserviceFor_ChangePassword()
    {
        
        var strDeviceToken = ""
        if strDeviceToken == "" {
            strDeviceToken = "123456"
        }
        
        strPassword = self.txtCurrentPassword.text!
        strNewPassword = self.txtNewPassword.text!

        let param = [
                     "password":checkForNULL(obj: strPassword),
                     "new_password":checkForNULL(obj: strNewPassword)
        ]
        
        objWebServiceManager.requestPost(strURL: webUrl.change_password, params: param, success: { (response) in
            
            let message = response["message"] as? String ?? ""
            Toast(text: message).show()

            let status = response["status"] as? String ?? ""
            if status == "success" || status == "SUCCESS"{
                
                UserDefaults.standard.setValue(self.txtNewPassword.text, forKey: UserDefaults.Keys.kRMPassword)
                
                self.txtConfirmPassword.text = ""
                self.txtCurrentPassword.text = ""
                self.txtNewPassword.text = ""
                self.viewBgChangePsw.isHidden = true
                
            }else if status == "fail" || status == "FAIL"{
                //objIndicator.StopIndicator()
                //objAlertVc.showAlert(message: message , title: kAlertTitle, controller: self)
            }else if status == "authFail"{
                //objIndicator.StopIndicator()
                //objAlertVc.showAlert(message: "Session Expire" , title: kAlertTitle, controller: self)
                //objAppDelegate.GoToNavigationOfLoginVC()
            }
        }) { (errore) in
            if (errore.localizedDescription.contains("The network connection was lost.")){
                self.call_WebserviceFor_ChangePassword()
            }else{
                //objIndicator.StopIndicator()
                //objAlertVc.showAlert(message: FailAPI , title: kAlertTitle, controller: self)
            }
        }
    }
}

// MARK: - Textfield Delegate Methods
extension MyProfileVC : UITextFieldDelegate {
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    // MARK: - Textfield Delegate Methods
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtCurrentPassword {
            txtNewPassword.becomeFirstResponder()
        }else if textField == txtNewPassword {
            txtConfirmPassword.becomeFirstResponder()
        }
        else
        {
            txtConfirmPassword.resignFirstResponder()
        }
        return true
    }
}

