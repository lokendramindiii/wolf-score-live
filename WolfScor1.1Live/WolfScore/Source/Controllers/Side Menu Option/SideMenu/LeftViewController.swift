//
//  LeftViewController.swift
//  WolfScore
//
//  Created by mac on 22/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD

enum LeftMenu: Int
{
    case prpfile
    case contactUs
    case termsCondition
    case privacyPolicy
    case reviewApp
    case logout
}
enum LeftMenuGuest: Int
{
    case favorite
    case contactUs
    case termsCondition
    case privacyPolicy
    case reviewApp
    case login
}

struct leftVC {
    
    static var PageType:String = ""
    static var strTermsUrl:String = ""
    static var strPrivacyUrl:String = ""
    static var strMail:String = ""
    static var strContactUsDescription:String = ""

}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftViewController: UIViewController, LeftMenuProtocol{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lbluserEmail: UILabel!

    var MyProfileVC: UIViewController!
    var Myfavorite: UIViewController!
    var ContactUsVC: UIViewController!
    var staticPageVC: UIViewController!
    var staticPagePrivacyVC: UIViewController!
    var MatchVC: UIViewController!


  //  var arrheading = ["TOP FEATURES","PREMIUM FEATURES"]
    
    var arrheading = ["TOP FEATURES"]

  //  var arrInfo =    [["My Profile", "Contact Us" ,"Terms & Conditions","Privacy Policy","Review Our App" , "Logout"],["Remove banner-ads","Restore previous purchases"]]
    
    var arrInfo =    [["My Profile", "Contact Us" ,"Terms & Conditions","Privacy Policy","Review Our App" , "Logout"]]

   // var arrImage =    [[UIImage(named: "profile_ico"),UIImage(named: "icon_contactus"),UIImage(named: "icon_condition"),UIImage(named: "icon_help"),UIImage(named: "icon_review"),UIImage(named: "icon_logout") ],[UIImage(named: "banner_ads"),UIImage(named: "previous_ico")]]
    
    var arrImage =    [[UIImage(named: "profile_ico"),UIImage(named: "icon_contactus"),UIImage(named: "icon_condition"),UIImage(named: "icon_help"),UIImage(named: "icon_review"),UIImage(named: "icon_logout") ]]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.call_Webservice_GetContent()

        self.img.layer.borderWidth = 2
        self.img.layer.borderColor = UIColor.white.cgColor
        
        let userId = UserDefaults.standard.string(forKey: UserDefaults.Keys.kUserId)
        if userId == nil{
         //   arrInfo =    [[ "Favorite","Contact Us" ,"Terms & Conditions","Privacy Policy","Review Our App" , "Login" ],["Remove banner-ads","Restore previous purchases"]]
            
            
            arrInfo =    [[ "Favorite","Contact Us" ,"Terms & Conditions","Privacy Policy","Review Our App" , "Login" ]]


         //   arrImage =    [[UIImage(named: "icon_review"), UIImage(named: "icon_contactus"),UIImage(named: "icon_condition"),UIImage(named: "icon_help"),UIImage(named: "icon_review"),UIImage(named: "icon_logout")],[UIImage(named: "banner_ads"),UIImage(named: "previous_ico")]]
            
             arrImage =    [[UIImage(named: "icon_review"), UIImage(named: "icon_contactus"),UIImage(named: "icon_condition"),UIImage(named: "icon_help"),UIImage(named: "icon_review"),UIImage(named: "icon_logout")]]
            
            lbluserEmail.text = ""
            lblUsername.text = ""
            img.image = UIImage(named: "new_logo_ico")
            
        }
        else
        {
            let defaults = UserDefaults.standard
            lbluserEmail.text = defaults.string(forKey: UserDefaults.Keys.kEmail)
            lblUsername.text = defaults.string(forKey: UserDefaults.Keys.kUserName)
            let ProfileUrl = defaults.string(forKey: UserDefaults.Keys.kUserProfileurl)
            if let url = URL(string: ProfileUrl ?? ""){
                img.af_setImage(withURL: url, placeholderImage: UIImage(named: "profile_updateicon"))
            }
        }
        
        let MatchsController = UIStoryboard(name: "UserTabbar",bundle: nil).instantiateViewController(withIdentifier: "MatchsVC") as! MatchsVC
        self.MatchVC = UINavigationController(rootViewController: MatchsController)
        
        let viewController = UIStoryboard(name: "UserTabbar",bundle: nil).instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        self.MyProfileVC = UINavigationController(rootViewController: viewController)
        
        
        let myfavoriteController = UIStoryboard(name: "UserTabbar",bundle: nil).instantiateViewController(withIdentifier: "MyFavoritesMainVC") as! MyFavoritesMainVC
        self.Myfavorite = UINavigationController(rootViewController: myfavoriteController)
        
        
        let contactusController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
        self.ContactUsVC = UINavigationController(rootViewController: contactusController)
        
        
        let staticPagesController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
        self.staticPageVC = UINavigationController(rootViewController: staticPagesController)
        
        
        let staticPagesVC = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
         self.staticPagePrivacyVC = UINavigationController(rootViewController: staticPagesVC)
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let ProfileUrl = UserDefaults.standard.string(forKey: UserDefaults.Keys.kUserProfileurl)
        if let url = URL(string: ProfileUrl ?? ""){
            img.af_setImage(withURL: url, placeholderImage: UIImage(named: "profile_updateicon"))
        }
        
        lblUsername.text = UserDefaults.standard.string(forKey: UserDefaults.Keys.kUserName)

    }
    
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
            
        case .prpfile:
            self.slideMenuController()?.changeMainViewController(self.MyProfileVC, close: true)
        case .contactUs :
            self.slideMenuController()?.changeMainViewController(self.ContactUsVC, close: true)
            
        case .termsCondition:
            
            objAppShareData.isLonchXLPageIndex = true

            leftVC.PageType = "Terms & Conditions"
            self.slideMenuController()?.changeMainViewController(self.staticPageVC, close: true)

        case .privacyPolicy:
            
            objAppShareData.isLonchXLPageIndex = true

            leftVC.PageType = "Privacy Policy"

        self.slideMenuController()?.changeMainViewController(self.staticPagePrivacyVC, close: true)

        case .reviewApp:
            
            let urlStr = "https://apps.apple.com/in/app/wolfscore-live-football-score/id1466739915"
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: urlStr)!, options: [:]
                    , completionHandler: nil)
                
            } else {
                UIApplication.shared.openURL(URL(string: urlStr)!)
            }
            self.slideMenuController()?.closeLeft()
            
            
        case .logout:
            
            let alert = UIAlertController(title: "WolfScore", message: "Are you sure you want to log out?", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Logout", style: UIAlertAction.Style.destructive, handler: { action in
                
                self.LogoutWebservicesCall()
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func changeViewControllerGuest(_ menu: LeftMenuGuest) {
        switch menu {
            
        case .contactUs :
            objAppShareData.isLonchXLPageIndex = true
 self.slideMenuController()?.changeMainViewController(self.ContactUsVC, close: true)
            
        case .favorite:
            
            self.slideMenuController()?.changeMainViewController(self.Myfavorite, close: true)

        case .termsCondition:
            objAppShareData.isLonchXLPageIndex = true

            leftVC.PageType = "Terms & Conditions"
            self.slideMenuController()?.changeMainViewController(self.staticPageVC, close: true)

        case .privacyPolicy:
            leftVC.PageType = "Privacy Policy"
            objAppShareData.isLonchXLPageIndex = true
            self.slideMenuController()?.changeMainViewController(self.staticPagePrivacyVC, close: true)

        case .reviewApp:
            
            let urlStr = "https://apps.apple.com/in/app/wolfscore-live-football-score/id1466739915"
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: urlStr)!, options: [:]
                    , completionHandler: nil)
                
            } else {
                UIApplication.shared.openURL(URL(string: urlStr)!)
            }
            self.slideMenuController()?.closeLeft()

            
        case .login:
            self.slideMenuController()?.closeLeft()
            UserDefaults.standard.setValue(nil, forKey: UserDefaults.Keys.kGuestLogin)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            _ = appDelegate.Go_ToWelcomeScreen()

        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
}


extension LeftViewController : UITableViewDataSource,UITableViewDelegate {
  
    func numberOfSections(in tableView: UITableView) -> Int{
        return self.arrheading.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.arrheading[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.arrInfo[section].count
    }
    
    
     func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        //header.backgroundView?.backgroundColor = .clear
        header.backgroundView?.backgroundColor = UIColor.colorConstant.appDarkBlack
        header.textLabel?.textColor = UIColor.colorConstant.appLightBlueColor
        header.textLabel?.font = UIFont(name: "Roboto-Bold", size: 18)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as? SideMenuCell{
            cell.lblTitle.text = self.arrInfo[indexPath.section][indexPath.row]
            
            if cell.lblTitle.text == "Contact Us" || cell.lblTitle.text == "Terms & Conditions" || cell.lblTitle.text == "Privacy Policy" || cell.lblTitle.text == "My Profile" || cell.lblTitle.text == "Matches"  || cell.lblTitle.text == "Favorite"
            {
                cell.imgArrow.isHidden = false
            }
            else
            {
                cell.imgArrow.isHidden = true
            }
            
            cell.img.image = self.arrImage[indexPath.section][indexPath.row]
           return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let userId = UserDefaults.standard.string(forKey: UserDefaults.Keys.kUserId)
        if userId == nil{
            
            if let menu = LeftMenuGuest(rawValue: indexPath.row) {
                self.changeViewControllerGuest(menu)
            }
        }
            // for user
        else{
            
            if let menu = LeftMenu(rawValue: indexPath.row) {
                self.changeViewController(menu)
            }
        }
    }
}
//MARK :- call_Webservice
extension LeftViewController{
    
    
    func resetDefaults() {
        
        let defaults = UserDefaults.standard
        
        let UserEmail = defaults.string(forKey: UserDefaults.Keys.kEmail)
        let UserPassword = defaults.string(forKey: UserDefaults.Keys.kRMPassword)
        let rememberMe = defaults.string(forKey: UserDefaults.Keys.kIsRemember)
        let arrCountry = UserDefaults.standard.array(forKey: UserDefaults.Keys.kCountryArr) as? [[String:Any]] ?? []

        
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        
        defaults.setValue(rememberMe , forKey: UserDefaults.Keys.kIsRemember)
        defaults.setValue(UserEmail , forKey: UserDefaults.Keys.kEmail)
        defaults.setValue(UserPassword , forKey: UserDefaults.Keys.kRMPassword)
        defaults.setValue(arrCountry , forKey: UserDefaults.Keys.kCountryArr)

    }
    
    func LogoutWebservicesCall()
    {
        
        SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.logout, params: nil, success: { (response) in
            SVProgressHUD.dismiss()
            _ = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                print("scucess")
                self.slideMenuController()?.closeLeft()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                _ = appDelegate.Go_ToWelcomeScreen()
                self.resetDefaults()
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    func call_Webservice_GetContent() {
        SVProgressHUD.show()
        objWebServiceManager.requestGet(strURL: webUrl.GetContent, params: nil, success: { (response) in
            print(response)
            SVProgressHUD.dismiss()
            //let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    leftVC.strTermsUrl = data["term_condition"] as? String ?? ""
                    leftVC.strPrivacyUrl = data["privacy_policy"] as? String ?? ""
                    leftVC.strContactUsDescription = data["contact_us"] as? String ?? ""
                    leftVC.strMail = data["mail"] as? String ?? ""
                }
                
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}





