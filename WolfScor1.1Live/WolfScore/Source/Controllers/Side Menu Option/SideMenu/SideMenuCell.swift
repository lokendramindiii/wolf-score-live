//
//  SideMenuCell.swift
//  WolfScore
//
//  Created by mac on 22/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var img:UIImageView!
    @IBOutlet var imgArrow:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
