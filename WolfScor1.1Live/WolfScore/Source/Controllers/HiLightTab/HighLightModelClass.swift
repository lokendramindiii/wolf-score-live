//
//  HighLightModelClass.swift
//  WolfScore
//
//  Created by mac on 24/04/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit


class HighLightModelClass: NSObject {
    
    var hightLight_id = ""
    var hightLight_url = ""
    var hightLight_date = ""
    var thumbImage:UIImage?
    var localTeamName = ""
    var visitorName = ""

    
    init(dict: [String : Any]) {
        super.init()
    
        if let location = dict["location"] as? String{
            hightLight_url = location
            let url = URL(string: location)
            //thumbImage = self.videoPreviewUIImage(moviePath: url!)
            // thumbImage = self.generateImage(moviePath: url!)
        }
        
        if let fixture_id = dict["fixture_id"] as? String{
            hightLight_id = fixture_id
        }
        
        if let fixture = dict["fixture"] as? [String:Any]{
            
            if   let fixturedata = fixture["data"] as? [String:Any]
            {
                
                if let dataLocalTeam = fixturedata["localTeam"] as? [String:Any]
                {
                    if   let name = dataLocalTeam["data"] as? [String:Any]
                    {
                        let localteamName = name["name"] as? String ?? ""
                        localTeamName = localteamName
                    }
                }
                
                if let datavisitorTeam = fixturedata["visitorTeam"] as? [String:Any]
                {
                    if   let name = datavisitorTeam["data"] as? [String:Any]
                    {
                        let visitorteamName = name["name"] as? String ?? ""
                        
                        visitorName = visitorteamName
                    }
                }

                
            }
        }
        
        if let created_at = dict["created_at"] as? [String: Any]{
            if let date = created_at["date"] as? String{
                hightLight_date = dayDate.CompareDate(strFirstDate: date, currentDate: Date())
            }
        }
        
    }
    
    
//
//    local team name
//
//    vs
//
//    vistor team name
//
    
    

    
    
    func videoPreviewUIImage(moviePath: URL) -> UIImage? {
        let asset = AVURLAsset(url: moviePath)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        let timestamp = CMTime(seconds: 2, preferredTimescale: 60)
        if let imageRef = try? generator.copyCGImage(at: timestamp, actualTime: nil) {
            return UIImage(cgImage: imageRef)
        } else {
            return nil
        }
    }
    
    func generateImage(moviePath: URL) -> UIImage {
        var image = UIImage()
        let asset = AVAsset.init(url: moviePath)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true;
        
        let maxSize = CGSize(width: 400, height: 300)
        assetImageGenerator.maximumSize = maxSize
        var time = asset.duration
        time.value = min(time.value, 2)
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            image =  UIImage.init(cgImage: imageRef)
            return image
        } catch {
        }
        return image
    }
}
