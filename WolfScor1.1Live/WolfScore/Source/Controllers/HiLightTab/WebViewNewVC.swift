//
//  WebViewVC.swift
//  Habito
//
//  Created by Mindiii 2 on 21/12/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import SVProgressHUD

class WebViewNewVC: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var lblHeaderText: UILabel!
    @IBOutlet weak var webViewOpen: UIWebView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgMenu: UIImageView!
    var strUrl = ""
}

// MARK: - View LifeCycle
extension WebViewNewVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnBack.isHidden = false
        self.imgBack.isHidden = false
        self.btnMenu.isHidden = true
        self.imgMenu.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SVProgressHUD.show()
        webViewOpen.delegate = self
        webViewOpen.backgroundColor = UIColor.clear
        configureView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - Local Func
extension WebViewNewVC{
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configureView(){
        //if objAppShareData.isFrom {
            self.lblHeaderText.text = "Terms & Policies"
            if strUrl == ""{
            }else{
                let url = NSURL (string:strUrl)!
            let requestObj = NSURLRequest(url: url as URL);
            webViewOpen.loadRequest(requestObj as URLRequest)
            }
      }
}

//MARK :- Button Action
extension WebViewNewVC{
    @IBAction func btnBackAction(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
}
extension WebViewNewVC{
   func webViewDidStartLoad(_ webView: UIWebView) {
    }
   func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
      SVProgressHUD.dismiss()
    }
   func webViewDidFinishLoad(_ webView: UIWebView){
      SVProgressHUD.dismiss()
    }
}

