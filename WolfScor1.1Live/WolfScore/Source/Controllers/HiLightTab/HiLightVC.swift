//
//  HiLightVC.swift
//  WolfScore
//
//  Created by Mindiii on 28/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import XLPagerTabStrip
import SVProgressHUD
import AlamofireImage
import GoogleMobileAds

class HiLightVC: UIViewController,AVPlayerViewControllerDelegate {

    
    var bannerView: GADBannerView!
    @IBOutlet weak var viewBannerBottom: UIView!

    @IBOutlet weak var lblNoRecodFound:UILabel!
    @IBOutlet weak var tblMedia:UITableView!
    
    var arrMedia = [HighLightModelClass]()
    
    var currentPageIndex = 1
    var totalPages = 0
    var isLoadData = false
    var placeholderRow = 10
    

    weak var timer: Timer?

    var pullToRefreshCtrl:UIRefreshControl!
    var Ispulltorefresh = false

    var shouldLoadMore = false {
        didSet {
            if shouldLoadMore == false {
                self.tblMedia.tableFooterView = UIView()
            } else {
                self.tblMedia.tableFooterView = AppShareData.loadingFooter()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.setPullToRefresh()
         self.call_Webservice_Get_Match_Highlight()
         self.bannerAdSetup()
        // Do any additional setup after loading the view.
    }

    
    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tblMedia.refreshControl = pullToRefreshCtrl
        }else{
            tblMedia.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        sender.endRefreshing()
//        if Ispulltorefresh == false
//        {
            currentPageIndex = 1
            shouldLoadMore = false
            self.call_Webservice_Get_Match_Highlight()
            sender.endRefreshing()
   //     }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        selected_my_Index = 3
        if APP_DELEGATE.isInterstialPresent == true
        {
            return
        }
        objAppShareData.isLonchXLPageIndex = true
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnMenuAction(_ sender : UIButton){
        self.view.endEditing(true)
        self.slideMenuController()?.toggleLeft()
    }

    @objc func btnPlayPressed(sender: UIButton){
        let objTag = self.arrMedia[sender.tag]
        if objTag.hightLight_url != "" {
            //let url = URL(string: "https://www.radiantmediaplayer.com/media/bbb-360p.mp4")
            let url = URL(string: objTag.hightLight_url)
            let player = AVPlayer(url:url!)
            let playerController = AVPlayerViewController()
            playerController.player = player
            playerController.allowsPictureInPicturePlayback = true
            playerController.delegate = self
            playerController.player?.play()
            self.present(playerController,animated:true,completion:nil)
        }
    }
}



//MARK :- call_Webservice
extension HiLightVC{
    
    func call_Webservice_Get_Match_Highlight() {
        
      //  SVProgressHUD.show()
        
        let paramDict = ["page":self.currentPageIndex] as [String:AnyObject]
        objWebServiceManager.requestGet(strURL: webUrl.get_highlights, params: paramDict, success: { (response) in
            
            self.isLoadData = true
            
            let status = response["status"] as? String ?? ""
            if status == "success"{
                if let data = response["data"] as? [String:Any] {
                    
                    if let metadata = data ["meta"] as? [String:Any]
                    {
                        let data1 = metadata["pagination"] as? [String:Any]
                        let TotalPages = data1?["total_pages"] as? Int  ?? 0
                        self.totalPages = TotalPages
                    }

                    if let dicArray = data["data"] as? [[String: Any]]{
                        
                        
                        if self.currentPageIndex == 1{
                            self.arrMedia.removeAll()
                            self.tblMedia.reloadData()
                        }
                        
                        for dic in dicArray{
                            let objectMedia =  HighLightModelClass.init(dict: dic)
                            self.arrMedia.append(objectMedia)
                        }
                        
                        if self.currentPageIndex < self.totalPages{
                            self.currentPageIndex += 1
                            self.shouldLoadMore = true
                        }
                        
                        if self.arrMedia.count == 0{
                            self.lblNoRecodFound.isHidden = false
                        }else{
                            self.lblNoRecodFound.isHidden = true
                        }
                        print(self.arrMedia.count)
                        self.tblMedia.reloadData()
                    }
                }
            }
            SVProgressHUD.dismiss()

        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}

//MARK: - tableView delegate method
extension HiLightVC:UITableViewDelegate,UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoadData == false{
            return placeholderRow
        }
        else
        {
        return arrMedia.count
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (arrMedia.count - 1) && shouldLoadMore == true {
           self.shouldLoadMore = false
           self.call_Webservice_Get_Match_Highlight()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "MediaCell"

        if isLoadData == false{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MediaCell
            
            cell.show_skelton()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MediaCell
        
        let objMedia = arrMedia[indexPath.row]
        cell.hide_skelton()
        cell.imgViewVedio.image = objMedia.thumbImage
        cell.lblTitle.text = objMedia.localTeamName + "  vs  " + objMedia.visitorName
        cell.lblTime.text = objMedia.hightLight_date
        cell.btnPlay.tag = indexPath.row
        cell.btnPlay.addTarget(self, action: #selector(btnPlayPressed(sender:)), for: .touchUpInside)
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isLoadData == false
        {
            return
        }
        
        let viewController = UIStoryboard(name: "UserTabbar",bundle: nil).instantiateViewController(withIdentifier: "WebViewNewVC") as! WebViewNewVC
        let objTag = self.arrMedia[indexPath.row]
        viewController.strUrl = objTag.hightLight_url
    self.navigationController?.pushViewController(viewController, animated: true)
    }
}


extension HiLightVC:GADBannerViewDelegate
{
    func bannerAdSetup()
    {
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        bannerView.adUnitID = objAppShareData.BannerId
        bannerView.rootViewController = self
        
        let request: GADRequest = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.load(request)
        bannerView.delegate = self
        

        
    }
    
    /// Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
        
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView)
    {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        viewBannerBottom.center = bannerView.center
        viewBannerBottom.addSubview(bannerView)
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerX, multiplier: 1, constant: 0))
        
        viewBannerBottom.addConstraint(NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: viewBannerBottom, attribute: .centerY, multiplier: 1, constant: 0))
    }
}
