//
//  ModelLineup.swift
//  WolfScore
//
//  Created by Mindiii on 2/20/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ModelLineup: NSObject {

    var localTeamFormation  = ""
    var visitorTeamFormation = ""
    var  arrLinup = [ArrayLinup]()
    var  arrBench = [ArrayBenchModel]()
    var  arrSubstitution = [ModelSubstitution]()

    var localTeamId = ""
    var visitorTeamId = ""
    var localTeamName = ""
    var visitorTeamName = ""

    init(localdictionary  dictionary:[String:Any]){
        
        // local team
        if let localTeam = dictionary["localTeam"] as? [String:Any] {
           let data = localTeam["data"] as! [String:Any]
           localTeamName = data["name"] as? String ?? ""
        }
        // visitor team
        if let vistorteam = dictionary["visitorTeam"] as? [String:Any] {
            let data = vistorteam["data"] as! [String:Any]
            visitorTeamName = data["name"] as? String ?? ""
        }
        
        if let teamid = dictionary["localteam_id"] as? Int {
            localTeamId = String(describing: teamid)
        }
        else  {
            localTeamId = dictionary["localteam_id"] as? String ?? ""
        }
        
        if let teamid = dictionary["visitorteam_id"] as? Int {
            visitorTeamId = String(describing: teamid)
        }
        else {
            visitorTeamId = dictionary["visitorteam_id"] as? String ?? ""
        }
        
        // formation
        if let foramation = dictionary["formations"] as? [String:Any] {
            
            localTeamFormation = foramation ["localteam_formation"] as? String ?? "4-4-2"
            visitorTeamFormation = foramation ["visitorteam_formation"] as? String  ?? "4-4-2"
        }
        // substitution
        
        if let substitutionDic = dictionary["substitutions"] as? [String:Any] {
            
            var subsData = [[String:Any]]()
            subsData = substitutionDic["data"] as! [[String:Any]]
            for i in 0..<subsData.count
            {
                let value =   ModelSubstitution.init(localdictionary: subsData, index: i)
                arrSubstitution.append(value)
            }
        }
        
        
        // arrlinup
        if let linupDic = dictionary["lineup"] as? [String:Any] {
            
            var linupData = [[String:Any]]()
            linupData = linupDic["data"] as! [[String:Any]]
            for i in 0..<linupData.count
            {
                let value =   ArrayLinup.init(localdictionary: linupData, index: i)
                arrLinup.append(value)
            }
        }
        
        // Bench
        if let benchDic = dictionary["bench"] as? [String:Any] {
            
            var benchData = [[String:Any]]()
            benchData = benchDic["data"] as! [[String:Any]]
            for i in 0..<benchData.count
            {
                let value =   ArrayBenchModel.init(localdictionary: benchData, index: i)
                arrBench.append(value)
            }
        }
    }
}


class ArrayLinup: NSObject {
    
    var teamId = ""
    var playerName : String!
    var position : String!
    var number = ""
    var playerId  = ""
    var formationPosition  = ""
    var commonName : String!
    var imagePath : String!
    var birthdayPlace : String!
    var nationality : String!
    var yellowCards = ""
    var redCards = ""
    
    var playerIn_Id = ""
    var playerOut_Id = ""
    var minute = ""

    init(localdictionary  array:[[String:Any]] , index:Int){
        
        if let teamid = array[index]["team_id"] as? Int {
            teamId = String(describing: teamid)
        }
        else  {
            teamId = array[index]["team_id"] as? String ?? ""
        }
        
        if let playerid = array[index]["player_id"] as? Int {
            playerId = String(describing: playerid)
        }
        else  {
            playerId = array[index]["player_id"] as? String ?? ""
        }
        
        playerName = array[index]["player_name"] as? String  ?? ""
        position = array[index]["position"] as? String ?? ""
        
        if let Number = array[index]["number"] as? Int {
            number = String(describing: Number)
        }
        else
        {
            number = array[index]["number"] as? String  ?? ""
        }
        
        if let FormationPosition = array[index]["formation_position"] as? Int {
            formationPosition = String(describing: FormationPosition)
        }
        else
        {
            formationPosition = array[index]["formation_position"] as? String  ?? ""
        }
        
        if let Palyerdic = array[index]["player"] as? [String:Any]
        {
            if  let data = Palyerdic["data"] as? [String : Any]
            {
                commonName = data["common_name"] as? String ?? ""
                imagePath = data["image_path"] as? String ?? ""
                birthdayPlace = data["birthplace"] as? String ?? ""
                nationality = data["nationality"] as? String ?? ""
            }
        }
        
        if let stats = array[index]["stats"] as? [String:Any]
        {
            if  let data = stats["cards"] as? [String : Any]
            {
                if let YellowCard = data["yellowcards"] as? Int
                {
                    yellowCards = String(describing: YellowCard)
                }
                if let RedCard = data["redcards"] as? Int
                {
                    redCards = String(describing: RedCard)
                }
            }
        }
        
    }
}



class ArrayBenchModel: NSObject {
    
    var teamId = ""
    var playerName : String!
    var position : String!
    var number = ""
    var playerId  = ""
    var formationPosition  = ""
    var commonName : String!
    var imagePath : String!
    var birthdayPlace : String!
    var nationality : String!
    var yellowCards = ""
    var redCards = ""
    
    var playerIn_Id = ""
    var playerOut_Id = ""
    var minute = ""

    init(localdictionary  array:[[String:Any]] , index:Int){
        
        if let teamid = array[index]["team_id"] as? Int {
            teamId = String(describing: teamid)
        }
        else  {
            teamId = array[index]["team_id"] as? String ?? ""
        }
        
        if let playerid = array[index]["player_id"] as? Int {
            playerId = String(describing: playerid)
        }
        else  {
            playerId = array[index]["player_id"] as? String ?? ""
        }
        
        playerName = array[index]["player_name"] as? String  ?? ""
        position = array[index]["position"] as? String ?? ""
        
        if let Number = array[index]["number"] as? Int {
            number = String(describing: Number)
        }
        else
        {
            number = array[index]["number"] as? String  ?? ""
        }
        
        if let FormationPosition = array[index]["formation_position"] as? Int {
            formationPosition = String(describing: FormationPosition)
        }
        else
        {
            formationPosition = array[index]["formation_position"] as? String  ?? ""
        }
        
        if let Palyerdic = array[index]["player"] as? [String:Any]
        {
            if  let data = Palyerdic["data"] as? [String : Any]
            {
                commonName = data["common_name"] as? String ?? ""
                imagePath = data["image_path"] as? String ?? ""
                birthdayPlace = data["birthplace"] as? String ?? ""
                nationality = data["nationality"] as? String ?? ""
            }
        }
        
        if let stats = array[index]["stats"] as? [String:Any]
        {
            if  let data = stats["cards"] as? [String : Any]
            {
               if let YellowCard = data["yellowcards"] as? Int
               {
                yellowCards = String(describing: YellowCard)
                }
                if let RedCard = data["redcards"] as? Int
                {
                    redCards = String(describing: RedCard)
                }
            }
        }
    }
}


class ModelSubstitution: NSObject {
    
    var teamId = ""
    var playerIn_Id = ""
    var playerOut_Id = ""
    var minute = ""
    
    init(localdictionary  array:[[String:Any]] , index:Int){
        
        if let teamid = array[index]["team_id"] as? Int {
            teamId = String(describing: teamid)
        }
        else  {
            teamId = array[index]["team_id"] as? String ?? ""
        }
        
        if let playerid = array[index]["player_in_id"] as? Int {
            playerIn_Id = String(describing: playerid)
        }
        else  {
            playerIn_Id = array[index]["player_in_id"] as? String ?? ""
        }
        
        if let min = array[index]["minute"] as? Int {
            minute = String(describing: min)
        }
        else  {
            minute = array[index]["minute"] as? String ?? ""
        }
        
        if let p_outid = array[index]["player_out_id"] as? Int {
            playerOut_Id = String(describing: p_outid)
        }
        else
        {
            playerOut_Id = array[index]["player_out_id"] as? String  ?? ""
        }
    }
}

