//
//  ModelMatchDetail.swift
//  WolfScore
//
//  Created by Mindiii on 2/9/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ModelMatchDetail: NSObject {

    var strStatus: String!
    var strDate : String!
    var strTime : String!
    var strDateTime : String!
    
    var strLocalTeamName: String!
    var strLocalLogoPath: String!
    var strLocalScore: String!
    var strLocalTeamId: String!
    
    var strVisitorTeamName: String!
    var strVisitorLogoPath: String!
    var strVisitorScore: String!
    var strVisitorTeamId: String!
    
    var  arrBenchModel = [[ModelBench]]()
    
    var  arrMatchFactModel = [ModelMatchFact]()
    
    var  visitorTeamFormation : String!
    var  localTeamFormation  : String!

    var  visitorRecentFormation : String = ""
    var  localRecentFormation  : String = ""

    // vinoad
    var strRefereeCommonName = ""
    var strVenueName = ""
    var strVenueCapacity = ""
    var strVenueAdress = ""
    var strVenueCity = ""
    var strLeagueName = ""
    //
    
    var strAttendance = ""

    init(fromDictionary dictionary: [String:Any])
    {
        //local team
        var localTeamDict:[String:Any]
        if let localDic = dictionary["localTeam"] as? [String:Any] {
            
            localTeamDict = (localDic["data"] as? [String:Any])!
            if let lcoalname = localTeamDict["name"]as? String
            {
                strLocalTeamName = lcoalname
            }
            if let logoPath = localTeamDict["logo_path"]as? String
            {
                strLocalLogoPath = logoPath
            }
            if let logoPath = localTeamDict["id"]as? Int
            {
                strLocalTeamId = String(logoPath)
            }
        }
        
        // Formation
        if let formation = dictionary["formations"] as? [String:Any] {
            
            if let lcoalFormation = formation["localteam_formation"]as? String
            {
                localTeamFormation = lcoalFormation
            }
            if let visitorFormation = formation["visitorteam_formation"]as? String
            {
                visitorTeamFormation = visitorFormation
            }
        }
        
       // visitor
        var visitorTeamDict:[String:Any]
        if let visitorlDic = dictionary["visitorTeam"] as? [String:Any] {
            
            visitorTeamDict = (visitorlDic["data"] as? [String:Any])!
            if let visitorName = visitorTeamDict["name"]as? String
            {
                strVisitorTeamName = visitorName
            }
            if let logoPath = visitorTeamDict["logo_path"]as? String
            {
                strVisitorLogoPath = logoPath
            }
            
            if let logoPath = visitorTeamDict["id"]as? Int
            {
                strVisitorTeamId = String(logoPath)
            }
        }
        
        // Team form ----start----
        
        if let standingDic = dictionary["standing_detail"] as? [String:Any] {

            var standingArray = [[String:Any]]()
            standingArray = standingDic["data"] as! [[String:Any]]
      
            print(standingArray.count)
            for i in 0..<standingArray.count
            {
                let  teamid = standingArray[i]["team_id"] as? Int
                let  StrTeamId:String = "\(teamid ?? 0)"
                if strVisitorTeamId  == StrTeamId
                {
                    visitorRecentFormation = standingArray[i]["recent_form"] as? String ?? ""
                }
                if strLocalTeamId == StrTeamId
                {
                    localRecentFormation = standingArray[i]["recent_form"] as? String ?? ""
                }
            }
        }
        // Team form ----close----

        // arrBench
        if let benchDic = dictionary["bench"] as? [String:Any] {
            
            var benchData = [[String:Any]]()
            benchData = benchDic["data"] as! [[String:Any]]
            
            for i in 0..<benchData.count
            {
                let value =   ModelBench.init(localdictionary: benchData, index: i)
                arrBenchModel.append([value])
            }
        }
       
       
        
        // arrMatchFact
        if let matchFactDict = dictionary["events"] as? [String:Any] {
            let matchFactData = matchFactDict["data"] as? [[String:Any]] ?? []
            for i in 0..<matchFactData.count
            {
                let value =   ModelMatchFact.init(localdictionary: matchFactData, index: i)
                if value.strType == "yellowcard" ||  value.strType == "redcard" || value.strType == "substitution" || value.strType == "goal"{
                    arrMatchFactModel.append(value)
                }
                //arrMatchFactModel.append(value)
            }
        }
        
        // Time
        if let timedic = dictionary["time"] as? [String:Any]
        {
            if let startingDict = timedic["starting_at"] as? [String:Any]  {
                strDate = startingDict["date"] as? String ?? ""
                strTime = startingDict["time"] as? String ?? ""
                
                strDateTime = startingDict["date_time"] as? String ?? ""
            }
            
            if let status = timedic["status"] as? String  {
                strStatus = status
            }
        }
        
        // score
        if let scoredict = dictionary["scores"] as? [String:Any]
        {
            if let localscore = scoredict["localteam_score"] as? String  {
                strLocalScore = localscore
            }
            else if let localscore = scoredict["localteam_score"] as? Int  {
                strLocalScore = String(localscore)
            }
            
            if let vistiorscore = scoredict["visitorteam_score"] as? String  {
                strVisitorScore = vistiorscore
            }
            else if let vistiorscore = scoredict["visitorteam_score"] as? Int  {
                strVisitorScore = String(vistiorscore)
            }
        }
        
        // Vinoad ref
        //referee
        if let refereeDic = dictionary["referee"] as? [String:Any] {
            
            if let localTeamDict = refereeDic["data"] as? [String:Any]{
                if let refereename = localTeamDict["common_name"]as? String
                {
                    strRefereeCommonName = refereename
                }
            }
            
            
        }
        //venue Dictionary
        if let venueDic = dictionary["venue"] as? [String:Any] {
            
            if let attendance = dictionary["attendance"] as? String
            {
                self.strAttendance = attendance
            }
            else if let attendance = dictionary["attendance"] as? Int
            {
                self.strAttendance = String(attendance)
            }
            
            
            if let venueDataDict = venueDic["data"] as? [String:Any]{
                if let name = venueDataDict["name"]as? String
                {
                    strVenueName = name
                }
                if let address = venueDataDict["address"]as? String
                {
                    strVenueAdress = address
                }
                if let capacity = venueDataDict["capacity"]as? String
                {
                    strVenueCapacity = capacity
                }
                if let city = venueDataDict["city"]as? String
                {
                    strVenueCity = city
                }
            }
        }
        
        
    // League Dictionary
        
        if let leagueDic = dictionary["league"] as? [String:Any] {
            
            if let data = leagueDic["data"] as? [String:Any]{
                if let name = data["name"]as? String
                {
                    strLeagueName = name
                }
            }
        }
    }
}


class ModelBench: NSObject {
    
    var teamId : String!
    var playerName : String!
    var position : String!
    var number : String!
    var playerId : String!
    
    init(localdictionary  array:[[String:Any]] , index:Int){
        
        if let teamid = array[index]["team_id"] as? String  {
            teamId = teamid
        }
        if let playerid = array[index]["player_id"] as? String  {
            playerId = playerid
        }
        if let playername = array[index]["player_name"] as? String  {
            playerName = playername
        }
        if let Position = array[index]["position"] as? String  {
            position = Position
        }
        if let Number = array[index]["number"] as? String  {
            number = Number
        }
    }
}

class ModelMatchFact: NSObject {
    
    var strTeamId = ""
    var strPlayerName = ""
    var strType = ""
    var strMinute = ""
    var strRelatedPlayerName = ""
    var strExtraMinute = ""
    var strReason = ""
    var strResult = ""
    var strInjuried = ""
    
    init(localdictionary  array:[[String:Any]] , index:Int){
        
        if let teamid = array[index]["team_id"] as? String  {
            strTeamId = teamid
        }
        if let playername = array[index]["player_name"] as? String  {
            strPlayerName = playername
        }
        if let relatedplayername = array[index]["related_player_name"] as? String  {
            strRelatedPlayerName = relatedplayername
        }
        if let typ = array[index]["type"] as? String  {
            strType = typ
        }
        if let min = array[index]["minute"] as? Int  {
            strMinute = String(min) + "'"
        }
        if let extraMin = array[index]["extra_minute"] as? String  {
            strExtraMinute = extraMin
        }
        if let reason = array[index]["reason"] as? String  {
            strReason = reason
        }
        
        if let result = array[index]["result"] as? String  {
            strResult = result
        }
        
        if let injuried = array[index]["injuried"] as? String  {
            strInjuried = injuried
        }
    }
}

