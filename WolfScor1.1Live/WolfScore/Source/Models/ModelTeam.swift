//
//  ModelTeam.swift
//  WolfScore
//
//  Created by Mindiii on 20/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ModelTeam: NSObject {
    var str_Team_id = ""
    var str_Team_name = ""
    var str_Team_Flag_Image = ""
    var isAddedToFav:Bool = false
    var str_add_Item = ""
    
    init(dict_Demo: [String : Any]) {
        if let status = dict_Demo["id"] as? String{
            str_Team_id = status
        }else if let status = dict_Demo["id"] as? Int{
            str_Team_id = String(status)
        }
        if let status = dict_Demo["image"] as? String{
            str_Team_Flag_Image = status
        }else if let status = dict_Demo["image"] as? Int{
            str_Team_Flag_Image = String(status)
        }
        if let status = dict_Demo["name"] as? String{
            str_Team_name = status
        }else if let status = dict_Demo["name"] as? Int{
            str_Team_name = String(status)
        }
    }

    
    var id = ""
    var team_id = ""
    var name = ""
    var country_id = ""
    var national_team = ""
    var founded = ""
    var logo_path = ""
    var venue_id = ""
    var uefaranking_postion = ""
    var created_on = ""
    var is_favorite = ""
   
    init(dict: [String : Any]) {
        if let status = dict["id"] as? String{
            id = status
        }else if let status = dict["id"] as? Int{
            id = String(status)
        }
        if let status = dict["team_id"] as? String{
            team_id = status
        }else if let status = dict["team_id"] as? Int{
            team_id = String(status)
        }
        if let status = dict["name"] as? String{
            name = status
        }else if let status = dict["name"] as? Int{
            name = String(status)
        }
        if let status = dict["country_id"] as? String{
            country_id = status
        }else if let status = dict["country_id"] as? Int{
            country_id = String(status)
        }
        if let status = dict["national_team"] as? String{
            national_team = status
        }else if let status = dict["national_team"] as? Int{
            national_team = String(status)
        }
        if let status = dict["logo_path"] as? String{
            logo_path = status
        }else if let status = dict["logo_path"] as? Int{
            logo_path = String(status)
        }
        if let status = dict["venue_id"] as? String{
            venue_id = status
        }else if let status = dict["venue_id"] as? Int{
            venue_id = String(status)
        }
        if let status = dict["uefaranking_postion"] as? String{
            uefaranking_postion = status
        }else if let status = dict["uefaranking_postion"] as? Int{
            uefaranking_postion = String(status)
        }
        if let status = dict["is_favorite"] as? String{
            is_favorite = status
        }else if let status = dict["is_favorite"] as? Int{
            is_favorite = String(status)
        }
    }
}



class ModelTopPlayer: NSObject {
    
    var id = ""
    var player_id = ""
    var team_id = ""
    var common_name = ""
    var full_name = ""
    var first_name = ""
    var last_name = ""
    var player_image = ""
    var team_logo = ""
    var isAddedToFav:Bool = false
    var country_name = ""
    var country_flag = ""
    var is_favorite = ""

    init(dict: [String : Any]) {
        if let status = dict["id"] as? String{
            id = status
        }else if let status = dict["id"] as? Int{
            id = String(status)
        }
        if let status = dict["player_id"] as? String{
            player_id = status
        }else if let status = dict["player_id"] as? Int{
            player_id = String(status)
        }
        if let status = dict["team_id"] as? String{
            team_id = status
        }else if let status = dict["team_id"] as? Int{
            team_id = String(status)
        }
        if let status = dict["common_name"] as? String{
            common_name = status
        }else if let status = dict["common_name"] as? Int{
            common_name = String(status)
        }
        if let status = dict["full_name"] as? String{
            full_name = status
        }else if let status = dict["full_name"] as? Int{
            full_name = String(status)
        }
        if let status = dict["first_name"] as? String{
            first_name = status
        }else if let status = dict["first_name"] as? Int{
            first_name = String(status)
        }
        if let status = dict["last_name"] as? String{
            last_name = status
        }else if let status = dict["venue_id"] as? Int{
            last_name = String(status)
        }
        if let status = dict["player_image"] as? String{
            player_image = status
        }else if let status = dict["player_image"] as? Int{
            player_image = String(status)
        }
        if let status = dict["team_logo"] as? String{
            team_logo = status
        }else if let status = dict["team_logo"] as? Int{
            team_logo = String(status)
        }
        if let status = dict["country_name"] as? String{
            country_name = status
        }else if let status = dict["country_name"] as? Int{
            country_name = String(status)
        }
        if let status = dict["country_flag"] as? String{
            country_flag = status
        }else if let status = dict["country_flag"] as? Int{
            country_flag = String(status)
        }
        if let status = dict["is_favorite"] as? String{
            is_favorite = status
        }else if let status = dict["is_favorite"] as? Int{
            is_favorite = String(status)
        }
    }
}

class ModelLeagueListGroupByTeam: NSObject {
    
    var leagueId = ""

    var leagueName = ""
    var leagueFlage = ""
    var isExpandation = false
    var arrTeamList = [ModelTeam]()
    
    init(fromDictionary dictionary:[String : Any]) {
        
        self.leagueId = dictionary["league_id"] as? String ?? ""
        self.leagueName = dictionary["name"] as? String ?? ""
        self.leagueFlage = dictionary["league_flag"] as? String ?? ""
        if let teams_list = dictionary["teams"] as? [[String:Any]]
        {
            for dict in teams_list{
                let obj = ModelTeam.init(dict: dict)
                self.arrTeamList.append(obj)
            }
        }
    }
    
}

class Model_Notification: NSObject {
    var notificationManageId = ""
    var user_id = ""
    var goal = ""
    var red_card = ""
    var yellow_card = ""
    var match_reminder = ""
    var match_start = ""
    var match_finish = ""

    var half_time = ""
    var full_time_result = ""
    
    init(dict: [String : Any]) {
        if let status = dict["notificationManageId"] as? String{
            notificationManageId = status
        }else if let status = dict["notificationManageId"] as? Int{
            notificationManageId = String(status)
        }
        if let status = dict["user_id"] as? String{
            user_id = status
        }else if let status = dict["user_id"] as? Int{
            user_id = String(status)
        }
        if let status = dict["goal"] as? String{
            goal = status
        }else if let status = dict["goal"] as? Int{
            goal = String(status)
        }
        if let status = dict["red_card"] as? String{
            red_card = status
        }else if let status = dict["red_card"] as? Int{
            red_card = String(status)
        }
        if let status = dict["yellow_card"] as? String{
            yellow_card = status
        }else if let status = dict["yellow_card"] as? Int{
            yellow_card = String(status)
        }
        if let status = dict["match_start"] as? String{
            match_start = status
        }else if let status = dict["match_start"] as? Int{
            match_start = String(status)
        }
        
        if let status = dict["match_finish"] as? String{
            match_finish = status
        }else if let status = dict["match_finish"] as? Int{
            match_finish = String(status)
        }
        
        if let status = dict["match_reminder"] as? String{
            match_reminder = status
        }else if let status = dict["match_reminder"] as? Int{
            match_reminder = String(status)
        }
        if let status = dict["half_time"] as? String{
            half_time = status
        }else if let status = dict["half_time"] as? Int{
            half_time = String(status)
        }
        if let status = dict["full_time_result"] as? String{
            full_time_result = status
        }else if let status = dict["full_time_result"] as? Int{
            full_time_result = String(status)
        }
    }
    
}

// leaguelistModelGroupByCountry
class ModelLeagueListGroupByCountry: NSObject {
    var countryId = ""
    var countryName = ""
    var countryFlage = ""
    var isExpandation = false
    
    var arrLeagueList = [ModelLeagueList]()
    
    init(fromDictionary dictionary:[String : Any]) {
      
        self.countryId = dictionary["country_id"] as? String ?? ""
        self.countryName = dictionary["country_name"] as? String ?? ""
        self.countryFlage = dictionary["country_flag"] as? String ?? ""
        
        if let league_list = dictionary["league_list"] as? [[String:Any]]
        {
            for dict in league_list{
                let obj = ModelLeagueList.init(fromDictionary: dict)
                self.arrLeagueList.append(obj)
            }
        
        }
    }
    
}


// leaguelistModel
class ModelLeagueList: NSObject {
    
    var countryId : String!
    var currentSeasonId : String!
    var currentRoundId : String!
    var countryName : String!
    var currentStageId : String!
    var isCup : String!
    var leagueFlage : String = ""
    var popularLeague : Int = 0

    var leagueId : String!
    var isSelected : String!
    var isFavorite : String!
    var leagueName : String!
    var liveStandings : String!
    
    init(fromDictionary dictionary:[String : Any]) {
        
        if let country_name = dictionary["country_name"] as? String  {
            countryName = country_name
        }
        
        if let popularleague = dictionary["popular_league"] as? String  {
            self.popularLeague = Int(popularleague)!
        }

        if let country_id = dictionary["country_id"] as? String  {
            countryId = country_id
        }
        if let isselectedID = dictionary["is_selected"] as? String  {
            isSelected = isselectedID
        }
        
        if let is_favorite = dictionary["is_favorite"] as? String  {
            isFavorite = is_favorite
        }
        
        if let currentSeason_id = dictionary["current_season_id"] as? String  {
            currentSeasonId = currentSeason_id
        }
        if let currentRound_id = dictionary["current_round_id"] as? String  {
            currentRoundId = currentRound_id
        }
        if let currentStage_id = dictionary["current_stage_id"] as? String  {
            currentStageId = currentStage_id
        }
        if let status = dictionary["is_cup"] as? String  {
            isCup = status
        }
        
        leagueFlage = dictionary["league_flag"] as? String ?? ""
      
        if let league_id = dictionary["league_id"] as? String  {
            leagueId = league_id
        }
        if let leauge_name = dictionary["league_name"] as? String  {
            leagueName = leauge_name
        }
        
        if let liveStanding = dictionary["live_standings"] as? String  {
            liveStandings = liveStanding
        }
    }
}

// NewslistModel
class ModelNewsList: NSObject {
    
    var title : String = ""
    var url : String = ""
    var uri_newsId : String = ""
    var dataType : String = ""
    var image : String = ""
    var body : String = ""
    var date : String = ""
    var time : String  = ""
    var dateTime : String = ""
    var dateTimeManual : String = ""
    var date_at_time : String = ""

    init(fromDictionary dictionary:[String : Any]) {
        
        if let NewsTitle = dictionary["title"] as? String  {
            title = NewsTitle
           }
        
        if let uri = dictionary["uri"] as? String
        {
            uri_newsId = uri
        }
        
        if let NewsUrl = dictionary["url"] as? String
        {
            url = NewsUrl
        }
        
        if let DataType = dictionary["dataType"] as? String{
            dataType = DataType
        }
        
        if let NewsImage = dictionary["image"] as? String  {
            image = NewsImage
        }
        
        if let NewsBody = dictionary["body"] as? String  {
            body = NewsBody
        }
        
        if let NewsDate = dictionary["date"] as? String  {
            date = NewsDate
        }
        
        if let NewsTime = dictionary["time"] as? String  {
            time = NewsTime
        }
        
        
        
        let MergeDateTime = "\(date) \(" ") \(time)"
        
  
        let df = DateFormatter()//2019-04-16 19:00:00
        df.dateFormat = "YYYY-MM-dd HH:mm:ss"
        df.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let date1 = df.date(from: MergeDateTime)


        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"

       //dateFormatter.timeZone = NSTimeZone.local
       // dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        
        
        dateTimeManual = dayDate.CompareDateNews(strFirstDate: MergeDateTime, currentDate: Date())

    
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.amSymbol = "Am"
        dateFormatter.pmSymbol = "Pm"
        var timeat: String = dateFormatter.string(from: date1 ?? Date())
        timeat = " at \(timeat)"

        dateFormatter.dateFormat = "MMMM dd, YYYY"

        let DateString: String = dateFormatter.string(from: date1 ?? Date())

        date_at_time = DateString + timeat
        

        if let NewsDateTime = dictionary["dateTime"] as? String  {
            dateTime = NewsDateTime
        }
    }
}


class ModelSubscriptionList: NSObject {
    
    var str_description:String = ""
    var str_duration:String = ""
    var str_duration_type:String = ""
    var str_ios_product_id:String = ""
    var str_level:String = ""
    var str_price:String = ""
    var str_status:String = ""
    var str_subscriptionPlanId:String = ""
    var str_title:String = ""

    init(fromdictionary dictionary:[String:Any]) {
        
        
        if let description = dictionary["description"] as? String  {
            str_description = description
        }
        
        if let duration = dictionary["duration"] as? String
        {
            str_duration = duration
        }
        
        if let durationType = dictionary["duration_type"] as? String
        {
            str_duration_type = durationType
        }
        
        if let iosProductId = dictionary["ios_product_id"] as? String{
            str_ios_product_id = iosProductId
        }
        
        if let level = dictionary["level"] as? String  {
            str_level = level
        }
        
        if let price = dictionary["price"] as? String  {
            str_price = price
        }
        
        if let status = dictionary["status"] as? String  {
            str_status = status
        }
        
        if let subscriptionPlanId = dictionary["subscriptionPlanId"] as? String  {
            str_subscriptionPlanId = subscriptionPlanId
        }
        
        if let title = dictionary["title"] as? String  {
            str_title = title
        }
    }
}
