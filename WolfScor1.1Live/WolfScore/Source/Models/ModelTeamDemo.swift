//
//  ModelTeamDemo.swift
//  WolfScore
//
//  Created by Mindiii on 28/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ModelTeamDemo: NSObject {
    var str_Team_id = ""
    var str_Team_name = ""
    var str_Team_Flag_Image = ""
    var isAddedToFav:Bool = false
    
    
    init(dict: [String : Any]) {
        if let status = dict["id"] as? String{
            str_Team_id = status
        }else if let status = dict["id"] as? Int{
            str_Team_id = String(status)
        }
        if let status = dict["image"] as? String{
            str_Team_Flag_Image = status
        }else if let status = dict["image"] as? Int{
            str_Team_Flag_Image = String(status)
        }
        if let status = dict["name"] as? String{
            str_Team_name = status
        }else if let status = dict["name"] as? Int{
            str_Team_name = String(status)
        }
    }
}
