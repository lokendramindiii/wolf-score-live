//
//  MatchesModelHeadToHead.swift
//  WolfScore
//
//  Created by Mindiii on 2/7/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class MatchesModelHeadToHead: NSObject {
    
    
//    season =                 {
//    data =                     {
//    "current_round_id" = 161698;
//    "current_stage_id" = 77442049;
//    id = 15610;
//    "is_current_season" = 1;
//    "league_id" = 779;
//    name = 2019;
//    };
//    };

    
    var strLeagueName = ""
    var strRoundName = ""
    var strStatus = ""
    var strDate = ""
    var strTime = ""
    var fixtureId = ""
    
    var strLocalTeamScore : String!
    var strLocalTeamId : String!
    var strVisitorTeamScore : String!
    var strVisitorTeamId : String!
    
    var strLogopathLocalTeam : String = ""
    var strLogopathVisitorTeam : String = ""
    
    var strNameloacalTeam : String!
    var strNameVisitorTeam : String!
    var seasonId : String = ""

    init(fromDictionary dictionary: [String:Any])
    {
        
        seasonId = dictionary["season_id"] as? String ?? ""
        if let season = dictionary["season_id"] as? Int
        {
            seasonId = String(season)
        }
        
        if let leaugeid = dictionary["id"] as? String  {
            fixtureId = leaugeid
        }else if let leaugeid = dictionary["id"] as? Int  {
            fixtureId = String(leaugeid)
        }
        
        // Localteam and visitor team
        var diclocal : [String:Any]
        let localteam = dictionary["localTeam"] as! [String:Any]
        
        diclocal = localteam["data"] as! [String : Any]
        var dicvisitor : [String:Any]
        let visitorTeam = dictionary["visitorTeam"] as? [String:Any]
        dicvisitor = visitorTeam?["data"] as! [String : Any]
        let score = dictionary["scores"] as? [String:Any]
        let time = dictionary["time"] as? [String:Any]
        
        //////////////////
        var dicleague : [String:Any]
        let league = dictionary["league"] as? [String:Any]
        dicleague = league?["data"] as! [String : Any]
        
        if let status = dicleague["name"] as? String{
            strLeagueName = status
        }else if let status = dicleague["name"] as? Int{
            strLeagueName = String(status)
        }
        
        //////////////
        //////////////////
        //var dicRound : [String:Any]
        if  let round = dictionary["round"] as? [String:Any]
        {
           if let dicRound = round["data"] as? [String : Any]
           {
            if let status = dicRound["name"] as? String {
                strRoundName = status
               //let status = response["status"] as? String ?? ""
            }else if let status = dicRound["name"] as? Int{
                strRoundName = String(status)
            }
        }
    }
        //////////////
        //super.init(localdictionary: localteam , visitordictionary: [String:Any], scoredict: [String:Any] ,timedict:[String:Any]){
        // Time
        if let status = time?["status"] as? String  {
            strStatus = status
        }

        if let startingDict = time?["starting_at"] as? [String:Any]  {
            let Date = startingDict["date"] as? String ?? ""
            strTime = startingDict["time"] as? String ?? ""
            if Date != ""{
              strDate = dayDate.dateFormatedWithDayName(strDate: Date)
            }
            
        }
        
        // score
        if let localscore = score?["localteam_score"] as? String  {
            strLocalTeamScore = localscore
        }else if let localscore = score?["localteam_score"] as? Int  {
            strLocalTeamScore = String(localscore)
        }
        
        
        if let vistiorscore = score?["visitorteam_score"] as? String  {
            strVisitorTeamScore = vistiorscore
        }else if let vistiorscore = score?["visitorteam_score"] as? Int  {
            strVisitorTeamScore = String(vistiorscore)
        }
        
        
        
        //  visitor team
        if let name = dicvisitor["name"] as? String  {
            strNameVisitorTeam = name
        }
        if let logopath = dicvisitor["logo_path"] as? String  {
            strLogopathVisitorTeam = logopath
        }
        
        strLogopathVisitorTeam = dicvisitor["logo_path"] as? String ?? ""
       
        
        if let vistiorscore = dicvisitor["id"] as? String  {
            strVisitorTeamId = vistiorscore
        }else if let vistiorscore = dicvisitor["id"] as? Int  {
            strVisitorTeamId = String(vistiorscore)
        }
        // local team
        if let name = diclocal["name"] as? String  {
            strNameloacalTeam = name
        }
      
        strLogopathLocalTeam = diclocal["logo_path"] as? String ?? ""
        
        // ID
        if let localscore = diclocal["id"] as? String  {
            strLocalTeamId = localscore
        }else if let localscore = diclocal["id"] as? Int  {
            strLocalTeamId = String(localscore)
        }
    }
}
