//
//  ModelGetFixer.swift
//  WolfScore
//
//  Created by Mindiii on 1/29/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class LeaugeModel: NSObject {

    var name = ""
    var bannerLeagueName = ""

    var leaugeId = ""
    var date = ""
    var countryFlag = ""
    
    var arrMatches = [MatchesModel]()
   
    init(fromDictionary dictionary: [String:Any],isPastMatches:Bool)
    {
        
        if let leaugeid = dictionary["league_id"] as? Int  {
            leaugeId = String(leaugeid)
        }
        
        var seasonId = ""
        seasonId = dictionary["season_id"] as? String ?? ""
        
        if let season = dictionary["season_id"] as? Int
        {
            seasonId = String(season)
        }
        
        var matchId = ""
        
        if let leaugeid = dictionary["id"] as? String  {
            matchId = leaugeid
        }else if let leaugeid = dictionary["id"] as? Int  {
            matchId = String(leaugeid)
        }
        var dictionaryleauge : [String:Any]
        
        if let league = dictionary["league"] as? [String:Any]  {
            dictionaryleauge = league["data"] as? [String : Any] ?? [:]
            if let Name = dictionaryleauge["name"] as? String  {
                name = Name
                bannerLeagueName = Name
            }
            
            let arrCountry = UserDefaults.standard.array(forKey: UserDefaults.Keys.kCountryArr) as? [[String:Any]] ?? []
            let country_id = dictionaryleauge["country_id"] as? Int  ?? 0
            for dict in arrCountry {
                let id = dict["country_id"] as? String ?? ""
                
                if id == String(country_id) {
                    
                   let countryName = dict["country_name"] as? String ?? ""
                    name = countryName + " - " + name
                    countryFlag = dict["country_flag"] as? String ?? ""
                    break
                }
            }
        }
        
        // Localteam and visitor team
        var diclocal : [String:Any]
        if let localteam = dictionary["localTeam"] as? [String:Any]
        {
            diclocal = localteam["data"] as! [String : Any]
            
            var dicvisitor : [String:Any]
            let visitorTeam = dictionary["visitorTeam"] as? [String:Any]
            dicvisitor = visitorTeam?["data"] as! [String : Any]
            let score = dictionary["scores"] as? [String:Any]
            let time = dictionary["time"] as? [String:Any]
           
            // goal
            var dictGoal = [String:Any]()
            if let arrGoals = dictionary["goals"] as? [[String:Any]]{
                for dict in arrGoals{
                    let obj = dict
                   dictGoal = dict
                }
            }
            
            let value = MatchesModel.init(localdictionary: diclocal, visitordictionary: dicvisitor , scoredict : score!, timedict: time!, fixture_Id: matchId, season_Id: seasonId, bannerLeagueName: "" )
            if isPastMatches == true {
                if value.strStatus != "NS"{
                    arrMatches.append(value)
                }
            }
            else{
                arrMatches.append(value)
                self.date = value.strDate
            }
            
        }
    }
}

// local team Model
class MatchesModel: NSObject {
    
    var matchType = ""
    
    var strbannerLeagueName = ""

    var strStatus : String!
    var strDate : String!
    var strTime : String!
    var fixtureId : String!
    var seasonId = ""

    var strLocalTeamScore : String!
    var strLocalTeamId : String!
    var strVisitorTeamScore : String!
    var strVisitorTeamId : String!


    var strNameloacalTeam : String!
    

    var strLogopathLocalTeam : String = ""
    var strLogopathVisitorTeam : String = ""
    
    var strNameVisitorTeam : String!
    
    init(localdictionary: [String:Any] , visitordictionary: [String:Any], scoredict: [String:Any] ,timedict:[String:Any],fixture_Id: String , season_Id:String,bannerLeagueName:String){
        // Time
        if let status = timedict["status"] as? String  {
            strStatus = status
        }
        
        strbannerLeagueName = bannerLeagueName
        
        fixtureId = fixture_Id
        seasonId = season_Id
        
        if let startingDict = timedict["starting_at"] as? [String:Any]  {
            strDate = startingDict["date"] as? String ?? ""
            strTime = startingDict["time"] as? String ?? ""
        }
        
        // score
        if let localscore = scoredict["localteam_score"] as? String  {
            strLocalTeamScore = localscore
        }else if let localscore = scoredict["localteam_score"] as? Int  {
            strLocalTeamScore = String(localscore)
        }
        
        if let vistiorscore = scoredict["visitorteam_score"] as? String  {
            strVisitorTeamScore = vistiorscore
        }else if let vistiorscore = scoredict["visitorteam_score"] as? Int  {
            strVisitorTeamScore = String(vistiorscore)
        }
        
        //  visitor team
        if let name = visitordictionary["name"] as? String  {
            strNameVisitorTeam = name
        }
        
        strLogopathVisitorTeam = visitordictionary["logo_path"] as? String ?? ""
         
        if let vistiorscore = visitordictionary["id"] as? String  {
            strVisitorTeamId = vistiorscore
        }else if let vistiorscore = visitordictionary["id"] as? Int  {
            strVisitorTeamId = String(vistiorscore)
        }
        // local team
        if let name = localdictionary["name"] as? String  {
            strNameloacalTeam = name
        }


        strLogopathLocalTeam = localdictionary["logo_path"] as? String ?? ""
        
        // ID
        if let localscore = localdictionary["id"] as? String  {
            strLocalTeamId = localscore
        }else if let localscore = localdictionary["id"] as? Int  {
            strLocalTeamId = String(localscore)
        }
    }
}



