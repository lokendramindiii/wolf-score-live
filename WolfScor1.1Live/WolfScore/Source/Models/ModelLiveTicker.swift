//
//  ModelLiveTicker.swift
//  WolfScore
//
//  Created by mac on 26/02/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class ModelLiveTicker: NSObject {
   
    var strOrder = 0
    var strMinute = ""
    var strExtraMinute = ""
    var isImportant:Bool = false
    var isGoal:Bool = false
    var strComment = ""
    
    init(dict: [String : Any]) {
        if let order = dict["order"] as? Int{
            strOrder = order
        }
        if let comment = dict["comment"] as? String{
            strComment = comment
        }
        if let minute = dict["minute"] as? Int{
            strMinute = String(minute)
        }
        if let extra_minute = dict["extra_minute"] as? Int{
            strExtraMinute = String(extra_minute)
        }
        if let important = dict["important"] as? Bool{
            isImportant = important
        }
        if let goal = dict["goal"] as? Bool{
            isGoal = goal
        }
    }

}



class ModelStatistics: NSObject {
    
    var arrCount = 0
    var strLocalTeam = ""
    var strLocTotalShots = ""
    var strLocOnTarget = ""
    var strLocOffTarget = ""
    var strLocBlocked = ""
    var strLocInsidebox = ""
    var strLocOutsidebox = ""
    var LocPossessionTime:Float?
    var strLocPossessionTime = ""
    
    var strVisitorTeam = ""
    var strVstTotalShots = ""
    var strVstOnTarget = ""
    var strVstOffTarget = ""
    var strVstBlocked = ""
    var strVstInsidebox = ""
    var strVstOutsidebox = ""
    var VstPossessionTime:Float?
    var strVstPossessionTime = ""
    
    var totalOfTotalShots = 0
    var totalOfShotsOnTarget = 0
    var totalOfShotsOffTarget = 0
    var totalOfBlockedShots = 0
    var totalOfShotsInsideBox = 0
    var totalOfShotsOutsideBox = 0
    
    var unfilledLocalOfTotalShots = 0
    var unfilledLocalOfShotsOnTarget = 0
    var unfilledLocalOfShotsOffTarget = 0
    var unfilledLocalOfBlockedShots = 0
    var unfilledLocalOfShotsInsideBox = 0
    var unfilledLocalOfShotsOutsideBox = 0
    
    var filledVisitorOfTotalShots = 0
    var filledVisitorOfShotsOnTarget = 0
    var filledVisitorOfShotsOffTarget = 0
    var filledVisitorOfBlockedShots = 0
    var filledVisitorOfShotsInsideBox = 0
    var filledVisitorOfShotsOutsideBox = 0
    
    init(dict: [String : Any]) {
        
        if let localteam_id = dict["localteam_id"] as? Int{
            strLocalTeam = String(localteam_id)
        }
        if let visitorteam_id = dict["visitorteam_id"] as? Int{
            strVisitorTeam = String(visitorteam_id)
        }
        
        if let dictStats = dict["stats"] as? [String:Any] {
            if let dicArray = dictStats["data"] as? [[String: Any]]{
                arrCount = dicArray.count
                for dic in dicArray{
                    var teamId = ""
                    if let team_id = dic["team_id"] as? Int{
                        teamId = String(team_id)
                    }
                    if teamId == strLocalTeam {
                        if let possessiontime = dic["possessiontime"] as? Int{
                            LocPossessionTime = Float(possessiontime)/100
                            
                            strLocPossessionTime = String(possessiontime)
                        }
                        if let dictShots = dic["shots"] as? [String:Any] {
                            
                            if let total = dictShots["total"] as? Int{
                                strLocTotalShots = String(total)
                            }
                            else if let total = dictShots["total"] as? String{
                                strLocTotalShots = String(total)
                            }
                            
                            if let ongoal = dictShots["ongoal"] as? Int{
                                strLocOnTarget = String(ongoal)
                            }
                            else if let ongoal = dictShots["ongoal"] as? String{
                                strLocOnTarget = String(ongoal)
                            }
                            
                            if let offgoal = dictShots["offgoal"] as? Int{
                                strLocOffTarget = String(offgoal)
                            }
                            else  if let offgoal = dictShots["offgoal"] as? String{
                                strLocOffTarget = String(offgoal)
                            }
                            
                            if let blocked = dictShots["blocked"] as? Int{
                                strLocBlocked = String(blocked)
                            }
                            else if let blocked = dictShots["blocked"] as? String{
                                strLocBlocked = String(blocked)
                            }
                            
                            if let insidebox = dictShots["insidebox"] as? Int{
                                strLocInsidebox = String(insidebox)
                            }
                            else if let insidebox = dictShots["insidebox"] as? String{
                                strLocInsidebox = String(insidebox)
                            }
                            
                            if let outsidebox = dictShots["outsidebox"] as? Int{
                                strLocOutsidebox = String(outsidebox)
                            }
                            if let outsidebox = dictShots["outsidebox"] as? String{
                                strLocOutsidebox = String(outsidebox)
                            }
                            
                        }
                    }else{
                        if let possessiontime = dic["possessiontime"] as? Int{
                            VstPossessionTime = Float(possessiontime)/100
                            strVstPossessionTime = String(possessiontime)
                        }
                        if let dictShots = dic["shots"] as? [String:Any] {
                            if let total = dictShots["total"] as? Int{
                                strVstTotalShots = String(total)
                            }
                            else if let total = dictShots["total"] as? String{
                                strVstTotalShots = String(total)
                            }
                            
                            if let ongoal = dictShots["ongoal"] as? Int{
                                strVstOnTarget = String(ongoal)
                            }
                            else  if let ongoal = dictShots["ongoal"] as? String{
                                strVstOnTarget = String(ongoal)
                            }
                            if let offgoal = dictShots["offgoal"] as? Int{
                                strVstOffTarget = String(offgoal)
                            }
                            else  if let offgoal = dictShots["offgoal"] as? String{
                                strVstOffTarget = String(offgoal)
                            }
                            if let blocked = dictShots["blocked"] as? Int{
                                strVstBlocked = String(blocked)
                            }
                            else if let blocked = dictShots["blocked"] as? String{
                                strVstBlocked = String(blocked)
                            }
                            if let insidebox = dictShots["insidebox"] as? Int{
                                strVstInsidebox = String(insidebox)
                            }
                            else if let insidebox = dictShots["insidebox"] as? String{
                                strVstInsidebox = String(insidebox)
                            }
                            if let outsidebox = dictShots["outsidebox"] as? Int{
                                strVstOutsidebox = String(outsidebox)
                            }
                            else  if let outsidebox = dictShots["outsidebox"] as? String{
                                strVstOutsidebox = String(outsidebox)
                            }
                        }
                    }
                    
                    totalOfTotalShots = (Int(strVstTotalShots) ?? 0) + (Int(strLocTotalShots) ?? 0)
                    totalOfShotsOnTarget = (Int(strVstOnTarget) ?? 0) + (Int(strLocOnTarget) ?? 0)
                    totalOfShotsOffTarget = (Int(strVstOffTarget) ?? 0) + (Int(strLocOffTarget) ?? 0)
                    totalOfBlockedShots = (Int(strVstBlocked) ?? 0) + (Int(strLocBlocked) ?? 0)
                    totalOfShotsInsideBox = (Int(strVstInsidebox) ?? 0) + (Int(strLocInsidebox) ?? 0)
                    totalOfShotsOutsideBox = (Int(strVstOutsidebox) ?? 0) + (Int(strLocOutsidebox) ?? 0)
                    
                    unfilledLocalOfTotalShots = Int(strVstTotalShots) ?? 0
                    unfilledLocalOfShotsOnTarget = Int(strVstOnTarget) ?? 0
                    unfilledLocalOfShotsOffTarget = Int(strVstOffTarget) ?? 0
                    unfilledLocalOfBlockedShots = Int(strVstBlocked) ?? 0
                    unfilledLocalOfShotsInsideBox = Int(strVstInsidebox) ?? 0
                    unfilledLocalOfShotsOutsideBox = Int(strVstOutsidebox) ?? 0
                    
                    filledVisitorOfTotalShots = Int(strVstTotalShots) ?? 0
                    filledVisitorOfShotsOnTarget = Int(strVstOnTarget) ?? 0
                    filledVisitorOfShotsOffTarget = Int(strVstOffTarget) ?? 0
                    filledVisitorOfBlockedShots = Int(strVstBlocked) ?? 0
                    filledVisitorOfShotsInsideBox = Int(strVstInsidebox) ?? 0
                    filledVisitorOfShotsOutsideBox = Int(strVstOutsidebox) ?? 0
                }
            }
        }
    }
    
}


class ModelMedia: NSObject {
    
    var strlocationUrl = ""
    var strDate = ""
    var thumbImage:UIImage?
    var localTeamName = ""
    var visitorName = ""
    

    init(dict: [String : Any]) {
        super.init()
        
        if let location = dict["location"] as? String{
            strlocationUrl = location
//            let url = URL(string: location)
//            thumbImage = self.videoPreviewUIImage(moviePath: url!)
        }
        
        if let created_at = dict["created_at"] as? [String: Any]{
            if let date = created_at["date"] as? String{
                strDate = dayDate.CompareDate(strFirstDate: date, currentDate: Date())
            }
        }
    }
    
    func videoPreviewUIImage(moviePath: URL) -> UIImage? {
        let asset = AVURLAsset(url: moviePath)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        let timestamp = CMTime(seconds: 2, preferredTimescale: 60)
        if let imageRef = try? generator.copyCGImage(at: timestamp, actualTime: nil) {
            return UIImage(cgImage: imageRef)
        } else {
            return nil
        }
    }
    
    func generateImage(moviePath: URL) -> UIImage {
        var image = UIImage()
        let asset = AVAsset.init(url: moviePath)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true;
        
        let maxSize = CGSize(width: 400, height: 300)
        assetImageGenerator.maximumSize = maxSize
        var time = asset.duration
        time.value = min(time.value, 2)
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            image =  UIImage.init(cgImage: imageRef)
            return image
        } catch {
        }
        return image
    }
    
}



