//
//  ModelStanding.swift
//  WolfScore
//
//  Created by Mindiii on 2/22/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ModelStanding: NSObject {
    
  
    var arrGroup = [ModelMatchGroup]()

    init(fromDictionary dicitonary: [String:Any])
    {
        
        if let data = dicitonary["data"] as? [[String:Any]] {
            
            for i in 0..<data.count
            {
                let value =   ModelMatchGroup.init(localdictionary: data, index: i)
                 arrGroup.append(value)
            }
        }
    }
}



class ModelMatchGroup: NSObject {
    
    var leaugeId = ""
    var seasionId = ""
    var name = ""
    var stageName = ""
    var stageId = ""
    var type = ""
    
    var arrStandings = [ModelStandingMatch]()
    
    init(localdictionary  array:[[String:Any]] , index:Int){
        
        if let leauge = array[index]["league_id"] as? String  {
            leaugeId = leauge
        }else if let leauge = array[index]["league_id"] as? Int  {
            leaugeId = String(leauge)
        }
        
        if let seasionid = array[index]["season_id"] as? String  {
            seasionId = seasionid
        }else if let seasionid = array[index]["season_id"] as? Int  {
            seasionId = String(seasionid)
        }
        
        if let stageid = array[index]["stage_id"] as? String  {
            stageId = stageid
        }else if let stageid = array[index]["stage_id"] as? Int  {
            stageId = String(stageid)
        }
        
        type = array[index]["type"] as? String ?? ""
        stageName = array[index]["stage_name"] as? String ?? ""
        name = array[index]["name"] as? String ?? ""
        
        
        if let standing = array[index]["standings"] as? [String:Any] {
            
            if let standingData = standing["data"] as? [[String:Any]] {
                
                for i in 0..<standingData.count
                {
                    let value =   ModelStandingMatch.init(Standing: standingData, StandingIndex: i)
                       arrStandings.append(value)
                }
            }
        }
    }
}
        

class ModelStandingMatch: NSObject {
    
    var position = ""
    var teamId = ""
    var teamName = ""
    var roundId = ""
    var gamesPlayed = ""
    var won = ""
    var draw = ""
    var lost = ""
    var goalsScored = ""
    var goalsAgainst = ""
    var goalDifference = ""
    var points = ""
    var logoPath = ""
    
    init(Standing  array:[[String:Any]] , StandingIndex:Int){
        
        if let positi = array[StandingIndex]["position"] as? String  {
            position = positi
        }else if let positi = array[StandingIndex]["position"] as? Int  {
            position = String(positi)
        }
        
        if let teamid = array[StandingIndex]["team_id"] as? String  {
            teamId = teamid
        }else if let teamid = array[StandingIndex]["team_id"] as? Int  {
            teamId = String(teamid)
        }
        
        teamName = array[StandingIndex]["team_name"] as? String ?? ""
        
        if let roundid = array[StandingIndex]["round_id"] as? String  {
            roundId = roundid
        }else if let roundid = array[StandingIndex]["round_id"] as? Int  {
            roundId = String(roundid)
        }
        
        if let overall = array[StandingIndex]["overall"] as? [String:Any]
        {
            
            if let gamesplayed = overall["games_played"] as? String  {
                gamesPlayed = gamesplayed
            }else if let gamesplayed = overall["games_played"] as? Int  {
                gamesPlayed = String(gamesplayed)
            }
            
            if let W = overall["won"] as? String  {
                won = W
            }else if let W = overall["won"] as? Int  {
                won = String(W)
            }
            
            if let d = overall["draw"] as? String  {
                draw = d
            }else if let d = overall["draw"] as? Int  {
                draw = String(d)
            }
            if let l = overall["lost"] as? String  {
                lost = l
            }else if let l = overall["lost"] as? Int  {
                lost = String(l)
            }
            
            if let goalscore = overall["goals_scored"] as? String  {
                goalsScored = goalscore
            }else if let goalscore = overall["goals_scored"] as? Int  {
                goalsScored = String(goalscore)
            }
            
            if let goalagainst = overall["goals_against"] as? String  {
                goalsAgainst = goalagainst
            }else if let goalagainst = overall["goals_against"] as? Int  {
                goalsAgainst = String(goalagainst)
            }
        }
    
        
        if let toatal = array[StandingIndex]["total"] as? [String:Any]
        {
            if let goaldiffernce = toatal["goal_difference"] as? String  {
                goalDifference = goaldiffernce
            }
            else if let goaldiffernce = toatal["goal_difference"] as? Int  {
                goalDifference = String(goaldiffernce)
            }
            
            if let point = toatal["points"] as? String  {
                points = point
            }else if let point = toatal["points"] as? Int  {
                points = String(point)
            }
        }
        
        if let team = array[StandingIndex]["team"] as? [String:Any]
        {
            if let data = team["data"] as? [String:Any]
            {
                if let logo_path = data["logo_path"] as? String  {
                    logoPath = logo_path
                }
            }
        }
    }
}


