//
//  TopPlayerTableViewCell.swift
//  WolfScore
//
//  Created by Mindiii on 09/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit


class TopPlayerTableViewCell: UITableViewCell,UIWebViewDelegate {
    
    @IBOutlet var lblPlayerName:UILabel!
    @IBOutlet var lblTeamName:UILabel!
    @IBOutlet var imgSelection:UIImageView!
    @IBOutlet var imgPlayer:UIImageView!
    
    @IBOutlet weak var imgCountryFlag: UIImageView!
    
    var objTeams: ModelTopPlayer?
    
    var actionSelectHandler: ((ModelTopPlayer?)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadCellData(obj:ModelTopPlayer) {
        //self.lblTeamName.text = obj.common_name
        //self.imgFlag.image =
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func actionAdd(_ sender: UIButton){
        self.actionSelectHandler?(self.objTeams)
    }
}
