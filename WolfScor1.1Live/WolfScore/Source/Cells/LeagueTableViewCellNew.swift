//
//  LeagueTableViewCell.swift
//  WolfScore
//
//  Created by mac on 08/03/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster
import AlamofireImage
import  SkeletonView
class LeagueTableViewCellNew: UITableViewCell {
    
    @IBOutlet var lblLeagueName:UILabel!
    @IBOutlet var imgSelection:UIImageView!
    @IBOutlet var imgLeague:UIImageView!
    @IBOutlet var imgTeam:UIImageView!

    @IBOutlet var btnAddFavorite:UIButton!
    
    @IBOutlet weak var viewSVGImage: UIView!
    
    var arrCountryLeagegs = [ModelLeagueList]()
    var arrPopulerLeagegs = [ModelLeagueList]()
    
    @IBOutlet weak var tblCountryLeagues:UITableView!
    @IBOutlet weak var tblCountryLeaguesHight: NSLayoutConstraint!
    
    @IBOutlet weak var tblPopulerLeagues:UITableView!
    @IBOutlet weak var tblPopulerLeaguesHight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func show_skelton()
    {
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblLeagueName].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
        [imgSelection,imgLeague].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
    }
    func hide_skelton()
    {
        [lblLeagueName].forEach { $0?.hideSkeleton()
        }
        [imgSelection,imgLeague].forEach { $0?.hideSkeleton()
        }
    }

    

}


// MARK: - TableView Delegates & Datasource
extension LeagueTableViewCellNew: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblPopulerLeagues
        {
            self.tblPopulerLeaguesHight.constant = CGFloat((self.arrPopulerLeagegs.count)*50)
            return arrPopulerLeagegs.count
        }
        else
        {
            self.tblCountryLeaguesHight.constant = CGFloat((self.arrCountryLeagegs.count)*50)
            return arrCountryLeagegs.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblPopulerLeagues
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCellNew", for: indexPath) as? LeagueTableViewCellNew{
                
                let objModel = self.arrPopulerLeagegs[indexPath.row]
                cell.lblLeagueName.text = objModel.leagueName
                if objModel.isFavorite == "1"{
                    cell.imgSelection.image = UIImage(named: "pin_favourite")
                }else{
                    cell.imgSelection.image = UIImage(named: "pin_unfavourite")
                }
                
                
                if let url = URL(string: objModel.leagueFlage){
                    cell.imgTeam.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgTeam.image =  UIImage(named: "icon_placeholderTeam")
                }
                
                if TopLeaguesVc.arrSelectedLeague.contains(objModel.leagueId) {
                    cell.imgSelection.image = UIImage(named: "pin_favourite")
                    objModel.isFavorite = "1"
                }
                else
                {
                    cell.imgSelection.image = UIImage(named: "pin_unfavourite")
                    objModel.isFavorite = "0"
                }
                
                
                cell.btnAddFavorite.tag = indexPath.row
                cell.btnAddFavorite.addTarget(self, action: #selector(btnPopulerLeaguePressed(sender:)), for: .touchUpInside)
                
                return cell
            }else{
                return UITableViewCell()
            }
        }
            
        else
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCellNew", for: indexPath) as? LeagueTableViewCellNew{
                
                let objModel = self.arrCountryLeagegs[indexPath.row]
                cell.lblLeagueName.text = objModel.leagueName
                
                if objModel.isFavorite == "1"{
                    cell.imgSelection.image = UIImage(named: "pin_favourite")
                }else{
                    cell.imgSelection.image = UIImage(named: "pin_unfavourite")
                }
                
                if let url = URL(string: objModel.leagueFlage){
                    cell.imgTeam.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgTeam.image =  UIImage(named: "icon_placeholderTeam")
                }
                
                if TopLeaguesVc.arrSelectedLeague.contains(objModel.leagueId) {
                    cell.imgSelection.image = UIImage(named: "pin_favourite")
                    objModel.isFavorite = "1"
                }
                else
                {
                    cell.imgSelection.image = UIImage(named: "pin_unfavourite")
                    objModel.isFavorite = "0"
                }
                cell.btnAddFavorite.tag = indexPath.row
                cell.btnAddFavorite.addTarget(self, action: #selector(btnCountryLeaguePressed(sender:)), for: .touchUpInside)
                
                return cell
            }else{
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if tableView == tblPopulerLeagues
//        {
//            let objModel = self.arrPopulerLeagegs[indexPath.row]
//
//            NotificationCenter.default.post(name: NSNotification.Name(KNotifiationSelectLeague), object: objModel)
//        }
//        else
//        {
//        let objModel = self.arrCountryLeagegs[indexPath.row]
//
//        NotificationCenter.default.post(name: NSNotification.Name(KNotifiationSelectLeague), object: objModel)
//        }
    }
    
    
    @objc func btnPopulerLeaguePressed(sender: UIButton){
        let objTag = self.arrPopulerLeagegs[sender.tag]
        
        if TopLeaguesVc.arrSelectedLeague.contains(objTag.leagueId)
        {
            let Indexobj = TopLeaguesVc.arrSelectedLeague.index(of: objTag.leagueId)
            TopLeaguesVc.arrSelectedLeague.remove(at: Indexobj!)
        }
        else
        {
            if TopLeaguesVc.arrSelectedLeague.count < 25
            {
                TopLeaguesVc.arrSelectedLeague.append( objTag.leagueId)
            }
        }
        
        if objTag.isFavorite == "0"{
            objTag.isFavorite = "1"
            self.call_Webservice_Post_PopulerFavorite(request_type: "1", request_id: objTag.leagueId, index: sender.tag)
        }else{
            objTag.isFavorite = "0"
            self.call_Webservice_Post_PopulerFavorite(request_type: "0", request_id: objTag.leagueId, index: sender.tag)
        }
        let indexPosition = IndexPath(row: sender.tag, section: 0)
     //   self.tblPopulerLeagues.reloadRows(at: [indexPosition], with: .none)
        self.tblPopulerLeagues.reloadData()
        
    }
    
    func call_Webservice_Post_PopulerFavorite(request_type:String,request_id:String, index:Int){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"league"] as [String:Any]
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                if self.arrPopulerLeagegs.count == 0
                {
                    return
                }
                
                let objTag = self.arrPopulerLeagegs[index]
                NotificationCenter.default.post(name: NSNotification.Name(KNotifiationFavouriteLeague), object: objTag)
            }else{
                let message = response["message"] as? String ?? ""
                if message == "Cannot select more than 25 leagues"{
                    let objTag = self.arrPopulerLeagegs[index]
                    objTag.isFavorite = "0"
                    self.tblPopulerLeagues.reloadData()
                    let window = UIApplication.shared.delegate?.window
                    let visibleVC = window??.visibleViewController
                    if  visibleVC?.title == "Alert"{
                        return
                    }
                    objWebServiceManager.showAlertWithTitle(title: "Alert", message: message)
                }
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
    
    @objc func btnCountryLeaguePressed(sender: UIButton){
        let objTag = self.arrCountryLeagegs[sender.tag]
        if TopLeaguesVc.arrSelectedLeague.contains(objTag.leagueId)
        {
            let Indexobj = TopLeaguesVc.arrSelectedLeague.index(of: objTag.leagueId)
            TopLeaguesVc.arrSelectedLeague.remove(at: Indexobj!)
        }
        else
        {
            if TopLeaguesVc.arrSelectedLeague.count < 25
            {
                TopLeaguesVc.arrSelectedLeague.append( objTag.leagueId)
            }
        }
        if objTag.isFavorite == "0"{
            objTag.isFavorite = "1"
            self.call_Webservice_Post_Favorite(request_type: "1", request_id: objTag.leagueId, index: sender.tag)
        }else{
            objTag.isFavorite = "0"
            self.call_Webservice_Post_Favorite(request_type: "0", request_id: objTag.leagueId, index: sender.tag)
        }
        let indexPosition = IndexPath(row: sender.tag, section: 0)
        // self.tblCountryLeagues.reloadRows(at: [indexPosition], with: .none)
       self.tblCountryLeagues.reloadData()
        
    }
    
    func call_Webservice_Post_Favorite(request_type:String,request_id:String, index:Int){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"league"] as [String:Any]
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                if self.arrCountryLeagegs.count == 0
                {
                    return
                }
                
                let objTag = self.arrCountryLeagegs[index]
                NotificationCenter.default.post(name: NSNotification.Name(KNotifiationFavouriteLeague), object: objTag)
            }else{
                let message = response["message"] as? String ?? ""
                if message == "Cannot select more than 25 leagues"{
                    let objTag = self.arrCountryLeagegs[index]
                    objTag.isFavorite = "0"
                    self.tblCountryLeagues.reloadData()
                    let window = UIApplication.shared.delegate?.window
                    let visibleVC = window??.visibleViewController
                    if  visibleVC?.title == "Alert"{
                        return
                    }
                    objWebServiceManager.showAlertWithTitle(title: "Alert", message: message)
                }
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
    }
    
}
