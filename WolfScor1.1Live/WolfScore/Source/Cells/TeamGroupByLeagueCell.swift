//
//  TeamGroupByLeagueCell.swift
//  WolfScore
//
//  Created by mac on 20/06/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster
import AlamofireImage

class TeamGroupByLeagueCell: UITableViewCell {

    @IBOutlet var lblName:UILabel!
    @IBOutlet var imgSelection:UIImageView!
    @IBOutlet var imgLeague:UIImageView!
    @IBOutlet var imgFlag:UIImageView!

    @IBOutlet var btnAddSelect:UIButton!
    @IBOutlet var btnDropDown:UIButton!
    var arrTeam = [ModelTeam]()
    var arrSelectedTeam = [String]()
    
    @IBOutlet weak var tblTeam :UITableView!
    @IBOutlet weak var tblTeamHight: NSLayoutConstraint!
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.tblCountryLeagues.rowHeight = UITableViewAutomaticDimension
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @objc func btnSelectPressed(sender: UIButton){
        let objTag = self.arrTeam[sender.tag]
        
        if LocalTeamList.arrSelectedTeam.contains(objTag.id)
        {
            let Indexobj = LocalTeamList.arrSelectedTeam.index(of: objTag.id)
            LocalTeamList.arrSelectedTeam.remove(at: Indexobj!)
        }
        else
        {
            if LocalTeamList.arrSelectedTeam.count < 100
            {
                LocalTeamList.arrSelectedTeam.append( objTag.id)
            }
        }
        
        
        
        if objTag.is_favorite == "0"{
            objTag.is_favorite = "1"
            self.call_Webservice_Post_Favorite(request_type: "1", request_id: objTag.id, index: sender.tag)
        }else{
            objTag.is_favorite = "0"
            self.call_Webservice_Post_Favorite(request_type: "0", request_id: objTag.id, index: sender.tag)
        }
        self.tblTeam.reloadData()
    }
   
    
    func call_Webservice_Post_Favorite(request_type:String,request_id:String, index:Int){
        
        let paramDict = ["request_type":request_type,
                         "request_id":request_id,
                         "type":"team"] as [String:Any]
        print(paramDict)
        
        objWebServiceManager.requestPost(strURL: webUrl.single_favorite_unfavorite, params: paramDict, success: { (response) in
            print(response)
            let message = response["message"] as? String ?? ""
            let status = response["status"] as? String ?? ""
            if status == "success"{
          
            }else{
                if message == "Cannot select more than 100 teams"{
                    let objTag = self.arrTeam[index]
                    objTag.is_favorite = "0"
                    self.tblTeam.reloadData()
                    let window = UIApplication.shared.delegate?.window
                    let visibleVC = window??.visibleViewController
                    if  visibleVC?.title == "Alert"{
                        return
                    }
                    objWebServiceManager.showAlertWithTitle(title: "Alert", message: message)
                }
            }
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
    }
    
    
}

// MARK: - TableView Delegates & Datasource
extension TeamGroupByLeagueCell: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tblTeamHight.constant = CGFloat((self.arrTeam.count)*50)
        return arrTeam.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TeamGroupByLeagueCell", for: indexPath) as? TeamGroupByLeagueCell{
            
            let objModel = self.arrTeam[indexPath.row]
                cell.lblName.text = objModel.name
            
            let strUrl = objModel.logo_path

            if let url = URL(string: strUrl){
                cell.imgFlag.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }else{
                cell.imgFlag.image = UIImage(named: "icon_placeholderTeam")
            }
            
                
                if LocalTeamList.arrSelectedTeam.contains(objModel.id) {
                    cell.imgSelection.image = UIImage(named: "pin_favourite")
                    objModel.is_favorite = "1"
                }
                else
                {
                    cell.imgSelection.image = UIImage(named: "pin_unfavourite")
                    objModel.is_favorite = "0"
                }
            
                cell.btnAddSelect.tag = indexPath.row
                cell.btnAddSelect.addTarget(self, action: #selector(btnSelectPressed(sender:)), for: .touchUpInside)
                
                return cell
            }else{
                return UITableViewCell()
            }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
}

