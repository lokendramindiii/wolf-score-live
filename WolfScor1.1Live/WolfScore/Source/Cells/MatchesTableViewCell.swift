//
//  MatchesTableViewCell.swift
//  WolfScore
//
//  Created by Mindiii on 02/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView

class MatchesTableViewCell: UITableViewCell {
    
    @IBOutlet var lblLocalTeamName:UILabel!
    @IBOutlet var lblVisitorTeamName:UILabel!
    @IBOutlet var imgLocal:UIImageView!
    @IBOutlet var imgVisitor:UIImageView!
    
    @IBOutlet var lblScore:UILabel!
    @IBOutlet var lblStatus:UILabel!
    @IBOutlet var lblStatusTime:UILabel!
    
    @IBOutlet var lblStartTime:UILabel!
    
    @IBOutlet var viewStatus:UIView!
    @IBOutlet var viewMain:UIView!
    
    @IBOutlet var viewAllStatus:UIView!
    @IBOutlet var lblAllStatus:UILabel!
    
    @IBOutlet var stackView:UIStackView!
    @IBOutlet var stackViewTimeStatus:UIStackView!
    
    @IBOutlet var lblDate:UILabel!
    @IBOutlet var lblLeagueName:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblLocalTeamName , lblVisitorTeamName ,lblScore , lblStatus , lblStatusTime,lblStartTime,lblAllStatus,lblDate,lblLeagueName].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        [imgLocal,imgVisitor].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
    }
    
    func hide_skelton()
    {
        [lblLocalTeamName , lblVisitorTeamName ,lblScore , lblStatus , lblStatusTime,lblStartTime,lblAllStatus,lblDate,lblLeagueName].forEach { $0?.hideSkeleton()
        }
        [imgVisitor ,imgLocal].forEach { $0?.hideSkeleton()
        }
    }
}
