
//
//  MyTeamCollectionViewCell.swift
//  WolfScore
//
//  Created by Mindiii on 23/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SkeletonView

class MyTeamCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewAdd: UIView!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblLine: UILabel!
}

class OverViewCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    func show_skelton(){
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblName,lblCountry,lblHeight,lblWeight, lblDescription].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        [imgPlayer].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
    }
    
    func hide_skelton(){
        [lblName,lblCountry,lblHeight,lblWeight, lblDescription].forEach { $0?.hideSkeleton()
        }
        [imgPlayer].forEach { $0?.hideSkeleton()
        }
    }
}
