//
//  LeagueFilterCell.swift
//  WolfScore
//
//  Created by mac on 30/03/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toaster
import AlamofireImage
import SkeletonView
class LeagueFilterCell: UITableViewCell {

    @IBOutlet var lblLeagueName:UILabel!
    @IBOutlet var imgSelection:UIImageView!
    @IBOutlet var imgLeague:UIImageView!
    @IBOutlet var imgTeam:UIImageView!

    @IBOutlet var btnAddFavorite:UIButton!
    
    @IBOutlet weak var viewSVGImage: UIView!
    
    var arrCountryLeagegs = [ModelLeagueList]()
    var arrPopulerLeagegs = [ModelLeagueList]()

    @IBOutlet weak var tblCountryLeagues:UITableView!
    @IBOutlet weak var tblCountryLeaguesHight: NSLayoutConstraint!
    
    @IBOutlet weak var tblPopulerLeagues:UITableView!
    @IBOutlet weak var tblPopulerLeaguesHight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func show_skelton()
    {
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblLeagueName].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
        [imgSelection,imgLeague].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
    }
    func hide_skelton()
    {
        [lblLeagueName].forEach { $0?.hideSkeleton()
        }
        [imgSelection,imgLeague].forEach { $0?.hideSkeleton()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

// MARK: - TableView Delegates & Datasource
extension LeagueFilterCell: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblPopulerLeagues
        {
            self.tblPopulerLeaguesHight.constant = CGFloat((self.arrPopulerLeagegs.count)*50)
            return arrPopulerLeagegs.count
        }
        else
        {
            self.tblCountryLeaguesHight.constant = CGFloat((self.arrCountryLeagegs.count)*50)
            return arrCountryLeagegs.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblPopulerLeagues
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueFilterCell", for: indexPath) as? LeagueFilterCell{
                
                let objModel = self.arrPopulerLeagegs[indexPath.row]
                cell.lblLeagueName.text = objModel.leagueName
                
                
                if let url = URL(string: objModel.leagueFlage){
                    cell.imgTeam.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
                }
                else
                {
                    cell.imgTeam.image = UIImage(named: "icon_placeholderTeam")
                }
                
                if objModel.isSelected == "1"{
                    cell.imgSelection.image = UIImage(named: "selct_icon")
                }else{
                    cell.imgSelection.image = UIImage(named: "white_circle")
                }
                
                if FilterLeague.arrSelectedLeague.contains(objModel.leagueId) {
                    cell.imgSelection.image = UIImage(named: "selct_icon")
                    objModel.isSelected = "1"
                }
                else
                {
                    cell.imgSelection.image = UIImage(named: "white_circle")
                    objModel.isSelected = "0"
                }
                
                cell.btnAddFavorite.tag = indexPath.row
                cell.btnAddFavorite.addTarget(self, action: #selector(btnPopulerLeaguePressed(sender:)), for: .touchUpInside)
                
                return cell
            }
            else{
                return UITableViewCell()
            }
        }
        else{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueFilterCell", for: indexPath) as? LeagueFilterCell{
            
            let objModel = self.arrCountryLeagegs[indexPath.row]
            cell.lblLeagueName.text = objModel.leagueName
            
            
            if let url = URL(string: objModel.leagueFlage){
                cell.imgTeam.af_setImage(withURL: url, placeholderImage: UIImage(named: "icon_placeholderTeam"))
            }
            else
            {
                cell.imgTeam.image = UIImage(named: "icon_placeholderTeam")
            }
            
            if objModel.isSelected == "1"{
                cell.imgSelection.image = UIImage(named: "selct_icon")
            }else{
                cell.imgSelection.image = UIImage(named: "white_circle")
            }
            
            if FilterLeague.arrSelectedLeague.contains(objModel.leagueId) {
                cell.imgSelection.image = UIImage(named: "selct_icon")
                objModel.isSelected = "1"
            }
            else
            {
                cell.imgSelection.image = UIImage(named: "white_circle")
                objModel.isSelected = "0"
            }
            cell.btnAddFavorite.tag = indexPath.row
            cell.btnAddFavorite.addTarget(self, action: #selector(btnCountryLeaguePressed(sender:)), for: .touchUpInside)
            
            return cell
        }
        else{
            return UITableViewCell()
        }
    }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    
    @objc func btnPopulerLeaguePressed(sender: UIButton){
        
        
        if self.arrPopulerLeagegs.count == 0
        {
            return
        }
        let objTag = self.arrPopulerLeagegs[sender.tag]
        
        if FilterLeague.arrSelectedLeague.contains(objTag.leagueId)
        {
            let Indexobj = FilterLeague.arrSelectedLeague.index(of: objTag.leagueId)
            FilterLeague.arrSelectedLeague.remove(at: Indexobj!)
        }
        else
        {
            if FilterLeague.arrSelectedLeague.count < 25
            {
                FilterLeague.arrSelectedLeague.append( objTag.leagueId)
            }
        }
        if objTag.isSelected == "0"{
            objTag.isSelected = "1"
            self.call_Webservice_Post_addRemovePopulerFilterLeague(request_type: "add", league_id: objTag.leagueId, index: sender.tag)
        }else{
            objTag.isSelected = "0"
            self.call_Webservice_Post_addRemovePopulerFilterLeague(request_type: "remove", league_id: objTag.leagueId, index: sender.tag)
        }
        let indexPosition = IndexPath(row: sender.tag, section: 0)
     //   self.tblPopulerLeagues.reloadRows(at: [indexPosition], with: .none)
       self.tblPopulerLeagues.reloadData()
        
    }
    
    func call_Webservice_Post_addRemovePopulerFilterLeague(request_type:String,league_id:String, index:Int){
        
        let paramDict = ["type":request_type,
                         "league_id":league_id] as [String:Any]
        
        objWebServiceManager.requestPost(strURL: webUrl.add_removeFilterleague, params: paramDict, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                if self.arrPopulerLeagegs.count == 0
                {
                    return
                }
                
                let objTag = self.arrPopulerLeagegs[index]
                
                NotificationCenter.default.post(name: NSNotification.Name(KNotifiationFavouriteLeague), object: objTag)
            }else{
                let message = response["message"] as? String ?? ""
                if message == "Can't select more than 25 leagues"{
                    let objTag = self.arrPopulerLeagegs[index]
                    objTag.isSelected = "0"
                    self.tblPopulerLeagues.reloadData()
                    let window = UIApplication.shared.delegate?.window
                    let visibleVC = window??.visibleViewController
                    if  visibleVC?.title == "Alert"{
                        return
                    }
                    objWebServiceManager.showAlertWithTitle(title: "Alert", message: message)
                }
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
            
        }
    }
    
    @objc func btnCountryLeaguePressed(sender: UIButton){
        
        if self.arrCountryLeagegs.count == 0
        {
            return
        }
        
        let objTag = self.arrCountryLeagegs[sender.tag]
        if FilterLeague.arrSelectedLeague.contains(objTag.leagueId)
        {
            let Indexobj = FilterLeague.arrSelectedLeague.index(of: objTag.leagueId)
            FilterLeague.arrSelectedLeague.remove(at: Indexobj!)
        }
        else
        {
            if FilterLeague.arrSelectedLeague.count < 25
            {
                FilterLeague.arrSelectedLeague.append( objTag.leagueId)
            }
            
        }
        if objTag.isSelected == "0"{
            objTag.isSelected = "1"
            self.call_Webservice_Post_addRemoveFilterLeague(request_type: "add", league_id: objTag.leagueId, index: sender.tag)
        }else{
            objTag.isSelected = "0"
            self.call_Webservice_Post_addRemoveFilterLeague(request_type: "remove", league_id: objTag.leagueId, index: sender.tag)
        }
        let indexPosition = IndexPath(row: sender.tag, section: 0)
       //  self.tblCountryLeagues.reloadRows(at: [indexPosition], with: .none)
        self.tblCountryLeagues.reloadData()
        
    }
    
    func call_Webservice_Post_addRemoveFilterLeague(request_type:String,league_id:String, index:Int){
        
        let paramDict = ["type":request_type,
                         "league_id":league_id] as [String:Any]
        
        objWebServiceManager.requestPost(strURL: webUrl.add_removeFilterleague, params: paramDict, success: { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                if self.arrCountryLeagegs.count == 0
                {
                    return
                }
                
                let objTag = self.arrCountryLeagegs[index]
                
                NotificationCenter.default.post(name: NSNotification.Name(KNotifiationFavouriteLeague), object: objTag)
            }else{
                let message = response["message"] as? String ?? ""
                if message == "Can't select more than 25 leagues"{
                    let objTag = self.arrCountryLeagegs[index]
                    objTag.isSelected = "0"
                    self.tblCountryLeagues.reloadData()
                    let window = UIApplication.shared.delegate?.window
                    let visibleVC = window??.visibleViewController
                    if  visibleVC?.title == "Alert"{
                        return
                    }
                    objWebServiceManager.showAlertWithTitle(title: "Alert", message: message)
                }
            }
            
        }) { (error) in
            print(error)
            SVProgressHUD.dismiss()
        }
    }
}

