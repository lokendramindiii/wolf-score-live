//
//  TeamTableViewCell.swift
//  WolfScore
//
//  Created by Mindiii on 20/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import  SkeletonView
class TeamTableViewCell: UITableViewCell {
    
    @IBOutlet var lblTeamName:UILabel!
    @IBOutlet var lblCountryName:UILabel!
    @IBOutlet var btnSlection:UIButton!
    @IBOutlet var btnDeleteSort:UIButton!

    @IBOutlet var imgFlag:UIImageView!
    @IBOutlet var imgSelection:UIImageView!
    
    var objTeams: ModelTeam?
    var objleague: ModelLeagueList?
    var actionSelectHandler: ((ModelTeam?)->())?
    var actionSelectLeagueHandler: ((ModelLeagueList?)->())?
    
    var actionDeleteLeagueHandler: ((_ btntag:Int?)->())?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func show_skelton()
    {
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        let gradient = SkeletonGradient(baseColor: UIColor(red: 64/255, green: 65/255, blue: 69/255, alpha: 1.0))
        
        [lblTeamName , lblCountryName ].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        
        [btnSlection , btnDeleteSort ].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation:animation)
        }
        [imgFlag,imgSelection].forEach { $0?.showAnimatedGradientSkeleton(usingGradient:gradient, animation: animation)
        }
        
    }
    
    func hide_skelton()
    {
        [lblTeamName , lblCountryName ].forEach { $0?.hideSkeleton()
        }
        [btnSlection , btnDeleteSort].forEach { $0?.hideSkeleton()
        }
        [imgFlag,imgSelection].forEach { $0?.hideSkeleton()
        }
    }
    func loadCellData(obj:ModelTeam) {
        self.lblTeamName.text = obj.str_Team_name
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    @IBAction func actionAdd(_ sender: UIButton){
        self.actionSelectHandler?(self.objTeams)
    }
    @IBAction func actionLeaugeSequence(_ sender: UIButton){
        self.actionSelectLeagueHandler?(self.objleague)
    }
    @IBAction func actionDelete(_ sender: UIButton){
        self.actionDeleteLeagueHandler?(sender.tag)
    }

}
