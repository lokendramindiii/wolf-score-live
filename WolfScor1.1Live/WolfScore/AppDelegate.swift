//
//  AppDelegate.swift
//  WolfScore
//
//  Created by Mindiii on 20/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//


import UIKit
import UserNotifications
import SlideMenuControllerSwift
import SVProgressHUD

import GoogleMobileAds

import Firebase
import FirebaseCore
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GADInterstitialDelegate,UIGestureRecognizerDelegate{

    var window: UIWindow?
    var navController:UINavigationController?
    var interstitial: GADInterstitial!
    
    
    var boolForFun = Bool()

    var isFromNotification = false
    var notificationType = ""
    var notificationId = ""

    weak var timer: Timer?
    var isTouchwindow =  false
    var isadPresent = false
    
    var isInterstialPresent = false
    var isBannerDisappearShow = false


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        var deviceID = ""
        if let strId = UIDevice.current.identifierForVendor?.uuidString{
            deviceID = strId
            objAppShareData.uniqDeviceTocken = deviceID
            UserDefaults.standard.setValue(deviceID, forKey: UserDefaults.Keys.kUnique_Device_Token)
            kUnique_Device_Token = deviceID
        }
        
        if kUnique_Device_Token != ""
        {
        let strDeviceId = kUnique_Device_Token.lowercased()
        kstrDeviceId = strDeviceId.replacingOccurrences(of: "-", with: "")
        print(kstrDeviceId)
        }
        FirebaseApp.configure()
        self.registerForRemoteNotification()
        
        print("unique token: \(String(describing: deviceID))")
        self.get_Current_Country()
        Thread.sleep(forTimeInterval: 2.0)
        
      //  client
        GADMobileAds.configure(withApplicationID: "ca-app-pub-3253051663291962~2565277066")
        
       // GADMobileAds.configure(withApplicationID: "ca-app-pub-3940256099942544~1458002511")
        
        
        // for user login
        let userId = UserDefaults.standard.string(forKey: UserDefaults.Keys.kUserId)
        
        // for guest user login
        let GuestUser = UserDefaults.standard.string(forKey: UserDefaults.Keys.kGuestLogin)
        if userId != nil || GuestUser != nil {
            self.goToTabBar()
          //  self.startTimerBanner()
        }
//        let tapGesture = UITapGestureRecognizer(target: self, action: nil)
//        tapGesture.delegate = self
//        self.window?.addGestureRecognizer(tapGesture)
        
        return true
    }
    
    func  startTimerBanner()
    {
//        timer?.invalidate()
//        timer = Timer.scheduledTimer(timeInterval:120.0, target: self, selector: #selector(self.bannerInterstitialShow), userInfo: nil, repeats: true)
    }
    
// MARK: Interstitial banner call

//    @objc func bannerInterstitialShow()
//    {
//        objAppShareData.isTwoMinutesComplete = true
//        self.isTouchwindow = false
//    }
//
    
//    func createAndLoadInterstitial() -> GADInterstitial {
//        interstitial = GADInterstitial(adUnitID:objAppShareData.InterstitialId)
//        interstitial.delegate = self
//        interstitial.load(GADRequest())
//        return interstitial
//    }
//
    // MARK: - Banner delegate method
    
    // Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        
        print("interstitialDidReceiveAd")
        
        if self.isadPresent == false
        {
            
            if let visibleVC = self.window?.visibleViewController{
                visibleVC.modalPresentationStyle = .overCurrentContext
                if interstitial.isReady {
                 interstitial.present(fromRootViewController: visibleVC)
                   
                }
                else {
                    print("Ad wasn't ready")
                }
            }
        }
    }
    
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial){
        print("interstitialWillPresentScreen")

         self.isBannerDisappearShow = true
         self.isadPresent = true
         self.isInterstialPresent = true
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
        self.isadPresent = false
        self.isBannerDisappearShow = false

        self.startTimerBanner()
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        self.isadPresent = false

        print("interstitialDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
    
    ///------------ GADInterstitial  close ----------------
    
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        if isTouchwindow == false  &&  objAppShareData.isTwoMinutesComplete == true
//        {
//            isTouchwindow = true
//            print("first touch Began")
//            interstitial = createAndLoadInterstitial()
//        }
//        return false
//    }

    
  // Welcome screen call  
    func Go_ToWelcomeScreen()
    {

        selected_my_Index = 0
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "WelcomeVC")
        let naviationcontroller =  UINavigationController(rootViewController: vc)
        naviationcontroller.isNavigationBarHidden = true
        self.window?.rootViewController = naviationcontroller
        self.window?.makeKeyAndVisible()
    }
  
    func applicationWillResignActive(_ application: UIApplication) {
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
   
    func get_Current_Country(){
        let countryLocale = NSLocale.current
        let countryCode = countryLocale.regionCode
        let country = (countryLocale as NSLocale).displayName(forKey: NSLocale.Key.countryCode, value: countryCode ?? "")
        kCurrent_Country = country ?? ""
        
     
//        let englishLocale : NSLocale = NSLocale.init(localeIdentifier :  countryCode!)
//        // get the current locale
//        let currentLocale = NSLocale.current
//        let theEnglishName : String? = englishLocale.displayName(forKey: NSLocale.Key.identifier, value: currentLocale.identifier)
//        if let theEnglishName = theEnglishName
//        {
//            //let countryName = theEnglishName.sp
//            let forReplace = "(\"English ("
//            let replaced1 = theEnglishName.replacingOccurrences(of: forReplace, with: "")
//            let replaced2 = ")\")"
//            let finale = replaced1.replacingOccurrences(of: replaced2, with: "")
//            let finale1 = finale.replacingOccurrences(of: "English (", with: "")
//            //let finale2 = replaced1.replacingOccurrences(of: "English (", with: "")
//            let finale2 = finale1.replacingOccurrences(of: ")", with: "")
//            print("the localized country name is \(finale2)")
//            kCurrent_Country = finale2
//            //print("the localized country name is \(finale1)")
//        }
    }
    func goToTabBar(){
        objAppShareData.isLonchXLPageIndex = true
        
        objAppDelegate.startTimerBanner()
        
        let storyboard = UIStoryboard(name: "UserTabbar", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "UserTabbarVC") as! UserTabbarVC
        
        let SideMenuStoryboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let leftViewController = SideMenuStoryboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        self.navController = nvc
        nvc.isNavigationBarHidden = true
        let slideMenuController = SlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        SlideMenuOptions.contentViewScale = 1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = slideMenuController
        self.window?.backgroundColor = UIColor.red
        self.window?.makeKeyAndVisible()
    }
}

extension UINavigationController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

extension AppDelegate : MessagingDelegate,UNUserNotificationCenterDelegate{
    func registerForRemoteNotification() {
        // iOS 10 support
        if #available(iOS 10, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options:authOptions){ (granted, error) in
                UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
                Messaging.messaging().delegate = self
            }
        }else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
        
        NotificationCenter.default.addObserver(self, selector:
            #selector(tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("FCM registration token: \(fcmToken)")
         kDevice_Tocken = fcmToken
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
         kDevice_Tocken = fcmToken
    }
    
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
//        print("APNs device token: \(deviceTokenString)")
//        kDevice_Tocken = deviceTokenString
//    }
//
//    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//        print("i am not available in simulator \(error)")
//    }
    
    
    //MARK: Notification
    @objc func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
             kDevice_Tocken = refreshedToken
        }
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo as? [String : Any] ?? [:]
        
        print("willPresent notification = \(userInfo)")
        completionHandler([.alert,.sound,.badge])
        
    }
    
    //called When you tap on the notification
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo as? [String : Any] ?? [:]
        print("didReceive notification = \(userInfo)")
        UIApplication.shared.applicationIconBadgeNumber = 0
        
      //  notificationVC.readUnreadStatus = "Unread"
        handleNotificationWith(userInfo: userInfo)
    }
    
    func handleNotificationWith(userInfo:[AnyHashable : Any]){

        let GuestUser = UserDefaults.standard.string(forKey: UserDefaults.Keys.kGuestLogin)
        
        let userId = UserDefaults.standard.string(forKey: UserDefaults.Keys.kUserId)

        if userId != nil || GuestUser != nil {
            
            isFromNotification = true
            let  msgBody = userInfo["body"] as? String ?? ""
            notificationType = userInfo["type"] as? String ?? ""
            
            notificationId = userInfo["reference_key"] as? String ?? ""
            
            let exName = userInfo["title"] as? String ?? ""
            print(notificationType)
            print(notificationId)
            print(exName)
            print(msgBody)
            
            if notificationType == "goal_updates" ||  notificationType == "card_updates" || notificationType == "match_finish" || notificationType == "match_start"
            {
                objAppShareData.str_match_Id = notificationId
                self.goToTabBar()
            }
        }
    }
}


